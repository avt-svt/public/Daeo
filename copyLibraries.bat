COPY "build\DaeoToolbox\Release\DaeoToolbox.dll" "src\Modelica\Resources\Library\win64\DaeoToolbox.dll"
COPY "build\SuiteSparse\lib\Release\DaeoToolbox.lib" "src\Modelica\Resources\Library\win64\DaeoToolbox.lib"
COPY "build\DaeoToolbox\Release\DaeoToolbox.dll" "..\jade\branches\NdaeSolverDcoTopl01_v2\build\Debug\DaeoToolbox.dll"
COPY "build\SuiteSparse\lib\Release\DaeoToolbox.lib" "..\jade\branches\NdaeSolverDcoTopl01_v2\build\Debug\DaeoToolbox.lib"
COPY "build\DaeoToolbox\Release\DaeoToolbox.dll" "..\jade\branches\NdaeSolverDcoTopl01_v2\build\Release\DaeoToolbox.dll"
COPY "build\SuiteSparse\lib\Release\DaeoToolbox.lib" "..\jade\branches\NdaeSolverDcoTopl01_v2\build\Release\DaeoToolbox.lib"

COPY "build\DaeoToolbox\Release\DaeoToolbox.dll" "..\jade\branches\NdaeSolverDcoTopl01_v2\src\ThirdParty\DaeoToolbox\bin\DaeoToolbox.dll"
COPY "build\SuiteSparse\lib\Release\DaeoToolbox.lib" "..\jade\branches\NdaeSolverDcoTopl01_v2\src\ThirdParty\DaeoToolbox\lib\DaeoToolbox.lib"

COPY "D:\Repositories\Daeo\src\DaeoToolbox\DaeoToolbox.hpp" "..\jade\branches\NdaeSolverDcoTopl01_v2\src\ThirdParty\DaeoToolbox\inc\DaeoToolbox.hpp"
COPY "D:\Repositories\Daeo\src\DaeoToolbox\DaeoToolboxAdditional.hpp" "..\jade\branches\NdaeSolverDcoTopl01_v2\src\ThirdParty\DaeoToolbox\inc\DaeoToolboxAdditional.hpp"
PAUSE