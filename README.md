# DAEO

Software for differential-algebraic equations with optimization criteria (DAEO).

This software is distributed under the GNU General Public Licence Version 3. It depends on third party software with separate licenses, that are placed on the associated folders.
