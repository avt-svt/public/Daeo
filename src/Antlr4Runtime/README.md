# Antlr4Runtime

Runtime library for the C++ target of antlr-4.7.2. Main authors are Terence Parr and Mike Lischke. Source code is from https://www.antlr.org/download/antlr4-cpp-runtime-4.7.2-source, but with our own CMakeLists.txt.