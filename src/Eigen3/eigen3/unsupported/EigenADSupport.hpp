#ifndef EIGENDCOSUPPORT_HPP
#define EIGENDCOSUPPORT_HPP

#include "Eigen/Core"
#include "ad/ad.hpp"

namespace ad {

template<typename T>
inline const typename ad::gt1s<T>::type& conj(const typename ad::gt1s<T>::type& x)  { return x; }

template<typename T>
inline const typename ad::gt1s<T>::type& real(const typename ad::gt1s<T>::type& x)  { return x; }

template<typename T>
inline typename ad::gt1s<T>::type imag(const typename ad::gt1s<T>::type&)    { return 0.; }

template<typename T>
inline typename ad::gt1s<T>::type abs(const typename ad::gt1s<T>::type&  x)  { return fabs(x); }

template<typename T>
inline typename ad::gt1s<T>::type abs2(const typename ad::gt1s<T>::type& x)  { return x*x; }
}

namespace Eigen {

template<typename T> struct NumTraits<typename ad::gt1s<T>::type>
    : NumTraits<T>
{
  using Real = typename ad::gt1s<T>::type;
  using NonInteger = typename ad::gt1s<T>::type;
  using Nested = typename ad::gt1s<T>::type;
  enum {
    IsComplex = 0,
    IsInteger = 0,
    IsSigned = 1,
    RequireInitialization = 1,
    ReadCost = 1,
    AddCost = 1,
    MulCost = 1
  };
};

template<typename T, typename BinaryOp>
struct ScalarBinaryOpTraits<T,typename ad::gt1s<T>::type,BinaryOp>
{
  using ReturnType = typename ad::gt1s<T>::type;
};

template<typename T, typename BinaryOp>
struct ScalarBinaryOpTraits<typename ad::gt1s<T>::type,T,BinaryOp>
{
  using ReturnType = typename ad::gt1s<T>::type;
};

}


#endif // EIGENDCOSUPPORT_HPP
