# Eigen3

Linear algebra package. Contains version 3.3.7 of Eigen3 plus our own modifications to enable adjoint and transpose solve for Eigen::SparseLU. The upstream URL is http://eigen.tuxfamily.org.