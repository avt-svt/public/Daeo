#include "gtest/gtest.h"
#include "Eigen/LU"
#include "Eigen/SparseLU"
#include <cmath>





TEST(Eigen, CanResizeFixeSizeMatrix)
{
  constexpr int N=3;
  Eigen::Matrix<double,N,N> A;
  std::cout << "sizeof(A) is " << sizeof(A) << std::endl;
  EXPECT_EQ(N,A.rows());
  A.resize(N,N);
  EXPECT_EQ(N,A.cols());
  Eigen::Matrix<double,N,1> b;
  b.resize(N);
  EXPECT_EQ(N,b.rows());
}


TEST(EigenPartialLU, CanDetectSingularMatrix)
{
  using Real = double;
  using std::isfinite;
  constexpr int N=2;
  Eigen::Matrix<Real,N,N> A;
  Eigen::Matrix<Real,N,1> b;
  A << 1, 0, 0, 0;
  b << 1,1;
  Eigen::PartialPivLU<Eigen::Matrix<Real,N,N> > lu;
  lu.compute(A);
  EXPECT_FALSE(lu.rcond() < 1e16);
  Eigen::Matrix<Real,N,1> x;
  x = lu.solve(b);

  EXPECT_FALSE(isfinite(x[0]));
  EXPECT_FALSE(isfinite(x[1]));
}

TEST(EigenSparseLU, CanDetectSingularMatrix)
{
  constexpr int N=2;
  Eigen::SparseMatrix<double> A(N,N);
  A.insert(0,0)=1;
  //A.insert(1,1)=1;
  Eigen::Matrix<double,N,1> b;
  b << 1,1;
  Eigen::SparseLU<Eigen::SparseMatrix<double>> lu;
  lu.compute(A);
  EXPECT_NE(lu.info(),Eigen::ComputationInfo::Success);
}
