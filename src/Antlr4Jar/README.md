# Antlr4Jar

CMake project for the antlr4 parser generator. Currently the complete java archive of antlr-4.7.2 is maintained. Main author of antlr4 is Terence Parr. The upstream URL is https://www.antlr.org/. 