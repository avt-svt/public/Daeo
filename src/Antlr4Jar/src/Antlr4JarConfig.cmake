# Compute locations from <prefix>/lib/cmake/Antlr4RuntimeConfig.cmake
get_filename_component(_ANTLR4_RUNTIME_CMAKE_SELF_DIR "${CMAKE_CURRENT_LIST_FILE}" PATH)
get_filename_component(_ANTLR4_RUNTIME_CMAKE_DIR "${_ANTLR4_RUNTIME_CMAKE_SELF_DIR}" PATH)
get_filename_component(_ANTLR4_RUNTIME_LIB_DIR "${_ANTLR4_RUNTIME_CMAKE_DIR}" PATH)
set(ANTLR4_JAR_LOCATION ${_ANTLR4_RUNTIME_LIB_DIR}/antlr-4.7.1-complete.jar)
message("Set ANTLR4_JAR_LOCATION to ${ANTLR4_JAR_LOCATION}")
