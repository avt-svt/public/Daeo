# Yasp

Yet another sparsity pattern (Yasp) library to compute sparsity patterns of branch free C++ functions. Based on the jsp data type of Uwe Naumann's group. 