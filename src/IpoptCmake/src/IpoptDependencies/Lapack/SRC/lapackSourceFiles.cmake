SET(LAPACK_SOURCE_FILES
    # ${LAPACK_SRCDIR}/dbdsqr.f
    # ${LAPACK_SRCDIR}/dgebal.f
    # ${LAPACK_SRCDIR}/dgebak.f
    # ${LAPACK_SRCDIR}/dgebd2.f
    # ${LAPACK_SRCDIR}/dgebrd.f
    # ${LAPACK_SRCDIR}/dgeev.f
    # ${LAPACK_SRCDIR}/dgehd2.f
    # ${LAPACK_SRCDIR}/dgehrd.f
    # ${LAPACK_SRCDIR}/dgelq2.f
    # ${LAPACK_SRCDIR}/dgelqf.f
    # ${LAPACK_SRCDIR}/dgels.f
    # ${LAPACK_SRCDIR}/dgeqr2.f
    # ${LAPACK_SRCDIR}/dgeqrf.f
    # ${LAPACK_SRCDIR}/dgesvd.f
    # ${LAPACK_SRCDIR}/dgesv.f
     ${LAPACK_SRCDIR}/dgetf2.f
     ${LAPACK_SRCDIR}/dgetrf.f
    # ${LAPACK_SRCDIR}/dgetri.f
     ${LAPACK_SRCDIR}/dgetrs.f
    # ${LAPACK_SRCDIR}/dggbak.f
    # ${LAPACK_SRCDIR}/dggbal.f
    # ${LAPACK_SRCDIR}/dgghrd.f
    # ${LAPACK_SRCDIR}/dggev.f
    # ${LAPACK_SRCDIR}/dhgeqz.f
    # ${LAPACK_SRCDIR}/dhseqr.f
     ${LAPACK_SRCDIR}/disnan.f
    # ${LAPACK_SRCDIR}/dlabad.f
    # ${LAPACK_SRCDIR}/dlabrd.f
    # ${LAPACK_SRCDIR}/dlacpy.f
    # ${LAPACK_SRCDIR}/dladiv.f
    # ${LAPACK_SRCDIR}/dlaebz.f
     ${LAPACK_SRCDIR}/dlae2.f
     ${LAPACK_SRCDIR}/dlaev2.f
    # ${LAPACK_SRCDIR}/dlaexc.f
    # ${LAPACK_SRCDIR}/dlagtf.f
    # ${LAPACK_SRCDIR}/dlagts.f
    # ${LAPACK_SRCDIR}/dlag2.f
    # ${LAPACK_SRCDIR}/dlahqr.f
    # ${LAPACK_SRCDIR}/dlahr2.f
     ${LAPACK_SRCDIR}/dlaisnan.f
    # ${LAPACK_SRCDIR}/dlaln2.f
    # ${LAPACK_SRCDIR}/dlaneg.f
    # ${LAPACK_SRCDIR}/dlange.f
    # ${LAPACK_SRCDIR}/dlanhs.f
     ${LAPACK_SRCDIR}/dlanst.f
     ${LAPACK_SRCDIR}/dlansy.f
    # ${LAPACK_SRCDIR}/dlanv2.f
     ${LAPACK_SRCDIR}/dlapy2.f
    # ${LAPACK_SRCDIR}/dlapy3.f
    # ${LAPACK_SRCDIR}/dlaqr0.f
    # ${LAPACK_SRCDIR}/dlaqr1.f
    # ${LAPACK_SRCDIR}/dlaqr2.f
    # ${LAPACK_SRCDIR}/dlaqr3.f
    # ${LAPACK_SRCDIR}/dlaqr4.f
    # ${LAPACK_SRCDIR}/dlaqr5.f
     ${LAPACK_SRCDIR}/dlarf.f
     ${LAPACK_SRCDIR}/dlarfb.f
     ${LAPACK_SRCDIR}/dlarfg.f
     ${LAPACK_SRCDIR}/dlarft.f
    # ${LAPACK_SRCDIR}/dlarfx.f
    # ${LAPACK_SRCDIR}/dlarnv.f
    # ${LAPACK_SRCDIR}/dlarra.f
    # ${LAPACK_SRCDIR}/dlarrb.f
    # ${LAPACK_SRCDIR}/dlarrc.f
    # ${LAPACK_SRCDIR}/dlarrd.f
    # ${LAPACK_SRCDIR}/dlarre.f
    # ${LAPACK_SRCDIR}/dlarrf.f
    # ${LAPACK_SRCDIR}/dlarrj.f
    # ${LAPACK_SRCDIR}/dlarrk.f
    # ${LAPACK_SRCDIR}/dlarrr.f
    # ${LAPACK_SRCDIR}/dlarrv.f
     ${LAPACK_SRCDIR}/dlartg.f
    # ${LAPACK_SRCDIR}/dlartv.f
    # ${LAPACK_SRCDIR}/dlaruv.f
    # ${LAPACK_SRCDIR}/dlar1v.f
    # ${LAPACK_SRCDIR}/dlas2.f
     ${LAPACK_SRCDIR}/dlascl.f
     ${LAPACK_SRCDIR}/dlaset.f
    # ${LAPACK_SRCDIR}/dlasq1.f
    # ${LAPACK_SRCDIR}/dlasq2.f
    # ${LAPACK_SRCDIR}/dlasq3.f
    # ${LAPACK_SRCDIR}/dlasq4.f
    # ${LAPACK_SRCDIR}/dlasq5.f
    # ${LAPACK_SRCDIR}/dlasq6.f
     ${LAPACK_SRCDIR}/dlasr.f
     ${LAPACK_SRCDIR}/dlasrt.f
     ${LAPACK_SRCDIR}/dlaswp.f
     ${LAPACK_SRCDIR}/dlassq.f
    # ${LAPACK_SRCDIR}/dlasv2.f
    # ${LAPACK_SRCDIR}/dlasyf.f
    # ${LAPACK_SRCDIR}/dlasy2.f
     ${LAPACK_SRCDIR}/dlatrd.f
     ${LAPACK_SRCDIR}/dorg2l.f
     ${LAPACK_SRCDIR}/dorg2r.f
    # ${LAPACK_SRCDIR}/dorgbr.f
    # ${LAPACK_SRCDIR}/dorghr.f
    # ${LAPACK_SRCDIR}/dorglq.f
    # ${LAPACK_SRCDIR}/dorgl2.f
     ${LAPACK_SRCDIR}/dorgql.f
     ${LAPACK_SRCDIR}/dorgqr.f
     ${LAPACK_SRCDIR}/dorgtr.f
    # ${LAPACK_SRCDIR}/dorm2r.f
    # ${LAPACK_SRCDIR}/dormbr.f
    # ${LAPACK_SRCDIR}/dormhr.f
    # ${LAPACK_SRCDIR}/dorml2.f
    # ${LAPACK_SRCDIR}/dormlq.f
    # ${LAPACK_SRCDIR}/dormql.f
    # ${LAPACK_SRCDIR}/dormqr.f
    # ${LAPACK_SRCDIR}/dormtr.f
    # ${LAPACK_SRCDIR}/dorm2l.f
     ${LAPACK_SRCDIR}/dpotf2.f
     ${LAPACK_SRCDIR}/dpotrf.f
     ${LAPACK_SRCDIR}/dpotrs.f
     ${LAPACK_SRCDIR}/dppsv.f
     ${LAPACK_SRCDIR}/dpptrf.f
     ${LAPACK_SRCDIR}/dpptrs.f
    # ${LAPACK_SRCDIR}/dstebz.f
    # ${LAPACK_SRCDIR}/dstein.f
    # ${LAPACK_SRCDIR}/dstemr.f
     ${LAPACK_SRCDIR}/dsteqr.f
     ${LAPACK_SRCDIR}/dsterf.f
     ${LAPACK_SRCDIR}/dsyev.f
    # ${LAPACK_SRCDIR}/dsyevr.f
    # ${LAPACK_SRCDIR}/dsyevx.f
     ${LAPACK_SRCDIR}/dsytd2.f
    # ${LAPACK_SRCDIR}/dsytf2.f
     ${LAPACK_SRCDIR}/dsytrd.f
    # ${LAPACK_SRCDIR}/dsytrf.f
    # ${LAPACK_SRCDIR}/dsytri.f
    # ${LAPACK_SRCDIR}/dtgevc.f
    # ${LAPACK_SRCDIR}/dtrevc.f
    # ${LAPACK_SRCDIR}/dtrexc.f
    # ${LAPACK_SRCDIR}/dtrti2.f
    # ${LAPACK_SRCDIR}/dtrtri.f
    # ${LAPACK_SRCDIR}/dtrtrs.f
     ${LAPACK_SRCDIR}/ieeeck.f
     ${LAPACK_SRCDIR}/iladlc.f
     ${LAPACK_SRCDIR}/iladlr.f
     ${LAPACK_SRCDIR}/ilaenv.f
     ${LAPACK_SRCDIR}/iparmq.f
    # ${LAPACK_SRCDIR}/sgetf2.f
    # ${LAPACK_SRCDIR}/sgetrf.f
    # ${LAPACK_SRCDIR}/slaswp.f
    # ${LAPACK_SRCDIR}/zgetf2.f
    # ${LAPACK_SRCDIR}/zgetrf.f
    # ${LAPACK_SRCDIR}/zlacgv.f
    # ${LAPACK_SRCDIR}/zlacpy.f
    # ${LAPACK_SRCDIR}/zlaev2.f
    # ${LAPACK_SRCDIR}/zlaswp.f
    # ${LAPACK_SRCDIR}/zpotf2.f
    # ${LAPACK_SRCDIR}/zrot.f
    # ${LAPACK_SRCDIR}/zsymv.f
    # ${LAPACK_SRCDIR}/zsyr.f
    # ${LAPACK_SRCDIR}/zsytri.f
    )
