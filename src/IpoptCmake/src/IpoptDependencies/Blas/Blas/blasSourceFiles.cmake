SET(BLAS_SOURCES
    ${BLAS_SRCDIR}/dasum.f
     ${BLAS_SRCDIR}/daxpy.f
    # ${BLAS_SRCDIR}/dcabs1.f
     ${BLAS_SRCDIR}/dcopy.f
     ${BLAS_SRCDIR}/ddot.f
    # #${BLAS_SRCDIR}/dgbmv.f
     ${BLAS_SRCDIR}/dgemm.f
     ${BLAS_SRCDIR}/dgemv.f
     ${BLAS_SRCDIR}/dger.f
     ${BLAS_SRCDIR}/dnrm2.f
    # ${BLAS_SRCDIR}/drot.f
    # #${BLAS_SRCDIR}/drotg.f
    # #${BLAS_SRCDIR}/drotm.f
    # ${BLAS_SRCDIR}/drotmg.f
    # #${BLAS_SRCDIR}/dsbmv.f
     ${BLAS_SRCDIR}/dscal.f
    # #${BLAS_SRCDIR}/dsdot.f
    # ${BLAS_SRCDIR}/dspmv.f
    # ${BLAS_SRCDIR}/dspr2.f
     ${BLAS_SRCDIR}/dspr.f
     ${BLAS_SRCDIR}/dswap.f
    # ${BLAS_SRCDIR}/dsymm.f
     ${BLAS_SRCDIR}/dsymv.f
     ${BLAS_SRCDIR}/dsyr2.f
     ${BLAS_SRCDIR}/dsyr2k.f
     ${BLAS_SRCDIR}/dsyr.f
     ${BLAS_SRCDIR}/dsyrk.f
    # #${BLAS_SRCDIR}/dtbmv.f
    # #${BLAS_SRCDIR}/dtbsv.f
    # #${BLAS_SRCDIR}/dtpmv.f
     ${BLAS_SRCDIR}/dtpsv.f
     ${BLAS_SRCDIR}/dtrmm.f
     ${BLAS_SRCDIR}/dtrmv.f
     ${BLAS_SRCDIR}/dtrsm.f
     ${BLAS_SRCDIR}/dtrsv.f
     ${BLAS_SRCDIR}/idamax.f
    # ${BLAS_SRCDIR}/isamax.f
    # ${BLAS_SRCDIR}/izamax.f
     ${BLAS_SRCDIR}/lsame.f
    # ${BLAS_SRCDIR}/sgemm.f
    # #${BLAS_SRCDIR}/sgemv.f
    # ${BLAS_SRCDIR}/sger.f
    # ${BLAS_SRCDIR}/sscal.f
    # ${BLAS_SRCDIR}/sswap.f
    # #${BLAS_SRCDIR}/ssyr.f
    # ${BLAS_SRCDIR}/strsm.f
     ${BLAS_SRCDIR}/xerbla.f
    # #${BLAS_SRCDIR}/zaxpy.f
    # ${BLAS_SRCDIR}/zcopy.f
    # ${BLAS_SRCDIR}/zdscal.f
    # ${BLAS_SRCDIR}/zdotc.f
    # ${BLAS_SRCDIR}/zdotu.f
    # ${BLAS_SRCDIR}/zgemm.f
    # ${BLAS_SRCDIR}/zgemv.f
    # ${BLAS_SRCDIR}/zgeru.f
    # #${BLAS_SRCDIR}/zher.f
    # ${BLAS_SRCDIR}/zscal.f
    # ${BLAS_SRCDIR}/zswap.f
    # ${BLAS_SRCDIR}/ztrsm.f
    )
