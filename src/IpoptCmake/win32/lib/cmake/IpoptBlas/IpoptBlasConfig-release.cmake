#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "IpoptBlas::IpoptBlas" for configuration "Release"
set_property(TARGET IpoptBlas::IpoptBlas APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(IpoptBlas::IpoptBlas PROPERTIES
  IMPORTED_IMPLIB_RELEASE "${_IMPORT_PREFIX}/lib/IpoptBlas.lib"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/IpoptBlas.dll"
  )

list(APPEND _IMPORT_CHECK_TARGETS IpoptBlas::IpoptBlas )
list(APPEND _IMPORT_CHECK_FILES_FOR_IpoptBlas::IpoptBlas "${_IMPORT_PREFIX}/lib/IpoptBlas.lib" "${_IMPORT_PREFIX}/bin/IpoptBlas.dll" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
