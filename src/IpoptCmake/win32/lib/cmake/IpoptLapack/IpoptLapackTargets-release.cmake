#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "IpoptLapack::IpoptLapack" for configuration "Release"
set_property(TARGET IpoptLapack::IpoptLapack APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(IpoptLapack::IpoptLapack PROPERTIES
  IMPORTED_IMPLIB_RELEASE "${_IMPORT_PREFIX}/lib/IpoptLapack.lib"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/IpoptLapack.dll"
  )

list(APPEND _IMPORT_CHECK_TARGETS IpoptLapack::IpoptLapack )
list(APPEND _IMPORT_CHECK_FILES_FOR_IpoptLapack::IpoptLapack "${_IMPORT_PREFIX}/lib/IpoptLapack.lib" "${_IMPORT_PREFIX}/bin/IpoptLapack.dll" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
