#if defined(__cplusplus)
  extern "C" {
#endif
  int DaeoDfba_Nonlinear_Example_ToyProblem_mayer(DATA* data, modelica_real** res, short*);
  int DaeoDfba_Nonlinear_Example_ToyProblem_lagrange(DATA* data, modelica_real** res, short *, short *);
  int DaeoDfba_Nonlinear_Example_ToyProblem_pickUpBoundsForInputsInOptimization(DATA* data, modelica_real* min, modelica_real* max, modelica_real*nominal, modelica_boolean *useNominal, char ** name, modelica_real * start, modelica_real * startTimeOpt);
  int DaeoDfba_Nonlinear_Example_ToyProblem_setInputData(DATA *data, const modelica_boolean file);
  int DaeoDfba_Nonlinear_Example_ToyProblem_getTimeGrid(DATA *data, modelica_integer * nsi, modelica_real**t);
#if defined(__cplusplus)
}
#endif