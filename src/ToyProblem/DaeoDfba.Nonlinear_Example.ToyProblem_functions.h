#ifndef DaeoDfba_Nonlinear_Example_ToyProblem__H
#define DaeoDfba_Nonlinear_Example_ToyProblem__H
#include "meta/meta_modelica.h"
#include "util/modelica.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "simulation/simulation_runtime.h"
#ifdef __cplusplus
extern "C" {
#endif


DLLExport
real_array omc_DaeoDfba_Solver_solveNonlinearActiveSet(threadData_t *threadData, modelica_complex _daeo, real_array _x, modelica_integer _n_y, modelica_integer _n_q, real_array *out_sigma, integer_array *out_info);
DLLExport
modelica_metatype boxptr_DaeoDfba_Solver_solveNonlinearActiveSet(threadData_t *threadData, modelica_metatype _daeo, modelica_metatype _x, modelica_metatype _n_y, modelica_metatype _n_q, modelica_metatype *out_sigma, modelica_metatype *out_info);
static const MMC_DEFSTRUCTLIT(boxvar_lit_DaeoDfba_Solver_solveNonlinearActiveSet,2,0) {(void*) boxptr_DaeoDfba_Solver_solveNonlinearActiveSet,0}};
#define boxvar_DaeoDfba_Solver_solveNonlinearActiveSet MMC_REFSTRUCTLIT(boxvar_lit_DaeoDfba_Solver_solveNonlinearActiveSet)

extern void solveNonlinearActiveSet(void * /*_daeo*/, const double* /*_x*/, double* /*_y*/, double* /*_sigma*/, int* /*_info*/);


DLLExport
integer_array omc_DaeoDfba_Solver_updateNonlinearActiveSet(threadData_t *threadData, modelica_complex _daeo, real_array _x);
DLLExport
modelica_metatype boxptr_DaeoDfba_Solver_updateNonlinearActiveSet(threadData_t *threadData, modelica_metatype _daeo, modelica_metatype _x);
static const MMC_DEFSTRUCTLIT(boxvar_lit_DaeoDfba_Solver_updateNonlinearActiveSet,2,0) {(void*) boxptr_DaeoDfba_Solver_updateNonlinearActiveSet,0}};
#define boxvar_DaeoDfba_Solver_updateNonlinearActiveSet MMC_REFSTRUCTLIT(boxvar_lit_DaeoDfba_Solver_updateNonlinearActiveSet)

extern void updateNonlinearActiveSet(void * /*_daeo*/, const double* /*_x*/, int* /*_info*/);


DLLExport
modelica_complex omc_DaeoDfba_Solver_NonlinearDaeoObject_constructor(threadData_t *threadData, string_array _params, string_array _variables, modelica_string _objective, string_array _equalities, string_array _inequalities);
DLLExport
modelica_metatype boxptr_DaeoDfba_Solver_NonlinearDaeoObject_constructor(threadData_t *threadData, modelica_metatype _params, modelica_metatype _variables, modelica_metatype _objective, modelica_metatype _equalities, modelica_metatype _inequalities);
static const MMC_DEFSTRUCTLIT(boxvar_lit_DaeoDfba_Solver_NonlinearDaeoObject_constructor,2,0) {(void*) boxptr_DaeoDfba_Solver_NonlinearDaeoObject_constructor,0}};
#define boxvar_DaeoDfba_Solver_NonlinearDaeoObject_constructor MMC_REFSTRUCTLIT(boxvar_lit_DaeoDfba_Solver_NonlinearDaeoObject_constructor)

extern void * parsenNonlinearDAEOmodel(const char** /*_params*/, const char** /*_variables*/, const char* /*_objective*/, const char** /*_equalities*/, const char** /*_inequalities*/, size_t, size_t, size_t, size_t, int* /*_info*/);


DLLExport
void omc_DaeoDfba_Solver_NonlinearDaeoObject_destructor(threadData_t *threadData, modelica_complex _daeo);
DLLExport
void boxptr_DaeoDfba_Solver_NonlinearDaeoObject_destructor(threadData_t *threadData, modelica_metatype _daeo);
static const MMC_DEFSTRUCTLIT(boxvar_lit_DaeoDfba_Solver_NonlinearDaeoObject_destructor,2,0) {(void*) boxptr_DaeoDfba_Solver_NonlinearDaeoObject_destructor,0}};
#define boxvar_DaeoDfba_Solver_NonlinearDaeoObject_destructor MMC_REFSTRUCTLIT(boxvar_lit_DaeoDfba_Solver_NonlinearDaeoObject_destructor)

extern void closeNonlinearDAEO(void * /*_daeo*/);
#include "DaeoDfba.Nonlinear_Example.ToyProblem_model.h"


#ifdef __cplusplus
}
#endif
#endif

