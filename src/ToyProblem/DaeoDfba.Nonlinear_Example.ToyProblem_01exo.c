/* External objects file */
#include "DaeoDfba.Nonlinear_Example.ToyProblem_model.h"
#if defined(__cplusplus)
extern "C" {
#endif

void DaeoDfba_Nonlinear_Example_ToyProblem_callExternalObjectDestructors(DATA *data, threadData_t *threadData)
{
  if(data->simulationInfo->extObjs)
  {
    omc_DaeoDfba_Solver_NonlinearDaeoObject_destructor(threadData,data->simulationInfo->extObjs[0]);
    free(data->simulationInfo->extObjs);
    data->simulationInfo->extObjs = 0;
  }
}
#if defined(__cplusplus)
}
#endif

