/* Initialization */
#include "DaeoDfba.Nonlinear_Example.ToyProblem_model.h"
#include "DaeoDfba.Nonlinear_Example.ToyProblem_11mix.h"
#include "DaeoDfba.Nonlinear_Example.ToyProblem_12jac.h"
#if defined(__cplusplus)
extern "C" {
#endif

void DaeoDfba_Nonlinear_Example_ToyProblem_functionInitialEquations_0(DATA *data, threadData_t *threadData);

/*
equation index: 1
type: SIMPLE_ASSIGN
y_d = $START.y_d
*/
void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_1(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,1};
  data->localData[0]->realVars[0] /* y_d STATE(1) */ = data->modelData->realVarsData[0].attribute /* y_d STATE(1) */.start;
  TRACE_POP
}

/*
equation index: 2
type: ALGORITHM

  (daeoOut, sigma, info) := DaeoDfba.Solver.solveNonlinearActiveSet(daeo, {y_d, p2, p3}, 2, 2);
*/
void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_2(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,2};
  real_array tmp0;
  integer_array tmp1;
  real_array tmp2;
  real_array tmp3;
  real_array_create(&tmp0, ((modelica_real*)&((&data->localData[0]->realVars[6] /* sigma[1] variable */)[calc_base_index_dims_subs(1, 2, ((modelica_integer) 1))])), 1, 2);
  integer_array_create(&tmp1, ((modelica_integer*)&((&data->localData[0]->integerVars[1] /* info[1] DISCRETE */)[calc_base_index_dims_subs(1, 2, ((modelica_integer) 1))])), 1, 2);
  array_alloc_scalar_real_array(&tmp2, 3, (modelica_real)data->localData[0]->realVars[0] /* y_d STATE(1) */, (modelica_real)data->simulationInfo->realParameter[3] /* p2 PARAM */, (modelica_real)data->simulationInfo->realParameter[4] /* p3 PARAM */);
  real_array_create(&tmp3, ((modelica_real*)&((&data->localData[0]->realVars[3] /* daeoOut[1] variable */)[calc_base_index_dims_subs(1, 2, ((modelica_integer) 1))])), 1, 2);
  copy_real_array_data(omc_DaeoDfba_Solver_solveNonlinearActiveSet(threadData, data->simulationInfo->extObjs[0], tmp2, ((modelica_integer) 2), ((modelica_integer) 2) ,&tmp0 ,&tmp1), &tmp3);
  TRACE_POP
}

/*
equation index: 3
type: SIMPLE_ASSIGN
$whenCondition2 = min(sigma[1], sigma[2]) < (-tolEvent)
*/
void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_3(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,3};
  modelica_boolean tmp4;
  tmp4 = Less(fmin(data->localData[0]->realVars[6] /* sigma[1] variable */,data->localData[0]->realVars[7] /* sigma[2] variable */),(-data->simulationInfo->realParameter[5] /* tolEvent PARAM */));
  data->localData[0]->booleanVars[1] /* $whenCondition2 DISCRETE */ = tmp4;
  TRACE_POP
}
extern void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_10(DATA *data, threadData_t *threadData);

extern void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_13(DATA *data, threadData_t *threadData);


/*
equation index: 6
type: SIMPLE_ASSIGN
$DER.y_d = 25.13274122871834 * cos(6.283185307179586 * time)
*/
void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_6(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,6};
  data->localData[0]->realVars[1] /* der(y_d) STATE_DER */ = (25.13274122871834) * (cos((6.283185307179586) * (data->localData[0]->timeValue)));
  TRACE_POP
}

/*
equation index: 7
type: SIMPLE_ASSIGN
$PRE.eventCounter = 0.0
*/
void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_7(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,7};
  data->simulationInfo->integerVarsPre[0] /* eventCounter DISCRETE */ = 0.0;
  TRACE_POP
}

/*
equation index: 8
type: SIMPLE_ASSIGN
eventCounter = $PRE.eventCounter
*/
void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_8(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,8};
  data->localData[0]->integerVars[0] /* eventCounter DISCRETE */ = data->simulationInfo->integerVarsPre[0] /* eventCounter DISCRETE */;
  TRACE_POP
}
OMC_DISABLE_OPT
void DaeoDfba_Nonlinear_Example_ToyProblem_functionInitialEquations_0(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_1(data, threadData);
  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_2(data, threadData);
  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_3(data, threadData);
  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_10(data, threadData);
  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_13(data, threadData);
  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_6(data, threadData);
  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_7(data, threadData);
  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_8(data, threadData);
  TRACE_POP
}


int DaeoDfba_Nonlinear_Example_ToyProblem_functionInitialEquations(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH

  data->simulationInfo->discreteCall = 1;
  DaeoDfba_Nonlinear_Example_ToyProblem_functionInitialEquations_0(data, threadData);
  data->simulationInfo->discreteCall = 0;
  
  TRACE_POP
  return 0;
}


int DaeoDfba_Nonlinear_Example_ToyProblem_functionInitialEquations_lambda0(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH

  data->simulationInfo->discreteCall = 1;
  data->simulationInfo->discreteCall = 0;
  
  TRACE_POP
  return 0;
}
int DaeoDfba_Nonlinear_Example_ToyProblem_functionRemovedInitialEquations(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int *equationIndexes = NULL;
  double res = 0.0;

  
  TRACE_POP
  return 0;
}


#if defined(__cplusplus)
}
#endif

