#include "omc_simulation_settings.h"
#include "DaeoDfba.Nonlinear_Example.ToyProblem_functions.h"
#ifdef __cplusplus
extern "C" {
#endif

#include "DaeoDfba.Nonlinear_Example.ToyProblem_includes.h"


real_array omc_DaeoDfba_Solver_solveNonlinearActiveSet(threadData_t *threadData, modelica_complex _daeo, real_array _x, modelica_integer _n_y, modelica_integer _n_q, real_array *out_sigma, integer_array *out_info)
{
  void * _daeo_ext;
  void *_x_c89;
  void *_y_c89;
  void *_sigma_c89;
  void *_info_c89;
  real_array _y;
  real_array _sigma;
  integer_array _info;
  alloc_real_array(&(_y), 1, _n_y); // _y has no default value.
  alloc_real_array(&(_sigma), 1, _n_q); // _sigma has no default value.
  alloc_integer_array(&(_info), 1, 2); // _info has no default value.
  _daeo_ext = (void *)_daeo;
  _x_c89 = (void*) data_of_real_c89_array(&(_x));
  _y_c89 = (void*) data_of_real_c89_array(&(_y));
  _sigma_c89 = (void*) data_of_real_c89_array(&(_sigma));
  _info_c89 = (void*) data_of_integer_c89_array(&(_info));
  solveNonlinearActiveSet(_daeo_ext, (const double*) _x_c89, (double*) _y_c89, (double*) _sigma_c89, (int*) _info_c89);
  unpack_integer_array(&_info);
  if (out_sigma) { if (out_sigma->dim_size == NULL) {copy_real_array(_sigma, out_sigma);} else {copy_real_array_data(_sigma, out_sigma);} }
  if (out_info) { if (out_info->dim_size == NULL) {copy_integer_array(_info, out_info);} else {copy_integer_array_data(_info, out_info);} }
  return _y;
}
modelica_metatype boxptr_DaeoDfba_Solver_solveNonlinearActiveSet(threadData_t *threadData, modelica_metatype _daeo, modelica_metatype _x, modelica_metatype _n_y, modelica_metatype _n_q, modelica_metatype *out_sigma, modelica_metatype *out_info)
{
  modelica_integer tmp1;
  modelica_integer tmp2;
  real_array _sigma;
  integer_array _info;
  real_array _y;
  modelica_metatype out_y;
  tmp1 = mmc_unbox_integer(_n_y);
  tmp2 = mmc_unbox_integer(_n_q);
  _y = omc_DaeoDfba_Solver_solveNonlinearActiveSet(threadData, _daeo, *((base_array_t*)_x), tmp1, tmp2, &_sigma, &_info);
  out_y = mmc_mk_modelica_array(_y);
  if (out_sigma) { *out_sigma = mmc_mk_modelica_array(_sigma); }
  if (out_info) { *out_info = mmc_mk_modelica_array(_info); }
  return out_y;
}

integer_array omc_DaeoDfba_Solver_updateNonlinearActiveSet(threadData_t *threadData, modelica_complex _daeo, real_array _x)
{
  void * _daeo_ext;
  void *_x_c89;
  void *_info_c89;
  integer_array _info;
  alloc_integer_array(&(_info), 1, 2); // _info has no default value.
  _daeo_ext = (void *)_daeo;
  _x_c89 = (void*) data_of_real_c89_array(&(_x));
  _info_c89 = (void*) data_of_integer_c89_array(&(_info));
  updateNonlinearActiveSet(_daeo_ext, (const double*) _x_c89, (int*) _info_c89);
  unpack_integer_array(&_info);
  return _info;
}
modelica_metatype boxptr_DaeoDfba_Solver_updateNonlinearActiveSet(threadData_t *threadData, modelica_metatype _daeo, modelica_metatype _x)
{
  integer_array _info;
  modelica_metatype out_info;
  _info = omc_DaeoDfba_Solver_updateNonlinearActiveSet(threadData, _daeo, *((base_array_t*)_x));
  out_info = mmc_mk_modelica_array(_info);
  return out_info;
}

modelica_complex omc_DaeoDfba_Solver_NonlinearDaeoObject_constructor(threadData_t *threadData, string_array _params, string_array _variables, modelica_string _objective, string_array _equalities, string_array _inequalities)
{
  integer_array _info;
  void *_params_c89;
  void *_variables_c89;
  void *_equalities_c89;
  void *_inequalities_c89;
  void *_info_c89;
  void * _daeo_ext;
  modelica_complex _daeo;
  // _daeo has no default value.
  alloc_integer_array(&_info, 1, 2);
  _params_c89 = (void*) data_of_string_c89_array(&(_params));
  _variables_c89 = (void*) data_of_string_c89_array(&(_variables));
  _equalities_c89 = (void*) data_of_string_c89_array(&(_equalities));
  _inequalities_c89 = (void*) data_of_string_c89_array(&(_inequalities));
  _info_c89 = (void*) data_of_integer_c89_array(&(_info));
  _daeo_ext = parsenNonlinearDAEOmodel((const char**) _params_c89, (const char**) _variables_c89, MMC_STRINGDATA(_objective), (const char**) _equalities_c89, (const char**) _inequalities_c89, size_of_dimension_base_array(_params, ((modelica_integer) 1)), size_of_dimension_base_array(_equalities, ((modelica_integer) 1)), size_of_dimension_base_array(_variables, ((modelica_integer) 1)), size_of_dimension_base_array(_inequalities, ((modelica_integer) 1)), (int*) _info_c89);
  _daeo = (modelica_complex)_daeo_ext;
  return _daeo;
}
modelica_metatype boxptr_DaeoDfba_Solver_NonlinearDaeoObject_constructor(threadData_t *threadData, modelica_metatype _params, modelica_metatype _variables, modelica_metatype _objective, modelica_metatype _equalities, modelica_metatype _inequalities)
{
  modelica_complex _daeo;
  _daeo = omc_DaeoDfba_Solver_NonlinearDaeoObject_constructor(threadData, *((base_array_t*)_params), *((base_array_t*)_variables), _objective, *((base_array_t*)_equalities), *((base_array_t*)_inequalities));
  /* skip box _daeo; ExternalObject DaeoDfba.Solver.NonlinearDaeoObject */
  return _daeo;
}

void omc_DaeoDfba_Solver_NonlinearDaeoObject_destructor(threadData_t *threadData, modelica_complex _daeo)
{
  void * _daeo_ext;
  _daeo_ext = (void *)_daeo;
  closeNonlinearDAEO(_daeo_ext);
  return;
}
void boxptr_DaeoDfba_Solver_NonlinearDaeoObject_destructor(threadData_t *threadData, modelica_metatype _daeo)
{
  omc_DaeoDfba_Solver_NonlinearDaeoObject_destructor(threadData, _daeo);
  return;
}

#ifdef __cplusplus
}
#endif
