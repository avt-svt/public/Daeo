#ifdef __cplusplus
extern "C" {
#endif

#define _OMC_LIT0_data "'p"
static const MMC_DEFSTRINGLIT(_OMC_LIT_STRUCT0,2,_OMC_LIT0_data);
#define _OMC_LIT0 MMC_REFSTRINGLIT(_OMC_LIT_STRUCT0)
#define _OMC_LIT1_data "'p/s"
static const MMC_DEFSTRINGLIT(_OMC_LIT_STRUCT1,4,_OMC_LIT1_data);
#define _OMC_LIT1 MMC_REFSTRINGLIT(_OMC_LIT_STRUCT1)
#define _OMC_LIT2_data "x"
static const MMC_DEFSTRINGLIT(_OMC_LIT_STRUCT2,1,_OMC_LIT2_data);
#define _OMC_LIT2 MMC_REFSTRINGLIT(_OMC_LIT_STRUCT2)
#define _OMC_LIT3_data "p2"
static const MMC_DEFSTRINGLIT(_OMC_LIT_STRUCT3,2,_OMC_LIT3_data);
#define _OMC_LIT3 MMC_REFSTRINGLIT(_OMC_LIT_STRUCT3)
#define _OMC_LIT4_data "p3"
static const MMC_DEFSTRINGLIT(_OMC_LIT_STRUCT4,2,_OMC_LIT4_data);
#define _OMC_LIT4 MMC_REFSTRINGLIT(_OMC_LIT_STRUCT4)
#define _OMC_LIT5_data "y"
static const MMC_DEFSTRINGLIT(_OMC_LIT_STRUCT5,1,_OMC_LIT5_data);
#define _OMC_LIT5 MMC_REFSTRINGLIT(_OMC_LIT_STRUCT5)
#define _OMC_LIT6_data "z"
static const MMC_DEFSTRINGLIT(_OMC_LIT_STRUCT6,1,_OMC_LIT6_data);
#define _OMC_LIT6 MMC_REFSTRINGLIT(_OMC_LIT_STRUCT6)
#define _OMC_LIT7_data "(-x + 2 * y + p3)^2"
static const MMC_DEFSTRINGLIT(_OMC_LIT_STRUCT7,19,_OMC_LIT7_data);
#define _OMC_LIT7 MMC_REFSTRINGLIT(_OMC_LIT_STRUCT7)
#define _OMC_LIT8_data "y - z"
static const MMC_DEFSTRINGLIT(_OMC_LIT_STRUCT8,5,_OMC_LIT8_data);
#define _OMC_LIT8 MMC_REFSTRINGLIT(_OMC_LIT_STRUCT8)
#define _OMC_LIT9_data "p2-y"
static const MMC_DEFSTRINGLIT(_OMC_LIT_STRUCT9,4,_OMC_LIT9_data);
#define _OMC_LIT9 MMC_REFSTRINGLIT(_OMC_LIT_STRUCT9)
static _index_t _OMC_LIT10_dims[1] = {3};
static const modelica_string _OMC_LIT10_data[] = {_OMC_LIT2, _OMC_LIT3, _OMC_LIT4};
static string_array const _OMC_LIT10 = {
  1, _OMC_LIT10_dims, (void*) _OMC_LIT10_data, (modelica_boolean) 0
};
static _index_t _OMC_LIT11_dims[1] = {2};
static const modelica_string _OMC_LIT11_data[] = {_OMC_LIT5, _OMC_LIT6};
static string_array const _OMC_LIT11 = {
  1, _OMC_LIT11_dims, (void*) _OMC_LIT11_data, (modelica_boolean) 0
};
static _index_t _OMC_LIT12_dims[1] = {1};
static const modelica_string _OMC_LIT12_data[] = {_OMC_LIT8};
static string_array const _OMC_LIT12 = {
  1, _OMC_LIT12_dims, (void*) _OMC_LIT12_data, (modelica_boolean) 0
};
static _index_t _OMC_LIT13_dims[1] = {2};
static const modelica_string _OMC_LIT13_data[] = {_OMC_LIT5, _OMC_LIT9};
static string_array const _OMC_LIT13 = {
  1, _OMC_LIT13_dims, (void*) _OMC_LIT13_data, (modelica_boolean) 0
};

#ifdef __cplusplus
}
#endif
