/* Main Simulation File */

#if defined(__cplusplus)
extern "C" {
#endif

#include "DaeoDfba.Nonlinear_Example.ToyProblem_model.h"
#include "simulation/solver/events.h"

#define prefixedName_performSimulation DaeoDfba_Nonlinear_Example_ToyProblem_performSimulation
#define prefixedName_updateContinuousSystem DaeoDfba_Nonlinear_Example_ToyProblem_updateContinuousSystem
#include <simulation/solver/perform_simulation.c.inc>

#define prefixedName_performQSSSimulation DaeoDfba_Nonlinear_Example_ToyProblem_performQSSSimulation
#include <simulation/solver/perform_qss_simulation.c.inc>

/* dummy VARINFO and FILEINFO */
const FILE_INFO dummyFILE_INFO = omc_dummyFileInfo;
const VAR_INFO dummyVAR_INFO = omc_dummyVarInfo;

int DaeoDfba_Nonlinear_Example_ToyProblem_input_function(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH

  
  TRACE_POP
  return 0;
}

int DaeoDfba_Nonlinear_Example_ToyProblem_input_function_init(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH

  
  TRACE_POP
  return 0;
}

int DaeoDfba_Nonlinear_Example_ToyProblem_input_function_updateStartValues(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH

  
  TRACE_POP
  return 0;
}

int DaeoDfba_Nonlinear_Example_ToyProblem_inputNames(DATA *data, char ** names){
  TRACE_PUSH

  
  TRACE_POP
  return 0;
}

int DaeoDfba_Nonlinear_Example_ToyProblem_data_function(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH

  TRACE_POP
  return 0;
}

int DaeoDfba_Nonlinear_Example_ToyProblem_output_function(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH

  
  TRACE_POP
  return 0;
}

int DaeoDfba_Nonlinear_Example_ToyProblem_setc_function(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH

  
  TRACE_POP
  return 0;
}


/*
equation index: 9
type: ALGORITHM

  (daeoOut, sigma, info) := DaeoDfba.Solver.solveNonlinearActiveSet(daeo, {y_d, p2, p3}, 2, 2);
*/
void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_9(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,9};
  real_array tmp0;
  integer_array tmp1;
  real_array tmp2;
  real_array tmp3;
  real_array_create(&tmp0, ((modelica_real*)&((&data->localData[0]->realVars[6] /* sigma[1] variable */)[calc_base_index_dims_subs(1, 2, ((modelica_integer) 1))])), 1, 2);
  integer_array_create(&tmp1, ((modelica_integer*)&((&data->localData[0]->integerVars[1] /* info[1] DISCRETE */)[calc_base_index_dims_subs(1, 2, ((modelica_integer) 1))])), 1, 2);
  array_alloc_scalar_real_array(&tmp2, 3, (modelica_real)data->localData[0]->realVars[0] /* y_d STATE(1) */, (modelica_real)data->simulationInfo->realParameter[3] /* p2 PARAM */, (modelica_real)data->simulationInfo->realParameter[4] /* p3 PARAM */);
  real_array_create(&tmp3, ((modelica_real*)&((&data->localData[0]->realVars[3] /* daeoOut[1] variable */)[calc_base_index_dims_subs(1, 2, ((modelica_integer) 1))])), 1, 2);
  copy_real_array_data(omc_DaeoDfba_Solver_solveNonlinearActiveSet(threadData, data->simulationInfo->extObjs[0], tmp2, ((modelica_integer) 2), ((modelica_integer) 2) ,&tmp0 ,&tmp1), &tmp3);
  TRACE_POP
}
/*
equation index: 10
type: SIMPLE_ASSIGN
minsigma = min(sigma[1], sigma[2])
*/
void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_10(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,10};
  data->localData[0]->realVars[5] /* minsigma variable */ = fmin(data->localData[0]->realVars[6] /* sigma[1] variable */,data->localData[0]->realVars[7] /* sigma[2] variable */);
  TRACE_POP
}
/*
equation index: 11
type: SIMPLE_ASSIGN
$cse1 = cos(6.283185307179586 * time)
*/
void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_11(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,11};
  data->localData[0]->realVars[2] /* $cse1 variable */ = cos((6.283185307179586) * (data->localData[0]->timeValue));
  TRACE_POP
}
/*
equation index: 12
type: SIMPLE_ASSIGN
$whenCondition2 = minsigma < (-tolEvent)
*/
void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_12(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,12};
  modelica_boolean tmp4;
  RELATIONHYSTERESIS(tmp4, data->localData[0]->realVars[5] /* minsigma variable */, (-data->simulationInfo->realParameter[5] /* tolEvent PARAM */), 0, Less);
  data->localData[0]->booleanVars[1] /* $whenCondition2 DISCRETE */ = tmp4;
  TRACE_POP
}
/*
equation index: 13
type: SIMPLE_ASSIGN
$whenCondition1 = minsigma < (-tolEvent)
*/
void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_13(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,13};
  modelica_boolean tmp5;
  RELATIONHYSTERESIS(tmp5, data->localData[0]->realVars[5] /* minsigma variable */, (-data->simulationInfo->realParameter[5] /* tolEvent PARAM */), 0, Less);
  data->localData[0]->booleanVars[0] /* $whenCondition1 DISCRETE */ = tmp5;
  TRACE_POP
}
/*
equation index: 14
type: SIMPLE_ASSIGN
$DER.y_d = 25.13274122871834 * $cse1
*/
void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_14(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,14};
  data->localData[0]->realVars[1] /* der(y_d) STATE_DER */ = (25.13274122871834) * (data->localData[0]->realVars[2] /* $cse1 variable */);
  TRACE_POP
}
/*
equation index: 15
type: WHEN

when {$whenCondition1} then
  eventCounter = 1 + pre(eventCounter);
end when;
*/
void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_15(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,15};
  if((data->localData[0]->booleanVars[0] /* $whenCondition1 DISCRETE */ && !data->simulationInfo->booleanVarsPre[0] /* $whenCondition1 DISCRETE */ /* edge */))
  {
    data->localData[0]->integerVars[0] /* eventCounter DISCRETE */ = ((modelica_integer) 1) + data->simulationInfo->integerVarsPre[0] /* eventCounter DISCRETE */;
  }
  TRACE_POP
}
/*
equation index: 16
type: WHEN

when {$whenCondition2} then
  noReturnCall(DaeoDfba.Solver.updateNonlinearActiveSet(daeo, {y_d, p2, p3}))%>);
end when;
*/
void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_16(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,16};
  real_array tmp6;
  if((data->localData[0]->booleanVars[1] /* $whenCondition2 DISCRETE */ && !data->simulationInfo->booleanVarsPre[1] /* $whenCondition2 DISCRETE */ /* edge */))
  {
    array_alloc_scalar_real_array(&tmp6, 3, (modelica_real)data->localData[0]->realVars[0] /* y_d STATE(1) */, (modelica_real)data->simulationInfo->realParameter[3] /* p2 PARAM */, (modelica_real)data->simulationInfo->realParameter[4] /* p3 PARAM */);
    omc_DaeoDfba_Solver_updateNonlinearActiveSet(threadData, data->simulationInfo->extObjs[0], tmp6);
  }
  TRACE_POP
}

OMC_DISABLE_OPT
int DaeoDfba_Nonlinear_Example_ToyProblem_functionDAE(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  int equationIndexes[1] = {0};
#if !defined(OMC_MINIMAL_RUNTIME)
  if (measure_time_flag) rt_tick(SIM_TIMER_DAE);
#endif

  data->simulationInfo->needToIterate = 0;
  data->simulationInfo->discreteCall = 1;
  DaeoDfba_Nonlinear_Example_ToyProblem_functionLocalKnownVars(data, threadData);
  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_9(data, threadData);

  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_10(data, threadData);

  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_11(data, threadData);

  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_12(data, threadData);

  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_13(data, threadData);

  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_14(data, threadData);

  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_15(data, threadData);

  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_16(data, threadData);
  data->simulationInfo->discreteCall = 0;
  
#if !defined(OMC_MINIMAL_RUNTIME)
  if (measure_time_flag) rt_accumulate(SIM_TIMER_DAE);
#endif
  TRACE_POP
  return 0;
}


int DaeoDfba_Nonlinear_Example_ToyProblem_functionLocalKnownVars(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH

  
  TRACE_POP
  return 0;
}


/* forwarded equations */
extern void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_11(DATA* data, threadData_t *threadData);
extern void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_14(DATA* data, threadData_t *threadData);

static void functionODE_system0(DATA *data, threadData_t *threadData)
{
    DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_11(data, threadData);

    DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_14(data, threadData);
}

int DaeoDfba_Nonlinear_Example_ToyProblem_functionODE(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
#if !defined(OMC_MINIMAL_RUNTIME)
  if (measure_time_flag) rt_tick(SIM_TIMER_FUNCTION_ODE);
#endif

  
  data->simulationInfo->callStatistics.functionODE++;
  
  DaeoDfba_Nonlinear_Example_ToyProblem_functionLocalKnownVars(data, threadData);
  functionODE_system0(data, threadData);

#if !defined(OMC_MINIMAL_RUNTIME)
  if (measure_time_flag) rt_accumulate(SIM_TIMER_FUNCTION_ODE);
#endif

  TRACE_POP
  return 0;
}

/* forward the main in the simulation runtime */
extern int _main_SimulationRuntime(int argc, char**argv, DATA *data, threadData_t *threadData);

#include "DaeoDfba.Nonlinear_Example.ToyProblem_12jac.h"
#include "DaeoDfba.Nonlinear_Example.ToyProblem_13opt.h"

struct OpenModelicaGeneratedFunctionCallbacks DaeoDfba_Nonlinear_Example_ToyProblem_callback = {
   (int (*)(DATA *, threadData_t *, void *)) DaeoDfba_Nonlinear_Example_ToyProblem_performSimulation,
   (int (*)(DATA *, threadData_t *, void *)) DaeoDfba_Nonlinear_Example_ToyProblem_performQSSSimulation,
   DaeoDfba_Nonlinear_Example_ToyProblem_updateContinuousSystem,
   DaeoDfba_Nonlinear_Example_ToyProblem_callExternalObjectDestructors,
   NULL,
   NULL,
   NULL,
   #if !defined(OMC_NO_STATESELECTION)
   DaeoDfba_Nonlinear_Example_ToyProblem_initializeStateSets,
   #else
   NULL,
   #endif
   DaeoDfba_Nonlinear_Example_ToyProblem_initializeDAEmodeData,
   DaeoDfba_Nonlinear_Example_ToyProblem_functionODE,
   DaeoDfba_Nonlinear_Example_ToyProblem_functionAlgebraics,
   DaeoDfba_Nonlinear_Example_ToyProblem_functionDAE,
   DaeoDfba_Nonlinear_Example_ToyProblem_functionLocalKnownVars,
   DaeoDfba_Nonlinear_Example_ToyProblem_input_function,
   DaeoDfba_Nonlinear_Example_ToyProblem_input_function_init,
   DaeoDfba_Nonlinear_Example_ToyProblem_input_function_updateStartValues,
   DaeoDfba_Nonlinear_Example_ToyProblem_data_function,
   DaeoDfba_Nonlinear_Example_ToyProblem_output_function,
   DaeoDfba_Nonlinear_Example_ToyProblem_setc_function,
   DaeoDfba_Nonlinear_Example_ToyProblem_function_storeDelayed,
   DaeoDfba_Nonlinear_Example_ToyProblem_updateBoundVariableAttributes,
   DaeoDfba_Nonlinear_Example_ToyProblem_functionInitialEquations,
   1, /* useHomotopy - 0: local homotopy (equidistant lambda), 1: global homotopy (equidistant lambda), 2: new global homotopy approach (adaptive lambda), 3: new local homotopy approach (adaptive lambda)*/
   DaeoDfba_Nonlinear_Example_ToyProblem_functionInitialEquations_lambda0,
   DaeoDfba_Nonlinear_Example_ToyProblem_functionRemovedInitialEquations,
   DaeoDfba_Nonlinear_Example_ToyProblem_updateBoundParameters,
   DaeoDfba_Nonlinear_Example_ToyProblem_checkForAsserts,
   DaeoDfba_Nonlinear_Example_ToyProblem_function_ZeroCrossingsEquations,
   DaeoDfba_Nonlinear_Example_ToyProblem_function_ZeroCrossings,
   DaeoDfba_Nonlinear_Example_ToyProblem_function_updateRelations,
   DaeoDfba_Nonlinear_Example_ToyProblem_zeroCrossingDescription,
   DaeoDfba_Nonlinear_Example_ToyProblem_relationDescription,
   DaeoDfba_Nonlinear_Example_ToyProblem_function_initSample,
   DaeoDfba_Nonlinear_Example_ToyProblem_INDEX_JAC_A,
   DaeoDfba_Nonlinear_Example_ToyProblem_INDEX_JAC_B,
   DaeoDfba_Nonlinear_Example_ToyProblem_INDEX_JAC_C,
   DaeoDfba_Nonlinear_Example_ToyProblem_INDEX_JAC_D,
   DaeoDfba_Nonlinear_Example_ToyProblem_INDEX_JAC_F,
   DaeoDfba_Nonlinear_Example_ToyProblem_initialAnalyticJacobianA,
   DaeoDfba_Nonlinear_Example_ToyProblem_initialAnalyticJacobianB,
   DaeoDfba_Nonlinear_Example_ToyProblem_initialAnalyticJacobianC,
   DaeoDfba_Nonlinear_Example_ToyProblem_initialAnalyticJacobianD,
   DaeoDfba_Nonlinear_Example_ToyProblem_initialAnalyticJacobianF,
   DaeoDfba_Nonlinear_Example_ToyProblem_functionJacA_column,
   DaeoDfba_Nonlinear_Example_ToyProblem_functionJacB_column,
   DaeoDfba_Nonlinear_Example_ToyProblem_functionJacC_column,
   DaeoDfba_Nonlinear_Example_ToyProblem_functionJacD_column,
   DaeoDfba_Nonlinear_Example_ToyProblem_functionJacF_column,
   DaeoDfba_Nonlinear_Example_ToyProblem_linear_model_frame,
   DaeoDfba_Nonlinear_Example_ToyProblem_linear_model_datarecovery_frame,
   DaeoDfba_Nonlinear_Example_ToyProblem_mayer,
   DaeoDfba_Nonlinear_Example_ToyProblem_lagrange,
   DaeoDfba_Nonlinear_Example_ToyProblem_pickUpBoundsForInputsInOptimization,
   DaeoDfba_Nonlinear_Example_ToyProblem_setInputData,
   DaeoDfba_Nonlinear_Example_ToyProblem_getTimeGrid,
   DaeoDfba_Nonlinear_Example_ToyProblem_symbolicInlineSystem,
   DaeoDfba_Nonlinear_Example_ToyProblem_function_initSynchronous,
   DaeoDfba_Nonlinear_Example_ToyProblem_function_updateSynchronous,
   DaeoDfba_Nonlinear_Example_ToyProblem_function_equationsSynchronous,
   DaeoDfba_Nonlinear_Example_ToyProblem_inputNames,
   NULL,
   NULL,
   NULL,
   -1

};

#define _OMC_LIT_RESOURCE_0_name_data "Complex"
#define _OMC_LIT_RESOURCE_0_dir_data "/opt/openmodelica/lib/omlibrary"
static const MMC_DEFSTRINGLIT(_OMC_LIT_RESOURCE_0_name,7,_OMC_LIT_RESOURCE_0_name_data);
static const MMC_DEFSTRINGLIT(_OMC_LIT_RESOURCE_0_dir,31,_OMC_LIT_RESOURCE_0_dir_data);

#define _OMC_LIT_RESOURCE_1_name_data "DaeoDfba"
#define _OMC_LIT_RESOURCE_1_dir_data "/Users/ralf/Documents/repo/Daeo/src/Modelica"
static const MMC_DEFSTRINGLIT(_OMC_LIT_RESOURCE_1_name,8,_OMC_LIT_RESOURCE_1_name_data);
static const MMC_DEFSTRINGLIT(_OMC_LIT_RESOURCE_1_dir,44,_OMC_LIT_RESOURCE_1_dir_data);

#define _OMC_LIT_RESOURCE_2_name_data "Modelica"
#define _OMC_LIT_RESOURCE_2_dir_data "/opt/openmodelica/lib/omlibrary/Modelica 3.2.2"
static const MMC_DEFSTRINGLIT(_OMC_LIT_RESOURCE_2_name,8,_OMC_LIT_RESOURCE_2_name_data);
static const MMC_DEFSTRINGLIT(_OMC_LIT_RESOURCE_2_dir,46,_OMC_LIT_RESOURCE_2_dir_data);

#define _OMC_LIT_RESOURCE_3_name_data "ModelicaServices"
#define _OMC_LIT_RESOURCE_3_dir_data "/opt/openmodelica/lib/omlibrary/ModelicaServices 3.2.2"
static const MMC_DEFSTRINGLIT(_OMC_LIT_RESOURCE_3_name,16,_OMC_LIT_RESOURCE_3_name_data);
static const MMC_DEFSTRINGLIT(_OMC_LIT_RESOURCE_3_dir,54,_OMC_LIT_RESOURCE_3_dir_data);

static const MMC_DEFSTRUCTLIT(_OMC_LIT_RESOURCES,8,MMC_ARRAY_TAG) {MMC_REFSTRINGLIT(_OMC_LIT_RESOURCE_0_name), MMC_REFSTRINGLIT(_OMC_LIT_RESOURCE_0_dir), MMC_REFSTRINGLIT(_OMC_LIT_RESOURCE_1_name), MMC_REFSTRINGLIT(_OMC_LIT_RESOURCE_1_dir), MMC_REFSTRINGLIT(_OMC_LIT_RESOURCE_2_name), MMC_REFSTRINGLIT(_OMC_LIT_RESOURCE_2_dir), MMC_REFSTRINGLIT(_OMC_LIT_RESOURCE_3_name), MMC_REFSTRINGLIT(_OMC_LIT_RESOURCE_3_dir)}};
void DaeoDfba_Nonlinear_Example_ToyProblem_setupDataStruc(DATA *data, threadData_t *threadData)
{
  assertStreamPrint(threadData,0!=data, "Error while initialize Data");
  threadData->localRoots[LOCAL_ROOT_SIMULATION_DATA] = data;
  data->callback = &DaeoDfba_Nonlinear_Example_ToyProblem_callback;
  OpenModelica_updateUriMapping(threadData, MMC_REFSTRUCTLIT(_OMC_LIT_RESOURCES));
  data->modelData->modelName = "DaeoDfba.Nonlinear_Example.ToyProblem";
  data->modelData->modelFilePrefix = "DaeoDfba.Nonlinear_Example.ToyProblem";
  data->modelData->resultFileName = NULL;
  data->modelData->modelDir = "/Users/ralf/Documents/repo/Daeo/src/Modelica";
  data->modelData->modelGUID = "{7b29dc25-4c96-4d79-ae70-05b0bb3ab67d}";
  #if defined(OPENMODELICA_XML_FROM_FILE_AT_RUNTIME)
  data->modelData->initXMLData = NULL;
  data->modelData->modelDataXml.infoXMLData = NULL;
  #else
  #if defined(_MSC_VER) /* handle joke compilers */
  {
  /* for MSVC we encode a string like char x[] = {'a', 'b', 'c', '\0'} */
  /* because the string constant limit is 65535 bytes */
  static const char contents_init[] =
    #include "DaeoDfba.Nonlinear_Example.ToyProblem_init.c"
    ;
  static const char contents_info[] =
    #include "DaeoDfba.Nonlinear_Example.ToyProblem_info.c"
    ;
    data->modelData->initXMLData = contents_init;
    data->modelData->modelDataXml.infoXMLData = contents_info;
  }
  #else /* handle real compilers */
  data->modelData->initXMLData =
  #include "DaeoDfba.Nonlinear_Example.ToyProblem_init.c"
    ;
  data->modelData->modelDataXml.infoXMLData =
  #include "DaeoDfba.Nonlinear_Example.ToyProblem_info.c"
    ;
  #endif /* defined(_MSC_VER) */
  #endif /* defined(OPENMODELICA_XML_FROM_FILE_AT_RUNTIME) */
  
  data->modelData->nStates = 1;
  data->modelData->nVariablesReal = 8;
  data->modelData->nDiscreteReal = 0;
  data->modelData->nVariablesInteger = 3;
  data->modelData->nVariablesBoolean = 2;
  data->modelData->nVariablesString = 0;
  data->modelData->nParametersReal = 6;
  data->modelData->nParametersInteger = 0;
  data->modelData->nParametersBoolean = 0;
  data->modelData->nParametersString = 0;
  data->modelData->nInputVars = 0;
  data->modelData->nOutputVars = 0;
  
  data->modelData->nAliasReal = 2;
  data->modelData->nAliasInteger = 0;
  data->modelData->nAliasBoolean = 0;
  data->modelData->nAliasString = 0;
  
  data->modelData->nZeroCrossings = 1;
  data->modelData->nSamples = 0;
  data->modelData->nRelations = 1;
  data->modelData->nMathEvents = 0;
  data->modelData->nExtObjs = 1;
  
  data->modelData->modelDataXml.fileName = "DaeoDfba.Nonlinear_Example.ToyProblem_info.json";
  data->modelData->modelDataXml.modelInfoXmlLength = 0;
  data->modelData->modelDataXml.nFunctions = 4;
  data->modelData->modelDataXml.nProfileBlocks = 0;
  data->modelData->modelDataXml.nEquations = 20;
  data->modelData->nMixedSystems = 0;
  data->modelData->nLinearSystems = 0;
  data->modelData->nNonLinearSystems = 0;
  data->modelData->nStateSets = 0;
  data->modelData->nJacobians = 5;
  data->modelData->nOptimizeConstraints = 0;
  data->modelData->nOptimizeFinalConstraints = 0;
  
  data->modelData->nDelayExpressions = 0;
  
  data->modelData->nClocks = 0;
  data->modelData->nSubClocks = 0;
  
  data->modelData->nSensitivityVars = 0;
  data->modelData->nSensitivityParamVars = 0;
  data->modelData->nSetcVars = 0;
  data->modelData->ndataReconVars = 0;
}

static int rml_execution_failed()
{
  fflush(NULL);
  fprintf(stderr, "Execution failed!\n");
  fflush(NULL);
  return 1;
}

#if defined(threadData)
#undef threadData
#endif
/* call the simulation runtime main from our main! */
int main(int argc, char**argv)
{
  int res;
  DATA data;
  MODEL_DATA modelData;
  SIMULATION_INFO simInfo;
  data.modelData = &modelData;
  data.simulationInfo = &simInfo;
  measure_time_flag = 0;
  compiledInDAEMode = 0;
  compiledWithSymSolver = 0;
  MMC_INIT(0);
  omc_alloc_interface.init();
  {
    MMC_TRY_TOP()
  
    MMC_TRY_STACK()
  
    DaeoDfba_Nonlinear_Example_ToyProblem_setupDataStruc(&data, threadData);
    res = _main_SimulationRuntime(argc, argv, &data, threadData);
    
    MMC_ELSE()
    rml_execution_failed();
    fprintf(stderr, "Stack overflow detected and was not caught.\nSend us a bug report at https://trac.openmodelica.org/OpenModelica/newticket\n    Include the following trace:\n");
    printStacktraceMessages();
    fflush(NULL);
    return 1;
    MMC_CATCH_STACK()
    
    MMC_CATCH_TOP(return rml_execution_failed());
  }

  fflush(NULL);
  EXIT(res);
  return res;
}

#ifdef __cplusplus
}
#endif


