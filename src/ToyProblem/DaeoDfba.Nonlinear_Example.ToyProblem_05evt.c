/* Events: Sample, Zero Crossings, Relations, Discrete Changes */
#include "DaeoDfba.Nonlinear_Example.ToyProblem_model.h"
#if defined(__cplusplus)
extern "C" {
#endif

/* Initializes the raw time events of the simulation using the now
   calcualted parameters. */
void DaeoDfba_Nonlinear_Example_ToyProblem_function_initSample(DATA *data, threadData_t *threadData)
{
  long i=0;
}

const char *DaeoDfba_Nonlinear_Example_ToyProblem_zeroCrossingDescription(int i, int **out_EquationIndexes)
{
  static const char *res[] = {"minsigma < (-tolEvent)"};
  static const int occurEqs0[] = {1,13};
  static const int *occurEqs[] = {occurEqs0};
  *out_EquationIndexes = (int*) occurEqs[i];
  return res[i];
}

/* forwarded equations */
extern void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_9(DATA* data, threadData_t *threadData);
extern void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_10(DATA* data, threadData_t *threadData);
extern void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_11(DATA* data, threadData_t *threadData);
extern void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_14(DATA* data, threadData_t *threadData);

int DaeoDfba_Nonlinear_Example_ToyProblem_function_ZeroCrossingsEquations(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH

  data->simulationInfo->callStatistics.functionZeroCrossingsEquations++;

  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_9(data, threadData);

  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_10(data, threadData);

  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_11(data, threadData);

  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_14(data, threadData);
  
  TRACE_POP
  return 0;
}

int DaeoDfba_Nonlinear_Example_ToyProblem_function_ZeroCrossings(DATA *data, threadData_t *threadData, double *gout)
{
  TRACE_PUSH
  modelica_boolean tmp0;

#if !defined(OMC_MINIMAL_RUNTIME)
  if (measure_time_flag) rt_tick(SIM_TIMER_ZC);
#endif
  data->simulationInfo->callStatistics.functionZeroCrossings++;

  tmp0 = LessZC(data->localData[0]->realVars[5] /* minsigma variable */, (-data->simulationInfo->realParameter[5] /* tolEvent PARAM */), data->simulationInfo->storedRelations[0]);
  gout[0] = (tmp0) ? 1 : -1;

#if !defined(OMC_MINIMAL_RUNTIME)
  if (measure_time_flag) rt_accumulate(SIM_TIMER_ZC);
#endif

  TRACE_POP
  return 0;
}

const char *DaeoDfba_Nonlinear_Example_ToyProblem_relationDescription(int i)
{
  const char *res[] = {"minsigma < (-tolEvent)"};
  return res[i];
}

int DaeoDfba_Nonlinear_Example_ToyProblem_function_updateRelations(DATA *data, threadData_t *threadData, int evalforZeroCross)
{
  TRACE_PUSH
  modelica_boolean tmp1;
  
  if(evalforZeroCross) {
    tmp1 = LessZC(data->localData[0]->realVars[5] /* minsigma variable */, (-data->simulationInfo->realParameter[5] /* tolEvent PARAM */), data->simulationInfo->storedRelations[0]);
    data->simulationInfo->relations[0] = tmp1;
  } else {
    data->simulationInfo->relations[0] = (data->localData[0]->realVars[5] /* minsigma variable */ < (-data->simulationInfo->realParameter[5] /* tolEvent PARAM */));
  }
  
  TRACE_POP
  return 0;
}

#if defined(__cplusplus)
}
#endif

