/* update bound parameters and variable attributes (start, nominal, min, max) */
#include "DaeoDfba.Nonlinear_Example.ToyProblem_model.h"
#if defined(__cplusplus)
extern "C" {
#endif

OMC_DISABLE_OPT
int DaeoDfba_Nonlinear_Example_ToyProblem_updateBoundVariableAttributes(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  /* min ******************************************************** */
  
  infoStreamPrint(LOG_INIT, 1, "updating min-values");
  if (ACTIVE_STREAM(LOG_INIT)) messageClose(LOG_INIT);
  
  /* max ******************************************************** */
  
  infoStreamPrint(LOG_INIT, 1, "updating max-values");
  if (ACTIVE_STREAM(LOG_INIT)) messageClose(LOG_INIT);
  
  /* nominal **************************************************** */
  
  infoStreamPrint(LOG_INIT, 1, "updating nominal-values");
  if (ACTIVE_STREAM(LOG_INIT)) messageClose(LOG_INIT);
  
  /* start ****************************************************** */
  infoStreamPrint(LOG_INIT, 1, "updating primary start-values");
  if (ACTIVE_STREAM(LOG_INIT)) messageClose(LOG_INIT);
  
  TRACE_POP
  return 0;
}

void DaeoDfba_Nonlinear_Example_ToyProblem_updateBoundParameters_0(DATA *data, threadData_t *threadData);

/*
equation index: 17
type: SIMPLE_ASSIGN
daeo = DaeoDfba.Solver.NonlinearDaeoObject.constructor({"x", "p2", "p3"}, {"y", "z"}, "(-x + 2 * y + p3)^2", {"y - z"}, {"y", "p2-y"})
*/
OMC_DISABLE_OPT
static void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_17(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,17};
  data->simulationInfo->extObjs[0] = omc_DaeoDfba_Solver_NonlinearDaeoObject_constructor(threadData, _OMC_LIT10, _OMC_LIT11, _OMC_LIT7, _OMC_LIT12, _OMC_LIT13);
  TRACE_POP
}

/*
equation index: 18
type: SIMPLE_ASSIGN
daeoIn[2] = p2
*/
OMC_DISABLE_OPT
static void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_18(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,18};
  data->simulationInfo->realParameter[0] /* daeoIn[2] PARAM */ = data->simulationInfo->realParameter[3] /* p2 PARAM */;
  TRACE_POP
}

/*
equation index: 19
type: SIMPLE_ASSIGN
daeoIn[3] = p3
*/
OMC_DISABLE_OPT
static void DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_19(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  const int equationIndexes[2] = {1,19};
  data->simulationInfo->realParameter[1] /* daeoIn[3] PARAM */ = data->simulationInfo->realParameter[4] /* p3 PARAM */;
  TRACE_POP
}
OMC_DISABLE_OPT
void DaeoDfba_Nonlinear_Example_ToyProblem_updateBoundParameters_0(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_17(data, threadData);
  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_18(data, threadData);
  DaeoDfba_Nonlinear_Example_ToyProblem_eqFunction_19(data, threadData);
  TRACE_POP
}
OMC_DISABLE_OPT
int DaeoDfba_Nonlinear_Example_ToyProblem_updateBoundParameters(DATA *data, threadData_t *threadData)
{
  TRACE_PUSH
  DaeoDfba_Nonlinear_Example_ToyProblem_updateBoundParameters_0(data, threadData);
  TRACE_POP
  return 0;
}

#if defined(__cplusplus)
}
#endif

