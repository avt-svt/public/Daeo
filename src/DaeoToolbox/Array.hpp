/**
* @file Array.hpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* Utilities - general utility functions and classes                    \n
* =====================================================================\n
* The Array class is a class managing the memory of C-Arrays           \n
* =====================================================================\n
* @author Ralf Hannemann, Tjalf Hoffmann
* @date 22.6.2011
*/

#pragma once

#include <iostream>
#include <algorithm>
#include <cassert>

#undef max //want to use std::max function - somehow this leads to a conflict with a macro

namespace utils
{
  /**
  * @class Array
  * @brief class to manage memory for C-arrays
  *
  * Many interface functions (e.g. fortran functions) require C-arrays as parameters.
  * The STL container classes are not designed to give access to the array they contain.
  * So this class simply allocates memory on construction, provides an random access operator
  * and frees memory on destruction. During the scope of an array object the size of the field
  * cannot be changed. The pointer to the field also is constant. This was a memory leak is not possible
  * within this class.
  *
  * @warning If the array allocated by this class is deleted in external code, the system probably will crash,
  *          unless the pointer is set to NULL
  * @note The array class is not designed for using an Array as Array<Array<T>>. This will lead to compiler errors
  * @author Ralf Hannemann, Tjalf Hoffmann
  * @date 28.6.2011
  */
  template<typename T>
  class Array{
  public:
    /**
    * @brief construct an array with unititialized values
    *
    * since the type is unknown, no default value can be provided
    * @param size size of the array to be allocated
    */
    Array(const unsigned size)
    {
      initialize(size);
    }

    /**
    * @brief construct an array with an default value
    *
    * @param size size of the array to be allocated
    * @param value default value with which each element is constructed
    * @note the value type should be usable with the assign operator
    */
    Array(const unsigned size, const T &value)
    {
      initialize(size);
      for(unsigned i=0; i<m_size; i++){
        m_data[i] = value;
      }
    }

    /** 
     * @brief construct on array by copying from a const T*
     *
     * @param size of the array
     * @param pointer to C-array of type const T*
     */
    Array(const unsigned size, const T* v)
    {
      initialize(size);
      std::copy(v, v+size, m_data);
    }

    /**
    * @brief copy constructor
    *
    * The constructed object is a deep copy of the given object
    * @param rhs Array object to be copied
    */
    Array(const Array &rhs)
    {
      initialize(rhs.m_size);
      for (unsigned i=0; i< m_size; ++i){
        m_data[i] = rhs.m_data[i];
      }
    }

    /**
    * @brief destructor
    *
    * The array is deleted in the destructor.
    */
    ~Array()
    {
      if(m_data!=NULL){
        delete[] m_data;
      }
    }

    /**
    * @brief assign operator
    *
    * Like the copy constructor a deep copy is attempted. This function does not chenge the actual array,
    * only its values, so the size of both arrays must match
    *
    * @param rhs Array object to be assigned
    * @return reference to the this object
    */
    Array& operator=(const Array &rhs)
    {
      assert(m_size == rhs.m_size);
      for(unsigned i=0; i<m_size; i++){
        m_data[i] = rhs.m_data[i];
      }
      return *this;
    }

    /**
    * @return pointer to the data field
    */
    T* getData()
    {
      return m_data;
    }

    /**
    * If the Array object is constant and the m_data pointer is needed for reading access only,
    * this function grants reading access for it.
    * @return pointer to the data field as constant pointer
    */
    const T* getData() const
    {
      return m_data;
    }

    /**
    * @return the size of the data field
    */
    unsigned getSize() const
    {
      return m_size;
    }

    /**
    * @return reference to the object at the i-th position in the data field
    */
    T& operator[](const unsigned i)
    {
      assert(i<m_size);
      return m_data[i];
    }

    /**
    * @return copy of the object at the ith position in the data field
    */
    const T operator[] (const unsigned i) const
    {
      assert(i<m_size);
      return m_data[i];
    }

  protected:
    /**
    * @brief standard constructor that may be used by child classes
    */
    Array(){}
    //!data array
    T* m_data;
    //!size of the array field
    unsigned m_size;
  private:
    /**
    * @brief initialize the array
    *
    * In this function the memory of the field is allocated. This function is called exactly once
    * during the scope of the object.
    * @param size the size of the field to be allocated
    * @throw bad_alloc
    */
    void initialize(unsigned size)
    {
      const unsigned fieldSize = std::max(size, unsigned(1));
      m_data = new T[fieldSize];
      m_size = size;
    }
  };

  /**
  * @brief operator for any std output streams
  *
  * e.g. Array ar(3,5);
  * std::cout<<ar<<std::endl;
  * @note The object type of array must have also an overloaded << operator for ostreams
  * @author Tjalf Hoffmann, Ralf Hannemann
  * @date 27.6.2011
  */
  template <typename T>
  std::ostream& operator << (std::ostream &out, const Array<T> &arrayIn)
  {
    out<<"(";
    const int double_precision = 15;
    out.precision(double_precision);
    if(arrayIn.getSize() >0){
      out << std::scientific << arrayIn[0];
    }
    for(unsigned i=1; i<arrayIn.getSize(); i++){
      out << "," << std::scientific << arrayIn[i];
    }
    out<<")";
    return out;
  }

  /**
  * @class WrappingArray
  * @brief in contrast to the Array class WrappingArray uses an extern pointer
  *
  * In some cases functions are bound to have C-Arrays in the interface. A WrappingArray
  * can be used to provide some basic safety working with this C-arrays (such as boundary checks).
  * Since the WrapperArray class does not manage the allocation of the arrays, the destructor
  * does not delete the field either.
  *
  * @author Tjalf Hoffmann
  * @date 27.6.2011
  */
  template<typename T>
  class WrappingArray:public Array<T>
  {
  public:
    /**
    * @brief constructor using a preallocated array
    *
    * @param size size of the preallocated array
    * @param inputCArray pointer to the preallocated array
    */
    WrappingArray(unsigned size, T* inputCArray)
    {
      setData(size, inputCArray);
    }

    /**
    * @brief destructor
    *
    * simply sets m_data to NULL.
    * This is important, since after this destructor the Array destructor is called
    * and m_data would be deleted there otherwise.
    */
    ~WrappingArray()
    {
      Array<T>::m_data = NULL;
    }

    /**
    * @brief reset the pointer to the pointer of another C-array
    *
    * @param size size of the new C-array
    * @param inputCArray pointer to the new C-array
    */
    void setData(unsigned size, T* inputCArray)
    {
      assert(inputCArray != NULL || size == 0);
      Array<T>::m_size = size;
      Array<T>::m_data = inputCArray;
    }


  protected:
    /**
    * @brief standard constructor that may be used by child classes
    */
    WrappingArray(){};

  private:
  };

  /**
  * @class ExtendableArray
  * @brief dynamic array version of class Array
  *
  * @sa Array
  * @author Fady Assassa, Tjalf Hoffmann
  * @date 13.1.2012
  */
  template<typename T>
  class ExtendableArray:public Array<T>
  {
  public:

  /**
    * @brief construct an array with unititialized values
    *
    * since the type is unknown, no default value can be provided
    * @param size size of the array to be allocated
    */
    ExtendableArray(const unsigned size):Array<T>::Array(size)
    {}

    /**
    * @brief copy constructor uses simply copy constructor of class Array
    *
    * @param rhs Array to be copied
    */
    ExtendableArray(const Array<T> &rhs) : Array<T>::Array(rhs)
    {}

    /**
    * @brief standard constructor
    */
    ExtendableArray() : Array<T>::Array()
    {
      Array<T>::m_size = 0;
      Array<T>::m_data = NULL;
    }

    /**
    * @brief construct an array with an default value
    *
    * @param size size of the array to be allocated
    * @param value default value with which each element is constructed
    * @note the value type should be usable with the assign operator
    */
    ExtendableArray(const unsigned size, const T &value) : Array<T>::Array(size, value)
    {}

    /**
    * @brief append a new element to the ExtendableArray
    *
    * The memory of the ExtendableArray will be reallocated at each call of this function
    * @param newElement element to be appended
    */
    void append(const T &newElement)
    {
      unsigned newSize = Array<T>::m_size + 1;
      T* newPointer = new T[newSize];
      for(unsigned i=0; i< Array<T>::m_size; i++){
        newPointer[i] = Array<T>::m_data[i];
      }
      newPointer[Array<T>::m_size] = newElement;
      delete[] Array<T>::m_data;
      Array<T>::m_data = newPointer;
      Array<T>::m_size = newSize;
    }

    /**
    * @brief resize the memory of the ExtendableArray
    *
    * The memory of the Extendable Array will be reallocated at each call of this function,
    * even if the size does not actually change. The complete content of the Array is copied into
    * the new memory. Memory can only be extended, but never reduced.
    *
    * @param size new size of the ExtendableArray
    * @param defaultVal dummy element copied into the new allocated elements
    */
    void resize(const unsigned size, const T &defaultVal)
    {
      if(size <= Array<T>::m_size){
        Array<T>::m_size = size;
      }
      else{
        T* newPointer = new T[size];
        for(unsigned i=0; i< Array<T>::m_size; i++){
          newPointer[i] = Array<T>::m_data[i];
        }
        for(unsigned i=Array<T>::m_size; i<size; i++){
          newPointer[i] = defaultVal;
        }
        delete[] Array<T>::m_data;
        Array<T>::m_data = newPointer;
        Array<T>::m_size = size;
      }
    }
  };

}//namespace utils
