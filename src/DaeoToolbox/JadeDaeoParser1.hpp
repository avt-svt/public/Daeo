#ifndef JADE_DAEO_PARSER1_HPP
#define JADE_DAEO_PARSER1_HPP


#if defined(_MSC_VER)
# pragma warning(disable: 4345)
#endif

#include <boost/config/warning_disable.hpp>
#include <boost/spirit/include/qi.hpp>
#include <boost/variant/recursive_variant.hpp>
#include <boost/variant/apply_visitor.hpp>
#include <boost/fusion/include/adapt_struct.hpp>
#include <boost/foreach.hpp>
#include <boost/spirit/include/qi_lexeme.hpp>
#include <boost/spirit/include/phoenix_core.hpp>
#include <boost/spirit/include/phoenix_operator.hpp>
#include <boost/spirit/include/phoenix_fusion.hpp>
#include <boost/spirit/include/phoenix_stl.hpp>


namespace client {
	namespace ast
	{
		///////////////////////////////////////////////////////////////////////////
		//  The AST
		///////////////////////////////////////////////////////////////////////////
		struct nil {};
		struct signed_;
		struct program;
		struct variable;

		typedef boost::variant<
			nil
			, double
			, variable
			, boost::recursive_wrapper<signed_>
			, boost::recursive_wrapper<program>
		>
			operand;

		struct variable
		{
			variable(std::size_t index = 1234) : index(index) {}
			std::size_t index;
		};

		struct signed_
		{
			char sign;
			operand operand_;
		};

		struct operation
		{
			char operator_;
			operand operand_;
		};

		struct program
		{
			operand first;
			std::list<operation> rest;
		};
	}
}

BOOST_FUSION_ADAPT_STRUCT(
	client::ast::variable,
	(std::size_t, index)
	)

	BOOST_FUSION_ADAPT_STRUCT(
		client::ast::signed_,
		(char, sign)
		(client::ast::operand, operand_)
		)

	BOOST_FUSION_ADAPT_STRUCT(
		client::ast::operation,
		(char, operator_)
		(client::ast::operand, operand_)
		)

	BOOST_FUSION_ADAPT_STRUCT(
		client::ast::program,
		(client::ast::operand, first)
		(std::list<client::ast::operation>, rest)
		)

namespace client {
	namespace ast
	{
		///////////////////////////////////////////////////////////////////////////
		//  The AST evaluator
		///////////////////////////////////////////////////////////////////////////

		template<typename TREAL>
		struct eval
		{
			typedef TREAL result_type;

			TREAL operator()(nil) const { BOOST_ASSERT(0); return 0; }
			TREAL operator()(double value) const { return value; }


			TREAL operator ()(variable const& x) const
			{
				return dictionary.at(x.index);
			}

			TREAL operator()(operation const& x, TREAL lhs) const
			{
				TREAL rhs = boost::apply_visitor(*this, x.operand_);
				switch (x.operator_)
				{
				case '+': return lhs + rhs;
				case '-': return lhs - rhs;
				case '*': return lhs * rhs;
				case '/': return lhs / rhs;
				case '^': return pow(lhs, rhs);
				}
				BOOST_ASSERT(0);
				return 0;
			}

			TREAL operator()(signed_ const& x) const
			{
				TREAL rhs = boost::apply_visitor(*this, x.operand_);
				switch (x.sign)
				{
				case '-': return -rhs;
				case '+': return +rhs;
				}
				BOOST_ASSERT(0);
				return 0;
			}

			TREAL operator()(program const& x) const
			{
				TREAL state = boost::apply_visitor(*this, x.first);
				BOOST_FOREACH(operation const& oper, x.rest)
				{
					state = (*this)(oper, state);
				}
				return state;
			}

			void set_values(std::vector<TREAL> const& x, std::vector<TREAL> const& y) {
				dictionary.resize(x.size() + y.size());
				std::size_t index = 0;
				for (std::size_t i = 0; i < x.size(); ++i) {
					dictionary[index] = x[i];
					++index;
				}
				for (std::size_t i = 0; i < y.size(); ++i) {
					dictionary[index] = y[i];
					++index;
				}
			}


			void set_values(const double* x, const int p, const double *y, const int n) {
				dictionary.resize(p + n);
				for (int i = 0; i < p; ++i) {
					dictionary[i] = x[i];
				}
				for (int i = 0; i <n; ++i) {
					dictionary[p + i] = y[i];
				}
			}

		private:
			std::vector<TREAL> dictionary;
		};


	}
}

namespace client
{
	namespace qi = boost::spirit::qi;
	namespace ascii = boost::spirit::ascii;

	///////////////////////////////////////////////////////////////////////////////
	//  The calculator grammar
	///////////////////////////////////////////////////////////////////////////////
	struct dict_ : qi::symbols<char, std::size_t>
	{
		void initialize(const std::vector<std::string>& params,
			const std::vector<std::string>& variables) {
			std::size_t index = 0;
			for (size_t i = 0; i < params.size(); ++i) {
				add(params[i].c_str(), index);
				++index;
			}
			for (size_t i = 0; i < variables.size(); ++i) {
				add(variables[i].c_str(), index);
				++index;
			}
		}

	};


	template <typename Iterator>
	struct calculator : qi::grammar<Iterator, ast::program(), ascii::space_type>
	{
		calculator(dict_ dict) : calculator::base_type(expression), m_dict(dict)
		{

			namespace fusion = boost::fusion;
			namespace phoenix = boost::phoenix;
			namespace qi = boost::spirit::qi;
			namespace ascii = boost::spirit::ascii;

			qi::double_type double_;
			using boost::spirit::ascii::alnum;
			using boost::spirit::ascii::blank;
			using boost::spirit::ascii::digit;
			using boost::spirit::ascii::lower;
			using boost::spirit::ascii::alpha;
			using boost::spirit::qi::lexeme;

			using ascii::char_;
			using ascii::string;
			using namespace qi::labels;

			using phoenix::at_c;
			using phoenix::push_back;

			variable = m_dict;

			expression =
				term
				>> *((char_('+') >> term)
					| (char_('-') >> term)
					)
				;

			term =
				factor
				>> *((char_('*') >> factor)
					| (char_('/') >> factor)
					)
				;
			factor = primary >> *((char_('^') >> primary))
				;


			primary =
				variable
				| double_
				| '(' >> expression >> ')'
				| (char_('-') >> primary)
				| (char_('+') >> primary)
				;

		}

		qi::rule<Iterator, ast::program(), ascii::space_type> expression;
		qi::rule<Iterator, ast::program(), ascii::space_type> term;
		qi::rule<Iterator, ast::program(), ascii::space_type> factor;
		qi::rule<Iterator, ast::operand(), ascii::space_type> primary;
		qi::rule<Iterator, ast::variable(), ascii::space_type> variable;
		dict_ m_dict;
	};
}

template <typename TREAL>
using AstEval = client::ast::eval<TREAL>;

using AstCalculator = client::calculator<std::string::const_iterator>;
using AstProgram = client::ast::program;
using AstDictionary = client::dict_;


#endif //JADE_DAEO_PARSER1_HPP
