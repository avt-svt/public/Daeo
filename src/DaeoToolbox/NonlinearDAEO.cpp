#include "DaeoToolbox.hpp"
#include "NonlinearDAEO.hpp"
#include "initStringVector.hpp"
#include "copyArray.hpp"
#include "Conti/ContiSolver.hpp"
#include "Eigen/Dense"

#include <map>
#include <iostream>
#include <string>
#include <cassert>

void* parsenNonlinearDAEOmodel(const char** params,
  const char** variables,
  const char* objective,
  const char** equalities,
  const char** inequalities,
  const int p, /** number of parameters */
  const int m, /** number of equalities  */
  const int n, /** number of optimization variables */
  const int q, /** number of inequalities */
  int* info)
{
  NonlinearDAEO* daeo = new NonlinearDAEO(params,variables,
    objective, equalities, inequalities, p, m, n, q, info);
  return daeo;
}

void closeNonlinearDAEO(void* object) {
  delete static_cast<NonlinearDAEO*>(object);
}


void solveNonlinearActiveSet(void* object, double* x, double* values, double* sigma, int* status)
{
  NonlinearDAEO* daeo = static_cast<NonlinearDAEO*>(object);
  if(daeo->isInitialized()){
    daeo->nlp().setX1(x);
    Conti::Solver<double> solver;
    const auto& y = daeo->nlp().getNlsY();
    const auto n = y.size();
    Eigen::VectorXd u(n+1);
    for(int i=0; i < n; ++i) u[i] = y[i];
    u[n] = 0;
    solver.Solve(u,daeo->nlp());
    daeo->nlp().setNlsY(u.data());
    daeo->nlp().setX0(x);
    daeo->nlp().refineSolution(status);
  }
  else {
    daeo->nlp().setX0(x);
    daeo->optimizeWithIpopt(status);
    daeo->nlp().refineSolution(status);
    daeo->setInitialized(true);
  }
  daeo->nlp().getPrimalSolution(values);
  daeo->nlp().getSigma(sigma);
  status[0] = 0;
}


void updateNonlinearActiveSet(void* object, double* x, int* status)
{
  NonlinearDAEO* daeo = static_cast<NonlinearDAEO*>(object);
  daeo->nlp().setX0(x);
  daeo->optimizeWithIpopt(status);
  daeo->nlp().refineSolution(status);
  daeo->setInitialized(true);
  status[0] = 0;
}




void NonlinearDAEO::optimizeWithIpopt(int* status)
{
  Ipopt::SmartPtr<Ipopt::IpoptApplication> app = IpoptApplicationFactory();
  app->RethrowNonIpoptException(true);

  // Change some options
  // Note: The following choices are only examples, they might not be
  //       suitable for your optimization problem.
  app->Options()->SetNumericValue("tol", nonlinearDaeoNlp->getOptimizationTolerance());
  app->Options()->SetStringValue("mu_strategy", "adaptive");
  //app->Options()->SetStringValue("output_file", "ipopt.out");
  app->Options()->SetIntegerValue("print_level", 0);
  // The following overwrites the default name (ipopt.opt) of the
  // options file
  // app->Options()->SetStringValue("option_file_name", "hs071.opt");

  // Initialize the IpoptApplication and process the options
  Ipopt::ApplicationReturnStatus ipoptStatus;
  ipoptStatus = app->Initialize();
  if (ipoptStatus != Ipopt::Solve_Succeeded) {
    status[1] = 1;
    return;
  }

  // Ask Ipopt to solve the problem
  ipoptStatus = app->OptimizeTNLP(nonlinearDaeoNlp);

  if (ipoptStatus == Ipopt::Solve_Succeeded) {
    status[1] = 0;
  }
  else {
    status[1] = 1;
    return;
  }

  nonlinearDaeoNlp->guessActiveSet();

}

void NonlinearDAEO::refineSolution(int *status)
{

  nonlinearDaeoNlp->refineSolution(status);

}

