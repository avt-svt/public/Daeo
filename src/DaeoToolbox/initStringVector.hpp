#ifndef INIT_STRING_VECTOR_HPP
#define INIT_STRING_VECTOR_HPP

#include <vector>
#include <string>

inline void initStringVector(std::vector<std::string>& stringVector, const char** src, int size) {
  stringVector.reserve(size);
  for (int i = 0; i < size; ++i)
    stringVector.push_back(src[i]);
}

#endif