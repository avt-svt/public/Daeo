#ifndef NONLINEAR_DAEO_HPP
#define NONLINEAR_DAEO_HPP

#include "NonlinearDaeoNlp.hpp"
#include "IpIpoptApplication.hpp"
#include <vector>

class NonlinearDAEO {
public:
	NonlinearDAEO(const char** params,
		const char** variables, const char* objective,
		const char** equalities, const char** inequalities,
		const int p, const int m, const int n, const int q, int* info) :
		nonlinearDaeoNlp(new NonlinearDaeoNlp(params, variables, objective, equalities,
			inequalities, p, m, n, q, info)),
		m_p(p),
		m_m(m),
		m_n(n),
		m_q(q) {}

	void optimizeWithIpopt(int* status);
	void refineSolution(int* status);
	bool isInitialized() { return _isInitialized; }
	void setInitialized(const bool value) { _isInitialized = value; }
	void setX0(double *values) { nonlinearDaeoNlp->setX0(values); }
	void setX1(double *values) { nonlinearDaeoNlp->setX1(values); }
	NonlinearDaeoNlp& nlp() { return *nonlinearDaeoNlp; }
private:
	Ipopt::SmartPtr<NonlinearDaeoNlp> nonlinearDaeoNlp; //!< Ipopt problem for EO
	int m_p; //!< number of EO parameters
	int m_m; //!< number of EO equalitiy constraints
	int m_n; //!< number of EO variables
	int m_q; //!< number of EO inequality constraints
	bool _isInitialized = false; //!< indicates whether optimizer has already been intially called

};




#endif // NONLINEAR_DAEO_HPP
