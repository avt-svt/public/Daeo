#include "DaeoToolbox.hpp"
#include "DaeoToolboxAdditional.hpp"
#include "antlr4-runtime-wrapper.h"
#include "JadeDaeoParser2.hpp"
#include "umfpack.h"

// if defined, output to console is enabled
//#define doPrinting
#define BASIS_OPTIMAL 1
#define BASIS_SUBOPTIMAL 0

#define NOTDEFINED 0
#define DIRECT 1
#define ACTIVE_SET 2
#define ACTIVE_SET_CPP 3

struct sparseComprColumnMatrix {
	std::vector<int> Ap;
	std::vector<int> Ai;
	std::vector<double> Ax;
	int nrow;
	int ncol;
	int nz;
};

ILOSTLBEGIN

// external object class that is used to solve DFBA problems
class dfba_cplex
{
	IloEnv env; /* CPLEX environment */
	IloModel model; /* CPLEX optimization model */
	IloCplex cplex; /* CPLEX algorithm to solve model */

	IloObjective   obj; /* objective function of CPLEX optimization model */
	IloNumVarArray var; /* variables of CPLEX optimization model */
	IloRangeArray  rng; /* constraints of CPLEX optimization model (lb <= expr <= ub) */

public:
	IloNumArray vals; /* values of variables at current solution */

	IloNumArray duals; /* values of duals (w.r.t. equations, both equalities and inequalities) at current solution */
	IloNumArray slacks; /* values of slackss (w.r.t. equations, both equalities and inequalities) at current solution */
	IloCplex::BasisStatusArray eqnstat;  /* status of equation */

private:
	int basisStatus; /* optimality of the basic solution (BASIS_OPTIMAL / BASIS_SUBOPTIMAL) */
	int solutionMethod; /* solution method for solving embedded optimization problem (DIRECT or ACTIVE_SET), initialized with NOTDEFINED */
	int bQP; /* Is the embedded optimization problem a quadratic program? */
	int sepCalc; /* Is separate calculation of solution and Lagrangian multiplier possible? If so, two separate linear equation systems are solved, otherwise one large linear equation system is solved. */
	int bTime=0; /* Is the simulation time passed to the toolbox? */
	int calcDerivatives = 0; /* Are derivatives calculated? */

	int n_n; /* number of variables with constant bounds */
	int n_p; /* number of variables with non-constant bounds (e.g. uptake rates) */
	int n_m; /* number of equality constraints (rows of stoichiometric matrix) */
	int n_q; /* number of inequality constraints */
	int n_var; /* total number of variables of CPLEX optimization problem (sum of n_n and n_p) */

	std::vector<int> ind_b; /* indices of variables with non-constant bounds (e.g. uptake rates) */
	std::vector<int> ind_x; /* indices of variables with constant bounds (maps entries of input variables to CPLEX variables) */
	std::vector<int> ind_x_conv; /* indices of variables with constant bounds (maps CPLEX variables to input variables) */

	std::vector<int> basic_eqns; /* is equation basic? */
	int n_basic = 0; //number of basic constraints
	int n_basic_ineq = 0; //number of basic inequality constraints

	std::vector<int> superbasic_var;
	int n_superbasic_var = 0;

	std::vector<int> linear_dependent_equalities;
	//int n_linear_dependent_equalities = 0;

	std::vector<double> Qdiag;
	std::vector<double> cost;

	std::vector<double> abdev;
	std::vector<double> abvec;
	std::vector<int> bind;
	std::vector<int> bind_t; // time-dependent elements in B_UL, mapping to b(t)

	std::vector<double> newDuals;
	std::vector<double> newSlacks;

	sparseTripletMatrix D;
	//sparseComprColumnMatrix cc_D;
	sparseTripletMatrix I_B;
	sparseTripletMatrix AD;
	sparseTripletMatrix M_UL;
	sparseTripletMatrix M_LR;
	sparseTripletMatrix M_total; 

	//sparseTripletMatrix M_UL_inv;
	//std::vector<double> dxdb;
	//int b_dxdb; //inverse already calculated?

	//std::vector<double> dvardb; // dvar/db, where var are the variables (ie fluxes and parameters) and b the time-dependent parameters
	//int b_dvardb; //inverse already calculated?

	//std::vector<double> dsigmadx;
	//int b_dsigmadx; //inverse already calculated?

	sparseComprColumnMatrix cc_M_UL;
	std::vector<double> B_UL;
	std::vector<double> x_UL; // v*

	void* n_M_UL = nullptr; //Numerical object of M_UL used by UMFPACK
	void* n_M_LR = nullptr; //Numerical object of M_LR used by UMFPACK
	void* n_M_total = nullptr; //Numerical object of M_total used by UMFPACK
	//void* n_D = nullptr; //Numerical object of D used by UMFPACK

	sparseComprColumnMatrix cc_M_LR;
	std::vector<double> B_LR;
	std::vector<double> x_LR; // [lambda*; s*]

	sparseComprColumnMatrix cc_M_total;
	std::vector<double> B_total;
	std::vector<double> x_total;

	std::vector<double> timePointsF;
	std::vector<double> timePointsSigma;

	ofstream myfile;
	ofstream mylogfile;
	ofstream mytimepointfile_CPP;

	int nfunctioncalls = 0;

	int ncalls_updateAS = 0;
	int ncalls_solveSolution = 0;
	int ncalls_solveSigma = 0;
	int ncalls_derivativeSolution = 0;
	int ncalls_derivativeSigma = 0;
	int ncalls_adjointSolution = 0;
	int ncalls_adjointSigma = 0;

	std::queue<string> qerrors;
	std::queue<string> qevents;
	std::stringstream ss_temp;
public:
	dfba_cplex(const char ** params,
		//const double *x,
		const char ** variables,
		const char * objective,
		const char ** equalities,
		const char ** inequalities,
		const int p,
		const int m,
		const int n,
		const int q,
		std::vector<double> Qtemp,
		int *info) :
		basisStatus(BASIS_SUBOPTIMAL),
		solutionMethod(NOTDEFINED),
		n_n(n),
		n_p(p),
		n_m(m + p),
		n_q(q),
		n_var(n + p),
		basic_eqns(m + p + q),
		D("D"),
		I_B("I_B"),
		AD("AD"),
		M_UL("M_UL"),
		M_LR("M_LR"),
		M_total("M_total"),
		//b_dxdb(0),
		//b_dsigmadx(0),
		//b_dsigmadb(0),
		sepCalc(0)
	{
		// CPLEX
		model = IloModel(env);
		cplex = IloCplex(env);
		cplex.setOut(env.getNullStream()); /* turn off CPLEX messages to screen */
		cplex.setWarning(env.getNullStream());
		cplex.setParam(IloCplex::Param::Simplex::Tolerances::Optimality, 1e-9);
		cplex.setParam(IloCplex::Param::Simplex::Tolerances::Feasibility, 1e-9);
		cplex.setParam(IloCplex::Param::Feasopt::Tolerance, 1e-9);
		cplex.setParam(IloCplex::Param::Barrier::ConvergeTol, 1e-9);
		cplex.setParam(IloCplex::Param::SolutionType, 1);
		cplex.setParam(IloCplex::Param::RootAlgorithm, 1);
		//cplex.setParam(IloCplex::Param::Preprocessing::Dependency,0);
		//auto test = cplex.getParam(IloCplex::Param::Preprocessing::Fill);
		//auto test2 = cplex.getParam(IloCplex::Param::SolutionType);
		//cplex.setParam(IloCplex::Param::Preprocessing::Aggregator,0);
		//auto test = cplex.getParam(IloCplex::Param::Preprocessing::Dependency);
		//cplex.setParam(IloCplex::ScaInd, -1);
		//cplex.setParam(IloCplex::PreInd, 0);
		var = IloNumVarArray(env);
		rng = IloRangeArray(env);

		vals = IloNumArray(env);
		duals = IloNumArray(env);
		slacks = IloNumArray(env);
		eqnstat = IloCplex::BasisStatusArray(env);

		Qdiag = Qtemp;
		bQP = Qdiag.size() > 0;
		// parse model
		parseModel(params,
			//x,
			variables,
			objective,
			equalities,
			inequalities,
			p,
			m,
			n,
			q,
			info);

		// find indices of all variables
		find_indices(variables, n, params, p);
		bind_t.resize(n_p);

		// find cost vector
		cost.resize(n_var);
		setCostVector();


		// initializes sparse matrix sparseA with the respective coefficients
		abvec.resize(n_m + n_q);
		abdev.resize(n_m + n_q);
		bind.resize(n_var);

		setMatrixD();
		//umfpack_triplet_to_col(D, cc_D);
		//umfpack_prepare(cc_D, &n_D);

		setMatrixAD();

		//dxdb.resize((n_n)*n_p);
		//dvardb.resize((n_n+n_p)*n_p);
		//dsigmadx.resize(n_q*n_n);
		//dsdb.resize(n_q*n_p);
		//dsigmadb.resize(n_q*n_p);

		newDuals.resize(n_q);
		newSlacks.resize(n_q);

		//updateBasicSet(x);

		//myfile.open("Log_Derivatives.txt");
		//myfile << "Object is being created, dimension = " << n << std::endl;

		//mytimepointfile_CPP.open("Log_TimePointsF_CPP.txt");
	}


	~dfba_cplex()
	{
		if (timePointsF.size() > 0) {
			ofstream mytimepointfile;
			mytimepointfile.open("Log_TimePointsF.txt");
			for (size_t i = 0; i < timePointsF.size(); i++) {
				mytimepointfile << timePointsF[i] << std::endl;
			}
			mytimepointfile.close();
		}
		if (timePointsSigma.size() > 0) {
			ofstream mytimepointfile;
			mytimepointfile.open("Log_TimePointsSigma.txt");
			for (size_t i = 0; i < timePointsSigma.size(); i++) {
				mytimepointfile << timePointsSigma[i] << std::endl;
			}
			mytimepointfile.close();
		}
		if (mytimepointfile_CPP.is_open()) {
			mytimepointfile_CPP.close();
		}

		//std::cout << "Function setKKTmatrices() has been called " << nfunctioncalls << " times." << std::endl;
		umfpack_di_free_numeric(&n_M_UL);
		umfpack_di_free_numeric(&n_M_LR);

		mylogfile.open("Log_DaeoToolbox.txt");
		if (mylogfile.is_open()) {
			printStatistic();
			mylogfile.close();
		}
		//export_model("LPtopl_end.lp");
		//umfpack_di_free_numeric(&n_D);
		env.end();


		//std::cout << "Function setKKTmatrices() has been called " << nfunctioncalls << " times." << std::endl;
	}

	/* Function to prints general statistics about solution method to log file. */
	void printStatistic() {
		if (solutionMethod == DIRECT)
			mylogfile << "Solution method : Direct Approach";// << endl;
		else if (solutionMethod == ACTIVE_SET)
			mylogfile << "Solution method : Active Set Approach";// << endl;
		else if (solutionMethod == ACTIVE_SET_CPP)
			mylogfile << "Solution method : Active Set Approach in CPP";// << endl;
		else
			mylogfile << "Solution method : Unknown";// << endl;

		if (bQP)
			mylogfile << ", QP " << endl;
		else
			mylogfile << ", LP " << endl;
		mylogfile << endl;
		mylogfile << "=== Model size ===" << endl;
		mylogfile << "Number of (unknown) variables (n) : " << n_n << endl;
		mylogfile << "Number of time-dependent parameters (p) : " << n_p << endl;
		mylogfile << "Total number of variables (unknowns + time-dependent parameters) : " << n_var << endl;
		mylogfile << "Number of equality constraints (m+p) : " << n_m << endl;
		mylogfile << "Number of inequality constraints (q) : " << n_q << endl;
		mylogfile << "There are " << n_superbasic_var << " superbasic variables";
		if (n_superbasic_var > 0) {
			mylogfile << " (their values are set to 0): ";
		}
		else {
			mylogfile << ".";
		}
		for (int i = 0; i < superbasic_var.size(); i++) {
			mylogfile << var[superbasic_var[i]];
		}
		mylogfile << endl;

		mylogfile << endl;
		mylogfile << "=== Solver statistics ===" << endl;
		mylogfile << "Number of calls to solve x=f(b(t)) : " << ncalls_solveSolution << endl;
		mylogfile << "Number of calls to solve sigma=f(b(t)) : " << ncalls_solveSigma << endl;
		if (solutionMethod == ACTIVE_SET || solutionMethod == ACTIVE_SET_CPP) {
			mylogfile << "Number of calls to solve for dx/db : " << ncalls_derivativeSolution << endl;
			mylogfile << "Number of calls to solve for dsigma/db : " << ncalls_derivativeSigma << endl;
			mylogfile << "Number of calls to solve for bx in adjoint mode : " << ncalls_adjointSolution << endl;
			mylogfile << "Number of calls to solve for bsigma in adjoint mode : " << ncalls_adjointSigma << endl;
			mylogfile << "Number of updates to Active Set : " << ncalls_updateAS << endl;
		}
		mylogfile << "Calculation of analytic derivatives : " ;
		if (calcDerivatives)
			mylogfile << "yes";
		else 
			mylogfile << "no";

		mylogfile << endl;

		mylogfile << endl;
		mylogfile << "=== ";
		if (bQP) mylogfile << "QP";
		else mylogfile << "LP";
		mylogfile << " solver settings === " << endl;
		mylogfile << "Solution method : ";
		switch (cplex.getParam(IloCplex::RootAlg)) {
		case CPX_ALG_AUTOMATIC:
			mylogfile << "Automatic" << endl;
			break;
		case CPX_ALG_PRIMAL:
			mylogfile << "Primal simplex" << endl;
			break;
		case CPX_ALG_DUAL:
			mylogfile << "Dual simplex" << endl;
			break;
		case CPX_ALG_NET:
			mylogfile << "Network simplex" << endl;
			break;
		case CPX_ALG_BARRIER:
			mylogfile << "Barrier" << endl;
			break;
		case CPX_ALG_SIFTING:
			mylogfile << "Sifting" << endl;
			break;
		case CPX_ALG_CONCURRENT:
			mylogfile << "Concurrent" << endl;
			break;
		}
		mylogfile << "Optimality tolerance : " << cplex.getParam(IloCplex::Param::Simplex::Tolerances::Optimality) << endl;
		mylogfile << "Feasibility tolerance : " << cplex.getParam(IloCplex::Param::Simplex::Tolerances::Feasibility) << endl;
		mylogfile << endl;
		if (!qevents.empty()) { mylogfile << "=== Events during simulation ===" << endl; }
		while (!qevents.empty()) {
			mylogfile << qevents.front() << endl;
			qevents.pop();
		}
		mylogfile << endl;
		if (!qerrors.empty()) { mylogfile << "=== Errors during simulation ===" << endl; }
		while (!qerrors.empty()) {
			mylogfile << qerrors.front() << endl;
			qerrors.pop();
		}
	}

	int get_n_n() { return n_n; }; /* number of variables with constant bounds */
	int get_n_p() {	return n_p; }; /* number of variables with non-constant bounds (e.g. uptake rates) */
	int get_n_m() { return n_m; }; /* number of equality constraints (rows of stoichiometric matrix) */
	int get_n_q() { return n_q; }; /* number of inequality constraints */
	int get_n_var() { return n_var; }; /* total number of variables of CPLEX optimization problem (sum of n_n and n_p) */


	/* updates rhs and solves problem using CPLEX */
	int solveCplex(double* b, double* x)
	{
		update_b(b);
		cplex.solve();
		
		if (cplex.getStatus() == IloAlgorithm::Optimal) {
#ifdef doPrinting
			detailed_solution();
#endif
			getValuesCplex(x);
			return SOLUTION_DIRECT_SUCCESSFUL;
		}
		else
		{
#ifdef doPrinting
			detailed_solution();
#endif
			ss_temp.str(std::string()); //clear stringstream
			ss_temp << "Cplex was unable to solve the problem. daeoIn = [";
			for (int i = 0; i < n_p - 1; i++) {
				ss_temp << b[i] << ", ";
			}
			ss_temp << b[n_p - 1] << "]. ";
			if (bTime)
				ss_temp << "t = " << timePointsF.back() << ". ";
			ss_temp << "Error code: " << cplex.getStatus();
			//ss_temp << ". bTime = " << bTime;
			qerrors.push(ss_temp.str());
			//cplex.exportModel("Fail.lp");
			return SOLUTION_ERROR_CPLEX;
			
		}
	}

	/* updates rhs and solves KKT conditions */
	int solveKKT(double* b, double* x) {
		int status = -17;

		status = solveKKTx(b, x);

		return status;
	}

	/* updates rhs and solves KKT conditions */
	int solveKKT(double* b, double* x, double* newSigma) {
		int status = -17;

		if (sepCalc) {
			status = solveKKTx(b, x);
			status = solveKKTsigma(b, x, newSigma);
		}
		else {
			status = solveKKTtotal(b, x, newSigma);
		}
		return status;
	}

	/* calculates derivatives */
	int solveKKT_derivatives(double* db, double* dx, double* dsigma) {
		int status = -17;
		if (!calcDerivatives) {
			calcDerivatives = 1;
		}

		if (sepCalc) {
			std::vector<double> dx_UL;
			dx_UL.resize(n_var);
			status = solveKKTx_derivative(db, dx_UL);
			getValues(dx_UL.data(), dx);
			status = solveKKTsigma_derivative(db, dx_UL, dsigma);
		}
		else {
			status = solveKKTtotal_derivative(db, dx, dsigma);//solveKKTtotal(b, x, newSigma);
		}
		return status;
	}

	/* updates rhs and solves KKT conditions for x */
	int solveKKTx(double* b, double* x)
	{
		int status = -17;

		update_bvec(b);

		try {
			//setRhsB_UL();
			//update_B_UL(b);
			update_B(b, B_UL);

			//double Control[UMFPACK_CONTROL];
			//umfpack_di_defaults(Control);
			//Control[UMFPACK_PRL] = 1;
			//umfpack_di_report_numeric(n_M_UL, Control);

			status=umfpack_solve(cc_M_UL, B_UL, x_UL, n_M_UL);// , &n_M_UL);
			getValues(x_UL.data(), x);
		}
		catch (...)
		{
			return SOLUTION_ERROR_KKT_X;
		}
		if (status == SOLUTION_LIN_EQN_SYS_SUCCESSFUL) {
			return SOLUTION_KKT_SUCCESSFUL;
		}
		else {
			return SOLUTION_ERROR_KKT_X;
		}
	}

	/* updates rhs and solves KKT conditions for x */
	int solveKKTx_derivative(double* db, std::vector<double>& dx_UL)
	{
		ncalls_derivativeSolution++;
		int status = -17;
		try {
			std::vector<double> dB_UL;
			dB_UL.resize(n_var);
			update_B(db, dB_UL);

			//std::vector<double> dx_UL;
			//dx_UL.resize(n_var);

			status=umfpack_solve(cc_M_UL, dB_UL, dx_UL, n_M_UL);// , &n_M_UL);
			//getValues(dx_UL.data(), dx);
		}
		catch (...)
		{
			return TANGENT_ERROR_X;
		}
		if (status == SOLUTION_LIN_EQN_SYS_SUCCESSFUL) {
			return TANGENT_SUCCESSFUL;
		}
		else {
			return TANGENT_ERROR_X;
		}
	}

	/* updates rhs and solves KKT conditions for sigma */
	// LP case: KKT system that is solved for Lagrangian does not depend on time
	// QP case: KKT system does depend on time
	int solveKKTsigma(double* b, double* x, double* newSigma)
	{
		int status=-17;
		try {
			if (bQP) {//QP case: resolve for x_LR
				setRhsB_LR(x_UL); // update rhs
				// resolve for x_LR (does depend on time, therefore in each step)
				status=umfpack_solve(cc_M_LR, B_LR, x_LR, n_M_LR);
			}

			getSwitchVars(newSigma);
		}
		catch (...)
		{
			status= SOLUTION_ERROR_KKT_SIGMA;
		}
		if (status == SOLUTION_LIN_EQN_SYS_SUCCESSFUL) {
			status= SOLUTION_KKT_SUCCESSFUL;
		}
		return status;
	}

	int solveKKTsigma_derivative(double* db, std::vector<double>& dx_UL, double* dsigma)
	{
		ncalls_derivativeSigma++;
		int status=-17;
		try {
			//setRhsB_LR(dx_UL); // update rhs
							   // resolve for x_LR (does depend on time, therefore in each step)
			std::vector<double> dx_LR;
			dx_LR.resize(n_q + n_m);

			std::vector<double> dB_LR;
			dB_LR.resize(n_q + n_m);
			for (int i = 0; i < Qdiag.size(); ++i) {
				dB_LR[i] = Qdiag[i] * dx_UL[i];
			}

			status=umfpack_solve(cc_M_LR, dB_LR, dx_LR, n_M_LR);

			getSwitchVars_derivatives(dsigma, dx_UL, dx_LR);
		}
		catch (...)
		{
			status = TANGENT_ERROR_SIGMA;
		}
		if (status == SOLUTION_LIN_EQN_SYS_SUCCESSFUL) {
			status = TANGENT_SUCCESSFUL;
		}
		return status;
	}



	/* updates rhs and solves KKT conditions for x and sigma */
	// LP case: KKT system that is solved for Lagrangian does not depend on time
	// QP case: KKT system does depend on time
	int solveKKTtotal(double* b, double* x, double* newSigma)
	{
		int status=-17;

		update_bvec(b);

		try {
			update_B(b, B_total);

			status=umfpack_solve(cc_M_total, B_total, x_total, n_M_total);
			x_UL.resize(n_var);
			x_UL = slice(x_total, 0, n_var-1);
			x_LR.resize(n_q + n_m);
			x_LR = slice(x_total, n_var, n_var+n_q+n_m-1);

			getValues(x_UL.data(), x);
			getSwitchVars(newSigma);
		}
		catch (...)
		{
			status = SOLUTION_ERROR_KKT_ALL;
		}
		if (status == SOLUTION_LIN_EQN_SYS_SUCCESSFUL) {
			status = SOLUTION_KKT_SUCCESSFUL;
		}
		return status;
	}

	/* updates rhs and solves KKT conditions for x and sigma */
	// LP case: KKT system that is solved for Lagrangian does not depend on time
	// QP case: KKT system does depend on time
	int solveKKTtotal_derivative(double* db, double* dx, double* dsigma)
	{
		ncalls_derivativeSolution++;
		ncalls_derivativeSigma++;
		int status=-17;

		//update_bvec(b);

		try {
			//setRhsB_UL();
			//update_B_total(b);
			std::vector<double> dB_total;
			dB_total.resize(n_var + n_m + n_q);

			update_B(db, dB_total);

			std::vector<double> dx_total;
			dx_total.resize(n_var + n_m + n_q);

			status=umfpack_solve(cc_M_total, dB_total, dx_total, n_M_total);
			CheckFlag(&status, "umfpack_solve");

			std::vector<double> dx_UL;
			dx_UL.resize(n_var);
			dx_UL = slice(dx_total, 0, n_var - 1);
			std::vector<double> dx_LR;
			dx_LR.resize(n_q + n_m);
			dx_LR = slice(dx_total, n_var, n_var + n_q + n_m - 1);

			getValues(dx_UL.data(), dx);
			//needs to be replaced!!
			getSwitchVars_derivatives(dsigma, dx_UL,dx_LR);
		}
		catch (...)
		{
			status = TANGENT_ERROR_ALL;
		}
		if (status == SOLUTION_LIN_EQN_SYS_SUCCESSFUL) {
			status = TANGENT_SUCCESSFUL;
		}
		return status;
	}

	// input bx has dimension n_n
	// output bb has dimension n_p
	int calc_adjoint_x(double* bx, double*  bb) {
		// here we need to solve bb = transpose(A^(-1))*bx
		if (!calcDerivatives) {
			calcDerivatives = 1;
		}
		ncalls_adjointSolution++;

		if (sepCalc) {
			return calc_adjoint_x_UL(bx, bb);
		}
		else {
			return calc_adjoint_x_total(bx, bb);
		}
	}

	// for separate adjoint calculation
	int calc_adjoint_x_UL(double* bx, double*  bb) {
		int status=-17;
		try {
			std::vector<double> bx_UL;
			bx_UL.resize(n_var);

			for (int i = 0; i < n_n; i++) {
				bx_UL[i + n_p] = bx[i];
			}

			//update_B(db, dB_total);

			std::vector<double> bb_UL;
			bb_UL.resize(n_var + n_m + n_q);

			status=umfpack_solve_transpose(cc_M_UL, bx_UL, bb_UL, n_M_UL);
			CheckFlag(&status, "umfpack_solve_transpose");

			for (int i = 0; i < n_p; i++) {
				bb[i] = bb_UL[i];
			}
		}
		catch (...)
		{
			status = ADJOINT_ERROR_X;
		}
		if (status == SOLUTION_LIN_EQN_SYS_SUCCESSFUL) {
			status = ADJOINT_SUCCESSFUL;
		}
		return status;
	}

	int calc_adjoint_x_total(double* bx, double*  bb) {
		int status;
		try {
			std::vector<double> bx_total;
			bx_total.resize(n_var + n_m + n_q);

			for (int i = 0; i < n_n; i++) {
				bx_total[i + n_p] = bx[i];
			}

			//update_B(db, dB_total);

			std::vector<double> bb_total;
			bb_total.resize(n_var + n_m + n_q);

			status=umfpack_solve_transpose(cc_M_total, bx_total, bb_total, n_M_total);
			CheckFlag(&status, "umfpack_solve_transpose");

			for (int i = 0; i < n_p; i++) {
				bb[i] = bb_total[i];
			}
		}
		catch(...)
		{
			status = ADJOINT_ERROR_ALL;
		}
		if (status == SOLUTION_LIN_EQN_SYS_SUCCESSFUL) {
			status = ADJOINT_SUCCESSFUL;
		}
		return status;
	}


	// input bsigma has dimension n_q
	// output bb has dimension n_p
	int calc_adjoint_sigma(double* bsigma, double*  bb) {
		if (!calcDerivatives) {
			calcDerivatives = 1;
		}
		ncalls_adjointSigma++;

		int status=-17;
		try {
			std::vector<double> bsigma_inactive(n_q, 0.0);
			std::vector<double> bsigma_active(n_q, 0.0);
			for (int i = 0; i < n_q; i++) {
				if (eqnstat[i + n_m]) { // inactive constraint
					bsigma_inactive[i] = bsigma[i];
				}
				else { // active constraint
					bsigma_active[i] = bsigma[i];
				}
			}

			std::vector<double> by(n_var, 0.0);
			matrixMultiplication_transpose(D, bsigma_inactive, by);
			if (sepCalc) {
				std::vector<double> bb_UL(n_var, 0.0);
				status=umfpack_solve_transpose(cc_M_UL, by, bb_UL, n_M_UL);
				CheckFlag(&status, "umfpack_solve_transpose");

				for (int i = 0; i < n_p; i++) {
					bb[i] = bb_UL[i];
				}
				//std::vector<double> by_LR(n_m + n_q, 0.0);
				//for (int i = 0; i < n_q; i++) {
				//	by_LR[n_m + i] = bsigma_active[i]; // bs[i]=bsigma_active[i] skipped for brevity
				//}
				//std::vector<double> bb_LR(n_m+n_q, 0.0);
				//umfpack_solve_transpose(cc_M_LR, by_LR, bb_LR, n_M_LR);
			}
			else {
				std::vector<double> bz(n_var + n_m + n_q, 0.0);
				for (int i = 0; i < n_var; i++) {
					bz[i] = by[i];
				}
				for (int i = 0; i < n_q; i++) {
					bz[n_var + n_m + i] = bsigma_active[i]; // bs[i]=bsigma_active[i] skipped for brevity
				}
				std::vector<double> bb_sigma_total(n_var + n_m + n_q, 0.0);
				status=umfpack_solve_transpose(cc_M_total, bz, bb_sigma_total, n_M_total);
				CheckFlag(&status, "umfpack_solve_transpose");
				//for (int j = 0; j < cc_M_total.ncol; j++) {
				//	int iRow = cc_M_total.Ap[j];
				//	while (iRow < cc_M_total.Ap[j + 1]) {
				//		std::cout << "A[" << cc_M_total.Ai[iRow]+1 << "," << j+1 << "] = " << cc_M_total.Ax[iRow] << std::endl;
				//		iRow++;
				//	}
				//	//std::cout << "j = " << j << ", row = " << cc_M_total.Ai[cc_M_total.Ap[j]] << ", val = " << cc_M_total.Ax[cc_M_total.Ap[j]] << std::endl;
				//}
				for (int i = 0; i < n_p; i++) {
					bb[i] = bb_sigma_total[i];
				}
			}
		}
		catch (...)
		{
			status = ADJOINT_ERROR_SIGMA;
		}
		if (status == SOLUTION_LIN_EQN_SYS_SUCCESSFUL) {
			status = ADJOINT_SUCCESSFUL;
		}
		return status;
	}

	// input bsigma has dimension n_q
	// output bb has dimension n_p
	int calc_adjoint(double* bx, double* bsigma, double* bb) {
		if (!calcDerivatives) {
			calcDerivatives = 1;
		}
		ncalls_adjointSigma++;
		ncalls_adjointSolution++;

		int status = -17;
		
		try {
			//1st step: reverse through calculation of sigma
			std::vector<double> bsigma_inactive(n_q, 0.0);
			std::vector<double> bsigma_active(n_q, 0.0);
			for (int i = 0; i < n_q; i++) {
				if (eqnstat[i + n_m]) { // inactive constraint
					bsigma_inactive[i] = bsigma[i];
				}
				else { // active constraint
					bsigma_active[i] = bsigma[i];
				}
			}

			// sigma_i = D_{i,:} * y - d_i     if inactive constraint i
			// sigma_i = s_i                   if active constraint i
			// the above as sigma = f_3(z) with
			// z = [y,mu,s]^T
			// bz = bf_3 bsigma
			// bz = bz_active + bz_inactive
			// bz_active_j = { s_i   at j=n_var+n_m+i
			// bz_inactive_j = { by_i at j=i ?

			std::vector<double> bz_sigma(n_var + n_m + n_q, 0.0);
			
			std::vector<double> by(n_var, 0.0);
			matrixMultiplication_transpose(D, bsigma_inactive, by);
			for (int i = 0; i < n_var; i++) {
				bz_sigma[i] += by[i]; //bz_inactive
			}
			for (int i = 0; i < n_q; i++) {
				bz_sigma[n_var+n_m+i] += bsigma_active[i]; //bz_active
			}		

			//2nd step: set values of bz_y
			std::vector<double> bz_y(n_var + n_m + n_q, 0.0);
			for (int i = 0; i < n_n; i++) {
				bz_y[i+n_p] = bx[i];
			}

			//3rd step: summation
			std::vector<double> bz(n_var + n_m + n_q, 0.0);
			for (int i = 0; i < n_var + n_m + n_q; i++) { bz[i] = bz_y[i] + bz_sigma[i]; }

			//4th step: reverse through f_1
			if (sepCalc) {
				for (int i = 0; i < n_var; i++) {
					by[i] = bz[i];
				}
				std::vector<double> bb_UL(n_var, 0.0);
				status = umfpack_solve_transpose(cc_M_UL, by, bb_UL, n_M_UL);
				CheckFlag(&status, "umfpack_solve_transpose");

				for (int i = 0; i < n_p; i++) {
					bb[i] = bb_UL[i];
				}
				//std::vector<double> by_LR(n_m + n_q, 0.0);
				//for (int i = 0; i < n_q; i++) {
				//	by_LR[n_m + i] = bsigma_active[i]; // bs[i]=bsigma_active[i] skipped for brevity
				//}
				//std::vector<double> bb_LR(n_m+n_q, 0.0);
				//umfpack_solve_transpose(cc_M_LR, by_LR, bb_LR, n_M_LR);
			}
			else {
				//std::vector<double> bz(n_var + n_m + n_q, 0.0);
				//for (int i = 0; i < n_var; i++) {
				//	bz[i] = by[i];
				//}
				//for (int i = 0; i < n_q; i++) {
				//	bz[n_var + n_m + i] = bsigma_active[i]; // bs[i]=bsigma_active[i] skipped for brevity
				//}
				std::vector<double> bb_sigma_total(n_var + n_m + n_q, 0.0);
				status = umfpack_solve_transpose(cc_M_total, bz, bb_sigma_total, n_M_total);
				CheckFlag(&status, "umfpack_solve_transpose");
				//for (int j = 0; j < cc_M_total.ncol; j++) {
				//	int iRow = cc_M_total.Ap[j];
				//	while (iRow < cc_M_total.Ap[j + 1]) {
				//		std::cout << "A[" << cc_M_total.Ai[iRow]+1 << "," << j+1 << "] = " << cc_M_total.Ax[iRow] << std::endl;
				//		iRow++;
				//	}
				//	//std::cout << "j = " << j << ", row = " << cc_M_total.Ai[cc_M_total.Ap[j]] << ", val = " << cc_M_total.Ax[cc_M_total.Ap[j]] << std::endl;
				//}
				for (int i = 0; i < n_p; i++) {
					bb[i] = bb_sigma_total[i];
				}
			}
		}
		catch (...)
		{
			status = ADJOINT_ERROR_SIGMA;
		}
		if (status == SOLUTION_LIN_EQN_SYS_SUCCESSFUL) {
			status = ADJOINT_SUCCESSFUL;
		}
		return status;

	}


	int setKKTmatrices() {
		int status = -17;

		getBasicSolution();
		getBasicSolutionEqualities();
		getBasicSolutionInequalities();
		getSuperBasicVariables(superbasic_var);

		//status=check_basic_eqns(basic_eqns);
		//CheckFlag(&status, "check_basic_eqns");
		//if (status < 0) { return status; }

		int n_nonbasic = n_m + n_q - n_basic + n_superbasic_var;
		sepCalc = (n_nonbasic == n_var);

		if (sepCalc) { //separate calculation of solution x and Lagrangian multiplier is possible
			// set matrix M_UL
			setSystemUL();

			umfpack_triplet_to_col(M_UL, cc_M_UL);
			if ((M_UL.getNrow() != n_var) || (M_UL.getNcol() != n_var)) {
				ss_temp.str(std::string()); //clear stringstream
				ss_temp << "The KKT system is not solveable because it has wrong dimensionality: ";
				//qerrors.push(ss_temp.str());
				//ss_temp.clear();
				ss_temp << "M_UL : " << M_UL.getNrow() << " x " << M_UL.getNcol() << ", should be quadratic with dimension : " << n_var;
				qerrors.push(ss_temp.str());
			}
			if (n_M_UL) { umfpack_di_free_numeric(&n_M_UL); }
			umfpack_prepare(cc_M_UL, &n_M_UL);

			setRhsB_UL();

			// need to solve for x first.
			// use either CPLEX solution
			//get_x_UL_Cplex();
			// or solve linear equations system for x_UL
			status=umfpack_solve(cc_M_UL, B_UL, x_UL, n_M_UL);
			CheckFlag(&status, "umfpack_solve");
			if (status < 0) { return status; }

			// set matrix M_LR
			setMatrixI_B();
			setSystemLR();
			// in the QP case, we need the solution x_UL here.
			setRhsB_LR(x_UL);

			umfpack_triplet_to_col(M_LR, cc_M_LR);

			if (n_M_LR) umfpack_di_free_numeric(&n_M_LR);
			umfpack_prepare(cc_M_LR, &n_M_LR);

			// solve for x_LR (in case of LP, need to solve only once; otherwise, it will be resolved in each iteration step)
			status=umfpack_solve(cc_M_LR, B_LR, x_LR, n_M_LR);
			CheckFlag(&status, "umfpack_solve");
			if (status < 0) { return status; }

			int iRowsUL = static_cast<int>(M_UL.getNrow() - B_UL.size());
			int iRowsLR = static_cast<int>(M_LR.getNrow() - B_LR.size());



		#ifdef doPrinting
			std::cout << "Dimensions of UL part: M_UL is " << M_UL.getNrow() << " x " << M_UL.getNcol();
			std::cout << " and B_UL " << B_UL.size() << " x 1." << std::endl;
			std::cout << "Dimensions of LR part: M_LR is " << M_LR.getNrow() << " x " << M_LR.getNcol();
			std::cout << " and B_LR " << B_LR.size() << " x 1." << std::endl;
		#endif
			//if ((iRowsUL == 0) && (iRowsLR == 0))// && (jColsLR == 0) && (delta == 0))
			//	return 0;
			//else
			//	return -1;
		}
		else { //separate calculation of solution x and Lagrangian multiplier is >>> not <<< possible
			setSystemTotal();
			umfpack_triplet_to_col(M_total, cc_M_total);
			setRhsB_total();
			if (n_M_total) { umfpack_di_free_numeric(&n_M_total); }
			umfpack_prepare(cc_M_total, &n_M_total);


			status=umfpack_solve(cc_M_total, B_total, x_total, n_M_total);
			CheckFlag(&status, "umfpack_solve");
			if (status < 0) { return status; }

			//return 0;

			//calc_pderivatives_total();

			//calc_dsigmadb();
		}
		return status;
	}
	void setBasicSolution() {
		IloCplex::BasisStatusArray cstat = IloCplex::BasisStatusArray(env);
		IloCplex::BasisStatusArray rstat = IloCplex::BasisStatusArray(env);
		cplex.getBasisStatuses(cstat, var);
		cplex.getBasisStatuses(rstat, rng);
		for (int i = 0; i < n_q; i++) {
			rstat[i + n_m] = basic_eqns[i + n_m] ? cplex.Basic : cplex.AtLower ;
		}
		cplex.setBasisStatuses(cstat, var, rstat, rng);
	}
	void getBasicSolution() {
		try {     // basis may not exist
			cplex.getBasisStatuses(eqnstat, rng);
			int atlower = 0, atupper = 0, nerror = 0;
			n_basic = 0;
			n_basic_ineq = 0;
			linear_dependent_equalities.clear();

			// check all constraints
			for (int i = 0; i < rng.getSize(); i++) {
				if (eqnstat[i] == cplex.Basic) {
					basic_eqns[i] = 1;
					n_basic += 1;
					if (i < n_p) {
						basic_eqns[i] = 0;
#ifdef doPrinting
						std::cout << "Basic: " << rng[i] << std::endl;
						std::cout << "Added to non-basic equations because parameter definition." << std::endl;
#endif
					}
					if (i >= n_m) { n_basic_ineq += 1; }
					else { linear_dependent_equalities.push_back(i); }
				}
				else if (eqnstat[i] == cplex.AtLower) {
					basic_eqns[i] = 0;
					//std::cout << "At lower: " << rng[i] << std::endl;
					atlower += 1;
				}
				else if (eqnstat[i] == cplex.AtUpper) {
					basic_eqns[i] = 0;
					//std::cout << "At upper: " << rng[i] << std::endl;
					atupper += 1;
				}
				else
					nerror += 1;
			}
#ifdef doPrinting
			cout << "------ Basic Constraints -------" << endl;
			cout << "Basic Constraints : " << n_basic << endl;
			cout << "At lower Constraints : " << atlower << endl;
			cout << "At upper Constraints : " << atupper << endl;
			cout << "Constraints w/ error : " << nerror << endl;
			cout << "------ end Basic Constraints -------" << endl;
#endif
		}
		catch (...) {
		}
	}
	void getBasicSolutionEqualities() {
		try {     // basis may not exist
			IloNumArray slacks_topl;
			slacks_topl = IloNumArray(env);
			cplex.getSlacks(slacks_topl, rng);

			IloNumArray duals_topl;
			duals_topl = IloNumArray(env);
			cplex.getDuals(duals_topl, rng);

			cplex.getBasisStatuses(eqnstat, rng);
			int atlower = 0, atupper = 0, nerror = 0, basic_ = 0;

			// check equalities only
			for (int i = 0; i < n_m; i++) {
				if (eqnstat[i] == cplex.Basic) {
					basic_ += 1;
					//cout << "Equality " << rng[i] << " is basic with slack " << slacks_topl[i] << " and dual " << duals_topl[i] << endl;
				}
				else if (eqnstat[i] == cplex.AtLower) {
					atlower += 1;
				}
				else if (eqnstat[i] == cplex.AtUpper) {
					atupper += 1;
				}
				else
					nerror += 1;
			}

#ifdef doPrinting
			cout << "------ Basic Equalities -------" << endl;
			cout << "Basic equalities : " << basic_ << endl;
			cout << "At lower equalities : " << atlower << endl;
			cout << "At upper equalities : " << atupper << endl;
			cout << "Equalities w/ error : " << nerror << endl;
			cout << "------ end Basic Equalities -------" << endl;
#endif
		}
		catch (...) {
		}
	}
	void getBasicSolutionInequalities() {
		try {     // basis may not exist
			IloNumArray slacks_topl;
			slacks_topl = IloNumArray(env);
			cplex.getSlacks(slacks_topl, rng);

			IloNumArray duals_topl;
			duals_topl = IloNumArray(env);
			cplex.getDuals(duals_topl, rng);

			cplex.getBasisStatuses(eqnstat, rng);
			int atlower = 0, atupper = 0, nerror = 0, basic_ = 0;

			// check inequalities only
			for (int i = n_m; i < rng.getSize(); i++) {
				if (eqnstat[i] == cplex.Basic) {
					basic_ += 1;
					//cout << "Inequality " << rng[i] << " is basic with slack " << slacks_topl[i] << " and dual " << duals_topl[i] << endl;
				}
				else if (eqnstat[i] == cplex.AtLower) {
					atlower += 1;
					//cout << "Inequality " << rng[i] << " is at lower with slack " << slacks_topl[i] << " and dual " << duals_topl[i] << endl;
				}
				else if (eqnstat[i] == cplex.AtUpper) {
					atupper += 1;
				}
				else
					nerror += 1;
			}
#ifdef doPrinting
			cout << "------ Basic Inequalities -------" << endl;
			cout << "Basic inequalities : " << basic_ << endl;
			cout << "At lower inequalities : " << atlower << endl;
			cout << "At upper inequalities : " << atupper << endl;
			cout << "Inequalities w/ error : " << nerror << endl;
			cout << "------ end Basic Inequalities -------" << endl;
#endif
		}
		catch (...) {
		}
	}

	void getBasicVariables() {
		try {     // basis may not exist
			IloCplex::BasisStatusArray varstat(env);
			cplex.getBasisStatuses(varstat, var);
			int atlower = 0, atupper = 0, nerror = 0, basic_ = 0;

			// check all variables
			for (int i = 0; i < var.getSize(); i++) {
				if (varstat[i] == cplex.Basic) {
					basic_ += 1;
				}
				else if (varstat[i] == cplex.AtLower) {
					atlower += 1;
				}
				else if (varstat[i] == cplex.AtUpper) {
					atupper += 1;
				}
				else {
					std::cout << var[i] << " with " << eqnstat[i] << std::endl;
					nerror += 1;
				}
			}
#ifdef doPrinting
			cout << "------ Basic variables -------" << endl;
			cout << "Basic variables : " << basic_ << endl;
			cout << "At lower variables : " << atlower << endl;
			cout << "At upper variables : " << atupper << endl;
			cout << "variables w/ error : " << nerror << endl;
			cout << "------ end Basic variables -------" << endl;
#endif
		}
		catch (...) {
		}
	}

	void getSuperBasicVariables(std::vector<int>& T) {
		try {     // basis may not exist
			T.clear();
			IloCplex::BasisStatusArray varstat(env);
			cplex.getBasisStatuses(varstat, var);
			int atlower = 0, atupper = 0, nerror = 0, basic_ = 0;// , superbasic_ = 0;
			n_superbasic_var = 0;
			// check all variables
			for (int i = 0; i < var.getSize(); i++) {
				if (varstat[i] == cplex.Basic) {
					basic_ += 1;
				}
				else if (varstat[i] == cplex.AtLower) {
					atlower += 1;
				}
				else if (varstat[i] == cplex.AtUpper) {
					atupper += 1;
				}
				else if (varstat[i] == cplex.FreeOrSuperbasic) {
					n_superbasic_var += 1;
					T.push_back(i);
				}
				else {
					std::cout << var[i] << " with " << eqnstat[i] << std::endl;
					nerror += 1;
				}
			}
			//std::cout << " ----------- All Variables -----------" << std::endl;
			//std::cout << "At lower: " << atlower << std::endl;
			//std::cout << "At upper: " << atupper << std::endl;
			//std::cout << "Basic: " << basic_ << std::endl;
			//std::cout << "Superbasic: " << n_superbasic_var << std::endl;
			//std::cout << "nerror: " << nerror << std::endl;
		}
		catch (...) {
		}
	}

	void detailed_solution()
	{
		cout << "------ Detailed solution -------" << endl;
		IloNumArray dualEqns(env);
		IloNumArray slackEqns(env);

		cout << "Solution status : " << cplex.getStatus() << endl;
		cout << "Objective function value  = " << cplex.getObjValue() << endl;
		cplex.getValues(vals, var);
		//cout << "Solution vector = [";
		//for (int i = 0; i < var.getSize(); i++) {
		//	if (i < var.getSize() - 1) { cout << vals[i] << ", "; }
		//	else { cout << vals[i]; }
		//}
		//cout << "]" << endl;
		try {     // basis may not exist
			IloCplex::BasisStatusArray cstat(env);
			IloCplex::BasisStatusArray eqstat(env);
			cplex.getBasisStatuses(eqstat, rng);
		}
		catch (...) {
		}
		cout << "------ end Detailed solution -------" << endl;
	}

	void getSwitchVars(double* newSigma) {
		// calculate tempSlacks = D*v-d
		std::vector<double> tempSlacks(n_q);
		matrixMultiplication(D, x_UL, tempSlacks);// , n_D);

		// return values for inequalities only
		for (int i = 0; i < n_q; i++) {
			if (sepCalc) {
				newDuals[i] = x_LR[i + n_m - (n_basic - n_basic_ineq) + n_superbasic_var];
			}
			else {
				newDuals[i] = x_LR[i + n_m]; // if total system is solved, redundant equations are not neglected.
			}			
			newSlacks[i] = tempSlacks[i] - abdev[n_m+i];
			newSigma[i] = eqnstat[i + n_m] ? newSlacks[i] : newDuals[i];
			//if (!sepCalc) {
			//	newSigma[i] = basic_eqns[i + n_m] ? newSlacks[i] : x_LR[i + n_m] ;
			//}
			//if (i == 44) {
			//	std::cout << "i = " << i << ", i+n_m =" << i+n_m << ", eqnstat =" << eqnstat[i + n_m] << ", Dual = " << newDuals[i] << " or " << x_LR[i + n_m] << ", Slack = " << newSlacks[i] << ", Sigma = " << newSigma[i] << std::endl;
			//}
		}

//#ifdef doPrinting
//		for (int i = 0; i < n_q; i++) {
//			std::cout << "sigma[" << i << "] = " << newSigma[i] << std::endl;
//		};
//#endif
	}

	void getSwitchVars_derivatives(double* dsigma, std::vector<double>& dx, std::vector<double>& dx_LR) {
		// calculate tempSlacks = D*v-d
		std::vector<double> tempSlacks(n_q);
		matrixMultiplication(D, dx, tempSlacks);// , n_D);

												  // return values for inequalities only
		for (int i = 0; i < n_q; i++) {
			//newDuals[i] = ds[i];
			//newSlacks[i] = tempSlacks[i];
			dsigma[i] = eqnstat[i + n_m] ? tempSlacks[i] : dx_LR[i + n_m - (n_basic - n_basic_ineq) + n_superbasic_var];
		}

//#ifdef doPrinting
//		for (int i = 0; i < n_q; i++) {
//			std::cout << "dsigma[" << i << "] = " << dsigma[i] << std::endl;
//		};
//#endif
	}

	void getSwitchVarsCplex(double* newSigma) {
		//update values of variables duals and slacks
		cplex.getDuals(duals, rng);
		cplex.getSlacks(slacks, rng);

		//update information about basic equations
		cplex.getBasisStatuses(eqnstat, rng);

		// return values for inequalities only
		for (int i = 0; i < n_q; i++) {
			newSigma[i] = eqnstat[i + n_m] ? -slacks[i + n_m] : duals[i + n_m];
		}
	}

	void getValues(double* x, double* newValues)
	{
		for (int i = 0; i < n_n; i++) {
			newValues[i] = x[ind_x[i]];
		}
	}

	//int* getBasicEqns() {
	//	getBasicSolution();
	//	auto xi = new int[n_q];
	//	for (int i = 0; i < n_q; i++) { xi[i] = basic_eqns[i]; }
	//	return xi;
	//}

	void getValuesCplex(double* newValues)
	{
		cplex.getValues(vals, var);
		for (int i = 0; i < n_n; i++) {
			newValues[i] = vals[ind_x[i]];
		}
	}

	void get_x_UL_Cplex()
	{
		cplex.getValues(vals, var);
		x_UL.clear();
		x_UL.resize(vals.getSize());
		for (int i = 0; i < vals.getSize(); i++) {
			x_UL[i] = vals[i];
		}
	}
	
	template<typename T>
	std::vector<T> slice(std::vector<T> const &v, int m, int n)
	{
		auto first = v.cbegin() + m;
		auto last = v.cbegin() + n + 1;

		std::vector<T> vec(first, last);
		return vec;
	}


	void find_indices(const char** var_x, const int n_n, const char** var_b, const int n_p)
	{
		ind_b.resize(n_p, 0);
		ind_b = find_rng_by_str(var_b, n_p);
		ind_x.resize(n_n, 0);
		int itemp = 0;
		for (int i = 0; i < var.getSize(); i++) {
			if (var[i].getId() > itemp)
				itemp = static_cast<int>(var[i].getId());
		}
		ind_x_conv.resize(itemp + 1, 0);
		find_vars_by_str(var_x, n_n);
	}

	std::vector<int> find_var_by_str(const char** var_str, const int n_str)
	{
		std::vector<int> ind_str(n_str, 0);
		for (int i = 0; i < n_str; i++) {
			std::string temp_str(var_str[i]);
			for (int j = 0; j < var.getSize(); j++) {
				//if (i==0) std::cout << var_str[i] << ", " << var[j] << ", " << var_str[i][0] << ", " << var[j].getName() << std::endl;
				if (temp_str.compare(var[j].getName()) == 0) { ind_str[i] = j; break; } //temp_str.compare(var[j].getName()) == 0)
			}
		}
		return ind_str;
	}

	int find_vars_by_str(const char** var_str, const int n_str)
	{
		//std::vector<int> ind_str(n_str, 0);
		for (int i = 0; i < n_str; i++) {
			std::string temp_str(var_str[i]);
			for (int j = 0; j < var.getSize(); j++) {
				if (temp_str.compare(var[j].getName()) == 0) { ind_x[i] = j; ind_x_conv[var[j].getId()] = i; break; } //temp_str.compare(var[j].getName()) == 0)//ind_x_conv[j] = i; std::cout << var_str[i] << ", " << var[j].getName() << ", id=" << var[j].getId() << ", j=" << j << std::endl; 
			}
		}
		return 0;
	}

	int find_var_by_str(const char* var_str)
	{
		//int ind_str = -1;
		//std::vector<int> ind_str(n_str, 0);
		//for (int i = 0; i < n_str; i++) {
		std::string temp_str(var_str);
		int index = 0;
		while (index < var.getSize())
		{
			if (temp_str.compare(var[index].getName()) == 0)
				return index;
			index++;
		}
		//for (int j = 0; j < var.getSize(); j++) {
		//	if (temp_str.compare(var[j].getName()) == 0) { ind_str = j; break; };
		//};
		//return ind_str;
		return -1;
	}

	std::vector<int> find_rng_by_str(const char**, const int n_str) {
		std::vector<int> ind_str(n_str, 0);
		IloRange temp;
		std::string temp_str;
		for (int i = 0; i < n_str; i++) {
			for (int irow = 0; irow < rng.getSize(); irow++) {
				std::stringstream ss;
				ss << "p" << i + 1;
				temp_str = ss.str();
				temp = rng[irow];
				if (temp_str.compare(temp.getName()) == 0) { ind_str[i] = irow; break; } //cout << rng[irow] << endl; 
			}
		}
		return ind_str;
	}

	std::vector<int> getIndX() { return ind_x; }

	std::vector<int> getIndB() { return ind_b; }

	void update_b(double* b)
	{
		//double tempbound;
		for (int i = 0; i < n_p; i++) {
			//tempbound = rng[ind_b[i]].getLB();
			rng[ind_b[i]].setBounds(b[i], b[i]);
		//	rng[ind_b[i]].setLB(-INFINITY);
		//	if (b[i] > tempbound) {
		//		rng[ind_b[i]].setUB(b[i]);
		//		rng[ind_b[i]].setLB(b[i]);
		//	}
		//	else {
		//		rng[ind_b[i]].setLB(b[i]);
		//		rng[ind_b[i]].setUB(b[i]);
		//	}
		}
		//cplex.exportModel("AfterUpdateB.lp");
	}

	// exports optimization model to file (with filename fname)
	void export_model(const char* fname) {
		cplex.exportModel(fname);
	}

	void parseModel(const char ** params,
		//const double *x,
		const char ** variables,
		const char * objective,
		const char ** equalities,
		const char ** inequalities,
		const int p,
		const int m,
		const int n,
		const int q,
		int *info) {
		int tempInfo;
		n_p = p;
		std::vector<double> x;
		x.resize(n);
		JadeDaeoParser2 parser(params, x.data(), variables, objective, equalities, inequalities, p, m, n, q);
		tempInfo = parser.createCplex(&env, model, var, rng, Qdiag);
		cplex.extract(model);
		cplex.exportModel("EmbeddedOptProblem.lp");
		info[0] = 0;// tempInfo;
		if (info[0] != PARSING_SUCCESSFUL) {
			std::cout << info[0] << std::endl;
		}
	}

	/* set cost vector from cplex optimization model */
	void setCostVector() {
		IloNumVar tempvar;
		IloObjective tempObj;
		int id = -1;
		for (IloIterator<IloObjective> itObj(env); itObj.ok(); ++itObj) {
			tempObj = *itObj;
			for (IloExpr::LinearIterator it = IloExpr(tempObj.getExpr()).getLinearIterator(); it.ok(); ++it) {
				tempvar = it.getVar();
				id = find_var_by_str(tempvar.getName());
				cost[id] = it.getCoef();
#ifdef doPrinting
				std::cout << "Cost of variable " << tempvar.getName() << " ist " << it.getCoef() << std::endl;
#endif
			}
		}
	}

	/* update bvec */
	void update_bvec(double* b) {
		for (int i = 0; i < n_p; i++) {
			//bvec[ind_b[i]] = b[i];
			abvec[ind_b[i]] = b[i];
			abdev[ind_b[i]] = 0.0;
			//std::cout << "b[" << i << "] = " << bvec[ind_b[i]] << std::endl;
		}
	}



	/* We intend to solve the following linear algebraic equation system (in matrix notation):

	/ A					\	/		\		/b	\
	| D_N				|	|v*		|	=	|0	|
	| Q		A^T		-D^T|	|lambda*|		|-c	|
	\				I_B	/	\s*		/		\0	/
	*/

	/* sets sparse matrix D from inequality equations */
	void setMatrixD() { //(D, n_m, n_m+n_q);
		D.resetMatrix();
		IloRange temp;
		IloNumVar tempvar;
		int id = -1;
		for (int irow = n_m; irow < n_m + n_q; irow++) {
			temp = rng[irow];
			for (IloExpr::LinearIterator it = IloExpr(temp.getExpr()).getLinearIterator(); it.ok(); ++it) {
				tempvar = it.getVar();
				id = find_var_by_str(tempvar.getName());
				D.addEntry(irow - n_m, id, it.getCoef());
			}
		}
#ifdef doPrinting
		D.printMatrix();
#endif
	}

	void setMatrixI_B() {
		I_B.resetMatrix();
		IloRange temp;
		IloNumVar tempvar;
		int rowI_B = 0;
		for (int irow = n_m; irow < n_m + n_q; irow++) {
			if (basic_eqns[irow]) {
				I_B.addEntry(rowI_B, irow - n_m, 1);
				rowI_B += 1;
				temp = rng[irow];
			}
		}
#ifdef doPrinting
		I_B.printMatrix();
#endif // doPrinting
	}

	void setMatrixAD() {
		AD.resetMatrix();
		IloRange temp;
		IloNumVar tempvar;
		int id = -1;
		int rowAD = 0;
		for (int irow = 0; irow < n_m + n_q; irow++) {
			temp = rng[irow];
			abdev[irow] = temp.getLb();
			//std::cout << temp << std::endl;
			//bind[rowAD] = irow;
			for (IloExpr::LinearIterator it = IloExpr(temp.getExpr()).getLinearIterator(); it.ok(); ++it) {
				tempvar = it.getVar();
				id = find_var_by_str(tempvar.getName());
				//std::cout << id << ", " << tempvar.getId() << ", " << ind_x_conv[tempvar.getId()] << std::endl;
				AD.addEntry(rowAD, id, it.getCoef());
				//A.Ai.push_back(1);
			}
			rowAD += 1;
		}
	}

	void setSystemUL() {
		M_UL.resetMatrix();
		superbasic_var.clear();
		IloRange temp;
		IloNumVar tempvar;
		int rowM_UL = 0;
		int it = 0;
		std::vector<int> indRowM_UL(AD.nrow, -1);
		while (it < AD.nz) {
			if (!basic_eqns[AD.iRow[it]]) {
				if (indRowM_UL[AD.iRow[it]] == -1) {
					indRowM_UL[AD.iRow[it]] = rowM_UL;
					bind[rowM_UL] = AD.iRow[it];
					for (int i = 0; i < n_p; i++) {
						if (AD.iRow[it] == ind_b[i]) { bind_t[i] = AD.iRow[it]; }
					}
					rowM_UL++;
				}

				M_UL.addEntry(indRowM_UL[AD.iRow[it]], AD.jCol[it], AD.values[it]);
			}
			it++;
		}
		if (M_UL.getNrow() < n_var) {
			getSuperBasicVariables(superbasic_var);
			for (int i = 0; i < static_cast<int>(superbasic_var.size()); i++) {
				M_UL.addEntry(rowM_UL, superbasic_var[i], 1.0);
				rowM_UL += 1;
			}
		}

	}

	void setRhsB_UL() {
		B_UL.resize(n_var);
		for (int i = 0; i < n_var; i++) {
			B_UL[i] = (abvec[bind[i]] + abdev[bind[i]]);
		}
	}

	void update_B_UL(double* b) {
		for (int i = 0; i < n_p; i++) {
			B_UL[bind_t[i]] = b[i];
		}
	}

	void update_B_total(double* b) {
		for (int i = 0; i < n_p; i++) {
			B_total[bind_t[i]] = b[i];
		}
	}

	void update_B(double* b, std::vector<double>& vec) {
		for (int i = 0; i < n_p; i++) {
			vec[bind_t[i]] = b[i];
		}
	}

	void setSystemLR() {
		M_LR.resetMatrix();
		int tempRow, tempCol = 0;
		double tempVal = 0.0;
		IloRange temp;
		IloNumVar tempvar;
		int rowM_LR = 0;
		int it = 0;
		std::vector<int> indRowM_LR(AD.nrow, -1);
		while (it < AD.nz) {
			if ((AD.iRow[it] < n_m) && (!basic_eqns[AD.iRow[it]])) {
				if (indRowM_LR[AD.iRow[it]] == -1) {
					indRowM_LR[AD.iRow[it]] = rowM_LR;
					rowM_LR++;
				}

				M_LR.addEntry(AD.jCol[it], indRowM_LR[AD.iRow[it]], AD.values[it]);
			}
			it++;
		}

		for (int i = 0; i < static_cast<int>(superbasic_var.size()); i++) {
			for (int i = 0; i < static_cast<int>(superbasic_var.size()); i++) {
				M_LR.addEntry(superbasic_var[i], rowM_LR, 1.0);
				rowM_LR += 1;
			}
		}
		// negative transpose of D
		for (int i = 0; i < D.getNz(); i++) {
			std::tie(tempRow, tempCol, tempVal) = D.returnEntry(i);
			M_LR.addEntry(tempCol, tempRow + rowM_LR, -tempVal); //check here
		}
		// I_B
		for (int i = 0; i < I_B.getNz(); i++) {
			std::tie(tempRow, tempCol, tempVal) = I_B.returnEntry(i);
			M_LR.addEntry(tempRow + n_var, tempCol + rowM_LR, tempVal);
		}
	}

	void setRhsB_LR(std::vector<double>& vec) {
		B_LR.clear();
		for (int i = 0; i < n_var; i++) {
			if (bQP) {
				B_LR.push_back(-cost[i] - Qdiag[i] * vec[i]);
			}
			else {
				B_LR.push_back(-cost[i]);
			}
		}
		for (int i = n_var; i < n_var + n_basic_ineq; i++) {
			B_LR.push_back(0.0);
		}
	}

	void setSystemTotal() {
		M_total.resetMatrix();
		superbasic_var.clear();
		IloRange temp;
		IloNumVar tempvar;
		int rowM_total = 0;
		int it = 0;
		int n_nonbasic = n_m + n_q - n_basic;
		std::vector<int> indRowM_total(AD.nrow, -1);
		//add AD part
		while (it < AD.nz) {
			if (!basic_eqns[AD.iRow[it]]) {
				if (indRowM_total[AD.iRow[it]] == -1) {
					indRowM_total[AD.iRow[it]] = rowM_total;
					bind[rowM_total] = AD.iRow[it];
					for (int i = 0; i < n_p; i++) {
						if (AD.iRow[it] == ind_b[i]) { bind_t[i] = AD.iRow[it]; }
					}
					rowM_total++;
				}

				M_total.addEntry(indRowM_total[AD.iRow[it]], AD.jCol[it], AD.values[it]);
			}
			M_total.addEntry(n_nonbasic + AD.jCol[it], n_var + AD.iRow[it], -AD.values[it]);
			it++;
		}
		if (M_total.getNrow() < n_var) {
			getSuperBasicVariables(superbasic_var);
			for (int i = 0; i < static_cast<int>(superbasic_var.size()); i++) {
				M_total.addEntry(rowM_total, superbasic_var[i], 1.0);
				rowM_total += 1;
			}
		}

		for (int i = 0; i < Qdiag.size(); ++i) {
			M_total.addEntry(n_nonbasic + i, i, Qdiag[i]);
		}
		int rowM_temp = 0;
		for (int i = 0; i < basic_eqns.size(); ++i) {
			if (basic_eqns[i]) {
				M_total.addEntry(n_nonbasic + n_var + rowM_temp, n_var + i, 1);
				rowM_temp += 1;
			}
		}
	}

	void setRhsB_total() {
		B_total.clear();
		B_total.resize(n_var + n_m + n_q);
		int n_nonbasic = n_m + n_q - n_basic;
		int temp_row = 0;
		for (int i = 0; i < basic_eqns.size(); ++i) {
			if (!basic_eqns[i]) {
				B_total[temp_row] = abdev[i];
				temp_row += 1;
			}
		}
		for (int i = 0; i < n_var; ++i) {
			B_total[n_nonbasic + i] = -cost[i];
		}
	}

	/* converts sparse matrix in triplet form to compressed column form for umfpack */
	void umfpack_triplet_to_col(sparseTripletMatrix& T, sparseComprColumnMatrix& cc_T) {
		int status = -17;
		cc_T.Ap.resize(T.getNcol() + 1);
		cc_T.Ai.resize(T.getNz());
		cc_T.Ax.resize(static_cast<int> (T.getNz()));

		cc_T.ncol = T.getNcol();
		cc_T.nrow = T.getNrow();
		cc_T.nz = T.getNz();

		status = umfpack_di_triplet_to_col(static_cast<int> (T.getNrow()), static_cast<int> (T.getNcol()), static_cast<int> (T.getNz()), T.get_Ti(), T.get_Tj(), T.get_Tx(),
			cc_T.Ap.data(), cc_T.Ai.data(), cc_T.Ax.data(), nullptr);
	}

	void umfpack_prepare(sparseComprColumnMatrix& cc_T, void** Numeric) {
		double *null = nullptr;
		void *Symbolic;
		int status;
		double Control[UMFPACK_CONTROL];
		umfpack_di_defaults(Control);
		Control[UMFPACK_PRL] = 1;
		umfpack_di_report_control(Control);

		status = umfpack_di_symbolic(cc_T.nrow, cc_T.ncol, cc_T.Ap.data(), cc_T.Ai.data(), cc_T.Ax.data(), &Symbolic, null, null);
		umfpack_di_report_symbolic(Symbolic, Control);
		status = umfpack_di_numeric(cc_T.Ap.data(), cc_T.Ai.data(), cc_T.Ax.data(), Symbolic, Numeric, null, null);
		umfpack_di_report_numeric(*Numeric, Control);

		umfpack_di_free_symbolic(&Symbolic);
	}


	/* solves x = cc_T\b */
	int umfpack_solve(sparseComprColumnMatrix& cc_T, std::vector<double>& b, std::vector<double>& x, void* Numeric)
	{
		//double *null = (double *)NULL;
		double info[UMFPACK_INFO];
		double Control[UMFPACK_CONTROL];
		umfpack_di_defaults(Control);
		Control[UMFPACK_PRL] = 1;
		umfpack_di_report_info(Control, info);
		//void *Symbolic, *Numeric;
		int n = static_cast<int> (cc_T.Ap.size() - 1);
		x.resize(n);
		umfpack_di_report_numeric(Numeric, Control);
		try {
			(void)umfpack_di_solve(UMFPACK_A, cc_T.Ap.data(), cc_T.Ai.data(), cc_T.Ax.data(), x.data(), b.data(), Numeric, Control, info);
			umfpack_di_report_info(Control, info);
		}
		catch (...) {
			return SOLUTION_ERROR_LIN_EQN_SYS;
		}
		//std::cout << "UMFPACK INFO: " << info[UMFPACK_STATUS] << std::endl;
		if (info[UMFPACK_STATUS] == UMFPACK_OK) {
			return SOLUTION_LIN_EQN_SYS_SUCCESSFUL;
		}
		else {
			return SOLUTION_ERROR_LIN_EQN_SYS;
		}

//#ifdef doPrinting
//		for (int i = 0; i < n; i++) printf("x [%d] = %g\n", i, x[i]);
//#endif // doPrinting		
	}

	/* solves (cc_T)^T x = b for x */
	int umfpack_solve_transpose(sparseComprColumnMatrix& cc_T, std::vector<double>& b, std::vector<double>& x, void* Numeric)
	{
		//double *null = (double *)NULL;
		double info[UMFPACK_INFO];
		double Control[UMFPACK_CONTROL];
		umfpack_di_defaults(Control);
		Control[UMFPACK_PRL] = 1;
		umfpack_di_report_info(Control, info);
		//void *Symbolic, *Numeric;
		int n = static_cast<int> (cc_T.Ap.size() - 1);
		//x.resize(n);
		umfpack_di_report_numeric(Numeric, Control);
		try {
			(void)umfpack_di_solve(UMFPACK_At, cc_T.Ap.data(), cc_T.Ai.data(), cc_T.Ax.data(), x.data(), b.data(), Numeric, Control, info);
			umfpack_di_report_info(Control, info);
		}
		catch (...) {
			return SOLUTION_ERROR_LIN_EQN_SYS_TRANSPOSE;
		}
		//std::cout << "UMFPACK INFO: " << info[UMFPACK_STATUS] << std::endl;
		if (info[UMFPACK_STATUS] == UMFPACK_OK) {
			return SOLUTION_LIN_EQN_SYS_SUCCESSFUL;
		}
		else {
			return SOLUTION_ERROR_LIN_EQN_SYS_TRANSPOSE;
		}
	}

	/* function to calculate x = T*b */
	void matrixMultiplication(sparseTripletMatrix& T, std::vector<double>& b, std::vector<double>& x) {
		for (int i = 0; i < T.getNz(); i++) {
			x[T.iRow[i]] += T.values[i] * b[T.jCol[i]];
		}
	}

	/* function to calculate x = T^T*b */
	void matrixMultiplication_transpose(sparseTripletMatrix& T, std::vector<double>& b, std::vector<double>& x) {
		for (int i = 0; i < T.getNz(); i++) {
			x[T.jCol[i]] += T.values[i] * b[T.iRow[i]];
		}
	}
	/* We consider the equation system M*x=b and calculate the derivative of x w.r.t. time-dependent elements of b. 
	   In order to do so, we need the inverse of M, i.e., M^-1 and select its relevant entries.
	   Inputs: Quadratic matrix M
	   Output: M_inv = [dx1/db1 ... dxN/db1 dx1/db2 ... dxN/db2 ...]^T; M_inv has (number of columns of M - number of parameters) * (number of parameters) entries */
	/* solves x = I\M, i.e., calculates inverse of M */
	void umfpack_inverse(sparseTripletMatrix& M, std::vector<double>& M_inv, std::vector<double>& M_inv_all)
	{
		int status = -17;
		//calcDerivatives = 1;

		double info[UMFPACK_INFO];
		double Control[UMFPACK_CONTROL];
		umfpack_di_defaults(Control);
		Control[UMFPACK_PRL] = 1;
		umfpack_di_report_info(Control, info);


		void *Symbolic = nullptr;
		void *Numeric = nullptr;


		// transform matrix M from triplet to column form
		sparseComprColumnMatrix cc_M;
		umfpack_triplet_to_col(M, cc_M);

		// symbolic factorization
		status = umfpack_di_symbolic(cc_M.nrow, cc_M.ncol, cc_M.Ap.data(), cc_M.Ai.data(), cc_M.Ax.data(), &Symbolic, nullptr, nullptr);
		umfpack_di_report_symbolic(Symbolic, Control);

		//numeric factorization
		status = umfpack_di_numeric(cc_M.Ap.data(), cc_M.Ai.data(), cc_M.Ax.data(), Symbolic, &Numeric, nullptr, nullptr);
		umfpack_di_report_numeric(Numeric, Control);

		//check for right dimensionality
		if (((cc_M.ncol - n_p)*n_p - M_inv.size()) != 0) {
			ss_temp.str(std::string()); //clear stringstream
			ss_temp << "Dimension missmatch in derivative calculation dx/db (if != 0): " << (cc_M.ncol - n_p)*n_p - M_inv.size();
			qerrors.push(ss_temp.str());
		}
		//std::cout << "Dimension missmatch in derivative calculation (if != 0): " << (cc_M.ncol - n_p)*n_p - M_inv.size() << std::endl;
		if (((cc_M.ncol)*n_p - M_inv_all.size()) != 0) {
			ss_temp.str(std::string()); //clear stringstream
			ss_temp << "Dimension missmatch in derivative calculation dxall/db (if != 0): " << (cc_M.ncol)*n_p - M_inv_all.size();
			qerrors.push(ss_temp.str());
		}
		//std::cout << "Dimension missmatch in derivative calculation (if != 0): " << (cc_M.ncol - n_p)*n_p - M_inv_all.size() << std::endl;

		std::vector<double> b_temp(cc_M.nrow, 0.0);
		std::vector<double> x_temp(cc_M.ncol, 0.0);
		//int col = 0;
		for (int i = 0; i < static_cast<int>(bind_t.size()); i++) {
			b_temp[bind_t[i]] = 1.0;
			(void)umfpack_di_solve(UMFPACK_A, cc_M.Ap.data(), cc_M.Ai.data(), cc_M.Ax.data(), x_temp.data(), b_temp.data(), Numeric, Control, info);
			umfpack_di_report_info(Control, info);
			int row = 0;
			int row_all = 0;
			for (int j = 0; j < cc_M.ncol; j++) {
				if (!(std::find(bind_t.begin(), bind_t.end(), j) != bind_t.end())) {
					M_inv[row + i*(n_n)] = x_temp[j];
					row++;
				}
				M_inv_all[row_all + i*(n_n)] = x_temp[j];
				row_all++;
			}
			b_temp[bind_t[i]] = 0.0;
		}


		umfpack_di_free_numeric(&Numeric);
		umfpack_di_free_symbolic(&Symbolic);

//#ifdef doPrinting
//		for (int i = 0; i < n; i++) printf("x [%d] = %g\n", i, x[i]);
//#endif // doPrinting		
	}

	//void calc_dsigmadx() {
	//	std::vector<double> dsigmadx_all(n_q*(n_n + n_p), 0.0);
	//	

	//	// fill non-zeros of derivative vector
	//	int i = 0;
	//	while (i < D.nz){
	//		if (eqnstat[D.iRow[i] + n_m]) {
	//			dsigmadx_all[D.jCol[i] *n_q + D.iRow[i]] = D.values[i];
	//		}
	//		i++;
	//	}
	//	std::fill(dsigmadx.begin(), dsigmadx.end(), 0.0);
	//	int col = 0;
	//	for (int jCol = 0; jCol < (n_n + n_p); jCol++) {
	//		if (!(std::find(bind_t.begin(), bind_t.end(), jCol) != bind_t.end())) {
	//			for (int iRow = 0; iRow < n_q; iRow++) {
	//				dsigmadx[col*n_q + iRow] = dsigmadx_all[jCol*n_q + iRow];
	//			}
	//			col++;
	//		}
	//	}
	//}

	/* direct approach for solving embedded LP using CPLEX */
	int solveDirect(double* b, double* x)
	{
		if (solutionMethod == NOTDEFINED) { solutionMethod = DIRECT; }
		if (solutionMethod == DIRECT) { ncalls_solveSolution++; }

		int res;

		res = solveCplex(b, x);

		return 0;//res
	}

	/* direct approach for solving embedded LP using CPLEX */
	int solveDirect_time(double* b, double* x, double myTime)
	{

		//if (solutionMethod == NOTDEFINED) { solutionMethod = DIRECT; }
		//if (solutionMethod == DIRECT) { ncalls_solveSolution++; }

		int res;

		res = solveDirect(b, x);

		timePointsF.push_back(myTime);

		//res = solveCplex(b, x);

		return 0;//res;
	}

	/* direct approach for solving embedded LP using CPLEX, returns additional information about inequality constraints */
	int solveDirect(double* b, double* x, double* newSigma)
	{
		////if (solutionMethod == NOTDEFINED) { solutionMethod = DIRECT; }
		//if (solutionMethod == DIRECT) { ncalls_solveSolution++; }
		//if (solutionMethod == DIRECT) { ncalls_solveSigma++; }
		int res;

		res = solveDirect(b, x);
		if (solutionMethod == DIRECT) { ncalls_solveSigma++; }
		getSwitchVarsCplex(newSigma);

		return 0;// res;
	}

	/* direct approach for solving embedded LP using CPLEX, returns additional information about inequality constraints */
	int solveDirect_time(double* b, double* x, double* newSigma, double myTime)
	{
		//if (solutionMethod == NOTDEFINED) { solutionMethod = DIRECT; }
		//if (solutionMethod == DIRECT) { ncalls_solveSolution++; }
		//if (solutionMethod == DIRECT) { ncalls_solveSigma++; }
		int res;
		res = solveDirect(b, x, newSigma);
		timePointsF.push_back(myTime);

		return 0;//res;
	}

	/* Active set approach for solving embedded LP */
	int solveActiveSet(double* b, double* x, double* newSigma)
	{
		int res;
		if (solutionMethod == NOTDEFINED) { solutionMethod = ACTIVE_SET; }
		if (solutionMethod == ACTIVE_SET) { ncalls_solveSolution++; }
		if (solutionMethod == ACTIVE_SET) { ncalls_solveSigma++; }
		//update_b(b);
		if (basisStatus == BASIS_OPTIMAL) {
			res = solveKKT(b, x, newSigma);
		}
		else {
			res = solveDirect(b, x, newSigma);
			
			if (res == SOLUTION_DIRECT_SUCCESSFUL) {
				//x_UL.clear();
				//x_UL.resize(n_n);
				//for (int i = 0; i < n_n; i++) {
				//	x_UL[i] = x[i];
				//}

				res = setKKTmatrices(); // if not equal to 0: matrix size mismatch

				ss_temp.str(std::string()); //clear stringstream
				ss_temp << "Active set update " << ncalls_updateAS << ": sepCalc = " << sepCalc << "; daeoIn = [";
				for (int i = 0; i < n_p - 1; i++) {
					ss_temp << b[i] << ", ";
				}
				ss_temp << b[n_p - 1] << "]";
				if (bTime)
					ss_temp << " at t = " << timePointsF.back() << ".";
				else
					ss_temp << ".";
				if (linear_dependent_equalities.size() > 0) {
					ss_temp << " " << linear_dependent_equalities.size() << " dependent equality constraints (that are ignored): ";
					for (int i = 0; i < linear_dependent_equalities.size() - 1; i++) {
						ss_temp << linear_dependent_equalities[i] << ", ";
					}
					ss_temp << linear_dependent_equalities[linear_dependent_equalities.size() - 1] << ".";
				}

				qevents.push(ss_temp.str());

				basisStatus = BASIS_OPTIMAL;
			}
		}
		if (res > 0) {
			res = SOLUTION_KKT_SUCCESSFUL;
		}
		return res;
	}

	int solveActiveSet_time(double* b, double* x, double* newSigma, double myTime)
	{
		if (~bTime) { bTime = 1; }
		timePointsF.push_back(myTime);
		return solveActiveSet(b, x, newSigma);
	}

	int solveActiveSetValues_time(double* b, double* x, double* newSigma, double myTime)
	{
		if (solutionMethod == NOTDEFINED) { solutionMethod = ACTIVE_SET; }
		if (solutionMethod == ACTIVE_SET) { ncalls_solveSolution++; }
		int res;
		if (~bTime) { bTime = 1; }
		timePointsF.push_back(myTime);
		if (basisStatus == BASIS_OPTIMAL) {
			res = solveKKT(b, x, newSigma);
		}
		else {
			res = solveDirect(b, x, newSigma);

			if (res == 0) {
				res = setKKTmatrices(); // if not equal to 0: matrix size mismatch

				basisStatus = BASIS_OPTIMAL;
			}
		}
		return res;
	}

	int solveActiveSetSigma_time(double* b, double* x, double* newSigma, double myTime)
	{
		if (solutionMethod == NOTDEFINED) { solutionMethod = ACTIVE_SET; }
		if (solutionMethod == ACTIVE_SET) { ncalls_solveSigma++; }
		int res;
		timePointsSigma.push_back(myTime);
		if (basisStatus == BASIS_OPTIMAL) {
			res = solveKKT(b, x, newSigma);
		}
		else {
			res = solveDirect(b, x, newSigma);

			if (res == 0) {
				res = setKKTmatrices(); // if not equal to 0: matrix size mismatch

				basisStatus = BASIS_OPTIMAL;
			}
		}
		return res;
	}

	/* Active set approach for solving embedded LP */
	int solveActiveSetCPP(double* b, double* x, double* newSigma)
	{
		int res;
		if (solutionMethod == NOTDEFINED) { solutionMethod = ACTIVE_SET_CPP; }
		if (solutionMethod == ACTIVE_SET_CPP) { ncalls_solveSolution++; }
		if (solutionMethod == ACTIVE_SET_CPP) { ncalls_solveSigma++; }

		res = solveActiveSet(b, x, newSigma);

		for (int i_q = 0; i_q < n_q; i_q++) {
			if (newSigma[i_q] < -2e-9) {
				updateBasicSet(b);
				res = 2;
				break;
			}
		}
		return res;
	}

	/* Active set approach for solving embedded LP */
	int solveActiveSetCPP_time(double* b, double* x, double* newSigma, double myTime)
	{
		int res;
		if (solutionMethod == NOTDEFINED) { solutionMethod = ACTIVE_SET_CPP; }
		if (solutionMethod == ACTIVE_SET_CPP) { ncalls_solveSolution++; }
		if (solutionMethod == ACTIVE_SET_CPP) { ncalls_solveSigma++; }
		if (~bTime) { bTime = 1; }
		res = solveActiveSetCPP(b, x, newSigma);
		mytimepointfile_CPP << myTime << std::endl;
		return res;
	}

	int updateBasicSet(double* b) {
		ncalls_updateAS++;
		int res=-17;
		std::vector<double> x;
		x.resize(n_var);
		res = solveDirect(b, x.data());
		if (res < 0) { return res; }
		umfpack_di_free_numeric(&n_M_UL);
		umfpack_di_free_numeric(&n_M_LR);
		res=setKKTmatrices();
		if (res < 0) { return res; }
		basisStatus = BASIS_OPTIMAL;
		//cplex.exportModel("LPtopl2.lp");
		if (res > 0) {
			//return 2;
			ss_temp.str(std::string()); //clear stringstream
			ss_temp << "Active set update " << ncalls_updateAS << ": sepCalc = " << sepCalc << "; daeoIn = [";
			for (int i = 0; i < n_p - 1; i++) {
				ss_temp << b[i] << ", ";
			}
			ss_temp << b[n_p - 1] << "]";
			if (bTime) {
				ss_temp << "at t = " << timePointsF.back() << ".";
			}
			else {
			ss_temp << ".";
			}
			if (linear_dependent_equalities.size() > 0) {
				ss_temp << " " << linear_dependent_equalities.size() << " dependent equality constraints (that are ignored): ";
				for (int i = 0; i < linear_dependent_equalities.size() - 1; i++) {
					ss_temp << linear_dependent_equalities[i] << ", ";
				}
				ss_temp << linear_dependent_equalities[linear_dependent_equalities.size() - 1] << ".";
			}
			qevents.push(ss_temp.str());
			return UPDATE_ACTIVE_SET_SUCCESSFUL;
		}
		else {
			return UPDATE_ACTIVE_SET_FAILED;
		}
	}

	// function to check number of equations:
	// There are n_var=n_n+n_p unkowns -> need same number of equations from equality and (active) inequality constraints
	//
	int check_basic_eqns(std::vector<int>& basicEqns) {
		int degrees_of_freedom = n_n + n_p;
		linear_dependent_equalities.clear();
		for (int i = 0; i < basicEqns.size(); i++) {
			if (basicEqns[i]) {
				if (i < n_m) { linear_dependent_equalities.push_back(i); }
			}
			else {
				degrees_of_freedom -= 1;
			}
		}
		//std::cout << "Degrees of freedom: " << degrees_of_freedom << std::endl;
		if (degrees_of_freedom == 0) {
			return SIZE_EQN_SYS_OK;
		}
		else {
			return ERROR_SIZE_EQN_SYS;
		}
	}

	// check return value
	// if error, write to log file
	int CheckFlag(void *flagvalue, const char *funcname){
		int *errflag;
		/* Check if flag < 0 */
		errflag = (int *)flagvalue;
		if (*errflag < 0) {
			ss_temp.str(std::string()); //clear stringstream
			ss_temp << "DaeoToolbox error: " << funcname << "() failed with flag = " << *errflag;
			qerrors.push(ss_temp.str());
			fprintf(stderr,
				"DaeoToolbox error: %s() failed with flag = %d\n",
				funcname, *errflag);
		}
		return(0);
	}

	int returnBasicSet(int* activeSet) {
		for (int i = 0; i < n_q; i++) {
			activeSet[i] = basic_eqns[i + n_m] ? 0 : 1;
		}
		ss_temp.str(std::string()); //clear stringstream
		ss_temp << "Active set was returned.";
		qevents.push(ss_temp.str());
		return 0;
	}

	int setBasicSet(const int* activeSet) {
		std::vector<int> old_basic_eqns(basic_eqns);
		for (int i = 0; i < n_q; i++) {
			basic_eqns[i + n_m] = activeSet[i] ? 0 : 1;
		}
		setBasicSolution();
		umfpack_di_free_numeric(&n_M_UL);
		umfpack_di_free_numeric(&n_M_LR);
		int res = setKKTmatrices();
		// need to check if the new active set is valid (i.e., right number of active inequalities)
		// if not, continue with old active set and return -1
		int retval = check_basic_eqns(basic_eqns);
		if (retval < 0) {
			basic_eqns = old_basic_eqns;
			ss_temp.str(std::string()); //clear stringstream
			ss_temp << "Attempt to overwrite active set with external input failed. Old active set is used.";
			qerrors.push(ss_temp.str());
			return -1;
		}
		else {
			ss_temp.str(std::string()); //clear stringstream
			ss_temp << "Active set was overwritten with external input.";
			if (linear_dependent_equalities.size() > 0) {
				ss_temp << " " << linear_dependent_equalities.size() << " dependent equality constraints (that are ignored): ";
				for (int i = 0; i < linear_dependent_equalities.size() - 1; i++) {
					ss_temp << linear_dependent_equalities[i] << ", ";
				}
				ss_temp << linear_dependent_equalities[linear_dependent_equalities.size() - 1] << ".";
			}
			qevents.push(ss_temp.str());
			return 0;
		}
	}

	int resetDaeoToolbox() {
		basisStatus = BASIS_SUBOPTIMAL;
		std::queue<std::string> empty;
		std::swap(qevents, empty);
		std::swap(qerrors, empty);
		return 0;
	}
	//int getDer_dxdb(double* b, double* der_dxdb) {
	//	nfunctioncalls++;
	//	ncalls_derivativeSolution++;

	//	if (!b_dxdb) {
	//		updateBasicSet(b);
	//	}
	//	for (size_t i = 0; i < dxdb.size(); i++) {
	//		der_dxdb[i] = dxdb[i];
	//		//myfile << dxdb[i] << ", ";
	//	}
	//	//myfile << std::endl;
	//	return 0;
	//}

//	int getDer_dsigmadx(double* b, double* der_dsigmadx) {
//		ncalls_derivativeSigma++;
//		if (!b_dsigmadx) {
//			updateBasicSet(b);
//		}
//
//		for (size_t i = 0; i < dsigmadx.size(); i++) {
//			der_dsigmadx[i] = dsigmadx[i];
//		}
//		return 0;
//	}
//
};


void* parseDFBAmodel(const char ** params,
	//const double *x,
	const char ** variables,
	const char * objective,
	const char ** equalities,
	const char ** inequalities,
	const int p,
	const int m,
	const int n,
	const int q,
	const double *Qdiag,
	const size_t nQdiag,
	int *info) {
	std::vector<double> Qtemp;
	Qtemp.resize(nQdiag);
	for (int i = 0; i < Qtemp.size(); ++i) {
		Qtemp[i] = Qdiag[i];
	}
	dfba_cplex* dfba_ptr = new dfba_cplex(params,
		//x,
		variables,
		objective,
		equalities,
		inequalities,
		p,
		m,
		n,
		q,
		Qtemp,
		info);
	return dfba_ptr;
};

void* parseDFBAmodelLP(const char ** params,
	//const double *x,
	const char ** variables,
	const char * objective, /** linear part of objective function, i.e. c'x, is parsed from here */
	const char ** equalities,
	const char ** inequalities,
	const int p, /** number of parameters */
	const int m, /** number of equalities  */
	const int n, /** number of optimization variables */
	const int q, /** number of inequalities */
	int *info) {
	std::vector<double> Qtemp;

	dfba_cplex* dfba_ptr = new dfba_cplex(params,
		//x,
		variables,
		objective,
		equalities,
		inequalities,
		p,
		m,
		n,
		q,
		Qtemp,
		info);
	return dfba_ptr;
}
;

void closeDFBAmodel(void* object) {
	delete static_cast<dfba_cplex*>(object);
}

void solveDirect(void* object, double* b, double* values, int* status) {
	try {
		dfba_cplex* ptr = static_cast<dfba_cplex*>(object);

		status[0] = ptr->solveDirect(b, values);
	}
	catch (...) {
		ofstream newfile;
		newfile.open("Output_updatebandsolve.txt");
		newfile << "Casting does not work in updatebandsolve" << endl;
		newfile.close();
	}
};

void solveDirect_time(void* object, double* b, double* values, double myTime, int* status) {
	try {
		dfba_cplex* ptr = static_cast<dfba_cplex*>(object);

		status[0] = ptr->solveDirect_time(b, values, myTime);
	}
	catch (...) {
		ofstream newfile;
		newfile.open("Output_updatebandsolve.txt");
		newfile << "Casting does not work in updatebandsolve" << endl;
		newfile.close();
	}
};

void solveDirectSigma(void* object, double* b, double* values, double* sigma, int* status) {
	try {
		dfba_cplex* ptr = static_cast<dfba_cplex*>(object);

		status[0] = ptr->solveDirect(b, values, sigma);
	}
	catch (...) {
		ofstream newfile;
		newfile.open("Output_updatebandsolve.txt");
		newfile << "Casting does not work in updatebandsolve" << endl;
		newfile.close();
	}
};

void solveDirectSigma_time(void* object, double* b, double* values, double* sigma, double myTime, int* status) {
	try {
		dfba_cplex* ptr = static_cast<dfba_cplex*>(object);

		status[0] = ptr->solveDirect_time(b, values, sigma, myTime);
	}
	catch (...) {
		ofstream newfile;
		newfile.open("Output_updatebandsolve.txt");
		newfile << "Casting does not work in updatebandsolve" << endl;
		newfile.close();
	}
};

void solveActiveSet(void* object, double* b, double* values, double* sigma, int* status) {
	try {
		dfba_cplex* ptr = static_cast<dfba_cplex*>(object);

		status[0] = ptr->solveActiveSet(b, values, sigma);
	}
	catch (...) {
		ofstream newfile;
		newfile.open("Output_updatebandsolve.txt");
		newfile << "Casting does not work in updatebandsolve" << endl;
		newfile.close();
	}
};

void solveActiveSet_time(void* object, double* b, double* values, double* sigma, double myTime, int* status) {
	try {
		dfba_cplex* ptr = static_cast<dfba_cplex*>(object);

		status[0] = ptr->solveActiveSet_time(b, values, sigma, myTime);
	}
	catch (...) {
		ofstream newfile;
		newfile.open("Output_updatebandsolve.txt");
		newfile << "Casting does not work in updatebandsolve" << endl;
		newfile.close();
	}
};

void solveActiveSetValues_time(void* object, double* b, double* values, double* sigma, double myTime, int* status) {
	try {
		dfba_cplex* ptr = static_cast<dfba_cplex*>(object);

		status[0] = ptr->solveActiveSetValues_time(b, values, sigma, myTime);
	}
	catch (...) {
		ofstream newfile;
		newfile.open("Output_updatebandsolve.txt");
		newfile << "Casting does not work in updatebandsolve" << endl;
		newfile.close();
	}
};

void solveActiveSetSigma_time(void* object, double* b, double* values, double* sigma, double myTime, int* status) {
	try {
		dfba_cplex* ptr = static_cast<dfba_cplex*>(object);

		status[0] = ptr->solveActiveSetSigma_time(b, values, sigma, myTime);
	}
	catch (...) {
		ofstream newfile;
		newfile.open("Output_updatebandsolve.txt");
		newfile << "Casting does not work in updatebandsolve" << endl;
		newfile.close();
	}
};

void solveActiveSetCPP(void* object, double* b, double* values, double* sigma, int* status) {
	try {
		dfba_cplex* ptr = static_cast<dfba_cplex*>(object);

		status[0] = ptr->solveActiveSetCPP(b, values, sigma);
	}
	catch (...) {
		ofstream newfile;
		newfile.open("Output_updatebandsolve.txt");
		newfile << "Casting does not work in updatebandsolve" << endl;
		newfile.close();
	}
};

void solveActiveSetCPP_time(void* object, double* b, double* values, double* sigma, double myTime, int* status) {
	try {
		dfba_cplex* ptr = static_cast<dfba_cplex*>(object);

		status[0] = ptr->solveActiveSetCPP_time(b, values, sigma, myTime);
	}
	catch (...) {
		ofstream newfile;
		newfile.open("Output_updatebandsolve.txt");
		newfile << "Casting does not work in updatebandsolve" << endl;
		newfile.close();
	}
};

void updateBasicSet(void* object, double* b, int* status) {
	dfba_cplex* ptr = static_cast<dfba_cplex*>(object);
	status[0] = ptr->updateBasicSet(b);
};

void returnActiveSet(void* object, int* activeInequality, int* status) {
	dfba_cplex* ptr = static_cast<dfba_cplex*>(object);
	//status[1] = ptr->updateBasicSet(b);
	ptr->returnBasicSet(activeInequality);
};

void setActiveSet(void* object, const int* activeInequality, int* status) {
	dfba_cplex* ptr = static_cast<dfba_cplex*>(object);
	//status[1] = ptr->updateBasicSet(b);S
	ptr->setBasicSet(activeInequality);
};

void resetDaeoToolbox(void* object, int* status) {
	dfba_cplex* ptr = static_cast<dfba_cplex*>(object);
	status[0] = ptr->resetDaeoToolbox();
};

void getDer_dxdb_new(void* object, double* db, double* dx, double* dsigma, int* status) {
	dfba_cplex* ptr = static_cast<dfba_cplex*>(object);
	status[0] = ptr->solveKKT_derivatives(db, dx, dsigma);
};

//void getDer_dsigmadx(void* object, double* b, double* der_dxdb, int* status) {
//	dfba_cplex* ptr = static_cast<dfba_cplex*>(object);
//
//	status[1] = ptr->getDer_dsigmadx(b, der_dxdb);
//};

void derivative_tangent(void* object, double* db, double* dx, double* dsigma, int* status) {
	getDer_dxdb_new(object, db, dx, dsigma, status);
}; //input: db; output: dx, dsigma

void derivative_adjoint_x(void* object, double* bx, double* bb, int* status) {
	dfba_cplex* ptr = static_cast<dfba_cplex*>(object);
	status[0] = ptr->calc_adjoint_x(bx, bb);
}; //input: bx; output bb


void derivative_adjoint_sigma(void* object, double* bsigma, double* bb, int* status) {
	dfba_cplex* ptr = static_cast<dfba_cplex*>(object);
	status[0] = ptr->calc_adjoint_sigma(bsigma, bb);
}; //input: bsigma; output bb

void derivative_adjoint(void* object, double* bx, double* bsigma, double* bb, int* status) {
	dfba_cplex* ptr = static_cast<dfba_cplex*>(object);
	status[0] = ptr->calc_adjoint(bx, bsigma, bb);
}; //input: bx, bsigma; output: bb

const int get_n_p(void* object) {
	dfba_cplex* ptr = static_cast<dfba_cplex*>(object);
	return ptr->get_n_p();
};
const int get_n_n(void* object) {
	dfba_cplex* ptr = static_cast<dfba_cplex*>(object);
	return ptr->get_n_n();
};
const int get_n_m(void* object) {
	dfba_cplex* ptr = static_cast<dfba_cplex*>(object);
	return ptr->get_n_m();
};
const int get_n_q(void* object) {
	dfba_cplex* ptr = static_cast<dfba_cplex*>(object);
	return ptr->get_n_q();
};
