#ifndef NONLINEARDAEONLP_HPP
#define NONLINEARDAEONLP_HPP

#include "AdeNlp.hpp"
#include "JadeDaeoParser1.hpp"
#include "IpIpoptApplication.hpp"
#include "copyArray.hpp"
#include "Conti/Conti.hpp"
#include "Eigen/Core"
#include "ad/ad.hpp"
#include <vector>




class NonlinearDaeoNlp : public AdeNlp
{
public:
  NonlinearDaeoNlp() = delete;
  NonlinearDaeoNlp(const NonlinearDaeoNlp&) = delete;
  NonlinearDaeoNlp& operator=(const NonlinearDaeoNlp&) = delete;
  NonlinearDaeoNlp(NonlinearDaeoNlp&&) = delete;
  NonlinearDaeoNlp& operator=(NonlinearDaeoNlp&&) = delete;
  NonlinearDaeoNlp(const char** params,
                   const char** variables, const char* objective,
                   const char** equalities, const char** inequalities,
                   const int p, const int m, const int n, const int q, int* info);

  bool doParsing();

  /** Template to return the objective value */
  template<class T> bool eval_obj(Index n, const T* x, T& obj_value);

  /** Template to compute contraints */
  template<class T> bool eval_constraints(Index n, const T* x, Index m, T* g);


  /** Template to compute Lagrangian of KKT NLS (considering only active inequalities */
  template<class T> void evalLagrangian(T& lagrangian, const T* y, const T& lambda);

  /** Template to compute the  gradient of the Lagrangian of KKT NLS (considering only active inequalities */
  template<class T> void evalLagrangianGradient(T* gradient, const T* y, const T & lambda);



  /*  START  AdeNLP/Ipopt callback functions */
  bool get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
                    Index& nnz_h_lag, IndexStyleEnum& index_style) override;

  bool get_bounds_info(Index n, Number* x_l, Number* x_u,
                       Index m, Number* g_l, Number* g_u) override;

  bool get_starting_point(Index n, bool init_x, Number* x,
                          bool init_z, Number* z_L, Number* z_U,
                          Index m, bool init_lambda, Number* lambda)   override;

  bool eval_f(Index n, const Number* x, bool new_x, Number& obj_value)  override;

  bool eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f) override;

  bool eval_g(Index n, const Number* x, bool new_x, Index m, Number* g) override;

  bool eval_jac_g(Index n, const Number* x, bool new_x,
                  Index m, Index nele_jac, Index* iRow, Index* jCol,
                  Number* values) override;

  bool eval_h(Index n, const Number* x, bool new_x,
              Number obj_factor, Index m, const Number* lambda,
              bool new_lambda, Index nele_hess, Index* iRow,
              Index* jCol, Number* values) override;



  void finalize_solution(
      Ipopt::SolverReturn status,
      Index n,
      const Number* x,
      const Number* z_L,
      const Number* z_U,
      Index m,
      const Number* g,
      const Number* lambda,
      Number obj_value,
      const Ipopt::IpoptData* ip_data,
      Ipopt::IpoptCalculatedQuantities* ip_cq
      ) override;

  void evalNlsResidual(Eigen::VectorXd& residual) {
    residual.resize(nlsY.size());
    double lambda = 0;
    evalLagrangianGradient(residual.data(),nlsY.data(), lambda);
  }



  void evalNlsJacobian(Eigen::MatrixXd& jacobian) {
    const auto n = nlsY.size();
    jacobian.resize(n,n);
    std::vector<ad::gt1s<double>::type> t1_y(n);
    ad::gt1s<double>::type t1_lambda;
    std::vector<ad::gt1s<double>::type> t1_gradient(n);
    for(int i=0; i <n; ++i) ad::value(t1_y[i]) = nlsY[i];
    ad::value(t1_lambda) = 0;
    for(int j=0; j<n; ++j) {
      ad::derivative(t1_y[j]) = 1;
      evalLagrangianGradient(t1_gradient.data(),t1_y.data(), t1_lambda);
      for(int i=0; i < n; ++i) {
        jacobian(i,j) = ad::derivative(t1_gradient[i]);
      }
      ad::derivative(t1_y[j]) = 0;
    }
  }

  /** dimension of the nonlinear system of the KKT system with only active constraints */
  int dimNls() const {
    int numActiveInequalities = 0;
    for(auto active : isActiveInequality) {
      if(active) ++numActiveInequalities;
    }
    return m_n + m_m + numActiveInequalities;
  }

  /** initialize vector for nonlinear KKT system  */
  void initNlsY() {
    nlsY.resize(dimNls());
    for(int i = 0; i < m_n; ++i) nlsY[i] = m_y[i]; // init primals
    for(int i = 0; i < m_m; ++i) nlsY[m_n+i] = m_lambda[i]; // init duals of equalities
    /* init duals of active inequalities */
    int index = 0;
    for(int i = 0; i < m_q; ++i) {
      if(isActiveInequality[i]) {
        nlsY[m_n+m_m + index] = m_lambda[m_m+i];
        ++index;
      }
    }
  }

  void operator()(Eigen::Matrix<double,Eigen::Dynamic,1>& H,
                  const Eigen::Matrix<double,Eigen::Dynamic,1>& u,
                  const Conti::EvalFunction)
  {
    const auto m = u.size()-1;
    H.resize(m);
    const double lambda = u[m];
    const double *y = u.data();   
    evalLagrangianGradient(H.data(),y,lambda);
  }


   void operator()(Eigen::Matrix<double,Eigen::Dynamic,Eigen::Dynamic>& J,
                   const Eigen::Matrix<double,Eigen::Dynamic,1>& u,
                   const Conti::EvalJacobian)
   {
     const auto m = u.size()-1;
     J.resize(m,m+1);
     Eigen::Matrix<ad::gt1s<double>::type,Eigen::Dynamic,1> t1_H(m);
     Eigen::Matrix<ad::gt1s<double>::type,Eigen::Dynamic,1> t1_y(m);
     ad::gt1s<double>::type t1_lambda;
     for(int i =0; i <m;++i) ad::value(t1_y[i])=u[i];
     ad::value(t1_lambda) = u[m];
     for(int j =0; j <m;++j) {
       ad::derivative(t1_y[j]) = 1.0;
       evalLagrangianGradient(t1_H.data(),t1_y.data(),t1_lambda);
       for(int i=0; i < m; ++i) J(i,j) = ad::derivative(t1_H[i]);
       ad::derivative(t1_y[j]) = 0.0;
     }
     ad::derivative(t1_lambda) = 1.0;
     evalLagrangianGradient(t1_H.data(),t1_y.data(),t1_lambda);
     for(int i=0; i < m; ++i) J(i,m) = ad::derivative(t1_H[i]);
   }

  /*  END AdeNLP/Ipopt callback functions */
  const auto& getLambda() { return m_lambda;}
  const auto& getX() { return _x0;}
  const auto& getY() { return m_y;}
  const auto& getC() { return m_c;}
  const auto& getActiveSet() { return isActiveInequality;}
  auto getNlsY() { return nlsY;}
  void setNlsY(const Eigen::VectorXd& y) { nlsY = y;}
  void setNlsY(const double *values) {for (int i=0; i < nlsY.size();++i) nlsY[i] = values[i];}
  double getOptimizationTolerance() const {return optimizationTolerance;}
  void setOptimizationTolerance(const double value) { optimizationTolerance = value;}
  void guessActiveSet();
  void refineSolution(int *status);
  void setX0(double* values) { for(int i=0; i < m_p; ++i) _x0[i] = values[i];}
  void setX1(double* values) { for(int i=0; i < m_p; ++i) _x1[i] = values[i];}
  void getPrimalSolution(double *solution);
  void getSigma(double *sigma);
private:
  int m_p; /** number of parameters */
  int m_m; /** number of equalities */
  int m_n; /** number of variables */
  int m_q; /** number of inequalities */
  std::string m_Objective;
  AstDictionary m_dict;
  AstCalculator m_calc;        // Our grammar
  AstProgram m_ObjectiveProgram;    // Our program (AST)
  std::vector<AstProgram> m_EqualitiesProgram;
  std::vector<AstProgram> m_InequalitiesProgram;
  std::vector<std::string> m_ParameterNames;
  std::vector<std::string> m_VariableNames;
  std::vector<std::string>  m_Equalities;
  std::vector<std::string>  m_Inequalties;
  std::vector<double> _x0; //!< parameters for EO optimization variables
  std::vector<double> _x1; //!< new parameters for EO optimization variables

  std::vector<double> m_y; //!< EO optimization variables
  std::vector<double> m_lambda; //!< EO Lagrange multipliers
  std::vector<double> m_c; //!< EO constraints (equality and inequality)
  std::vector<bool> isActiveInequality; //!< mark active inequalities (dimension m_q)
  Eigen::VectorXd nlsY; //!< unknowns for KKT NLS, first are the primals, then the duals
  Eigen::VectorXd m_yy; //!< unknowns for continuation method
  Eigen::VectorXd m_initial_yy; //!< initial values for refinement by continuation method
  double optimizationTolerance = 1e-11;
};


template<class T> bool  NonlinearDaeoNlp::eval_obj(Index /* n */, const T* y, T& obj_value)
{

  std::vector<T> yy(m_n);
  copyArray(yy.data(), y, m_n);
  std::vector<T> x(m_p);
  for(int i=0; i < m_p; ++i) x[i] = _x0[i];
  AstEval<T> eval;
  eval.set_values(x, yy);
  obj_value = eval(m_ObjectiveProgram);

  return true;
}

template<class T> bool  NonlinearDaeoNlp::eval_constraints(Index /* n */, const T* y, Index /* m */, T* g)
{

  std::vector<T> yy(m_n);
  copyArray(yy.data(), y, m_n);
  std::vector<T> x(m_p);
  for(int i=0; i < m_p; ++i) x[i] = _x0[i];
  AstEval<T> eval;
  eval.set_values(x, yy);

  for (int i = 0; i < m_m; ++i) {
    g[i] = eval(m_EqualitiesProgram[i]);
  }

  for (int i = 0; i < m_q; ++i) {
    g[m_m + i] = eval(m_InequalitiesProgram[i]);
  }

  return true;
}

template<class T> void NonlinearDaeoNlp::evalLagrangian(T& lagrangian, const T* y, const T& lambda)
{
  std::vector<T> yy(static_cast<size_t>(m_n));
  copyArray(yy.data(), y, m_n);
  std::vector<T> x(static_cast<size_t>(m_p));
  AstEval<T> eval;
  for(int i=0; i < m_p; ++i) {
    x[i] = lambda * _x1[i] + (1.-lambda) * _x0[i];
  }
  eval.set_values(x, yy);
  lagrangian = 0;
  lagrangian += eval(m_ObjectiveProgram);
  for (int i = 0; i < m_m; ++i) {
    lagrangian += y[m_n+i] * eval(m_EqualitiesProgram[i]);
  }
  int index = 0;
  for(int i = 0; i < m_q; ++i) {
    if(isActiveInequality[i]) {
      lagrangian += y[m_n+m_m+index] * eval(m_InequalitiesProgram[i]);
      ++index;
    }
  }

}

template<class T> void NonlinearDaeoNlp::evalLagrangianGradient(T* gradient, const T* y, const T& lambda)
{
  std::vector<typename ad::gt1s<T>::type> t1_y(nlsY.size());
  typename ad::gt1s<T>::type t1_lambda;
  typename ad::gt1s<T>::type t1_lagrangian;
  for(int i=0; i < nlsY.size();++i){
    ad::value(t1_y[i]) = y[i];
  }
  ad::value(t1_lambda) = lambda;
  for(int i=0; i < nlsY.size();++i){
    ad::derivative(t1_y[i]) = 1;
    evalLagrangian(t1_lagrangian,t1_y.data(),t1_lambda);
    gradient[i] = ad::derivative(t1_lagrangian);
    ad::derivative(t1_y[i]) = 0;
  }
}


#endif // NONLINEARDAEONLP_HPP
