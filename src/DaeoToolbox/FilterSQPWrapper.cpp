/**
* @file FilterSQPWrapper.cpp
*
* =====================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                    \n
* =====================================================================\n
* FilterSQPWrapper - Part of DyOS                                          \n
* =====================================================================\n
* @author Mohammadsobhan Moazemi Goodarzi, Fady Assassa
* @date 31.10.2012
*/

#include "FilterSQPWrapper.hpp"
#include <algorithm>

//#include <boost/filesystem.hpp>
//#include <boost/weak_ptr.hpp>
//#include <boost/shared_ptr.hpp>

#include <string>
#include <cassert>
#include <iostream>
#include "Array.hpp"
#include <memory>



// initialization of statics. Instead of doing it in every test suite we do it here for all together.


//std::fstream FilterSQPWrapper::m_staticFileReader;

typedef void (*objfunPtrFilterSQP_t)(double*, // x
                                   int *, // n
                                   double*, // f
                                   double*, // user
                                   int*, // iuser
                                   int* // flag
                                   );

typedef void (*Wdotd_t)(int *, double*, double*, int* , double*);

/** typedef of function pointer for call of FilterSQP - only for better readability of the filterSQPFunc typedef
  * This is a C function.
  */
typedef void (*confunPtrFilterSQP_t)(double*, // x
                                   int *, // n
                                   int *, // m
                                   double*, // c
                                   double*, // a
                                   int*, //la
                                   double*, // user
                                   int*, // iuser
                                   int * // flag
                                   );

/** typedef of function pointer for call of FilterSQP - only for better readability of the filterSQPFunc typedef
  * This is a C function.
  */
typedef void (*gradientPtrFilterSQP_t)(int *, // n
                                     int *, // m
                                     int *, // mxa
                                     double*, // x
                                     double*, // a
                                     int*, //la
                                     int *, // maxa
                                     double*, // user
                                     int*, //iuser
                                     int * //flag
                                     );

/** typedef of function pointer for call of FilterSQP - only for better readability of the filterSQPFunc typedef
  * This is a C function.
  */
typedef void (*hessianPtrFilterSQP_t)(double*, // x
                                    int *, // n
                                    int *, // m
                                    int *, // phase
                                    double*, // lam
                                    double*, // ws
                                    int*, // lws
                                    double*, // user
                                    int*, // iuser
                                    int *, // l_hess
                                    int *, // li_hess
                                    int * // flag
                                    );


/**
* typedef of FORTRAN routine from filterSQP DLL for dynamic loading
* This is a FORTRAN function called from C.
*/
extern "C" void filtersqp_(int *, // n
                                      int *, // m
                                      int *, // kmax
                                      int *, // maxa
                                      int *, // maxf
                                      int *, // mlp
                                      int *, // mxwk
                                      int *, // mxiwk
                                      int *, // iprint
                                      int *, // nout
                                      int *, // ifail
                                      double*, // rho
                                      double*, // x
                                      double*, // c
                                      double*, // f
                                      double*, // fmin
                                      double*, // blo,
                                      double*, // bup,
                                      double*, // s,
                                      double*, // a,
                                      int*, // la,
                                      double*, // ws,
                                      int*, // lws,
                                      double*, // lam,
                                      char*, //cstype,
                                      double*, // user,
                                      int*, // iuser,
                                      int *, // max_iter,
                                      int*, // istat,
                                      double*, // rstat,
                                      objfunPtrFilterSQP_t,
                                      confunPtrFilterSQP_t,
                                      gradientPtrFilterSQP_t,
                                      hessianPtrFilterSQP_t,
                                      Wdotd_t
                                      //int //length of cstype
                                      );



void filterSqpLoader(int n, int numConstraints, int kmax, int maxa, int maxf,
               int mlp, int mxwk, int mxiwk, int iprint,
               int nout, int &ifail, double rho, utils::Array<double> &x,
               utils::Array<double> &c, double& f, double fmin, utils::Array<double> &blo,
               utils::Array<double> &bup, utils::Array<double> &s, utils::Array<double> &a,
               utils::Array<int> &la, utils::Array<double> &ws, utils::Array<int> &lws,
               utils::Array<double> &lam, utils::Array<char> &cstype, int cstypelen,
               utils::Array<double> &user, int* iuser, int max_iter,
               utils::Array<int> &istat, utils::Array<double> &rstat,
               objfunPtrFilterSQP_t objfun_ptr, confunPtrFilterSQP_t confun_ptr,
               gradientPtrFilterSQP_t gradient_ptr, hessianPtrFilterSQP_t hessian_ptr,
               Wdotd_t wdot_ptr){

  filtersqp_(&n, &numConstraints, &kmax, &maxa, &maxf, &mlp, &mxwk, &mxiwk,
                      &iprint, &nout, &ifail, &rho, x.getData(), c.getData(), &f, &fmin, blo.getData(),
                      bup.getData(), s.getData(), a.getData(), la.getData(), ws.getData(),
                      lws.getData(), lam.getData(), cstype.getData(),
                      user.getData(), iuser, &max_iter, istat.getData(), rstat.getData(),
                      objfun_ptr, confun_ptr, gradient_ptr, hessian_ptr, wdot_ptr
                      );
}


/**
* @brief constructor: initialize resources which are independent of current problem (e.g. open output files)
*
* @param optMD pointer to an OptimizerMetaData object used for the optimization
* @param genInt pointer to an integrator object used for the optimization
*/
FilterSQPWrapper::FilterSQPWrapper() {}

//void FilterSQPWrapper::openLogFile()
//{
//  std::string summary = "filterSQP.summary";
//  //delete old file
//  std::fstream deleter(summary.c_str(), std::ios_base::out);
//  deleter.close();

//  m_staticFileReader.open(summary.c_str(), std::ios_base::in);
//  // if file is still held open by filterSQP,
//  // set file pointer to the end of the file
//  std::string content;
//  std::streampos pos;
//  //find the end of the file
//  while(!m_staticFileReader.eof()){
//    pos = m_staticFileReader.tellg();
//    std::getline(m_staticFileReader, content);
//  }
//  // reset eof bit and fail bit
//  m_staticFileReader.clear();
//  // set position of the file reader to the last valid position
//  m_staticFileReader.seekg(pos);
//}
//void FilterSQPWrapper::closeLogFile()
//{
//  m_staticFileReader.close();
//}



/**
* @brief solve optimization problem
*/
int FilterSQPWrapper::solve(AdeNlp *nlp)
{
  Nlp = nlp;
  Ipopt::TNLP::IndexStyleEnum index_style;
  int  n,m,nnz_jac_g,nnz_h_lag;
  Nlp->get_nlp_info(n,m,nnz_jac_g,nnz_h_lag,index_style);


  int* iuser = reinterpret_cast<int*>(this);
   // the order of the constraints doesn't matter. We will assume first linear constraints and then nonlinear.
  const int kmax = n; // maximum size of null-space
  const int maxf = 100; // maximum size of the filter; value according to manual
  const int mlp = 500; // maximum level parameter for resolving degeneracy in bqpd
  const int mxwk = 30000;  // max size of real workspace
  const int mxiwk = 30000; // max size of integer worlspace
  const int iprint = 0; // print flag (todo: this should be set in the options)
  const int nout = 6; // output channel (6 = screen)
  int ifail = 0; // fail flag indicating successful run
  double rho = 10; // initial trust-region radius; default is 10
  static bool warmstart = false;
  if(warmstart) {
      ifail = -1;
      rho = 1.5;
  }

  warmstart = true;

  const double fmin = -1e20; // lower bound on the objective value (todo could be set by the user)
  // the bounds arrays include decisionvariable bounds and constraint bounds.
  // first linear constraints then nonlinear constraints



  static std::unique_ptr<TripletMatrix> static_JacAsTriplet(new TripletMatrix(m,n,nnz_jac_g));
  JacAsTriplet = static_JacAsTriplet.get();

  static std::unique_ptr<TripletMatrix> static_HessianAsTriplet(new TripletMatrix(n,n,nnz_h_lag));
  HessianAsTriplet = static_HessianAsTriplet.get();

  static utils::Array<double> s(n+m, 1.0); // scale factors; thus no scaling selected
  static utils::Array<double> ws(mxwk, 0.0); // real workspace
  static utils::Array<int> lws(mxiwk, 0); // integer workspace
  static utils::Array<double> lam(n+m, 0.0); // lagrange multipliers
  static utils::Array<char> cstype(m, 'N');
  const int cstypelen = m;
  // was hier?
  static utils::Array<double> user(1, 0.0); // real workspace passed through user routines
  int max_iter = 300; //user supplied iteration unit for SQP solver //todo: option
  static utils::Array<int> istat(100, 1); // integer space for solution statistics
  static utils::Array<double> rstat(100, 0.0); // real space for solution statistics
  //FilterSqpStat filterStat(istat.getData(),rstat.getData());


  static utils::Array<double> x(n,0.0);
  static utils::Array<double> z_L(n,0.0);
  static utils::Array<double> z_U(n,0.0);
  static utils::Array<double> lambda(m,0.0);
  Nlp->get_starting_point(n,true,x.getData(),true,z_L.getData(),z_U.getData(),m,
                          true,lambda.getData());
  for(int i=0; i<n; i++){
      lam[i]=z_L[i]-z_U[i];
  }
  for(int i=0; i<m; i++){
      lam[i+n]=-lambda[i];
  }
  static utils::Array<double> c(m,0.0);
  static utils::Array<double> lowerBounds(n+m,0.0);
  static utils::Array<double> upperBounds(n+m,0.0);
  double f=0;
  double *x_l = lowerBounds.getData();
  double *x_u = upperBounds.getData();
  double *g_l = x_l + n;
  double *g_u = x_u + n;

  Nlp->get_bounds_info(n,x_l,x_u,m,g_l,g_u);

  // sparsity pattern of Jacobian and Hessian
  Nlp->eval_jac_g(n,x.getData(),true,m,nnz_jac_g,JacAsTriplet->i,JacAsTriplet->p,0);
  JacAsTriplet->nz = nnz_jac_g;
  utils::Array<double> lambda2(m,0.0);
  Nlp->eval_h(n,x.getData(),true,1.0,m,lambda2.getData(),true,nnz_h_lag,
              HessianAsTriplet->i,HessianAsTriplet->p,0);



  // Matrix to store gradient (objective gradient and constraint Jacobian)
  int maxa = n + m*n;
  static utils::Array<double> a(maxa, 0.0); // stores the objective gradient and constraint normals
  static utils::Array<int> la(1,0);
  la[0]= n;


  //openLogFile();


  filterSqpLoader(n,
    m,
    kmax,
    maxa,
    maxf,
    mlp,
    mxwk,
    mxiwk,
    iprint,
    nout,
    ifail,
    rho,
    x,
    c,
    f,
    fmin,
    lowerBounds,
    upperBounds,
    s,
    a,
    la,
    ws,
    lws,
    lam,
    cstype,
    cstypelen,
    user,
    iuser,
    max_iter,
    istat,
    rstat,
    &FilterSQPWrapper::objfun,
    &FilterSQPWrapper::confun,
    &FilterSQPWrapper::gradient,
    &FilterSQPWrapper::hessian,
    &FilterSQPWrapper::Wdotd);

  //closeLogFile();



  // call finalize_solution
  Ipopt::SolverReturn status = Ipopt::SUCCESS;



  // map bound and constraint multipliers to Ipopt format
  for(int i=0; i<n; i++) {
      if (lam[i] >= 0) {
          z_L[i] = lam[i];
          z_U[i] = 0;
      }
      else {
          z_U[i] = -lam[i];
          z_L[i] = 0;
      }
  }

  for(int i=0; i<m; i++) {
      lambda[i] = - lam[i+n];
  }


  Nlp->finalize_solution(status, n, x.getData(), z_L.getData(), z_U.getData(),
                         m, c.getData(), lambda.getData(),f,nullptr,nullptr);




  return ifail;
}


/** @brief evaluate the linear and nonlienar constraints of the problem
@param[in] x x(n) the value of the current variable
@param[in] n number of variables
@param[in] m number of constraints (linear + nonlinear)
@param[out] c vector of linear and nonlinear constraints
@param[in/out] a the jacobian vector storing the nonzeros of the jacobian
@param[in] la column indices for a and pointer to start of each row
@param[in] user workspace (real)
@param[in] iuser workspace (integer)
@param[out] flag Set to 1 if arithemtics exception occured in confun, 0 otherwise. If an arithemtic
exception occured, then the constraints must not be modified by the function
*/
void FilterSQPWrapper::confun(double *x,
                              int *n,
                              int *m,
                              double *c,
                              double *a,
                              int *la,
                              double *user,
                              int *iuser,
                              int *flag)
{
    *flag = 0;
    FilterSQPWrapper *wrapper = reinterpret_cast<FilterSQPWrapper*>(iuser);
    wrapper->Nlp->eval_g(*n,x,true,*m,c);
}

/** @brief evaluate the objective funtion of the problem
@param[in] x x(n) the value of the current variable
@param[in] n number of variables
@param[out] f objective function
@param[in] user workspace (real)
@param[in] iuser workspace (integer)
@param[out] flag Set to 1 if arithemtics exception occured in objfun, 0 otherwise. If an arithemtic
exception occured, then the objective must not be modified by the function
*/
void FilterSQPWrapper::objfun(double *x,
                              int *n,
                              double *f,
                              double *user,
                              int *iuser,
                              int *flag)
{
    *flag = 0;
    FilterSQPWrapper* wrapper = reinterpret_cast<FilterSQPWrapper*>(iuser);
    wrapper->Nlp->eval_f(*n,x,true,*f);
}
/** @brief evaluate the gradient of the objective and the constraints (linear/nonlinear)
@param[in] n number of variables
@param[in] m number of constraints (linear + nonlinear)
@param[in] x x(n) the value of the current variable
@param[in/out] a the jacobian vector storing the nonzeros of the jacobian
@param[in] la column indices for a and pointer to start of each row
@param[in] maxa maximum size of a
@param[in] user workspace (real)
@param[in] iuser workspace (integer)
@param[out] flag Set to 1 if arithemtics exception occured in gradient, 0 otherwise. If an arithemtic
exception occured, then the gradient must not be modified by the function
*/
void FilterSQPWrapper::gradient(int *n,
                                int *m,
                                int *mxa,
                                double *x,
                                double *a,
                                int *la,
                                int *maxa,
                                double *user,
                                int *iuser,
                                int *flag)
{
    *flag = 0;
    FilterSQPWrapper* wrapper = reinterpret_cast<FilterSQPWrapper*>(iuser);
    int nele_jac = wrapper->JacAsTriplet->nz;
    int *iRow = wrapper->JacAsTriplet->i;
    int *jCol = wrapper->JacAsTriplet->p;
    double *values = wrapper->JacAsTriplet->x;
    for(int i=0; i<(*n) * (*m + 1); i++) {
        a[i]= 0;
    }
    wrapper->Nlp->eval_grad_f(*n,x,true,a); // first column is gradient of objective
    wrapper->Nlp->eval_jac_g(*n,x,true,*m,nele_jac,
                             iRow,jCol,values); //evaluate Jacobian in Triplet-form
    //transform to dense matrix starting in Column 1
    for(int i=0; i < nele_jac; i++){
        int index = (*n) +  jCol[i]  + (*n)*iRow[i];
        a[index] = values[i];
    }


}

/** @brief evaluate the hessian matrix of the problem
@param[in] x x(n) the value of the current variable
@param[in] n number of variables
@param[in] m number of constraints (linear + nonlinear)
@param[in] phase indicates what kind of hessian matrix is required; phase = 2 Hessian
of the lagrangian, phase = 1 hessian of the langrangian without the objective hessian
@param[in] lam vector of lagrange multipliers (size (n+m))
@param[out] ws workspace of the hessian, passed by Wdotd (real)
@param[out] lws workspace of the hessian, passed by Wdotd (integer)
@param[in] user workspace (real)
@param[in] iuser workspace (integer)
@param[in/out] l_hess On enry: max. space allows for hessian storage in ws.
On exit: actual amount of hessian storage used in ws. (double)
@param[in/out] li_hess On enry: max. space allows for hessian storage in ws.
On exit: actual amount of hessian storage used in ws. (integer)
@param[out] flag Set to 1 if arithemtics exception occured in hessian, 0 otherwise. If an arithemtic
exception occured, then the Hessian must not be modified by the function
*/
void FilterSQPWrapper::hessian(double *x,
                               int *n,
                               int *m,
                               int *phase,
                               double *lam,
                               double *ws,
                               int *lws,
                               double *user,
                               int *iuser,
                               int *l_hess,
                               int *li_hess,
                               int *flag)
{
    *flag = 0;
    FilterSQPWrapper* wrapper = reinterpret_cast<FilterSQPWrapper*>(iuser);
    int nele_hess = wrapper->HessianAsTriplet->nz;
    int *iRow = wrapper->HessianAsTriplet->i;
    int *jCol = wrapper->HessianAsTriplet->p;
    double *values = wrapper->HessianAsTriplet->x;
    for(int i=0; i<(*n) * (*n); i++) {
        ws[i]= 0;
    }
    double obj_factor = (*phase == 2)?1.0:0.0;
    utils::Array<double> lambda(*m);
    for(int i=0; i< *m; i++){
        lambda[i] = -lam[(*n) + i];
    }

    wrapper->Nlp->eval_h(*n,x,true,obj_factor,*m,lambda.getData(),true,nele_hess,
                         iRow,jCol,values);

    for(int i=0; i< nele_hess; i++) {
        int index1 = (*n) * iRow[i] + jCol[i];
        int index2 = (*n) * jCol[i] + iRow[i];
        ws[index1] = values[i];
        ws[index2] = values[i];
    }
    *l_hess = (*n)*(*n);
    *li_hess = 1;

}

void FilterSQPWrapper::Wdotd(int *nn, double *d, double *ws, int *lws, double *v)
{
    // compute v = W * d
    const int n= *nn;
    for(int iRow = 0; iRow < n; iRow++) {
        v[iRow] = 0;
        for(int jCol = 0; jCol < n; jCol++ ) {
            int index = n*jCol+iRow;
            v[iRow] += ws[index]*d[jCol];
        }
    }
}


/** @brief calculates the real workspace for SQP, QP and linear algebra solvers according to the manual
* @param optionaFlag switches between sparse and dense
* @param n number of variables
* @param m number of constraints (linear + nonlinear)
* @param mlp maximum level parameter for solving degeneracy in bqpd
* @param maxf maximum size of the filter
* @param kmax maximum sizfe of the null-space
* @return size of the real workspace for SQP, QP and linear algebra solvers
*/
int FilterSQPWrapper::maxRealWS(const int n,
                                const int m,
                                const int mlp,
                                const int maxf,
                                const int kmax) const
{
  int mxm1;
  if (n<=m+1)
    mxm1 = n;
  else
    mxm1 = m+1;
  // dense linear algebra solver
  return 16*n + 8*m + mlp + 8*maxf + kmax*(kmax+9)/2 + mxm1*(mxm1+3)/2;
}

/** @brief calculates the integer workspace for SQP, QP and linear algebra solvers according to the manual
* @param optionaFlag switches between sparse and dense
* @param n number of variables
* @param m number of constraints (linear + nonlinear)
* @param mlp maximum level parameter for solving degeneracy in bqpd
* @param kmax maximum sizfe of the null-space
* @return size of the integer workspace for SQP, QP and linear algebra solvers
*/
int FilterSQPWrapper::maxIntWS(const int n,
                               const int m,
                               const int mlp,
                               const int kmax) const
{

  int mxm1;
  if (n<=m+1)
    mxm1 = n;
  else
    mxm1 = m+1;

  return  4*n + 3*m + mlp + 100 + kmax + mxm1;

}

