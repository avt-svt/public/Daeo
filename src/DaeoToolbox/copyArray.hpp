#ifndef COPY_ARRAY_HPP
#define COPY_ARRAY_HPP


template<typename T> void copyArray(T* dest, const T* src, const int n){
  for (int i=0; i < n; ++i) {
    dest[i] = src[i];
  }
}


#endif // COPY_ARRAY_HPP