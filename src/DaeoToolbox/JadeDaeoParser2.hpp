#ifndef JADE_DAEO_PARSER2_HPP
#define JADE_DAEO_PARSER2_HPP


#if defined(_MSC_VER)
# pragma warning(disable: 4345)
#endif

#include "antlr4-runtime-wrapper.h"
#include "DaeoExprParser.h"
#include "DaeoExprLexer.h"
#include "DaeoExprBaseVisitor.h"
#include "DaeoToolbox.hpp"
#include <map>
#include <iostream>
#include <sstream>
#include <string>
#include <cassert>
#include <ilcplex/ilocplex.h>
#include <memory>


#define PARSING_SUCCESSFUL 10
#define PARSING_ERROR_VARIABLES -11
#define	PARSING_ERROR_OBJECTIVE -12
#define PARSING_ERROR_EQUALITIES -13
#define PARSING_ERROR_INEQUALITIES -14

//#define DEBUG_PARSER

class CreateCplexVisitor : public sm::DaeoExprBaseVisitor {
public:

	virtual antlrcpp::Any visitArithmetic_expression(sm::DaeoExprParser::Arithmetic_expressionContext *ctx) override {
		const std::size_t numAddOp = ctx->add_op().size();
		const std::size_t numTerm = ctx->term().size();
		// if numAddOp < numTerm, then leading node is a term (otherwise a operator)
		IloNumExpr result = visit(ctx->term(0));
		const bool leadingNodeIsOp = (numAddOp == numTerm);
		if (leadingNodeIsOp && ctx->add_op(0)->op->getType() == sm::DaeoExprParser::SUB) {
			result = -result;
		}
		for (std::size_t i = 1; i < numTerm; ++i) {
			const std::size_t iTerm = i;
			const std::size_t iOp = leadingNodeIsOp ? iTerm : iTerm - 1;
			IloNumExpr rhs = visitTerm(ctx->term(iTerm));
			if (ctx->add_op(iOp)->op->getType() == sm::DaeoExprParser::ADD)
				result += rhs;
			else
				result -= rhs;
		}
    return antlrcpp::Any{result};

	}


	virtual antlrcpp::Any visitTerm(sm::DaeoExprParser::TermContext *ctx) override {
		const std::size_t numPrimaries = ctx->primary().size();
		IloNumExpr result = visit(ctx->primary(0));
		for (std::size_t i = 1; i < numPrimaries; ++i) {
			const std::size_t iOp = i - 1;
			IloNumExpr rhs = visit(ctx->primary(i));
			if (ctx->mul_op(iOp)->op->getType() == sm::DaeoExprParser::MUL)
				result = result * rhs;
			else
				result = result / rhs;
		}
    return  antlrcpp::Any{result};
	}



	virtual antlrcpp::Any visitPrimary_unsigned_number(sm::DaeoExprParser::Primary_unsigned_numberContext *ctx) override {
    double value = std::stod(ctx->UNSIGNED_NUMBER()->getText());
		IloNumExpr result(*iloEnv, value);
    return  antlrcpp::Any{result};
	}

	virtual antlrcpp::Any visitPrimary_name(sm::DaeoExprParser::Primary_nameContext *ctx) override {
		IloNumExpr result = (*iloVariables)[(*symbolTable).at(ctx->getText())];
    return antlrcpp::Any{result};
	}

	virtual antlrcpp::Any visitPrimary_parens_arithmetic_expression(sm::DaeoExprParser::Primary_parens_arithmetic_expressionContext *ctx) override {
		return visit(ctx->arithmetic_expression());
	}


	void setSymbolTable(std::map<std::string, int> *table) {
		symbolTable = table;
	}
	void setIloEnv(IloEnv *env) {
		iloEnv = env;
	}

	void setIloVariables(IloNumVarArray* variables) {
		iloVariables = variables;
	}

	std::map<std::string, int> *symbolTable;
	IloNumVarArray *iloVariables;
	IloEnv *iloEnv;
};




class JadeDaeoParser2
{
public:
  ~JadeDaeoParser2() {

  }
	JadeDaeoParser2(const char ** params,
		const double *x,
		const char ** variables,
		const char * objective,
		const char ** equalities,
		const char ** inequalities,
		const int p,
		const int m,
		const int n,
		const int q) :	
    m_n(n),
		m_m(m),
    m_p(p),
    m_q(q),
    m_params(p),
    m_param_values(p),
    m_variables(n),
    m_equalities(m),
    m_inequalities(q)
	{
		
		copyArray(m_param_values.data(), x, m_p);
		copyStringArray(m_params, params, m_p);
		copyStringArray(m_variables, variables, m_n);
		copyString(m_objective, objective);
		copyStringArray(m_equalities, equalities, m_m);
		copyStringArray(m_inequalities, inequalities, m_q);


	}


  int createCplex(IloEnv* iloEnv, IloModel& cplexmodel, IloNumVarArray& cplexvar, IloRangeArray&  cplexrng, std::vector<double> Qdiag)
	{
    env = iloEnv;
    var = IloNumVarArray(*env);
	double lowerBound = -INFINITY;
	double upperBound = INFINITY;
	try {
		int index = 0;
		// add bounds and names for parameters
		for (int i = 0; i < m_p; ++i) {
			cplexvar.add(IloNumVar(*env, lowerBound, upperBound));
			cplexvar[i].setName(m_params[i].c_str());
			cplexrng.add(m_param_values[i] <= cplexvar[i] <= m_param_values[i]);
			std::stringstream ss;
			ss << "p" << i + 1;
			cplexrng[i].setName(ss.str().c_str());
			symbolTable[m_params[i]] = index++;
		}

		// add bounds and names for variables
		for (int i = 0; i < m_n; ++i) {

			cplexvar.add(IloNumVar(*env, lowerBound, upperBound));
			cplexvar[i + m_p].setName(m_variables[i].c_str());
			symbolTable[m_variables[i]] = index++;
		}
		var = cplexvar;
	}
	catch (...) { return PARSING_ERROR_VARIABLES; }
		// add objective
		try {
			//linear term
			IloNumExpr linearTerm = getIloNumExpr(m_objective.c_str());			
			//std::cout << linearTerm << std::endl;
			//quadratic term
			IloExpr quadraticTerm(*env);
			if (Qdiag.size() > 0) {

				IloInt nvars = cplexvar.getSize();
				if (Qdiag.size() != nvars){
					std::cout << "Length of vector Qdiag does not match number of CPLEX variables" << std::endl;
				}
								
				for (IloInt i = 0; i < nvars; ++i) {
					quadraticTerm += 0.5*Qdiag[i]*cplexvar[i] * cplexvar[i];
				}
				//std::cout << quadraticTerm << std::endl;
			}
			// objective
			IloObjective obj = IloMinimize(*env, linearTerm+quadraticTerm, "objective");
			cplexmodel.add(obj);
		}
		catch (...) {
			return PARSING_ERROR_OBJECTIVE;
		}

		// add equality constraints
		try {
			for (int i = 0; i < m_m; ++i) {
				IloNumExpr currentConstraint = getIloNumExpr(m_equalities[i].c_str());
				cplexrng.add(0.0 <= currentConstraint <= 0.0);
				std::stringstream ss;
				ss << "ceq" << i + 1;
				const std::string tmp = ss.str();
				const char* cstr = tmp.c_str();
				cplexrng[i+m_p].setName(cstr);
			}
		}
		catch (...) {
			return PARSING_ERROR_EQUALITIES;
		}

		// add inequality constraints
		try {
			for (int i = 0; i < m_q; ++i) {
				double upperBound = INFINITY;
				IloNumExpr currentConstraint = getIloNumExpr(m_inequalities[i].c_str());
				cplexrng.add(0 <= currentConstraint <= upperBound);
				std::stringstream ss;
				ss << "cineq" << i + 1;
				const std::string tmp = ss.str();
				const char* cstr = tmp.c_str();
				cplexrng[i + m_m + m_p].setName(cstr);
				#ifdef DEBUG_PARSER
					std::cout << "inequality[" << i << "] : 0 <= " << m_inequalities[i].c_str() << " <= INF" << std::endl;
				#endif
			}
		}
		catch (...) {
			return PARSING_ERROR_INEQUALITIES;
		}
		cplexmodel.add(cplexrng);
		return PARSING_SUCCESSFUL;
	}

	void printCplex() {
		cplex->exportModel("Model2.LP");
	}

private:
	int m_n;
	int m_m;
	int m_p;
	int m_q;
	std::vector<std::string> m_params;
	std::vector<double> m_param_values;
	std::vector<std::string> m_variables;
	//std::vector<double> m_variables_l;
	//std::vector<double> m_variables_u;
	std::string m_objective;
	std::vector<std::string> m_equalities;
	//std::vector<double> m_equalities_rhs;
	std::vector<std::string> m_inequalities;

	//std::vector<double> m_constraints_u;
	std::map<std::string, int> symbolTable;

	template<typename T> void copyArray(T* dest, const T* src, const int n) {
		for (int i = 0; i < n; ++i) {
			dest[i] = src[i];
		}
	}

	void copyString(std::string& dest, const char* src)
	{
		std::string tmp(src);
		dest = tmp;
	}

	void copyStringArray(std::vector<std::string>& dest, const char* src[], const int n) {
		for (int i = 0; i<n; ++i) {
			#ifdef DEBUG_PARSER
				std::cout << "i = " << i << ", src = " << src[i] <<  std::endl;
			#endif
			copyString(dest[i], src[i]);
		}
	}

	antlr4::tree::ParseTree* getParseTree(const char *model)
	{
		input.reset(new antlr4::ANTLRInputStream(model));
		lexer.reset(new sm::DaeoExprLexer(input.get()));
		tokens.reset(new antlr4::CommonTokenStream(lexer.get()));
		parser.reset(new sm::DaeoExprParser(tokens.get()));
		parser->getInterpreter<antlr4::atn::ParserATNSimulator>()->setPredictionMode(antlr4::atn::PredictionMode::SLL);
		parser->removeErrorListeners();
		parser->setErrorHandler(std::make_shared<antlr4::BailErrorStrategy>());
		return parser->arithmetic_expression();

	}

	IloNumExpr getIloNumExpr(const char* model)
	{
		CreateCplexVisitor visitor;
		visitor.setSymbolTable(&symbolTable);
    visitor.setIloEnv(env);
		visitor.setIloVariables(&var);
		antlr4::tree::ParseTree* tree = getParseTree(model);
		IloNumExpr result = visitor.visit(tree);
		//std::cout << result << std::endl;
		return result;
	}

  IloEnv *env;
	std::unique_ptr<IloCplex> cplex;

	IloObjective   obj;
	IloNumVarArray var;
	IloRangeArray  rng;

	std::shared_ptr<sm::DaeoExprParser> parser;
	std::shared_ptr<sm::DaeoExprLexer> lexer;
	std::shared_ptr<antlr4::ANTLRInputStream> input;
	std::shared_ptr<antlr4::CommonTokenStream> tokens;

};

#endif //JADE_DAEO_PARSER2_HPP
 