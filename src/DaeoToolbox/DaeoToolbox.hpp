#ifndef DFBACPLEX_H_
#define DFBACPLEX_H_

#include <stddef.h>

#ifdef __cplusplus
extern "C" {
#endif // __cplusplus




#define SOLUTION_DIRECT_SUCCESSFUL 0 //20
#define SOLUTION_ERROR_CPLEX -21

#define SOLUTION_KKT_SUCCESSFUL 0 //30
#define SOLUTION_ERROR_KKT_X -31
#define SOLUTION_ERROR_KKT_SIGMA -32
#define SOLUTION_ERROR_KKT_ALL -33

#define SOLUTION_LIN_EQN_SYS_SUCCESSFUL 0 //40
#define SOLUTION_ERROR_LIN_EQN_SYS -41
#define	SOLUTION_ERROR_LIN_EQN_SYS_TRANSPOSE -42
// does the basic solution have enough active inequalities? (after successful call to CPLEX this should be the case)
#define ERROR_SIZE_EQN_SYS -43
#define SIZE_EQN_SYS_OK 0

#define TANGENT_SUCCESSFUL 0 //50
#define TANGENT_ERROR_X -51
#define	TANGENT_ERROR_SIGMA -52
#define TANGENT_ERROR_ALL -53

#define ADJOINT_SUCCESSFUL 0 //60
#define ADJOINT_ERROR_X -61
#define	ADJOINT_ERROR_SIGMA -62
#define ADJOINT_ERROR_ALL -63

#define UPDATE_ACTIVE_SET_SUCCESSFUL 0 //70
#define UPDATE_ACTIVE_SET_FAILED -71
#ifdef DFBACPLEX_EXPORTS
#if defined _WIN32 || defined WIN32
#define DECLSPEC __declspec(dllexport)
#else
#if defined __GNUC__ || defined __clang__
#define DECLSPEC __attribute__ ((visibility ("default")))
#else
#define DECLSPEC
#endif
#endif
#endif

#ifndef DFBACPLEX_EXPORTS
#if defined _WIN32 || defined WIN32
#define DECLSPEC __declspec(dllimport)
#else
#define DECLSPEC
#endif
#endif

	// parse to QP
	DECLSPEC void* parseDFBAmodel(const char ** params,
		//const double *x,
		const char ** variables,
		const char * objective, /** linear part of objective function, i.e. c'x, is parsed from here */
		const char ** equalities,
		const char ** inequalities,
		const int p, /** number of parameters */
		const int m, /** number of equalities  */
		const int n, /** number of optimization variables */
		const int q, /** number of inequalities */
		const double *Qdiag, /** vector of diagonal elements of matrix Q in QP objective function of form c'x + 1/2 x'Qx */
		const size_t nQdiag, /** size of Qdiag vector; in LP case: 0 */
		int *info);

	DECLSPEC void closeDFBAmodel(void* object);
	DECLSPEC void solveDirect(void* object, double* b, double* values, int* status);
	DECLSPEC void solveDirect_time(void* object, double* b, double* values, double myTime, int* status);
	DECLSPEC void solveDirectSigma(void* object, double* b, double* values, double* sigma, int* status);
	DECLSPEC void solveDirectSigma_time(void* object, double* b, double* values, double* sigma, double myTime, int* status);
	DECLSPEC void solveActiveSet(void* object, double* b, double* values, double* sigma, int* status);
	DECLSPEC void solveActiveSet_time(void* object, double* b, double* values, double* sigma, double myTime, int* status);
	DECLSPEC void solveActiveSetSigma_time(void* object, double* b, double* values, double* sigma, double myTime, int* status);
	DECLSPEC void solveActiveSetValues_time(void* object, double* b, double* values, double* sigma, double myTime, int* status);
	DECLSPEC void solveActiveSetCPP(void* object, double* b, double* values, double* sigma, int* status);
	DECLSPEC void solveActiveSetCPP_time(void* object, double* b, double* values, double* sigma, double myTime, int* status);
	DECLSPEC void updateBasicSet(void* object, double* b, int* status);
	DECLSPEC void returnActiveSet(void* object, int* activeInequality, int* status);
	DECLSPEC void setActiveSet(void* object, const int* activeInequality, int* status);
	DECLSPEC void resetDaeoToolbox(void* object, int* status);
	DECLSPEC void derivative_tangent(void* object, double* db, double* dx, double* dsigma, int* status); //input: db; output: dx, dsigma
	DECLSPEC void derivative_adjoint_sigma(void* object, double* bsigma, double* bb, int* status); //input: bsigma; output bb
	DECLSPEC void derivative_adjoint_x(void* object, double* bx, double* bb, int* status); //input: bx; output bb
	DECLSPEC void derivative_adjoint(void* object, double* bx, double* bsigma, double* bb, int* status); //input: bx, bsigma; output: bb

	DECLSPEC void getDer_dxdb_new(void* object, double* db, double* dx, double* dsigma, int* status);

	DECLSPEC const int get_n_p(void* object);
	DECLSPEC const int get_n_n(void* object);
	DECLSPEC const int get_n_m(void* object);
	DECLSPEC const int get_n_q(void* object);

	// Nonlinear stuff
	DECLSPEC void* parsenNonlinearDAEOmodel(const char** params,
		const char** variables,
		const char* objective,
		const char** equalities,
		const char** inequalities,
		const int p, /** number of parameters (x) */
		const int m, /** number of equalities  */
		const int n, /** number of variables */
		const int q, /** number of inequalities */
		int* info);

	DECLSPEC void closeNonlinearDAEO(void* object);

	DECLSPEC void solveNonlinearActiveSet(void* object, double* x, double* values, double* sigma, int* status);

	DECLSPEC void updateNonlinearActiveSet(void* object, double* x, int* status);

#ifdef __cplusplus
}
#endif
#endif // DFBACPLEX_H_
