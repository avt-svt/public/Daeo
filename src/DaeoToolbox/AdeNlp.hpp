#ifndef ADE_NLP_HPP__
#define ADE_NLP_HPP__

#include "IpTNLP.hpp"

using Ipopt::Index;
using Ipopt::Number;


enum SolverReturn {
  SUCCESS,
  FAIL
};

class AdeNlp : public Ipopt::TNLP
{
public:
  /** default constructor **/
  AdeNlp() {}

  /** default destructor (virtual) */
  virtual ~AdeNlp() {}

  /** Method to return some info about the nlp */
  virtual bool get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
      Index& nnz_h_lag, IndexStyleEnum& index_style) = 0;


  /** Method to return the bounds for my problem */
  virtual bool get_bounds_info(Index n, Number* x_l, Number* x_u,
      Index m, Number* g_l, Number* g_u) = 0;

  /** Method to return the starting point for the algorithm */
  virtual bool get_starting_point(Index n, bool init_x, Number* x,
      bool init_z, Number* z_L, Number* z_U,
      Index m, bool init_lambda,
      Number* lambda) = 0;

  /** Original method from Ipopt to return the objective value */
  /** remains unchanged */
  virtual bool eval_f(Index n, const Number* x, bool new_x, Number& obj_value) = 0;

  /** Original method from Ipopt to return the gradient of the objective */
  /** remains unchanged */
  virtual bool eval_grad_f(Index n, const Number* x, bool new_x, Number* grad_f) = 0;

  /**  Original method from Ipopt to return the constraint residuals */
  /** remains unchanged */
  virtual bool eval_g(Index n, const Number* x, bool new_x, Index m, Number* g) = 0;

  /** Original method from Ipopt to return:
   *   1) The structure of the jacobian (if "values" is NULL)
   *   2) The values of the jacobian (if "values" is not NULL)
   */
  /** remains unchanged */
  virtual bool eval_jac_g(Index n, const Number* x, bool new_x,
      Index m, Index nele_jac, Index* iRow, Index *jCol,
      Number* values) = 0;

  /** Original method from Ipopt to return:
   *   1) The structure of the hessian of the lagrangian (if "values" is NULL)
   *   2) The values of the hessian of the lagrangian (if "values" is not NULL)
   */
  /** remains unchanged */
  virtual bool eval_h(Index n, const Number* x, bool new_x,
      Number obj_factor, Index m, const Number* lambda,
      bool new_lambda, Index nele_hess, Index* iRow,
      Index* jCol, Number* values) = 0;

  //@}

  ///** @name Solution Methods */
  ////@{
  ///** This method is called when the algorithm is complete so the TNLP can store/write the solution */
  //virtual void finalize_solution(
  //  SolverReturn               status,
  //  Index                      n,
  //  const Number* x,
  //  const Number* z_L,
  //  const Number* z_U,
  //  Index                      m,
  //  const Number* g,
  //  const Number* lambda,
  //  Number                     obj_value,
  //  const Ipopt::IpoptData* ip_data,
  //  Ipopt::IpoptCalculatedQuantities* ip_cq
  //) = 0;

private:
  /** Copy Constructor */
  AdeNlp(const AdeNlp&);

  /** Overloaded Equals Operator */
  void operator=(const AdeNlp&);
	
};

#endif
