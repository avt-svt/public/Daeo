#include "NonlinearDaeoNlp.hpp"
#include "initStringVector.hpp"
#include "ad/ad.hpp"
#include "Eigen/Dense"

NonlinearDaeoNlp::NonlinearDaeoNlp(const char** params,
                                   const char** variables,  const char* objective,
                                   const char** equalities,  const char** inequalities,
                                   const int p,  const int m,  const int n,  const int q,  int* info) :
  m_p(p), m_m(m), m_n(n), m_q(q),
  m_calc(m_dict), m_EqualitiesProgram(m), m_InequalitiesProgram(q),
  _x0(p), _x1(p),  m_y(n), m_lambda(m+q), m_c(m+q),
  isActiveInequality(q)
{
  m_Objective = objective;
  initStringVector(m_ParameterNames, params, p);
  initStringVector(m_VariableNames, variables, n);
  initStringVector(m_Equalities, equalities, m);
  initStringVector(m_Inequalties, inequalities, q);
  bool parseSucceded = doParsing();
  info[0] = parseSucceded ? 0 : 1;
  info[1] = 0;
}


bool NonlinearDaeoNlp::doParsing() {
  m_dict.initialize(m_ParameterNames, m_VariableNames);

  // parse objective program
  std::string::const_iterator iter = m_Objective.begin();
  std::string::const_iterator end = m_Objective.end();
  boost::spirit::ascii::space_type space;
  bool r = phrase_parse(iter, end, m_calc, space, m_ObjectiveProgram);
  if (!r) return false;

  //parse equalities programs
  for (int i = 0; i < m_m; ++i) {
    std::string::const_iterator iter = m_Equalities[i].begin();
    std::string::const_iterator end = m_Equalities[i].end();
    boost::spirit::ascii::space_type space;
    bool r = phrase_parse(iter, end, m_calc, space, m_EqualitiesProgram[i]);
    if (!r) return false;
  }

  //parse inequalities programs
  for (int i = 0; i < m_q; ++i) {
    std::string::const_iterator iter = m_Inequalties[i].begin();
    std::string::const_iterator end = m_Inequalties[i].end();
    boost::spirit::ascii::space_type space;
    bool r = phrase_parse(iter, end, m_calc, space, m_InequalitiesProgram[i]);
    if (!r) return false;
  }

  return true;
}

bool NonlinearDaeoNlp::get_nlp_info(Index& n, Index& m, Index& nnz_jac_g,
                                    Index& nnz_h_lag, IndexStyleEnum& index_style)
{
  n = m_n;
  m = m_m + m_q;

  // For our convenience we store the Jacobian as a dense matrix. Hence, it contains n*m nonzeros
  nnz_jac_g = n * m;

  // the hessian is also dense and has n*n total nonzeros, but we
  // only need the lower left corner (since it is symmetric)
  nnz_h_lag = n * (n - 1) / 2 + n;

  // use the C style indexing (0-based)
  index_style = C_STYLE;

  return true;
}

bool NonlinearDaeoNlp::get_bounds_info(Index n, Number* x_l, Number* x_u,
                                       Index /* m */, Number* g_l, Number* g_u)  {
  for (Index i = 0; i < n; ++i) {
    x_l[i] = -1e20;
    x_u[i] = 1e20;
  }
  for (Index i = 0; i < m_m; ++i) {
    g_l[i] = 0;
    g_u[i] = 0;
  }
  for (Index i = m_m; i < m_m + m_q; ++i) {
    g_l[i] = 0;
    g_u[i] = 1e20;
  }
  return true;
}



bool NonlinearDaeoNlp::get_starting_point(Index n, bool init_x, Number* x,
                                          bool init_z, Number* z_L, Number* z_U,
                                          Index m, bool init_lambda,
                                          Number* lambda) {
  if (init_x) {
    for (Index i = 0; i < n; ++i) x[i] = 1;
  }
  if (init_z) {
    for (Index i = 0; i < n; ++i) {
      z_L[i] = 0;
      z_U[i] = 0;
    }
  }
  if (init_lambda) {
    for (Index i = 0; i < m; ++i) lambda[i] = 1;
  }
  return true;
}


bool NonlinearDaeoNlp::eval_f(Index n, const Number* x, bool /* new_x */, Number& obj_value)
{
  eval_obj(n, x, obj_value);

  return true;
}

bool NonlinearDaeoNlp::eval_grad_f(Index n, const Number* x, bool /* new_x */, Number* grad_f)
{
  ad::gt1s<Number>::type* x_ad = new ad::gt1s<Number>::type[n];
  ad::gt1s<Number>::type obj_value_ad;

  for (Index i = 0; i < n; ++i) {
    for (Index j = 0; j < n; ++j) {
      ad::value(x_ad[j]) = x[j];
      ad::derivative(x_ad[j]) = 0.0;
    }

    ad::derivative(x_ad[i]) = 1.0;

    eval_obj(n, x_ad, obj_value_ad);

    grad_f[i] = ad::derivative(obj_value_ad);
  }

  delete[] x_ad;

  return true;
}

bool NonlinearDaeoNlp::eval_g(Index n, const Number* x, bool /* new_x */, Index m, Number* g)
{
  eval_constraints(n, x, m, g);

  return true;
}

bool NonlinearDaeoNlp::eval_jac_g(Index n, const Number* x, bool /* new_x */,
                                  Index m, Index /* nele_jac */, Index* iRow, Index* jCol,
                                  Number* values)
{
  if (values == nullptr) {
    // return the structure of the jacobian,
    // assuming that the Jacobian is dense

    Index idx = 0;
    for (Index i = 0; i < m; i++) {
      for (Index j = 0; j < n; j++)
      {
        iRow[idx] = i;
        jCol[idx++] = j;
      }
    }
  }
  else {
    ad::gt1s<Number>::type* x_ad = new ad::gt1s<Number>::type[n];
    ad::gt1s<Number>::type* g_ad = new ad::gt1s<Number>::type[m];

    for (Index i = 0; i < n; ++i) {
      for (Index j = 0; j < n; ++j) {
        ad::value(x_ad[j]) = x[j];
        ad::derivative(x_ad[j]) = 0.0;
      }

      ad::derivative(x_ad[i]) = 1.0;

      eval_constraints(n, x_ad, m, g_ad);

      for (Index j = 0; j < m; ++j) {
        values[j * n + i] = ad::derivative(g_ad[j]);
      }
    }

    delete[] g_ad;
    delete[] x_ad;
  }

  return true;
}

bool NonlinearDaeoNlp::eval_h(Index n, const Number* x, bool /* new_x */,
                              Number obj_factor, Index m, const Number* lambda,
                              bool /* new_lambda */, Index nele_hess , Index* iRow,
                              Index* jCol, Number* values)
{
  if (values == nullptr) {
    // return the structure. This is a symmetric matrix, fill the lower left
    // triangle only.

    // the hessian for this problem is actually dense
    Index idx = 0;
    for (Index row = 0; row < n; row++) {
      for (Index col = 0; col <= row; col++) {
        iRow[idx] = row;
        jCol[idx] = col;
        idx++;
      }
    }

    assert(idx == nele_hess);
  }
  else {
    ad::gt1s<ad::gt1s<Number>::type >::type* x_ad = new ad::gt1s<ad::gt1s<Number>::type >::type[n];
    ad::gt1s<ad::gt1s<Number>::type >::type* g_ad = new ad::gt1s<ad::gt1s<Number>::type >::type[m];
    ad::gt1s<ad::gt1s<Number>::type >::type obj_value_ad;

    Index idx = 0;
    for (Index i = 0; i < n; ++i) {
      for (Index j = 0; j <= i; ++j) {
        values[idx] = 0.0;

        for (Index k = 0; k < n; ++k) {
          ad::value(ad::value(x_ad[k])) = x[k];
          ad::derivative(ad::value(x_ad[k])) = 0.0;
          ad::value(ad::derivative(x_ad[k])) = 0.0;
          ad::derivative(ad::derivative(x_ad[k])) = 0.0;
        }

        ad::derivative(ad::value(x_ad[i])) = 1.0;
        ad::value(ad::derivative(x_ad[j])) = 1.0;

        eval_obj(n, x_ad, obj_value_ad);
        values[idx] += obj_factor * ad::derivative(ad::derivative(obj_value_ad));

        eval_constraints(n, x_ad, m, g_ad);
        for (Index l = 0; l < m; ++l) {
          values[idx] += lambda[l] * ad::derivative(ad::derivative(g_ad[l]));
        }
        ++idx;
      }
    }
    assert(idx == nele_hess);

    delete[] g_ad;
    delete[] x_ad;
  }

  return true;
}



void NonlinearDaeoNlp::finalize_solution(Ipopt::SolverReturn status,
                                         Index n, const Number* x, const Number* /* z_L */, const Number* /* z_U */,
                                         Index m, const Number* g, const Number* lambda,
                                         Number /* obj_value */,
                                         const Ipopt::IpoptData* /* ip_data */,
                                         Ipopt::IpoptCalculatedQuantities* /* ip_cq */)
{
  if(status != Ipopt::SolverReturn::SUCCESS) return;
  for (int i = 0; i < n; ++i) m_y[i] = x[i];
  for (int i = 0; i < m; ++i) m_lambda[i] = lambda[i];
  for (int i = 0; i < m; ++i) m_c[i] = g[i];
  return;
}

/** Assume the optimizer has been called before and set the required variables */
void NonlinearDaeoNlp::guessActiveSet()
{
  using std::abs;
  const int maxActiveInequalities = m_n - m_m;
  int numActiveInequalties=0;
  for(int i=0; i < m_q; ++i){
    if(abs(m_c[m_m+i]) <  1e-6  && numActiveInequalties < maxActiveInequalities) {
      isActiveInequality[i] = true;
      ++numActiveInequalties;
    }
    else {
      isActiveInequality[i] = false;
    }
  }

}

void NonlinearDaeoNlp::refineSolution(int *status)
{
  const int maxIter = 5; // number of simplified Newton iterations
  initNlsY();
  const auto n = nlsY.size();
  Eigen::VectorXd deltaY;
  Eigen::VectorXd residual;
  Eigen::MatrixXd jacobian;

  evalNlsJacobian(jacobian);
  Eigen::HouseholderQR<Eigen::MatrixXd> qr(n,n);
  qr.compute(jacobian);
  for(int i=0; i < maxIter; ++i){
    evalNlsResidual(residual);
    deltaY = qr.solve(-residual);
    nlsY += deltaY;
    if(deltaY.norm()/(1+nlsY.norm()) < 1e-14) {
      for(int j=0; j < m_n; ++j) m_y[j] = nlsY[j];
      for(int j=0; j < m_m; ++j) m_lambda[j] = nlsY[m_n+j];
      for(int index=0,j=0; j < m_q; ++j)
        m_lambda[m_m+j] = isActiveInequality[j]?nlsY[m_n+m_m+index++]:0;
      status[1] = 0;
      break;
    }
    else {
      status[1] = 1;
    }
  }

}

void NonlinearDaeoNlp::getPrimalSolution(double *solution)
{
  for(int i=0; i < m_n; ++i){
    solution[i] = nlsY[i];
  }
}

void NonlinearDaeoNlp::getSigma(double *sigma)
{
   AstEval<double> eval;
   std::vector<double> y(m_n);
   getPrimalSolution(y.data());
   eval.set_values(_x0,y);
   for(int index=0,i=0; i < m_q; ++i){
     if(isActiveInequality[i]) {
       sigma[i] = -nlsY[m_n + m_m + index++];
     }
     else {
       sigma[i] = eval(m_InequalitiesProgram[i]);
     }
   }
}
