#include "DaeoToolbox.hpp"
#include "DaeoToolboxAdditional.hpp"
#include "gtest/gtest.h"
#include <vector>
#include <iostream>
#include <array>

struct ToyProblemToplTestData {
  static constexpr int n = 6;
  static constexpr int p = 1;
  static constexpr int m = 4;
  static constexpr int q = 4;
  const char* params[p] = { "b_1" };
  const char* variables[n] = { "x1","x2","sp1","sl2","sl3","sp4" };
  // DAEO objective function
  const char* objective = "x1+x2";
  // DAEO constraints with bounds
  const char* equalities[m] = { 
    "x1 - sp1 + 2 + b_1",
    "x1 + sl2 - 3",
    "x2 - 0.5 * x1 + sl3 - 2",
    "x2 + 0.5 * x1 - sp4 + 3" };
  const char* inequalities[q] = { "sp1", "sl2", "sl3", "sp4" };
  struct TestData {
    double x[p];
    double y[n];
    void verify(double* z) {
      for (int i = 0; i < n; ++i) {
        EXPECT_NEAR(y[i], z[i], 1e-5);
      }
    }
  };

  static constexpr int lenTestSet = 4;
  std::array<TestData, lenTestSet> data =  
  { 1.,  -3, -1.5, 0., 6., 2, 0. ,
   0.5,-2.5, -1.75, 0., 5.5, 2.5, 0.,
   0 , -2, -2, 0, 5, 3, 0,
   -0.5,  -1.5, -2.25, 0., 4.5, 3.5, 0. };
 
};



TEST(DaeoToolbox, runToyProblemToplWithCplex)
{

  std::cout << "------------ Solving model ToyProblemTopl ------------" << std::endl;
  ToyProblemToplTestData testData;
  const int n = testData.n;
  const int p = testData.p;
  const int m = testData.m;
  const int q = testData.q;


  auto params = testData.params;
  auto variables = testData.variables;
  auto objective = testData.objective; 
  auto equalities = testData.equalities;
  auto inequalities = testData.inequalities;

  int info[2];
  double x[p] = { 1.0 };
  double y[n];
  double sigma[q];
  std::vector<double> QdiagLP;


  void* toyEO = parseDFBAmodel(params,
	  variables,
	  objective,
	  equalities,
	  inequalities,
	  p,
	  m,
	  n,
	  q,
	  QdiagLP.data(),
	  QdiagLP.size(),
		info);

  for(int i =0 ; i < testData.lenTestSet; ++i)
  {
    solveActiveSet(toyEO, testData.data[i].x, y, sigma, info);
    for (int i = 0; i < q; i++) {
      if (sigma[i] < 0) {
        updateBasicSet(toyEO, x, info);
        solveActiveSet(toyEO, testData.data[i].x, y, sigma, info);
        break;
      }
    }
    testData.data[i].verify(y);
  }

  closeDFBAmodel(toyEO);
}



TEST(DaeoToolbox, runToyProblemToplWithNLPSolver)
{

  std::cout << "------------ Solving model ToyProblemTopl ------------" << std::endl;
  ToyProblemToplTestData testData;
  const int n = testData.n; // number of optimization variables
  const int p = testData.p; // number of parameters 
  const int m = testData.m; // number of equalities
  const int q = testData.q; // number of inequalities


  auto params = testData.params;
  auto variables = testData.variables;
  auto objective = testData.objective;
  auto equalities = testData.equalities;
  auto inequalities = testData.inequalities;

  int info[2];
  double x[p] = { 1.0 };
  double y[n];
  double sigma[q];

  void* toyEO = parsenNonlinearDAEOmodel(params,
    variables,
    objective,
    equalities,
    inequalities,
    p,
    m,
    n,
    q,
    info);

  for (int i = 0; i < testData.lenTestSet; ++i)
  {
    solveNonlinearActiveSet(toyEO, testData.data[i].x, y, sigma, info);
    for (int j = 0; j < q; j++) {
      if (sigma[j] < 0) {
        updateNonlinearActiveSet(toyEO, x, info);
        solveNonlinearActiveSet(toyEO, testData.data[i].x, y, sigma, info);
        break;
      }
    }
    testData.data[i].verify(y);
  }

  closeNonlinearDAEO(toyEO);
}
