#include "JadeDaeoParser2.hpp"


 

int main()
{
	const int n = 7; // number of DAEO variables
	const int p = 1; // number of DAEO parameters
	const int m = 6; // number of DAEO constraints


	const char* params[p] = { "vupt" };
	const char* variables[n] = { "v1","v2","v3","v4","v5","v6","v7" };
	const double variables_l[n] = { -1e20,0,0,0,-1e20,0,0 };
	const double variables_u[n] = { 1e20,1e20,1e20,1e20,1e20,1e20,1e20};

	const char* objective = "-v6";

	const char* constraints[m] = {
		"vupt - 1.0 * v1",
		"v1 - v5 - v2",
		"v5 - v2 + v4 - v6",
		"v2 -v3",
		"v3 - v4",
		"v3 + v4 - v7 " };
	const double constraints_l[m] = { 0,0,0,0,0,0 };
	const double constraints_u[m] = { 0,0,0,0,0,0 };

	//JadeDaeoParser2 parser(params, variables, variables_l, variables_u,
	//	objective, constraints, constraints_l, constraints_u, p, m, n);
	//parser.doParsing();
	//parser.createCplex();
	//parser.printCplex();





	return 0;

}