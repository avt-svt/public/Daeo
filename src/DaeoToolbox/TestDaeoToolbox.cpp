#include "DaeoToolbox.hpp"
#include "DaeoToolboxAdditional.hpp"
#include <vector>
#include <iostream>

#define printExternal

#define RES_OK 1
#define RES_DEV 0
#define RES_ERROR -1

// functions to create the example models for illustration and testing
void* create_object_SmallLinearToyExample();
void* create_object_SmallLinearToyExample(int createQP);
void* create_object_SmallLinearToyExample(int createQP, std::vector<double>& b0, std::vector<double>& bend);

void* create_object_Spirallus();
void* create_object_Spirallus(int createQP);
void* create_object_Spirallus(int createQP, std::vector<double>& b0, std::vector<double>& bend);

void* create_object_CG();
void* create_object_CG(int createQP);
void* create_object_CG(int createQP, std::vector<double>& b0, std::vector<double>& bend);

void* create_object_Ecoli();
void* create_object_Ecoli(int createQP);
void* create_object_Ecoli(int createQP, std::vector<double>& b0, std::vector<double>& bend);

//functions for testing
int compare_tangents(void * object, const double h, double* x);
int compare_adjoints(void * object, const double h, double* x);
int compare_LP_QP(void* ext_obj_LP, void* ext_obj_QP, double* x);
int compare_KKT_direct(void* ext_obj_KKT, void* ext_obj_direct, double* x);

int
main(int argc, char **argv)
{	
	int res = 0;
	std::vector<double> b0;
	std::vector<double> bend;
	bool b_create_QP = true;
	void* ext_obj = create_object_CG(b_create_QP,b0,bend); //create external object for testing
	void* ext_obj_2 = create_object_CG(!b_create_QP); //second external object for comparing LP and QP solutions
	void* ext_obj_direct = create_object_CG(b_create_QP); //external object for direct solution method

	//select checks to be run on example
	bool b_compare_LP_QP = false; //compare solution and switching function values between LP and QP
	bool b_compare_KKT_direct = false; //compare KKT embedding to direct solution approach
	bool b_check_tangents = false; //check derivatives obtained in tangent mode
	bool b_check_adjoints = false; //check derivatives obtained in adjoint mode

	//parameters for checks
	const double h = 1.e-3;//spacing for finite forward differences
	int nsteps = 2; //number of steps between b0 and bend
	const double eventTol = 1e-6; //tolerance for active set changes

	int p= get_n_p(ext_obj); //number of parameters for current example
	int n = get_n_n(ext_obj); //number of variables for current example
	int q = get_n_q(ext_obj); //number of inequality constraints for current example
	std::vector<double> b = b0; //parameter vector that is varied between b0 and bend

	std::vector<double> y(n, 0.0);
	std::vector<double> sigma(q, 0.0);
	int info[2];
	
	std::vector<int> activeInequality(q, 0);

	int i = 0;
	while (i < nsteps && res>=0) {
		for (int j = 0; j < p; j++) {
			b[j] = b0[j] - i / (double)nsteps*(b0[j] - bend[j]);
		}

		std::cout << "x = [";
		for (int j = 0; j < p - 1; j++) {
			std::cout << b[j] << ", ";
		}
		std::cout << b[p - 1] << "]." << std::endl;

		solveActiveSet(ext_obj, b.data(), y.data(), sigma.data(), info);
		for (int i = 0; i < q; i++) {
			if (sigma[i] < -eventTol) {
				updateBasicSet(ext_obj, b.data(), info);
				std::cout << "Active set update!" << std::endl;
				break;
			}			
		}
		returnActiveSet(ext_obj, activeInequality.data(),info);
		setActiveSet(ext_obj, activeInequality.data(), info);
		if (b_compare_KKT_direct) { 
			if(compare_KKT_direct(ext_obj, ext_obj_direct, b.data())<0) res=-1; 
		}


		if (b_compare_LP_QP) {
			if (b_create_QP) {
				if (compare_LP_QP(ext_obj_2, ext_obj, b.data()) < 0) res = -1;
			}
			else {
				if (compare_LP_QP(ext_obj, ext_obj_2, b.data()) < 0) res = -1;
			}
		}
		
		if (b_check_tangents) {
			if (compare_tangents(ext_obj, h, b.data()) < 0) res = -1;
		}

		if (b_check_adjoints) {
			if (compare_adjoints(ext_obj, h, b.data()) < 0) res = -1;
		}
		res = 0;
		std::cout << "--------------------------------------------------------------------------------------" << std::endl;
		i += 1;
	}
	if (res < 0) {
		std::cout << "ABORTED DUE TO ERRORS." << std::endl;
	}
	else {
		std::cout << "FINISHED WITHOUT ERRORS." << std::endl;
	}
	closeDFBAmodel(ext_obj_2);
	closeDFBAmodel(ext_obj_direct);
	closeDFBAmodel(ext_obj);
	return 0;
}

int compare_derivatives(void * object, const int n, const int p, const int m, const int q) {
	int info[2];
	double dev;
	// [y,sigma]=f(b)

	//variables for derivative calculation in tangent mode
	//input
	std::vector<double> db(p,0.0);
	//output
	std::vector<double> dy(n, 0.0);
	std::vector<double> dsigma(q, 0.0);

	//variables for derivative calculation in adjoint mode
	//input
	std::vector<double> by(n, 0.0);
	std::vector<double> bsigma(q, 0.0);
	//output
	std::vector<double> bb(p, 0.0);
	//after further calculation
	std::vector<double> dy_adjoint(n, 0.0);
	std::vector<double> dsigma_adjoint(q, 0.0);

	std::cout << "Verification of derivatives by comparison of tangent and adjoint mode" << std::endl;
	std::cout << "Output format: der. w. tangent, der. w. adjoint, squared deviation" << std::endl;
	for (int der_ind = 0; der_ind < p; der_ind++) {
		//derivatives in tangent mode
		std::fill(db.begin(), db.end(), 0.0);
		db[der_ind] = 1;
		std::cout << "dx = [";
		for (int j = 0; j < p-1; j++) {
			std::cout << db[j] << ", ";
		}
		std::cout << db[p-1] << "]" << std::endl;

		derivative_tangent(object, db.data(), dy.data(), dsigma.data(), info);		
		for (int i = 0; i < n; i++) {
			//std::cout << "dy" << i << "/db" << der_ind << " = " << dy[i] << std::endl;
		}
		for (int i = 0; i < q; i++) {
			//std::cout << "dsigma" << i << "/db" << der_ind << " = " << dsigma[i] << std::endl;
		}

		//derivatives in adjoint mode
		for (int i = 0; i < n; i++) {
			std::fill(by.begin(), by.end(), 0.0);
			by[i] = 1.0;
			derivative_adjoint_x(object, by.data(), bb.data(), info);
			dy_adjoint[i] = bb[der_ind];
			//std::cout << "dy" << i << "/db" << der_ind << " = " << bb[der_ind] << std::endl;
		}
		for (int i = 0; i < q; i++) {
			std::fill(bsigma.begin(), bsigma.end(), 0.0);
			bsigma[i] = 1.0;
			derivative_adjoint_sigma(object, bsigma.data(), bb.data(), info);
			dsigma_adjoint[i] = bb[der_ind];
			//std::cout << "dsigma" << i << "/db" << der_ind << " = " << bb[der_ind] << std::endl;
		}

		for (int i = 0; i < n; i++) {
			dev = (dy[i] - dy_adjoint[i])*(dy[i] - dy_adjoint[i]);
			if (dev < 1e-6) {
				std::cout << "dy" << i << "/db" << der_ind << " = " << dy[i] << ", " << dy_adjoint[i] << ", " << dev << std::endl;
			}
			else {
				std::cout << "dy" << i << "/db" << der_ind << " = " << dy[i] << ", " << dy_adjoint[i] << ", " << dev << " <<<<<<<<<<<<<<<" << std::endl;
				std::cin.get();
			}
		}
		for (int i = 0; i < q; i++) {
			dev = (dsigma[i] - dsigma_adjoint[i])*(dsigma[i] - dsigma_adjoint[i]);
			if (dev < 1e-6) {
				std::cout << "dsigma" << i << "/db" << der_ind << " = " << dsigma[i] << ", " << dsigma_adjoint[i] << ", " << dev << std::endl;
			}
			else {
				std::cout << "dsigma" << i << "/db" << der_ind << " = " << dsigma[i] << ", " << dsigma_adjoint[i] << ", " << dev << " <<<<<<<<<<<<<<<" << std::endl;
				std::cin.get();
			}
		}
	}
	return 0;
};

int compare_tangents(void * object, const double h, double* x) {
	int res=RES_OK;
	int info[2];

	// [y,sigma]=f(x)
	const int p = get_n_p(object);
	const int n = get_n_n(object);
	const int m = get_n_m(object);
	const int q = get_n_q(object);

	std::vector<std::vector<double>> jac_y_fd(p, std::vector<double>(n));
	std::vector<std::vector<double>> jac_sigma_fd(p, std::vector<double>(q));

	std::vector<std::vector<double>> jac_y_tangent(p, std::vector<double>(n));
	std::vector<std::vector<double>> jac_sigma_tangent(p, std::vector<double>(q));

	std::vector<double> values;
	values.resize(n);
	std::vector<double> sigma;
	sigma.resize(q);

	std::vector<double> x_vec(p, 0.0);
	for (int i = 0; i < p; i++) {
		x_vec[i] = x[i];
	}
	solveActiveSet(object, x_vec.data(), values.data(), sigma.data(), info);

	std::vector<double> x_fd(p, 0.0);
	std::vector<double> y_plus(n, 0.0);
	std::vector<double> sigma_plus(q, 0.0);

	std::vector<double> dy_fd(n, 0.0);
	std::vector<double> dsigma_fd(q, 0.0);
	for (int i = 0; i < p; i++) {
		x_fd = x_vec;
		x_fd[i] = x_fd[i]*(1.0 + h);
		solveActiveSet(object, x_fd.data(), y_plus.data(), sigma_plus.data(), info);
		for (int j = 0; j < dy_fd.size(); j++) {
			dy_fd[j] = (y_plus[j] - values[j]) / (x_fd[i]*h);
		}
		for (int j = 0; j < dsigma_fd.size(); j++) {
			dsigma_fd[j] = (sigma_plus[j] - sigma[j]) / (x_fd[i] * h);
		}
		jac_y_fd[i] = dy_fd;
		jac_sigma_fd[i] = dsigma_fd;
	}

	//variables for derivative calculation in tangent mode
	solveActiveSet(object, x_vec.data(), values.data(), sigma.data(), info);
	////input
	std::vector<double> db(p, 0.0);
	////output
	std::vector<double> dy(n, 0.0);
	std::vector<double> dsigma(q, 0.0);

	for (int der_ind = 0; der_ind < p; der_ind++) {
		//derivatives in tangent mode
		std::fill(db.begin(), db.end(), 0.0);
		db[der_ind] = 1.0;

		derivative_tangent(object, db.data(), dy.data(), dsigma.data(), info);
		jac_y_tangent[der_ind] = dy;
		jac_sigma_tangent[der_ind] = dsigma;

	}

	// Evluation of results (comparing jacobian matrices with each other).
	double max_sq_dev_y = -1.0;
	int i_max_y, j_max_y = -1;

	double max_sq_dev_sigma = -1.0;
	int i_max_sigma, j_max_sigma = -1;

	double temp = 0.0;

	for (int i = 0; i < jac_y_fd.size(); i++) {
		for (int j = 0; j < jac_y_fd[i].size(); j++) {
			temp = (jac_y_fd[i][j] - jac_y_tangent[i][j])*(jac_y_fd[i][j] - jac_y_tangent[i][j]);
			if (temp > max_sq_dev_y) {
				max_sq_dev_y = temp;
				i_max_y = i;
				j_max_y = j;
			}
		}
	}
	for (int i = 0; i < jac_sigma_fd.size(); i++) {
		for (int j = 0; j < jac_sigma_fd[i].size(); j++) {
			temp = (jac_sigma_fd[i][j] - jac_sigma_tangent[i][j])*(jac_sigma_fd[i][j] - jac_sigma_tangent[i][j]);
			if (temp > max_sq_dev_sigma) {
				max_sq_dev_sigma = temp;
				i_max_sigma = i;
				j_max_sigma = j;
			}
		}
	}

	if ((max_sq_dev_y < h) && (max_sq_dev_sigma < h)) { std::cout << ">>> Comparing tangents to finite differences: OK!" << std::endl; res = RES_OK; }
	else { std::cout << ">>> Comparing tangents to finite differences: Error!" << std::endl; res = RES_ERROR; }
	//std::cout << "Max. sq. dev. w.r.t. solution: " << max_sq_dev_y << " with dy_" << j_max_y << "/dp_" << i_max_y << "|fd = " << jac_y_fd[i_max_y][j_max_y] << " and ..|tangent = " << jac_y_tangent[i_max_y][j_max_y] << std::endl;
	//std::cout << "Max. sq. dev. w.r.t. sigma: " << max_sq_dev_sigma << " with dsigma_" << j_max_sigma << "/dp_" << i_max_y << "|fd = " << jac_sigma_fd[i_max_y][j_max_y] << " and ..|tangent = " << jac_sigma_tangent[i_max_y][j_max_y] << std::endl;
	if (max_sq_dev_y < h) {
		std::cout << "Jacobian (via tangents) of solution ok." << std::endl;
	}
	else {
		std::cout << "Jacobian (via tangents) of solution via tangents potentially wrong." << std::endl;
		std::cout << "Max. sq. dev. w.r.t. solution: " << max_sq_dev_y << " with dy_" << j_max_y << "/dp_" << i_max_y << "|fd = " << jac_y_fd[i_max_y][j_max_y] << " and ..|tangent = " << jac_y_tangent[i_max_y][j_max_y] << std::endl;
	}
	if (max_sq_dev_sigma < h) {
		std::cout << "Jacobian (via tangents) of sigma ok." << std::endl;
	}
	else {
		std::cout << "Jacobian (via tangents) of sigma via tangents potentially wrong." << std::endl;
		std::cout << "Max. sq. dev. w.r.t. sigma: " << max_sq_dev_sigma << " with dsigma_" << j_max_sigma << "/dp_" << i_max_sigma << "|fd = " << jac_sigma_fd[i_max_sigma][j_max_sigma] << " and ..|tangent = " << jac_sigma_tangent[i_max_sigma][j_max_sigma] << std::endl;
	}
	return res;
};

int compare_adjoints(void * object, const double h, double* x) {
	int res = RES_OK;
	int info[2];

	// [y,sigma]=f(x)
	const int p = get_n_p(object);
	const int n = get_n_n(object);
	const int m = get_n_m(object);
	const int q = get_n_q(object);

	std::vector<std::vector<double>> jac_y_fd(p, std::vector<double>(n));
	std::vector<std::vector<double>> jac_sigma_fd(p, std::vector<double>(q));

	std::vector<std::vector<double>> jac_y_adjoint(p, std::vector<double>(n));
	std::vector<std::vector<double>> jac_sigma_adjoint(p, std::vector<double>(q));

	std::vector<double> values;
	values.resize(n);
	std::vector<double> sigma;
	sigma.resize(q);

	std::vector<double> x_vec(p, 0.0);
	for (int i = 0; i < p; i++) {
		x_vec[i] = x[i];
	}
	solveActiveSet(object, x_vec.data(), values.data(), sigma.data(), info);

	std::vector<double> x_fd(p, 0.0);
	std::vector<double> y_plus(n, 0.0);
	std::vector<double> sigma_plus(q, 0.0);

	std::vector<double> dy_fd(n, 0.0);
	std::vector<double> dsigma_fd(q, 0.0);
	for (int i = 0; i < p; i++) {
		x_fd = x_vec;
		x_fd[i] = x_fd[i]*(1.0 + h);
		solveActiveSet(object, x_fd.data(), y_plus.data(), sigma_plus.data(), info);
		for (int j = 0; j < dy_fd.size(); j++) {
			dy_fd[j] = (y_plus[j] - values[j]) / (x_fd[i] * h);
		}
		for (int j = 0; j < dsigma_fd.size(); j++) {
			dsigma_fd[j] = (sigma_plus[j] - sigma[j]) / (x_fd[i] * h);
		}
		jac_y_fd[i] = dy_fd;
		jac_sigma_fd[i] = dsigma_fd;
	}

	//variables for derivative calculation in adjoint mode
	//seed
	std::vector<double> by(n, 0.0);
	std::vector<double> bsigma(q, 0.0);
	//output
	std::vector<double> bb(p, 0.0);
	//after further calculation
	std::vector<double> dy_adjoint(n, 0.0);
	std::vector<double> dsigma_adjoint(q, 0.0);

	for (int der_ind = 0; der_ind < p; der_ind++) {
		//derivatives in adjoint mode
		for (int i = 0; i < n; i++) {
			std::fill(by.begin(), by.end(), 0.0);
			by[i] = 1.0;
			std::fill(bsigma.begin(), bsigma.end(), 0.0);
			derivative_adjoint(object, by.data(), bsigma.data(), bb.data(), info);
			//derivative_adjoint_x(object, by.data(), bb.data(), info);
			dy_adjoint[i] = bb[der_ind];
			//std::cout << "dy" << i << "/db" << der_ind << " = " << bb[der_ind] << std::endl;
		}
		for (int i = 0; i < q; i++) {
			std::fill(by.begin(), by.end(), 0.0);
			std::fill(bsigma.begin(), bsigma.end(), 0.0);
			bsigma[i] = 1.0;
			derivative_adjoint(object, by.data(), bsigma.data(), bb.data(), info);
			//derivative_adjoint_sigma(object, bsigma.data(), bb.data(), info);
			dsigma_adjoint[i] = bb[der_ind];
			//std::cout << "dsigma" << i << "/db" << der_ind << " = " << bb[der_ind] << std::endl;
		}
		jac_y_adjoint[der_ind] = dy_adjoint;
		jac_sigma_adjoint[der_ind] = dsigma_adjoint;

	}

	// Evluation of results (comparing jacobian matrices with each other).
	double max_sq_dev_y = -1.0;
	int i_max_y, j_max_y = -1;

	double max_sq_dev_sigma = -1.0;
	int i_max_sigma, j_max_sigma = -1;

	double temp = 0.0;

	for (int i = 0; i < jac_y_fd.size(); i++) {
		for (int j = 0; j < jac_y_fd[i].size(); j++) {
			temp = (jac_y_fd[i][j] - jac_y_adjoint[i][j])*(jac_y_fd[i][j] - jac_y_adjoint[i][j]);
			if (temp > max_sq_dev_y) {
				max_sq_dev_y = temp;
				i_max_y = i;
				j_max_y = j;
			}
		}
	}
	for (int i = 0; i < jac_sigma_fd.size(); i++) {
		for (int j = 0; j < jac_sigma_fd[i].size(); j++) {
			temp = (jac_sigma_fd[i][j] - jac_sigma_adjoint[i][j])*(jac_sigma_fd[i][j] - jac_sigma_adjoint[i][j]);
			if (temp > max_sq_dev_sigma) {
				max_sq_dev_sigma = temp;
				i_max_sigma = i;
				j_max_sigma = j;
			}
		}
	}
	if ((max_sq_dev_y < h) && (max_sq_dev_sigma < h)) { std::cout << ">>> Comparing adjoints to finite differences: OK!" << std::endl; res = RES_OK; }
	else { std::cout << ">>> Comparing adjoints to finite differences: Error!" << std::endl; res = RES_ERROR; }


	//std::cout << "Max. sq. dev. w.r.t. solution: " << max_sq_dev_y << " with dy_" << j_max_y << "/dp_" << i_max_y << "|fd = " << jac_y_fd[i_max_y][j_max_y] << " and ..|tangent = " << jac_y_tangent[i_max_y][j_max_y] << std::endl;
	//std::cout << "Max. sq. dev. w.r.t. sigma: " << max_sq_dev_sigma << " with dsigma_" << j_max_sigma << "/dp_" << i_max_y << "|fd = " << jac_sigma_fd[i_max_y][j_max_y] << " and ..|tangent = " << jac_sigma_tangent[i_max_y][j_max_y] << std::endl;
	if (max_sq_dev_y < h) {
		std::cout << "Jacobian (via adjoints) of solution ok." << std::endl;
		//std::cout << "Max. sq. dev. w.r.t. solution: " << max_sq_dev_y << " with dy_" << j_max_y << "/dp_" << i_max_y << "|fd = " << jac_y_fd[i_max_y][j_max_y] << " and ..|tangent = " << jac_y_adjoint[i_max_y][j_max_y] << std::endl;
	}
	else {
		std::cout << "Jacobian (via adjoints) of solution via tangents potentially wrong." << std::endl;
		std::cout << "Max. sq. dev. w.r.t. solution: " << max_sq_dev_y << " with dy_" << j_max_y << "/dp_" << i_max_y << "|fd = " << jac_y_fd[i_max_y][j_max_y] << " and ..|tangent = " << jac_y_adjoint[i_max_y][j_max_y] << std::endl;
	}
	if (max_sq_dev_sigma < h) {
		std::cout << "Jacobian (via adjoints) of sigma ok." << std::endl;
		//std::cout << "Max. sq. dev. w.r.t. sigma: " << max_sq_dev_sigma << " with dsigma_" << j_max_sigma << "/dp_" << i_max_y << "|fd = " << jac_sigma_fd[i_max_y][j_max_y] << " and ..|tangent = " << jac_sigma_adjoint[i_max_y][j_max_y] << std::endl;
	}
	else {
		std::cout << "Jacobian (via adjoints) of sigma via tangents potentially wrong." << std::endl;
		std::cout << "Max. sq. dev. w.r.t. sigma: " << max_sq_dev_sigma << " with dsigma_" << j_max_sigma << "/dp_" << i_max_sigma << "|fd = " << jac_sigma_fd[i_max_sigma][j_max_sigma] << " and ..|tangent = " << jac_sigma_adjoint[i_max_sigma][j_max_sigma] << std::endl;
	}
	return res;
};

int compare_KKT_direct(void* ext_obj_KKT, void* ext_obj_direct, double* x) {
	int res = RES_OK;
	const double sq_dev_tol = 1.e-3;
	int info[2];

	const int p = get_n_p(ext_obj_KKT);
	const int n = get_n_n(ext_obj_KKT);
	const int m = get_n_m(ext_obj_KKT);
	const int q = get_n_q(ext_obj_KKT);

	std::vector<double> y_KKT(n, 0.0);
	std::vector<double> y_direct(n, 0.0);
	std::vector<double> sigma_KKT(q, 0.0);
	std::vector<double> sigma_direct(q, 0.0);

	solveActiveSet(ext_obj_KKT, x, y_KKT.data(), sigma_KKT.data(), info);
	solveDirectSigma(ext_obj_direct, x, y_direct.data(), sigma_direct.data(), info);

	std::vector<int> dev_y;
	std::vector<int> dev_sigma;

	double temp = 0.0;
	double max_sq_dev_y = -1.0;
	double max_sq_dev_sigma = -1.0;
	int i_max_y = -1;
	int i_max_sigma = -1;
	for (int i = 0; i < n; i++) {
		temp = (y_KKT[i] - y_direct[i])*(y_KKT[i] - y_direct[i]);
		if (temp > max_sq_dev_y) {
			max_sq_dev_y = temp;
			i_max_y = i;
		}
		if (temp > sq_dev_tol) {
			dev_y.push_back(i);
		}
	}
	for (int i = 0; i < q; i++) {
		temp = (sigma_KKT[i] - sigma_direct[i])*(sigma_KKT[i] - sigma_direct[i]);
		if (temp > max_sq_dev_sigma) {
			max_sq_dev_sigma = temp;
			i_max_sigma = i;
		}
		if (temp > sq_dev_tol) {
			dev_sigma.push_back(i);
		}
	}
	if (dev_y.empty() && dev_sigma.empty()) { std::cout << ">>> Comparing KKT with direct approach: OK!" << std::endl; res = RES_OK; }
	else { std::cout << ">>> Comparing KKT with direct approach: Error!" << std::endl; res = RES_ERROR; }

	for (int i = 0; i < dev_y.size(); i++) {
		std::cout << "i=" << dev_y[i] << ", sq. dev. = " << (y_KKT[dev_y[i]] - y_direct[dev_y[i]])*(y_KKT[dev_y[i]] - y_direct[dev_y[i]]) << ", y_KKT = " << y_KKT[dev_y[i]] << " ; y_direct = " << y_direct[dev_y[i]] << std::endl;
	}
	if (dev_y.empty()) {
		std::cout << "Max. dev. for y[" << i_max_y << "] with dev = " << (y_KKT[i_max_y] - y_direct[i_max_y])*(y_KKT[i_max_y] - y_direct[i_max_y]) << ", y_KKT = " << y_KKT[i_max_y] << ", y_direct = " << y_direct[i_max_y] << std::endl;
	}
	for (int i = 0; i < dev_sigma.size(); i++) {
		std::cout << "i=" << dev_sigma[i] << ", sq. dev. = " << (sigma_KKT[dev_sigma[i]] - sigma_direct[dev_sigma[i]])*(sigma_KKT[dev_sigma[i]] - sigma_direct[dev_sigma[i]]) << ", sigma_KKT = " << sigma_KKT[dev_sigma[i]] << ", sigma_direct = " << sigma_direct[dev_sigma[i]] << std::endl;
	}
	if (dev_sigma.empty()) {
		std::cout << "Max. dev. for sigma[" << i_max_sigma << "] with sq. dev. = " << (sigma_KKT[i_max_sigma] - sigma_direct[i_max_sigma])*(sigma_KKT[i_max_sigma] - sigma_direct[i_max_sigma]) << ", sigma_KKT = " << sigma_KKT[i_max_sigma] << " ; sigma_direct = " << sigma_direct[i_max_sigma] << std::endl;
	}
	return res;
}

int compare_LP_QP(void* ext_obj_LP, void* ext_obj_QP, double* x) {
	int res = RES_OK;
	const double sq_dev_tol = 1.e-3;
	int info[2];

	const int p = get_n_p(ext_obj_LP);
	const int n = get_n_n(ext_obj_LP);
	const int m = get_n_m(ext_obj_LP);
	const int q = get_n_q(ext_obj_LP);

	std::vector<double> y_LP(n,0.0);
	std::vector<double> y_QP(n, 0.0);
	std::vector<double> sigma_LP(q, 0.0);
	std::vector<double> sigma_QP(q, 0.0);

	solveActiveSet(ext_obj_LP, x, y_LP.data(), sigma_LP.data(), info);
	solveActiveSet(ext_obj_QP, x, y_QP.data(), sigma_QP.data(), info);

	std::vector<int> dev_y;
	std::vector<int> dev_sigma;

	double temp=0.0;
	double max_sq_dev_y=-1.0;
	double max_sq_dev_sigma=-1.0;
	int i_max_y = -1;
	int i_max_sigma = -1;
	for (int i = 0; i < n; i++) {
		temp = (y_LP[i] - y_QP[i])*(y_LP[i] - y_QP[i]);
		if (temp > max_sq_dev_y) {
			max_sq_dev_y = temp;
			i_max_y = i;
		}
		if (temp > sq_dev_tol) {
			dev_y.push_back(i);
		}
	}
	for (int i = 0; i < q; i++) {
		temp = (sigma_LP[i] - sigma_QP[i])*(sigma_LP[i] - sigma_QP[i]);
		if (temp > max_sq_dev_sigma) {
			max_sq_dev_sigma = temp;
			i_max_sigma = i;
		}
		if (temp > sq_dev_tol) {
			dev_sigma.push_back(i);
		}
	}
	if (dev_y.empty() && dev_sigma.empty()) { std::cout << ">>> Comparing LP to QP approach: OK!" << std::endl; res = RES_OK; }
	else { std::cout << ">>> Comparing LP to QP approach: Deviation!" << std::endl; res = RES_DEV; }

	for (int i = 0; i < dev_y.size(); i++) {
		std::cout << "i=" << dev_y[i] << ", sq. dev. = " << (y_LP[dev_y[i]] - y_QP[dev_y[i]])*(y_LP[dev_y[i]] - y_QP[dev_y[i]]) << ", y_LP = " << y_LP[dev_y[i]] << " ; y_QP = " << y_QP[dev_y[i]] << std::endl;
	}
	if (dev_y.empty()) {
		std::cout << "Max. dev. for y[" << i_max_y << "] with dev = " << (y_LP[i_max_y] - y_QP[i_max_y])*(y_LP[i_max_y] - y_QP[i_max_y]) << ", y_LP = " << y_LP[i_max_y] << ", y_QP = " << y_QP[i_max_y] << std::endl;
	}
	for (int i = 0; i < dev_sigma.size(); i++) {
		std::cout << "i=" << dev_sigma[i] << ", sq. dev. = " << (sigma_LP[dev_sigma[i]] - sigma_QP[dev_sigma[i]])*(sigma_LP[dev_sigma[i]] - sigma_QP[dev_sigma[i]]) << ", sigma_LP = " << sigma_LP[dev_sigma[i]] << ", sigma_QP = " << sigma_QP[dev_sigma[i]] << std::endl;
	}
	if (dev_sigma.empty()) {
		std::cout << "Max. dev. for sigma[" << i_max_sigma << "] with sq. dev. = " << (sigma_LP[i_max_y] - sigma_QP[i_max_y])*(sigma_LP[i_max_y] - sigma_QP[i_max_sigma]) << ", sigma_LP = " << sigma_LP[i_max_sigma] << " ; sigma_QP = " << sigma_QP[i_max_sigma] << std::endl;
	}
	return res;
};

void* create_object_Spirallus() { return create_object_Spirallus(false); };
void* create_object_Spirallus(int createQP) {
	std::cout << "------------ Create model Spirallus ------------" << std::endl;
	const int n = 8; // number of DAEO variables
	const int p = 1; // number of DAEO parameters
	const int m = 7; // number of DAEO equalities
	const int q = 5; // number of DAEO inequalities

	const char* params[p] = { "b_1" };
	const char* variables[n] = { "vupt", "v1", "v2", "v3", "v4", "v5", "v6", "v7" };
	const char* objective = "-v6";
	const char* equalities[m] = {
		"vupt - 1.0*v1", "v1 - 1.0*v2 - 1.0*v5", "v4 - 1.0*v2 + v5 - 1.0*v6", "v2 - 1.0*v3", "v3 - 1.0*v4", "v3 + v4 - 1.0*v7", "vupt - 1.0*b_1" };
	const char* inequalities[q] = { "v3", "v4", "v6", "v2", "v7" };


	std::vector<int> info;
	info.resize(2);

    const char* fname = "Spirallus.lp";

	std::vector<double> Qdiag;
	if (createQP) {
		Qdiag.resize(n + p);
		for (int i = 0; i < n + p; ++i) {
			if (i < p) {
				Qdiag[i] = 1e-12;
			}
			else {
				Qdiag[i] = 1e-6;
			}
		}
	}

	return parseDFBAmodel(params,
		variables,
		objective,
		equalities,
		inequalities,
		p,
		m,
		n,
		q,
		Qdiag.data(),
		Qdiag.size(),
		info.data());
};
void* create_object_Spirallus(int createQP, std::vector<double>& b0, std::vector<double>& bend) {
	std::vector<double> b0_ = { 0.8333 };
	std::vector<double> bend_ = { 0.0 };
	b0.clear();
	b0 = b0_;
	bend.clear();
	bend = bend_;
	return create_object_Spirallus(createQP);
}

void* create_object_SmallLinearToyExample() { return create_object_SmallLinearToyExample(false); };
void* create_object_SmallLinearToyExample(int createQP) {
	std::cout << "------------ Creating model SmallLinearToyExample ------------" << std::endl;
	const int n = 2;
	const int p = 1;
	const int m = 1;
	const int q = 3;

	const char* params[p] = { "p" };
	const char* variables[n] = { "y_1","y_2" };
	const char* objective = "-y_1";
	const char* equalities[m] = { "y_1+y_2-1" };
	const char* inequalities[q] = { "y_1","y_2","p-y_1" };

	std::vector<double> Qdiag;
	if (createQP) {
		Qdiag.resize(n + p);
		for (int i = 0; i < n + p; ++i) {
			if (i < p) {
				Qdiag[i] = 1e-12;
			}
			else {
				Qdiag[i] = 1e-6;
			}
		}
	}
	int info[2];

	return parseDFBAmodel(params,
		variables,
		objective,
		equalities,
		inequalities,
		p,
		m,
		n,
		q,
		Qdiag.data(),
		Qdiag.size(),
		info);
};
void* create_object_SmallLinearToyExample(int createQP, std::vector<double>& b0, std::vector<double>& bend) {
	std::vector<double> b0_ = { 0.1 };
	std::vector<double> bend_ = { 1.1 };
	b0.clear();
	b0 = b0_;
	bend.clear();
	bend = bend_;
	return create_object_SmallLinearToyExample(createQP);
}

void* create_object_CG() { return create_object_CG(false); };
void* create_object_CG(int createQP) {
	std::cout << "------------ Creating model Corynebacterium glutamicum ------------" << std::endl;
	const int n = 68; // number of DAEO variables
	const int p = 9; // number of DAEO parameters
	const int m = 57; // number of DAEO equalities
	const int q = 50; // number of DAEO inequalities

	double M = 100000.0;

	const char* params[p] = { "b_1","b_2","b_3","upt_max","xdh_ub","kdy_ub","xi_ub","yagE_ub","xr_ub" };
	double x[p] = { 4.5,4,7.54,4.5,M,M,M,M,M };
	const char* variables[n] = { "icd","odhA","sucD","sdhCAB_m","fumC","mqo_mdh","gltA","pgi",
		"pyk","pdh","aceB","aceA","odx","pyc","mez","pckG","ppc","pfkA","fda","gapA","eno","zwf",
		"opcA","gnd","rpe","rpi","tkt_1","tal","tkt_2","tpiA","fbp","CO2_t","bmsynth","SO3_t",
		"NH3_t","bc1_aa3","O2_t","ksh","Xyl_t","GLC_t_PEP","pgm","pgk","acnB","acnA","SUCC_t",
		"ndh","cyto_bd","ATPase","xdh","xls","xdy","kdy","dGLC_t","dXyl_t","dO2_t","xi","xylB",
		"yagE","aldA","glcD","xr","xldh","pta","ackA","pqo","AC_t","ldh","LAC_t" };
	const char* objective = "2*dGLC_t - bmsynth + 2*dXyl_t";
	const char* equalities[m] = {
		"acnB - 1.0*aceA - 1.0*icd","icd - 1.224*bmsynth + ksh - 1.0*odhA",
		"odhA - 1.0*sucD","sdhCAB_m - 1.0*fumC","aceB + fumC - 1.0*mez - 1.0*mqo_mdh","mqo_mdh - 1.0*gltA - 1.68*bmsynth - 1.0*odx - 1.0*pckG + ppc + pyc",
		"aceA - 1.0*SUCC_t - 1.0*sdhCAB_m + sucD","pdh - 3.177*bmsynth - 1.0*gltA - 1.0*aceB - 1.0*pta",
		"GLC_t_PEP - 2.604*bmsynth - 1.0*ldh + mez + odx - 1.0*pdh - 1.0*pqo - 1.0*pyc + pyk + yagE",
		"eno - 0.652*bmsynth - 1.0*GLC_t_PEP + pckG - 1.0*ppc - 1.0*pyk","GLC_t_PEP - 0.205*bmsynth - 1.0*pgi - 1.0*zwf",
		"aceA - 1.0*aceB + glcD","fbp - 0.308*bmsynth - 1.0*pfkA + pgi + tal + tkt_2","pfkA - 1.0*fda - 1.0*fbp",
		"fda - 0.129*bmsynth - 1.0*gapA - 1.0*tal + tkt_1 + tkt_2 + tpiA","gnd - 1.0*rpe - 1.0*rpi",
		"zwf - 1.0*opcA","opcA - 1.0*gnd","rpe - 1.0*tkt_1 - 1.0*tkt_2 + xylB","rpi - 0.879*bmsynth - 1.0*tkt_1",
		"tkt_1 - 1.0*tal","tal - 0.268*bmsynth - 1.0*tkt_2","fda - 1.0*tpiA","bmsynth - 1.0*CO2_t + gnd + icd + mez + odhA + odx + pckG + pdh - 1.0*ppc - 1.0*pyc",
		"pgm - 1.0*eno","ATPase - 1.0*Xyl_t + ackA - 17.002*bmsynth - 1.0*pckG - 1.0*pfkA + pgk - 1.0*pyc + pyk + sucD - 1.0*xylB",
		"aldA + 3.111*bmsynth + gapA + ksh - 1.0*ldh - 1.0*ndh + odhA + pdh + xdh + xldh","glcD - 16.429*bmsynth + gnd + icd + mez - 1.0*xr + zwf",
		"2.0*bc1_aa3 + 2.0*cyto_bd - 1.0*mqo_mdh - 1.0*ndh - 1.0*pqo - 1.0*sdhCAB_m","mqo_mdh - 2.0*cyto_bd - 2.0*bc1_aa3 + ndh + pqo + sdhCAB_m",
		"NH3_t - 6.231*bmsynth","O2_t - 1.0*bc1_aa3 - 1.0*cyto_bd","Xyl_t - 1.0*xdh - 1.0*xi - 1.0*xr",
		"pgk - 1.295*bmsynth - 1.0*pgm","gapA - 1.0*pgk","SO3_t - 0.233*bmsynth","gltA - 1.0*acnA",
		"acnA - 1.0*acnB","4.0*ATPase - 12.0*bc1_aa3 - 4.0*cyto_bd + 2.0*sdhCAB_m","kdy - 1.0*ksh",
		"xdy - 1.0*kdy - 1.0*yagE","xls - 1.0*xdy","xdh - 1.0*xls","GLC_t_PEP - 1.0*b_1 + dGLC_t",
		"Xyl_t - 1.0*b_2 + dXyl_t","O2_t - 1.0*b_3 + dO2_t","xi + xldh - 1.0*xylB","yagE - 1.0*aldA",
		"aldA - 1.0*glcD","xr - 1.0*xldh","pta - 1.0*ackA","ackA - 1.0*AC_t + pqo","ldh - 1.0*LAC_t",
		"mez","odx","ppc","pqo" };
	const char* inequalities[q] = { "icd","odhA","sucD","gltA","pyk","pdh","aceB","aceA","pyc",
		"pckG","pfkA","zwf","opcA","gnd","fbp","bmsynth","SO3_t","NH3_t","bc1_aa3","O2_t","ksh",
		"Xyl_t","GLC_t_PEP","SUCC_t","ndh","cyto_bd","ATPase","xdh","xls","xdy","kdy","dGLC_t",
		"dXyl_t","dO2_t","xi","xylB","yagE","aldA","glcD","xr","xldh","AC_t","ldh","LAC_t","upt_max-GLC_t_PEP-Xyl_t",
		"xdh_ub-xdh","kdy_ub-kdy","xi_ub-xi","yagE_ub-yagE","xr_ub-xr" };

	std::vector<double> Qdiag;
	if (createQP){
		Qdiag.resize(n + p);
		for (int i = 0; i < n + p; ++i) {
			if (i < p) {
				Qdiag[i] = 1e-12;
			}
			else {
				Qdiag[i] = 1e-6;
			}
		}
	}
	int info[2];

	return parseDFBAmodel(params,
		variables,
		objective,
		equalities,
		inequalities,
		p,
		m,
		n,
		q,
		Qdiag.data(),
		Qdiag.size(),
		info);
};
void* create_object_CG(int createQP, std::vector<double>& b0, std::vector<double>& bend){
	double M = 100000.0;
	std::vector<double> b0_ = { 4.5,4,7.54,4.5,M,M,M,M,M };
	std::vector<double> bend_ = { 0.0,4,7.54,4.5,M,M,M,M,M };
	b0.clear();
	b0 = b0_;
	bend.clear();
	bend = bend_;
	return create_object_CG(createQP);
}

void* create_object_Ecoli() { return create_object_Ecoli(false); };
void* create_object_Ecoli(int createQP) {
	std::cout << "------------ Creating model Ecoli ------------" << std::endl;
	const int n = 1152; // number of DAEO variables
	const int p = 3; // number of DAEO parameters
	const int m = 839; // number of DAEO equalities
	const int q = 822; // number of DAEO inequalities

	const char* params[p] = { "b1","b2","b3" };
	const char* variables[n] = { "v1","v2","v3","v4","v5","v6","v7","v8","v9","v10","v11","v12",
		"v13","v14","v15","v16","v17","v18","v19","v20","v21","v22","v23","v24","v25","v26","v27",
		"v28","v29","v30","v31","v32","v33","v34","v35","v36","v37","v38","v39","v40","v41","v42",
		"v43","v44","v45","v46","v47","v48","v49","v50","v51","v52","v53","v54","v55","v56","v57",
		"v58","v59","v60","v61","v62","v63","v64","v65","v66","v67","v68","v69","v70","v71","v72",
		"v73","v74","v75","v76","v77","v78","v79","v80","v81","v82","v83","v84","v85","v86","v87",
		"v88","v89","v90","v91","v92","v93","v94","v95","v96","v97","v98","v99","v100","v101",
		"v102","v103","v104","v105","v106","v107","v108","v109","v110","v111","v112","v113","v114",
		"v115","v116","v117","v118","v119","v120","v121","v122","v123","v124","v125","v126","v127",
		"v128","v129","v130","v131","v132","v133","v134","v135","v136","v137","v138","v139","v140",
		"v141","v142","v143","v144","v145","v146","v147","v148","v149","v150","v151","v152","v153",
		"v154","v155","v156","v157","v158","v159","v160","v161","v162","v163","v164","v165","v166",
		"v167","v168","v169","v170","v171","v172","v173","v174","v175","v176","v177","v178","v179",
		"v180","v181","v182","v183","v184","v185","v186","v187","v188","v189","v190","v191","v192",
		"v193","v194","v195","v196","v197","v198","v199","v200","v201","v202","v203","v204","v205",
		"v206","v207","v208","v209","v210","v211","v212","v213","v214","v215","v216","v217","v218",
		"v219","v220","v221","v222","v223","v224","v225","v226","v227","v228","v229","v230","v231",
		"v232","v233","v234","v235","v236","v237","v238","v239","v240","v241","v242","v243","v244",
		"v245","v246","v247","v248","v249","v250","v251","v252","v253","v254","v255","v256","v257",
		"v258","v259","v260","v261","v262","v263","v264","v265","v266","v267","v268","v269","v270",
		"v271","v272","v273","v274","v275","v276","v277","v278","v279","v280","v281","v282","v283",
		"v284","v285","v286","v287","v288","v289","v290","v291","v292","v293","v294","v295","v296",
		"v297","v298","v299","v300","v301","v302","v303","v304","v305","v306","v307","v308","v309",
		"v310","v311","v312","v313","v314","v315","v316","v317","v318","v319","v320","v321","v322",
		"v323","v324","v325","v326","v327","v328","v329","v330","v331","v332","v333","v334","v335",
		"v336","v337","v338","v339","v340","v341","v342","v343","v344","v345","v346","v347","v348",
		"v349","v350","v351","v352","v353","v354","v355","v356","v357","v358","v359","v360","v361",
		"v362","v363","v364","v365","v366","v367","v368","v369","v370","v371","v372","v373","v374",
		"v375","v376","v377","v378","v379","v380","v381","v382","v383","v384","v385","v386","v387",
		"v388","v389","v390","v391","v392","v393","v394","v395","v396","v397","v398","v399","v400",
		"v401","v402","v403","v404","v405","v406","v407","v408","v409","v410","v411","v412","v413",
		"v414","v415","v416","v417","v418","v419","v420","v421","v422","v423","v424","v425","v426",
		"v427","v428","v429","v430","v431","v432","v433","v434","v435","v436","v437","v438","v439",
		"v440","v441","v442","v443","v444","v445","v446","v447","v448","v449","v450","v451","v452",
		"v453","v454","v455","v456","v457","v458","v459","v460","v461","v462","v463","v464","v465",
		"v466","v467","v468","v469","v470","v471","v472","v473","v474","v475","v476","v477","v478",
		"v479","v480","v481","v482","v483","v484","v485","v486","v487","v488","v489","v490","v491",
		"v492","v493","v494","v495","v496","v497","v498","v499","v500","v501","v502","v503","v504",
		"v505","v506","v507","v508","v509","v510","v511","v512","v513","v514","v515","v516","v517",
		"v518","v519","v520","v521","v522","v523","v524","v525","v526","v527","v528","v529","v530",
		"v531","v532","v533","v534","v535","v536","v537","v538","v539","v540","v541","v542","v543",
		"v544","v545","v546","v547","v548","v549","v550","v551","v552","v553","v554","v555","v556",
		"v557","v558","v559","v560","v561","v562","v563","v564","v565","v566","v567","v568","v569",
		"v570","v571","v572","v573","v574","v575","v576","v577","v578","v579","v580","v581","v582",
		"v583","v584","v585","v586","v587","v588","v589","v590","v591","v592","v593","v594","v595",
		"v596","v597","v598","v599","v600","v601","v602","v603","v604","v605","v606","v607","v608",
		"v609","v610","v611","v612","v613","v614","v615","v616","v617","v618","v619","v620","v621",
		"v622","v623","v624","v625","v626","v627","v628","v629","v630","v631","v632","v633","v634",
		"v635","v636","v637","v638","v639","v640","v641","v642","v643","v644","v645","v646","v647",
		"v648","v649","v650","v651","v652","v653","v654","v655","v656","v657","v658","v659","v660",
		"v661","v662","v663","v664","v665","v666","v667","v668","v669","v670","v671","v672","v673",
		"v674","v675","v676","v677","v678","v679","v680","v681","v682","v683","v684","v685","v686",
		"v687","v688","v689","v690","v691","v692","v693","v694","v695","v696","v697","v698","v699",
		"v700","v701","v702","v703","v704","v705","v706","v707","v708","v709","v710","v711","v712",
		"v713","v714","v715","v716","v717","v718","v719","v720","v721","v722","v723","v724","v725",
		"v726","v727","v728","v729","v730","v731","v732","v733","v734","v735","v736","v737","v738",
		"v739","v740","v741","v742","v743","v744","v745","v746","v747","v748","v749","v750","v751",
		"v752","v753","v754","v755","v756","v757","v758","v759","v760","v761","v762","v763","v764",
		"v765","v766","v767","v768","v769","v770","v771","v772","v773","v774","v775","v776","v777",
		"v778","v779","v780","v781","v782","v783","v784","v785","v786","v787","v788","v789","v790",
		"v791","v792","v793","v794","v795","v796","v797","v798","v799","v800","v801","v802","v803",
		"v804","v805","v806","v807","v808","v809","v810","v811","v812","v813","v814","v815","v816",
		"v817","v818","v819","v820","v821","v822","v823","v824","v825","v826","v827","v828","v829",
		"v830","v831","v832","v833","v834","v835","v836","v837","v838","v839","v840","v841","v842",
		"v843","v844","v845","v846","v847","v848","v849","v850","v851","v852","v853","v854","v855",
		"v856","v857","v858","v859","v860","v861","v862","v863","v864","v865","v866","v867","v868",
		"v869","v870","v871","v872","v873","v874","v875","v876","v877","v878","v879","v880","v881",
		"v882","v883","v884","v885","v886","v887","v888","v889","v890","v891","v892","v893","v894",
		"v895","v896","v897","v898","v899","v900","v901","v902","v903","v904","v905","v906","v907",
		"v908","v909","v910","v911","v912","v913","v914","v915","v916","v917","v918","v919","v920",
		"v921","v922","v923","v924","v925","v926","v927","v928","v929","v930","v931","v932","v933",
		"v934","v935","v936","v937","v938","v939","v940","v941","v942","v943","v944","v945","v946",
		"v947","v948","v949","v950","v951","v952","v953","v954","v955","v956","v957","v958","v959",
		"v960","v961","v962","v963","v964","v965","v966","v967","v968","v969","v970","v971","v972",
		"v973","v974","v975","v976","v977","v978","v979","v980","v981","v982","v983","v984","v985",
		"v986","v987","v988","v989","v990","v991","v992","v993","v994","v995","v996","v997","v998",
		"v999","v1000","v1001","v1002","v1003","v1004","v1005","v1006","v1007","v1008","v1009",
		"v1010","v1011","v1012","v1013","v1014","v1015","v1016","v1017","v1018","v1019","v1020",
		"v1021","v1022","v1023","v1024","v1025","v1026","v1027","v1028","v1029","v1030","v1031",
		"v1032","v1033","v1034","v1035","v1036","v1037","v1038","v1039","v1040","v1041","v1042",
		"v1043","v1044","v1045","v1046","v1047","v1048","v1049","v1050","v1051","v1052","v1053",
		"v1054","v1055","v1056","v1057","v1058","v1059","v1060","v1061","v1062","v1063","v1064",
		"v1065","v1066","v1067","v1068","v1069","v1070","v1071","v1072","v1073","v1074","v1075",
		"tr_126_1","tr_151_1","tr_151_2","tr_151_3","tr_151_4","tr_151_5","tr_180_1","tr_200_1",
		"tr_200_2","tr_200_3","tr_200_4","tr_200_5","tr_200_6","tr_200_7","tr_200_8","tr_229_1",
		"tr_229_2","tr_231_1","tr_231_2","tr_388_1","tr_388_2","tr_424_1","tr_424_2","tr_424_3",
		"tr_424_4","tr_424_5","tr_424_6","tr_424_7","tr_424_8","tr_424_9","tr_424_10","tr_424_11",
		"tr_424_12","tr_427_1","tr_427_2","tr_427_3","tr_427_4","tr_427_5","tr_427_6","tr_427_7",
		"tr_427_8","tr_427_9","tr_427_10","tr_427_11","tr_427_12","tr_427_13","tr_427_14","tr_427_15",
		"tr_427_16","tr_427_17","tr_428_1","tr_428_2","tr_428_3","tr_428_4","tr_428_5","tr_536_1",
		"tr_536_2","tr_536_3","tr_538_1","tr_538_2","tr_538_3","tr_539_1","tr_539_2","tr_540_1",
		"tr_540_2","tr_542_1","tr_542_2","tr_589_1","tr_589_2","tr_589_3","tr_589_4","tr_589_5",
		"tr_599_1","tr_599_2","tr_599_3","tr_623_1","tr_623_2" };
	const char* objective = "-v150";
	const char* equalities[m] = {
		"v719 - 1.0*v453 - 1.0*v486 - 1.0*v75","0.02*v811 - 0.02*v205 + 0.02*v820",
		"v1 + v643","- 1.0*v1 - 1.0*v287","v485 + v828","v659 - 1.0*v157","v157 - 1.0*v288","v467 - 1.0*v807 - 1.0*v808 + v873",
		"v606 - 1.0*v227","v227 - 1.0*v228","v228 - 3.0*v284","v232 - 1.0*v231","v629 - 1.0*v223",
		"v630 - 1.0*v224","-1.0*v261","v869 - 1.0*v67","- 1.0*v247 - 1.0*v248 - 1.0*v249","v564 - 1.0*v241",
		"v934 - 1.0*v208","v208 - 1.0*v207 + v209 - 1.0*v1029","- 1.0*v209 - 1.0*v289","v33 - 1.0*v630",
		"v991 - 1.0*v523","v867 - 1.0*v613","v219 - 1.0*v245","v218 - 1.0*v277 + v278","v94 + v217 - 1.0*v218 + v714",
		"- 1.0*v217 - 1.0*v290","v216 - 1.0*v220","v473 - 1.0*v216","- 1.0*v2 - 1.0*v3","v247 - 1.0*v5 - 1.0*v4",
		"v717 - 1.0*v263","v235 - 1.0*v102 + v257 + v258 + v449 - 1.0*v466 - 1.0*v536 - 1.0*v601 - 1.0*v732 - 1.0*v733 + v1002 + v1003",
		"v466 - 1.0*v258 - 1.0*v449 - 1.0*v257 + v536 + v601 + v732 + v733 - 1.0*v1002 - 1.0*v1003",
		"v268 - 1.0*v856 + v891 + v893 + v895 + v1005","v856 - 1.0*v264","v496 + v521 + v591 - 1.0*v1023",
		"v626 - 1.0*v627","v250 + v251 - 1.0*v1049","v844 - 1.0*v1009","v681 - 1.0*v701","v683 - 1.0*v681",
		"v271 - 1.0*v694","v692 - 1.0*v691","v992 - 1.0*v788 - 1.0*v33","v801 - 1.0*v793","v798 - 1.0*v794",
		"v796 - 1.0*v254","v794 - 1.0*v796","v793 - 1.0*v798","v800 - 1.0*v801","v165 - 1.0*v692",
		"- 1.0*v283 - 1.0*v831","-1.0*v830","v945 - 1.0*v960","v858 + v1026","- 1.0*v625 - 1.0*v626",
		"v627 + v628","v625 - 1.0*v795","v261","-1.0*v637","v245 - 1.0*v244","v244 - 1.0*v948",
		"v573 - 1.0*v6","- 1.0*v291 - 1.0*v573","v633 - 1.0*v1028 - 1.0*v1032","v590 - 1.0*v7",
		"- 1.0*v292 - 1.0*v590","v613 - 1.0*v1019 - 1.0*v1021","v223 - 1.0*v628 - 1.0*v717 + v1062 - 1.0*v1065",
		"v224 + v614","v572 - 1.0*v800","v529 - 1.0*v826 - 1.0*v828 + v831","v826 - 1.0*v880",
		"v878 - 1.0*v169","v19 - 1.0*v18 + v20 - 1.0*v508 + v510","v508 - 1.0*v293 - 1.0*v20",
		"v887 - 1.0*v19","v49 - 1.0*v242","v50 - 1.0*v49","-1.0*v586","v103 + v586 - 1.0*v844",
		"v694 - 1.0*v165","v799 - 1.0*v587","v997","v170 - 1.0*v572","v8","-1.0*v577","v795 - 1.0*v647",
		"v577 + v997 - 1.0*v1009","v119 + v127","v275 - 1.0*v821","v845 - 1.0*v857","v857 - 1.0*v852",
		"v843 - 1.0*v907 + v908","- 1.0*v77 - 1.0*v869","v462 - 2.0*v850","v110 - 1.0*v843","v241 - 1.0*v110",
		"v76 + v77","v248 - 1.0*v9 + v249 - 1.0*v608 - 1.0*v609","v472 - 1.0*v496 + v497","v723 - 1.0*v722",
		"v722 - 1.0*v688","v955 - 1.0*v718","v721 - 1.0*v697 - 0.05*v150","v718 - 1.0*v723","v758 - 1.0*v928",
		"v236 - 1.0*v588","v588 - 1.0*v242","v548 - 1.0*v547 - 1.0*v278 + v829","v470 - 1.0*v829",
		"v107 - 1.0*v101","v45 - 1.0*v12 - 1.0*v13 - 1.0*v14 - 1.0*v15 - 1.0*v40 - 1.0*v11 + 4.0*v151 + 5.0*v152 + 5.0*v153 + 6.0*v154 + 6.0*v155 + 7.0*v156 + v279 + v280 + v281 + v282 + v631 + v633 - 1.0*v684 + 2.0*v814 + v1028 + v1032",
		"v952","v976","v21 + v22 + v434","v40 - 1.0*v631 + v666","v21 - 1.0*v34 + v41 - 1.0*v46 + v47 + v69 + v90 + v148 + v172 + v190 + v724 + v847 + v1047",
		"- 1.0*v47 - 1.0*v294","v23 - 1.0*v21","- 1.0*v23 - 1.0*v295","v25 - 1.0*v24 - 1.0*v90 + v264 + v285 + v587 + v990",
		"- 1.0*v25 - 1.0*v296","v31 + v72","v44 - 1.0*v72 - 1.0*v724","v459 - 1.0*v1034","v30 - 1.0*v69 + v99",
		"- 1.0*v30 - 1.0*v297","v32 - 1.0*v31","v39 - 1.0*v100","- 1.0*v37 - 1.0*v298","v37 - 1.0*v99 + v100",
		"v38 - 1.0*v39","- 1.0*v38 - 1.0*v299","-1.0*v42","v42","- 1.0*v41 - 1.0*v44","v938 - 1.0*v190",
		"v631 - 1.0*v152 - 1.0*v153 - 1.0*v154 - 1.0*v155 - 1.0*v156 - 1.0*v151 + v632","v34 + v884",
		"v52 - 1.0*v51 - 1.0*v64 + v74 + v104 + v718 + v890 + v891","- 1.0*v52 - 1.0*v300","v60 - 1.0*v59 - 1.0*v48 + v61 + v73 + v773 - 1.0*v890",
		"- 1.0*v60 - 1.0*v61 - 1.0*v301","v159 - 1.0*v62","v62 - 1.0*v26","v63 + v161","v503 - 1.0*v499",
		"v542 - 1.0*v70","v70 - 3.0*v658","v26 - 1.0*v63","v114 - 1.0*v71","0.02*v841 - 0.02*v656 - 0.02*v654",
		"0.02*v840 - 0.02*v655 - 0.02*v653","0.02*v839 - 0.02*v657 - 0.02*v652","v42 - 1.0*v73 - 1.0*v74 + v102 + v254 + v793 + v794 + 2.0*v1050",
		"v563 - 1.0*v260","v67 - 1.0*v75 + v611","v866 - 1.0*v103 - 1.0*v76","v79 - 1.0*v44 - 1.0*v78 - 1.0*v18 - 1.0*v84 - 1.0*v133 + v511 - 1.0*v515 + v597 + v604 - 1.0*v614 + v647 + v792 - 1.0*v803 - 1.0*v806 - 1.0*v835 + v880 - 1.0*v887 - 1.0*v935 - 1.0*v951 - 1.0*v976 + v979 - 1.0*v1026 + v1049 - 1.0*v1062",
		"- 1.0*v79 - 1.0*v302","v125 - 1.0*v810","v81 - 2.0*v80 + v82 - 1.0*v83 - 1.0*v201 + v206",
		"- 1.0*v206 - 1.0*v303","v86 - 1.0*v82 - 1.0*v84 - 1.0*v85 - 1.0*v81 + v87 - 1.0*v107 + v147 - 0.488*v150 + v997 - 1.0*v1037 - 1.0*v1065",
		"- 1.0*v86 - 1.0*v87 - 1.0*v304","v80 - 1.0*v1046","v35 - 1.0*v629","v93 - 1.0*v92","- 1.0*v93 - 1.0*v305",
		"v92 - 1.0*v91","- 1.0*v94 - 1.0*v972","v695 - 1.0*v57 - 1.0*v101 - 1.0*v102 - 1.0*v254 - 1.0*v42 - 1.0*v793 - 1.0*v794 - 2.0*v1050",
		"v57 - 1.0*v955","v101","v727 - 1.0*v306","v106 - 1.0*v105","-1.0*v108","-1.0*v109","0.02*v655 + 0.02*v656 + 0.02*v657",
		"-1.0*v45","v931 - 1.0*v65","v10 - 1.0*v636","v112 - 1.0*v111 + v113","- 1.0*v112 - 1.0*v113 - 1.0*v307",
		"-1.0*v17","v115 - 1.0*v114 + v116 + v118 - 1.0*v138 - 0.281*v150","- 1.0*v115 - 1.0*v118 - 1.0*v308",
		"v117 - 1.0*v116","v121 - 1.0*v120 + v122 + v123 + v124 - 0.229*v150","- 1.0*v123 - 1.0*v124 - 1.0*v309",
		"v120 - 1.0*v117 - 1.0*v68 - 1.0*v121 - 1.0*v122 - 1.0*v125 - 1.0*v126 - 1.0*v127 - 1.0*v128 - 1.0*v129 - 1.0*v130 - 1.0*v131 - 1.0*v132 - 1.0*v133 + v134 + v135 + v136 + v137 - 0.229*v150 - 1.0*v869",
		"- 1.0*v134 - 1.0*v135 - 1.0*v136 - 1.0*v137 - 1.0*v310","v594 - 1.0*v232 - 1.0*v119",
		"-1.0*v178","- 1.0*v142 - 1.0*v143","v148 - 1.0*v434","v145 + v146 + v147","- 1.0*v145 - 1.0*v146",
		"v149 - 1.0*v148","- 1.0*v149 - 1.0*v311","v58","v126 + v239","-1.0*v159","v160 - 1.0*v161",
		"- 1.0*v160 - 1.0*v312","v162 - 1.0*v126 + v163 - 1.0*v789","v199 + 2.0*v658 - 1.0*v744 - 1.0*v920",
		"0.02*v210 - 0.02*v164 - 0.02*v834 - 0.02*v882","v820 - 2.0*v658","v861 - 1.0*v240","v171 - 1.0*v229",
		"v166 + v167 + v551","- 1.0*v166 - 1.0*v167 - 1.0*v313","v169 - 1.0*v106 - 1.0*v168 - 1.0*v50 - 1.0*v170 - 1.0*v605",
		"-1.0*v171","v173 - 1.0*v172 - 1.0*v43 + v182","- 1.0*v173 - 1.0*v314","v789 - 1.0*v117",
		"v634 - 3.0*v658 - 1.0*v715 - 1.0*v716","0.02*v174 - 0.00012899999999999999140964934696285*v150",
		"v164 - 1.0*v175 + v196 - 1.0*v199 + 3.0*v658 + v692 + v715 + v716 - 1.0*v770 + v779 - 1.0*v820 + v834 + v857 + v882",
		"- 1.0*v176 - 1.0*v315","v1052 - 1.0*v177","v181 - 1.0*v179 - 1.0*v178","- 1.0*v181 - 1.0*v316",
		"v178 + v179 - 1.0*v180","v175 - 1.0*v183 + v184","- 1.0*v184 - 1.0*v317","v179","v180 - 1.0*v179",
		"v185 - 0.126*v150 - 1.0*v210 - 1.0*v634 - 1.0*v694 + v744 - 1.0*v779 - 1.0*v857 - 1.0*v924",
		"-1.0*v186","v188 - 1.0*v187","- 1.0*v188 - 1.0*v318","v190 - 0.087*v150 - 1.0*v189 - 1.0*v147 + v192 - 1.0*v509 - 1.0*v857 - 1.0*v950 - 1.0*v997",
		"- 1.0*v192 - 1.0*v319","v950 - 1.0*v191","v197 - 1.0*v196 - 1.0*v195 + v198 + v770","- 1.0*v197 - 1.0*v198 - 1.0*v320",
		"v204 - 1.0*v202 + v772 - 1.0*v891","- 1.0*v204 - 1.0*v321","v203 - 1.0*v749 + v918","v780 - 1.0*v772 - 1.0*v203",
		"v101 - 1.0*v212","v749 - 0.0247*v150 - 1.0*v780 + v922","v211 - 1.0*v907","v68 - 1.0*v66",
		"v200 - 1.0*v748 + v920","v778 - 1.0*v769 - 1.0*v200","v748 - 1.0*v213 - 0.0254*v150 - 1.0*v778 + v924",
		"v215 - 1.0*v214 + v769","- 1.0*v215 - 1.0*v322","v151 - 1.0*v279 - 1.0*v633","v221 - 1.0*v746 + v919",
		"v776 - 1.0*v774 - 1.0*v221","v222 + v774 + v784 - 1.0*v893","- 1.0*v222 - 1.0*v323","v746 - 0.0254*v150 - 1.0*v776 - 1.0*v784 + v923",
		"v226 - 1.0*v225 + v430 + v528","- 1.0*v226 - 1.0*v324","v225 + v435 + v439 + v463 + v464 + v465 + v466 - 1.0*v699 - 1.0*v905 + v917 + v984 - 1.0*v1010",
		"v6 + v229 - 1.0*v230","v234 - 1.0*v233 + v1006","v764 - 1.0*v235","v259 - 1.0*v236","- 1.0*v237 - 1.0*v238 - 1.0*v239",
		"v260 - 1.0*v259","v7 + v240 - 1.0*v589","v242 - 1.0*v234","v912 - 1.0*v243","v202 + v246 - 1.0*v895",
		"- 1.0*v246 - 1.0*v325","v688 - 1.0*v251 - 1.0*v250","-1.0*v758","v907 - 2.0*v908","v253 - 1.0*v252 + v623",
		"v255 + v257","v256 + v258 - 1.0*v326","- 1.0*v255 - 1.0*v257","- 1.0*v256 - 1.0*v258 - 1.0*v327",
		"v757 - 1.0*v736","v885 - 1.0*v262","v1050 - 1.0*v946","v212 - 1.0*v147","v978 - 1.0*v16",
		"v979 - 1.0*v978","v982 - 1.0*v980 - 1.0*v979","v980 - 1.0*v981","v16 + v266 - 1.0*v745",
		"v461 - 1.0*v982","v981","v782 - 1.0*v771 - 1.0*v266 + v1004 + v1006","v745 - 1.0*v461 - 0.0247*v150 - 1.0*v782",
		"v921 - 1.0*v747 + v1057","v267 + v270 - 1.0*v765 - 1.0*v1006 - 1.0*v1057","v214 - 1.0*v267 - 1.0*v268 + v269 + v765",
		"- 1.0*v269 - 1.0*v328","v213 - 1.0*v270 + v747 + v925","v272 - 1.0*v271 + v273 - 1.0*v817 - 1.0*v997",
		"-1.0*v273","v973 - 1.0*v275 - 1.0*v219 - 1.0*v999","v276","v611 - 1.0*v612","v284","v552 - 1.0*v285",
		"v53 + v286","- 1.0*v286 - 1.0*v329","v451 - 1.0*v450","v436 - 1.0*v430 + v452 + v469 - 1.0*v490 + v665 + v677 - 1.0*v822 + v827 + v932 + v973 + v999",
		"v446 - 1.0*v201 - 6.0*v431 - 7.0*v432 - 8.0*v433 - 1.0*v434 - 0.00001*v150 - 1.0*v873 - 1.0*v965 + v966",
		"v201 + 6.0*v431 + 7.0*v432 + 8.0*v433 + v434 + v873 + v965 - 1.0*v966","v438 - 1.0*v439",
		"v437 - 1.0*v438","v450 - 1.0*v436 - 1.0*v435 + v822","v443 - 1.0*v440 - 1.0*v947","- 1.0*v330 - 1.0*v443",
		"v486 + v487 - 1.0*v871","v906 - 1.0*v446","2.0*v103 + v211 + v250 + v251 - 1.0*v441 - 1.0*v442 - 1.0*v445 + v447 + v453 - 1.0*v487 + v563 + v564 + v788 + v824",
		"- 1.0*v331 - 1.0*v447","v871 - 1.0*v866","v75 + v617","v556 - 1.0*v576 - 1.0*v791 - 1.0*v1040",
		"v444 - 1.0*v1072","- 1.0*v332 - 1.0*v451 - 1.0*v452","v569 + v679","v454","- 1.0*v333 - 1.0*v454",
		"v455 - 1.0*v437","- 1.0*v334 - 1.0*v455","v66 + v67 + v116 - 1.0*v130 + v132 - 1.0*v448 - 1.0*v449 - 1.0*v456 + v457 + v458 + v584 + v965 + v967",
		"- 1.0*v335 - 1.0*v457 - 1.0*v458 - 1.0*v967","v495 - 1.0*v461 - 1.0*v479 - 1.0*v481 - 1.0*v460 - 1.0*v503 + v707 + v708 + v709 - 1.0*v832 + v1044",
		"v220 + v264 - 1.0*v272 + v277 + v430 + v435 - 1.0*v485 - 1.0*v973 + v984 + v998 + v999 + v1010 + v1019 + v1021",
		"v654 - 1.0*v551 + v656","v653 - 1.0*v552 + v655","v652 - 1.0*v554 + v657","-1.0*v555",
		"-1.0*v553","v17 + v444 - 1.0*v470 + v471 + v501 + v578 - 1.0*v827 + v832 + v1012 - 1.0*v1014",
		"- 1.0*v336 - 1.0*v471","v476 - 1.0*v1044","v477 - 1.0*v476 + v482 + v483 + v640","- 1.0*v337 - 1.0*v482 - 1.0*v483",
		"v475 - 1.0*v472","- 1.0*v338 - 1.0*v475","v474 - 1.0*v473","- 1.0*v339 - 1.0*v474","v478 - 1.0*v506",
		"- 1.0*v340 - 1.0*v478","v480 - 1.0*v570","- 1.0*v341 - 1.0*v480","- 1.0*v459 - 1.0*v825",
		"v69 - 1.0*v469 + v484 + v490 + v825","- 1.0*v342 - 1.0*v484","v865 - 1.0*v487 - 1.0*v486",
		"v178 - 1.0*v181","v181 - 1.0*v343","v236 - 1.0*v488","v55 + v68 + v196 + v492 + 2.0*v550 - 1.0*v676 - 1.0*v742 - 1.0*v919 + v931 + v1058",
		"v541 - 1.0*v489","v549","v676 - 1.0*v541","v489 - 1.0*v549","v95 + v96 + v97 + v98 + v460 + v477 + v502 - 1.0*v578 + v640 + v702 + v703 + v704 + v705 + v706 + v1012 + 2.0*v1015 + v1072",
		"2.0*v1016 - 1.0*v493 - 1.0*v501 - 1.0*v502 - 1.0*v344","v2 + v3 + v9 + v494 - 1.0*v548",
		"v493 - 1.0*v345 - 1.0*v494","v498 - 1.0*v497","- 1.0*v346 - 1.0*v498","v500 - 1.0*v569",
		"- 1.0*v347 - 1.0*v500","v504 - 1.0*v106 - 1.0*v121 - 0.25*v150 - 1.0*v163 - 1.0*v185 - 1.0*v490 - 1.0*v50 + v505 - 1.0*v512 - 1.0*v513 - 1.0*v515 - 1.0*v546 - 1.0*v611 - 1.0*v871",
		"- 1.0*v348 - 1.0*v505","v516 - 1.0*v462","v507 - 1.0*v468","v468 - 1.0*v467 + v724 + v803",
		"- 1.0*v514 - 1.0*v1036","- 1.0*v349 - 1.0*v508 - 1.0*v518 - 1.0*v519 - 1.0*v520","v509 - 1.0*v562",
		"v517 - 1.0*v516","v534 - 1.0*v531 - 1.0*v532 - 2.0*v521 + v535 + v536 + v607 - 1.0*v667 + v1045",
		"v491 - 0.582*v150 + v523 - 1.0*v530 + v540 - 1.0*v562 - 1.0*v865 + v990","- 1.0*v350 - 1.0*v540",
		"v522 - 1.0*v88","- 1.0*v351 - 1.0*v522","v142 + v143 + v524 + v525","- 1.0*v352 - 1.0*v524 - 1.0*v525",
		"v526 - 1.0*v464 - 1.0*v465 - 1.0*v466 - 1.0*v463 + v527 + v538 + v551 + v552 + v553 + v554 + v555 - 1.0*v814 - 1.0*v834",
		"- 1.0*v353 - 1.0*v526 - 1.0*v527","v592 - 1.0*v529 + v593 + v1023","v88 + v174 - 1.0*v528 - 1.0*v537 - 1.0*v538 + v554",
		"v537 - 1.0*v354","v488 + v531 + v532 + v533 - 1.0*v534 - 1.0*v535 - 1.0*v536 + v830",
		"- 1.0*v355 - 1.0*v533","v499 - 1.0*v495 - 0.154*v150","v543 - 1.0*v544","v544 - 1.0*v542",
		"v929 - 1.0*v543","v63 - 1.0*v492 - 1.0*v545 + v546 + v557 + v566 - 1.0*v775 + v777","-1.0*v550",
		"v252 - 1.0*v556","v558 - 1.0*v557 + v775 + v785 - 1.0*v892","- 1.0*v356 - 1.0*v558","-1.0*v561",
		"v539 + v559 - 1.0*v560 + 2.0*v561 + v562 - 1.0*v650","v742 - 1.0*v55 - 1.0*v68 - 0.203*v150 - 1.0*v196 - 1.0*v563 - 1.0*v564 - 1.0*v26 - 1.0*v777 - 1.0*v785 - 1.0*v923 - 1.0*v931 - 1.0*v1058",
		"v560 - 1.0*v559","v567 - 1.0*v566 - 1.0*v565 + v568 + v892 + v893","- 1.0*v357 - 1.0*v567 - 1.0*v568",
		"v445 - 1.0*v599 - 1.0*v600 - 1.0*v601","v691 - 1.0*v624 - 1.0*v253","v131 - 2.0*v158 + v816 + v819 + v897 + v956",
		"v256 + v258 - 1.0*v358 - 1.0*v493 - 1.0*v571 - 1.0*v727 + v1001 + v1003 - 1.0*v1016",
		"v189 - 1.0*v190 - 1.0*v970","v574 - 1.0*v76 - 1.0*v163 - 1.0*v187 - 1.0*v27","v73 + v191 - 1.0*v697 + v912",
		"v575 - 1.0*v432 - 1.0*v13 + 0.36*v652 + 0.36*v653 + 0.36*v654 + 0.36*v839 + 0.36*v840 + 0.36*v841",
		"- 1.0*v360 - 1.0*v575","0.07*v652 - 1.0*v14 + 0.07*v653 + 0.07*v654 + 0.07*v839 + 0.07*v840 + 0.07*v841",
		"v14 + v155 - 1.0*v281 - 0.14*v814","v576","v579 - 0.09*v150 + v581 + v582","- 1.0*v361 - 1.0*v581 - 1.0*v582",
		"v597 - 1.0*v580","v580 - 1.0*v579","v589 - 1.0*v583","v230 - 1.0*v584","v585 - 1.0*v1051",
		"v243","- 1.0*v594 - 1.0*v595 - 1.0*v596","- 1.0*v591 - 1.0*v592 - 1.0*v593","v17","v51 - 1.0*v598 + v603 + v894 + v895",
		"- 1.0*v362 - 1.0*v603","v128 + v129 + v130 + v131 - 1.0*v905","v605 - 1.0*v606 - 1.0*v945",
		"v43 - 1.0*v604 - 1.0*v607","v4 + v5 + v608 + v609 + v610","- 1.0*v363 - 1.0*v610","v56",
		"v615 - 1.0*v614 - 0.276*v150 + v616","- 1.0*v364 - 1.0*v615 - 1.0*v616","v612 - 1.0*v597",
		"v545 - 1.0*v68 + v598 - 1.0*v617 - 1.0*v618 + v620 - 1.0*v767","v619 + v1018 - 1.0*v1020 + v1021",
		"- 1.0*v365 - 1.0*v619","v555 + v700","v48 - 1.0*v620 + v621 + v622 + v767 - 1.0*v894",
		"- 1.0*v366 - 1.0*v621 - 1.0*v622","v624 - 1.0*v556 - 1.0*v623 - 1.0*v252 - 5.0*v791 - 8.0*v1040",
		"-1.0*v56","v638 + v639","- 1.0*v367 - 1.0*v638 - 1.0*v639","v279 - 1.0*v280","v716 - 1.0*v281 - 1.0*v279",
		"v281 - 1.0*v282","v636 - 1.0*v635","v635 - 1.0*v634","v715 - 1.0*v716","v274 + v539 - 1.0*v645 - 1.0*v646",
		"- 1.0*v274 - 1.0*v368","v641 + v642 - 1.0*v662 - 1.0*v663 + v664","- 1.0*v369 - 1.0*v664",
		"v439 - 1.0*v641 - 1.0*v642 - 1.0*v643 + v917","v644 - 1.0*v640","- 1.0*v370 - 1.0*v644",
		"v647 - 0.428*v150 + v648 + v649","- 1.0*v371 - 1.0*v648 - 1.0*v649","v650 - 1.0*v539",
		"v280 - 1.0*v658","v282","v983 - 1.0*v715","v651 - 1.0*v983","v1061 - 1.0*v651","v658 - 0.0084*v150",
		"v157 - 0.326*v150 + v207 - 1.0*v659 + v660 + v661","- 1.0*v157 - 1.0*v372 - 1.0*v660 - 1.0*v661",
		"v684 - 5.0*v152 - 5.0*v153 - 6.0*v154 - 6.0*v155 - 7.0*v156 - 1.0*v631 - 1.0*v632 - 1.0*v633 - 1.0*v666 - 4.0*v151",
		"v456 + v667 + v674 + v675 - 1.0*v685 - 1.0*v686 - 1.0*v687 - 1.0*v689 - 1.0*v690","- 1.0*v373 - 1.0*v674 - 1.0*v675",
		"v27 - 1.0*v684","v673","v672 - 1.0*v96 - 1.0*v97 - 1.0*v98 - 1.0*v95 + v702","- 1.0*v374 - 1.0*v672 - 1.0*v673",
		"v98 - 1.0*v706 - 1.0*v709","v97 - 1.0*v98 + v668 - 1.0*v705 + v706 - 1.0*v708 + v709",
		"- 1.0*v375 - 1.0*v668","v96 - 1.0*v97 + v669 - 1.0*v704 + v705 - 1.0*v707 + v708","- 1.0*v376 - 1.0*v669",
		"v670 - 1.0*v95 - 1.0*v702 + v703","- 1.0*v377 - 1.0*v670","v95 - 1.0*v96 + v671 - 1.0*v703 + v704 + v707",
		"- 1.0*v378 - 1.0*v671","- 1.0*v676 - 1.0*v842","v678 - 1.0*v677 + v680 + v842","- 1.0*v379 - 1.0*v678",
		"- 1.0*v380 - 1.0*v680","- 1.0*v679 - 1.0*v714","v693 - 1.0*v477","- 1.0*v381 - 1.0*v693",
		"v696","- 1.0*v382 - 1.0*v696","v697 - 1.0*v695 - 0.146*v150 + v698 + v1049","- 1.0*v383 - 1.0*v698",
		"v720 - 1.0*v719","-1.0*v700","v701 - 1.0*v682","v491 + v530 - 1.0*v717 - 1.0*v720 - 1.0*v721 - 1.0*v1006",
		"v712 - 1.0*v711","v711 - 1.0*v710","v713 - 1.0*v665","- 1.0*v384 - 1.0*v713","v129 + v238 - 1.0*v255 - 1.0*v256 + v442 - 1.0*v448 + v465 + v535 + v600 + v663 + v687 + v728 + v731 - 1.0*v762 - 1.0*v1000 - 1.0*v1001",
		"v102 - 1.0*v129 - 1.0*v238 + v255 + v256 - 1.0*v442 + v448 - 1.0*v465 - 1.0*v535 - 1.0*v600 - 1.0*v663 - 1.0*v687 - 1.0*v728 - 1.0*v731 + v762 + v1000 + v1001",
		"v699 - 1.0*v650","v11 + v152 - 1.0*v280 - 1.0*v282 - 0.04*v814","v953","v520 - 1.0*v739 - 2.0*v740 - 1.0*v741 + v846 + v876 + v942 + v996",
		"v739 - 1.0*v520 - 1.0*v385 + 2.0*v740 + v741 - 1.0*v846 - 1.0*v876 - 1.0*v942 - 1.0*v996",
		"v725 - 1.0*v738 + v756 + v758","- 1.0*v386 - 1.0*v725","- 1.0*v387 - 1.0*v727","v753 + v755 - 1.0*v756",
		"- 1.0*v388 - 1.0*v750","v738 + v752 - 1.0*v757 - 1.0*v758 + v759","v726 - 1.0*v751 - 1.0*v752 - 1.0*v753 + v754",
		"v727 - 1.0*v389 - 1.0*v754 - 1.0*v755","v760 + v761 + v762 - 1.0*v763 - 1.0*v786","v763 - 1.0*v760 - 1.0*v390",
		"v763 - 1.0*v762 - 1.0*v761","- 1.0*v391 - 1.0*v763","v158 - 1.0*v7 - 1.0*v131 - 1.0*v6 - 1.0*v171 - 1.0*v177 - 0.5*v193 - 0.5*v194 - 1.0*v230 - 1.0*v250 - 1.0*v589 + v787 - 0.5*v796 - 0.5*v798 - 0.5*v801 - 1.0*v816 - 1.0*v819 - 1.5*v860 - 1.0*v861 - 1.0*v897 + v956 - 1.0*v976",
		"- 1.0*v392 - 1.0*v787","-2.0*v956","v133 + v172 - 1.0*v182 - 1.0*v683 + v685 + v686 + v687 + v851 - 1.0*v853 + v974",
		"v790 - 1.0*v433","- 1.0*v393 - 1.0*v790","0.5*v652 - 1.0*v15 + 0.5*v653 + 0.5*v654 + 0.5*v839 + 0.5*v840 + 0.5*v841",
		"v791 - 1.0*v572 - 1.0*v235","v15 + v156 - 1.0*v814","v821 - 1.0*v792","v583 + v584 - 1.0*v799",
		"v41 - 1.0*v115 - 1.0*v789 - 1.0*v802 - 1.0*v803 + v804 - 1.0*v886","v115 - 1.0*v394 - 1.0*v804 + v886",
		"- 1.0*v797 - 1.0*v805","v237 + v238 + v805","0.02*v164 + 0.02*v205 - 0.02*v210 - 0.02*v811 + 0.02*v814",
		"v89 - 1.0*v809","v819 - 1.0*v89","v13 + v154 - 0.72*v814","v852 - 1.0*v885","v263 - 1.0*v810",
		"v45 - 1.0*v144 + v813","v65 - 1.0*v813","-0.02*v841","v817 - 1.0*v816 - 1.0*v818 + v900",
		"0.02*v879 - 0.02*v820 - 0.02*v840 - 0.0019350000000000000879851747015437*v150","-1.0*v819",
		"v283 - 1.0*v37 - 1.0*v219 - 1.0*v225 - 1.0*v30 - 1.0*v451 - 1.0*v452 - 1.0*v478 - 1.0*v484 - 1.0*v501 - 1.0*v636 - 1.0*v673 - 1.0*v680 - 1.0*v713 - 1.0*v851 + v853 + v863 - 1.0*v878 - 1.0*v902 - 1.0*v933 - 1.0*v969 - 1.0*v1017 - 1.0*v1033",
		"v864 - 0.0276*v150","0.02*v833 - 0.04*v174 - 0.02*v655 - 0.02*v656 - 0.02*v657 - 0.000464*v150 - 0.02*v839",
		"0.02*v834 - 0.02*v833","v809","v836 - 1.0*v835 - 0.176*v150","- 1.0*v395 - 1.0*v836",
		"v440 - 1.0*v576","v595 - 1.0*v993","v835 + v859","v792 - 1.0*v8 - 1.0*v817","2.0*v454 - 1.0*v396 + 2.0*v471 + v527 + 2.0*v678 - 1.0*v837 - 1.0*v838",
		"-1.0*v107","v810 - 1.0*v845 + v846","- 1.0*v397 - 1.0*v846","v849 - 1.0*v28 + v854","v883 - 1.0*v849",
		"v850 - 4.0*v585","v28 - 1.0*v683 + v710 + v788 - 1.0*v854 - 1.0*v883","v168 - 1.0*v858 - 1.0*v859",
		"v860 - 1.0*v440","v177 - 1.0*v860","v784 + v785","v862 - 1.0*v861","- 1.0*v398 - 1.0*v862",
		"v513 - 1.0*v865","v105 - 1.0*v867","v870 - 1.0*v868","v140 - 1.0*v870","v868 - 1.0*v872",
		"v872 - 1.0*v611","v808 - 0.21*v150 - 1.0*v873 + v874 + v875 + v876","- 1.0*v399 - 1.0*v874 - 1.0*v875 - 1.0*v876",
		"v805 - 1.0*v105 - 1.0*v140 - 1.0*v513 - 1.0*v566 - 1.0*v598 - 1.0*v738 - 1.0*v759 - 1.0*v64 + v877 - 1.0*v1053 - 1.0*v1069",
		"0.02*v882 - 0.02*v879 - 0.000051999999999999996799868867691785*v150","v880 - 1.0*v881",
		"v71 - 0.035*v150 + v802 + v886 - 1.0*v887 + v888 + v889 - 1.0*v955","- 1.0*v400 - 1.0*v886 - 1.0*v888 - 1.0*v889",
		"v83 + v85 - 1.0*v602 - 1.0*v897 + v898","v602 - 1.0*v898","v816 - 1.0*v85 - 1.0*v83 + v897 + v899 - 1.0*v901",
		"v901 - 1.0*v899","v818 - 1.0*v900","- 1.0*v401 - 1.0*v904","v193 - 1.0*v128 + v194 - 1.0*v237 - 1.0*v441 - 1.0*v464 - 1.0*v493 - 1.0*v534 - 1.0*v599 - 1.0*v646 - 1.0*v662 - 1.0*v686 - 1.0*v729 - 1.0*v730 + v761 - 1.0*v847 - 1.0*v966",
		"v128 - 1.0*v193 - 1.0*v194 + v237 + v254 + v441 + v464 + v493 + v534 + v599 + v646 + v662 + v686 + v729 + v730 - 1.0*v761 + v847 + v966",
		"v905 - 1.0*v759","v890 - 1.0*v855 + v892 + v894 + v896 + v903","v104 + v175 + v753 + v755 + v855 - 1.0*v877 + v909 - 1.0*v927 - 1.0*v998",
		"v111 - 1.0*v910","v928 - 1.0*v63","v74 - 1.0*v912","v913 - 1.0*v909","- 1.0*v402 - 1.0*v913",
		"v908 - 1.0*v906","v915 - 1.0*v917","v914 - 1.0*v915","v916 - 1.0*v914","- 1.0*v403 - 1.0*v916",
		"v547 - 1.0*v211 - 1.0*v10 - 1.0*v926 + v927","v910 - 1.0*v911 + v1066","v998 - 1.0*v973 - 1.0*v929",
		"v933 - 1.0*v932","- 1.0*v404 - 1.0*v933","v959 - 1.0*v764","v946 - 1.0*v947","-1.0*v936",
		"v936","v265 - 1.0*v939","- 1.0*v265 - 1.0*v405","v553 - 1.0*v491 - 0.205*v150 + v881 - 1.0*v882 - 1.0*v937 - 1.0*v938 - 1.0*v940 + v941 + v942 - 1.0*v1019 - 1.0*v1020",
		"- 1.0*v406 - 1.0*v941 - 1.0*v942","v937 - 3.0*v284","v947","v949 - 1.0*v878","v948 - 1.0*v949",
		"- 1.0*v934 - 1.0*v935","v935 + v986","v186 + v813 + v970 + v976","v971 - 1.0*v931","- 1.0*v407 - 1.0*v971",
		"v559 - 0.007*v150 - 1.0*v560 - 1.0*v952 - 1.0*v953 + v954 + v955","- 1.0*v408 - 1.0*v954",
		"v806 - 1.0*v945","v969 - 1.0*v444","v138 - 1.0*v930","v960 - 1.0*v959","v130 - 1.0*v173 + v448 + v449 + v583 + v607 + v682 - 1.0*v854 + v934 + v943 + v950 + v957 + v958 + v961 + v962 + v963 - 1.0*v964 - 1.0*v965 - 1.0*v967 - 1.0*v968 - 1.0*v975 + v976",
		"v173 - 1.0*v409 - 1.0*v961 - 1.0*v962 - 1.0*v963 + v964 + v967 + v975","v78 - 1.0*v138 - 0.0000030000000000000000760025722912339*v150 - 1.0*v596 - 1.0*v712 + v854 + v968 - 1.0*v986",
		"v944 - 1.0*v943","v951 - 1.0*v944","v596 - 1.0*v950","v930 - 1.0*v951","- 1.0*v410 - 1.0*v969",
		"v18 - 1.0*v957 - 1.0*v958","v506 - 1.0*v823","v823 - 1.0*v984","v570 + v972","v975 - 1.0*v974",
		"- 1.0*v411 - 1.0*v975","v977 - 1.0*v976","- 1.0*v412 - 1.0*v977","v186","v12 + v153 - 0.1*v814",
		"v231 - 1.0*v986","v75 + v233 + v453 + v486 - 1.0*v491 - 1.0*v530 + v697 + v717","v989 - 1.0*v1007",
		"- 1.0*v413 - 1.0*v989","v1007 - 1.0*v1008 + v1009","v945 - 1.0*v806 + v1008","v993 - 1.0*v990 - 1.0*v991 - 1.0*v992 - 0.241*v150 + v994 + v995 + v996",
		"- 1.0*v414 - 1.0*v994 - 1.0*v995 - 1.0*v996","v1005","v771 + v987 + v988 - 1.0*v1004 - 1.0*v1005",
		"- 1.0*v415 - 1.0*v987 - 1.0*v988","v1000 + v1002","v1001 - 1.0*v416 + v1003","- 1.0*v1000 - 1.0*v1002",
		"- 1.0*v417 - 1.0*v1001 - 1.0*v1003","v813 + v918 + v919 + v920 + v921 + v922 + v923 + v924 + v925 - 1.0*v1011",
		"v1011 - 1.0*v918 - 1.0*v919 - 1.0*v920 - 1.0*v921 - 1.0*v922 - 1.0*v923 - 1.0*v924 - 1.0*v925 - 1.0*v813",
		"v1014 - 1.0*v1013 - 1.0*v1012 + v1017","v1013 - 1.0*v1015","- 1.0*v418 - 1.0*v1016 - 1.0*v1017",
		"v516 - 1.0*v517","v1019 - 1.0*v1018 - 0.054*v150 + v1020 + v1022","- 1.0*v419 - 1.0*v1022",
		"v1024 - 1.0*v186","- 1.0*v420 - 1.0*v1024","0.02*v652 - 1.0*v431 - 1.0*v11 + 0.02*v653 + 0.02*v654 + 0.02*v839 + 0.02*v840 + 0.02*v841 + v1025",
		"- 1.0*v421 - 1.0*v1025","0.05*v652 - 1.0*v12 + 0.05*v653 + 0.05*v654 + 0.05*v839 + 0.05*v840 + 0.05*v841",
		"v1027 - 1.0*v997 - 1.0*v1026 - 0.131*v150","- 1.0*v422 - 1.0*v1027","v1028 - 1.0*v651 - 1.0*v1061",
		"v1032 - 1.0*v1047","v1047 - 1.0*v1028","v1035 - 1.0*v864","v1033 - 1.0*v1038","v1034 - 1.0*v1031 - 1.0*v1032 - 1.0*v1033 - 1.0*v29 - 1.0*v1035",
		"v1031 - 1.0*v1030","v1030 - 1.0*v36","v812 - 1.0*v1035","v1037 - 1.0*v1036","v1036 - 1.0*v1029",
		"v1038 - 1.0*v1037","v276 + v864 - 1.0*v1039 + v1040","v1039 - 1.0*v812 - 1.0*v29","v36 + v651 + 2.0*v658 - 1.0*v743 - 1.0*v921 + v1014 + v1035 + v1048",
		"v479 - 0.003*v150 + v481 - 2.0*v658 - 1.0*v1014 - 1.0*v1041 - 1.0*v1043 - 1.0*v1044",
		"v1041 - 1.0*v1042 + v1044","v1042","v1043","v1029 - 1.0*v1046","v1046 - 1.0*v812","v29 - 1.0*v768 + v783 + v797 + v812 - 1.0*v1048 + v1053 + v1058 + v1061",
		"v29 - 1.0*v36","v36 - 1.0*v16","v16 - 1.0*v276","v1051 - 1.0*v1050 - 1.0*v1052","v183 + v268 + v903 - 1.0*v1053 + v1054 + v1055",
		"- 1.0*v423 - 1.0*v1054 - 1.0*v1055","v91 - 1.0*v1045","v71 + v91 + v1056","- 1.0*v424 - 1.0*v1056",
		"v195 + v768 - 1.0*v903 - 1.0*v1058 + v1059 + v1060","- 1.0*v425 - 1.0*v1059 - 1.0*v1060",
		"v743 - 1.0*v185 - 1.0*v479 - 1.0*v481 - 0.136*v150 - 1.0*v783 - 1.0*v925 - 1.0*v1034",
		"v1063 - 1.0*v1062 - 0.402*v150 + v1064 + v1065","- 1.0*v426 - 1.0*v1063 - 1.0*v1064",
		"v565 + v896 + v1067 + v1068 - 1.0*v1069","- 1.0*v427 - 1.0*v1067 - 1.0*v1068","v618 - 1.0*v546 - 1.0*v766 + v1069",
		"v766 - 1.0*v896 + v1070","- 1.0*v428 - 1.0*v1070","v911 + v926 - 1.0*v998 - 1.0*v999 + v1073",
		"v637 - 1.0*v1066","v1074 - 1.0*v1071 + v1075","- 1.0*v429 - 1.0*v1074 - 1.0*v1075","v1071 - 1.0*v1073",
		"v139 - 7.6","v24 - 2.0*v22 - 1.0*v21 - 1.0*v27 - 1.0*v32 - 1.0*v40 + v46 - 1.0*v53 - 1.0*v148 - 0.00005*v150 - 1.0*v182 + 7.0*v431 + 8.0*v432 + 9.0*v433 - 1.0*v459 + v523 - 1.0*v628 - 1.0*v632 - 1.0*v667 + v815 + v824 - 1.0*v884 - 1.0*v938 - 1.0*v952 - 1.0*v953 - 1.0*v978- tr_126_1",
		"tr_126_1","v27 + v28 + v31 + v34 + 2.0*v54 + v55 + v56 + v59 + v62 + v65 + v76 + v80 + v86 + v100 + 2.0*v108 + v109 + v112 + v118 + v123 + v127 + v134 + v139 - 1.0*v141 + 45.5608*v150 + v160 + v162 + 2.0*v163 + v165 + v166 + v185 + v192 + v199 + v200 + v203 + v205 - tr_151_1",
		" v209 + v212 + v216 + v218 + v221 + v234 + v262 + v266 + v267 + v273 + v438 + v443 + v450 + v476 + v482 + v487 + v492 + v499 + v504 + v505 + v507 + v509 + v518 + v524 + v526 + v529 + v538 + v543 + v548 + v557 + v560 + v562 + v577 + v578 + v581 - tr_151_2",
		" v586 + v595 + v615 + v620 + v638 + v648 + 3.0*v658 + v660 + v668 + v669 + v670 + v671 + v672 + v696 + v698 + v723 + v734 + v738 + v742 + v743 + v744 + v745 + v746 + v747 + v748 + v749 + v804 + v822 + v823 + v828 + v837 + v844 + v845 - 1.0*v849 + v853 - tr_151_3",
		" v865 + v866 + v869 + v871 + v874 + v888 + v898 + v899 + v900 - 1.0*v902 + v906 + v909 + v910 + v913 + v915 - 1.0*v918 + v949 + v954 + v961 + v968 + v971 + v977 + v983 + v989 + v994 + v1004 + v1007 + v1008 + v1024 + v1029 + v1036 + v1037 + v1046 - tr_151_4",
		" v1048 + v1057 + v1063 + v1073 + v1074- tr_151_5","tr_151_1 + tr_151_2 + tr_151_3 + tr_151_4 + tr_151_5",
		"v11 + v12 + v13 + v14 + v15 + v46 - 1.0*v54 - 1.0*v55 - 1.0*v56 + v59 + v64 + v66 - 1.0*v104 + v117 + v121 + v122 + v144 - 0.001*v150 + 6.0*v284 + v431 + v432 + v433 + v517 + v546 + v588 + v726 + v736 - 1.0*v773 + v781 + v809 + v810 + v863 + v877 + v936 + v959 + v997- tr_180_1",
		"tr_180_1","v109 - 1.0*v12 - 1.0*v13 - 1.0*v14 - 1.0*v15 - 1.0*v27 - 1.0*v28 - 1.0*v31 - 1.0*v34 - 1.0*v46 - 1.0*v54 - 1.0*v58 - 1.0*v59 - 1.0*v62 - 1.0*v65 - 1.0*v76 - 1.0*v80 - 1.0*v86 - 1.0*v100 - 1.0*v11 - 1.0*v112 - 1.0*v117 - 1.0*v118 - 1.0*v121 - 1.0*v122 - tr_200_1",
		"- 1.0*v123 - 1.0*v127 - 1.0*v134 - 1.0*v139 - 1.0*v140 + v141 - 45.73179999999999978399500832893*v150 - 1.0*v159 - 1.0*v160 - 1.0*v161 - 1.0*v162 - 2.0*v163 - 1.0*v165 - 1.0*v166 - 1.0*v185 - 1.0*v192 - 1.0*v199 - 1.0*v200 - 1.0*v203 - 1.0*v205 - 1.0*v209 - tr_200_2",
		"- 1.0*v212 - 1.0*v216 - 1.0*v218 - 1.0*v221 - 1.0*v228 - 1.0*v234 - 1.0*v262 - 1.0*v266 - 1.0*v267 - 1.0*v273 - 1.0*v431 - 1.0*v432 - 1.0*v433 - 1.0*v438 - 1.0*v443 - 1.0*v446 - 1.0*v450 - 1.0*v476 - 1.0*v482 - 1.0*v487 - 1.0*v492 - 1.0*v503 - tr_200_3",
		"- 1.0*v504 - 1.0*v505 - 1.0*v507 - 1.0*v509 - 1.0*v517 - 1.0*v518 - 1.0*v524 - 1.0*v526 - 1.0*v529 - 1.0*v538 - 1.0*v542 - 1.0*v543 - 1.0*v546 - 1.0*v548 - 1.0*v557 - 1.0*v560 - 1.0*v562 - 1.0*v577 - 1.0*v578 - 1.0*v581 - 1.0*v586 - 1.0*v588 - 1.0*v595 - tr_200_4",
		"- 1.0*v615 - 1.0*v620 - 1.0*v638 - 1.0*v648 - 1.0*v660 - 1.0*v668 - 1.0*v669 - 1.0*v670 - 1.0*v671 - 1.0*v672 - 1.0*v695 - 1.0*v696 - 1.0*v698 - 1.0*v723 - 1.0*v734 - 1.0*v736 - 1.0*v738 - 1.0*v742 - 1.0*v743 - 1.0*v744 - 1.0*v745 - 1.0*v746 - 1.0*v747 - tr_200_5",
		"- 1.0*v748 - 1.0*v749 - 1.0*v751 - 1.0*v757 - 1.0*v781 - 1.0*v804 - 1.0*v809 - 1.0*v810 - 1.0*v822 - 1.0*v823 - 1.0*v828 - 1.0*v837 - 1.0*v844 - 1.0*v845 + v849 - 1.0*v853 - 1.0*v863 - 1.0*v865 - 1.0*v866 - 1.0*v869 - 1.0*v871 - 1.0*v874 - 1.0*v877 - tr_200_6",
		"- 1.0*v885 - 1.0*v888 - 1.0*v898 - 1.0*v899 - 1.0*v900 + v902 - 1.0*v906 - 1.0*v909 - 1.0*v910 - 1.0*v913 - 1.0*v915 - 1.0*v922 - 1.0*v931 - 1.0*v936 - 1.0*v937 - 1.0*v949 - 1.0*v954 - 1.0*v959 - 1.0*v961 - 1.0*v968 - 1.0*v971 - 1.0*v977 - 1.0*v983 - tr_200_7",
		"- 1.0*v989 - 1.0*v994 - 1.0*v997 - 1.0*v1004 - 1.0*v1007 - 1.0*v1008 - 1.0*v1024 - 1.0*v1029 - 1.0*v1036 - 1.0*v1037 - 1.0*v1046 - 1.0*v1048 - 1.0*v1057 - 1.0*v1063 - 1.0*v1073 - 1.0*v1074- tr_200_8",
		"tr_200_1 + tr_200_2 + tr_200_3 + tr_200_4 + tr_200_5 + tr_200_6 + tr_200_7 + tr_200_8",
		"v33 + v35 + v57 + v78 + v107 + v114 + v125 + 4.0*v151 + 5.0*v152 + 5.0*v153 + 6.0*v154 + 6.0*v155 + 7.0*v156 - 1.0*v162 + v176 + 2.0*v177 + 2.0*v187 + v207 - 1.0*v212 + v235 + v272 + v441 + v442 + v445 + v510 + v521 + v530 + v547 - 1.0*v574 + v604 + v613 - tr_229_1",
		" v631 + v632 + v633 + v637 + v659 + v666 + v689 + v690 + v710 + v759 + v795 + v797 + v800 + v802 + v806 + v815 + v817 + v847 - 1.0*v851 + v852 + v853 + v858 + v859 + v879 + v930 + v976 + v997 + v1045 + 4.0*v1052- tr_229_2",
		"tr_229_1 + tr_229_2","v22 - 1.0*v24 - 1.0*v28 + v32 + v40 - 1.0*v45 - 1.0*v46 + v53 - 1.0*v78 + v107 + v138 - 0.0000060000000000000001520051445824677*v150 + v182 + v262 - 7.0*v431 - 8.0*v432 - 9.0*v433 + v459 - 1.0*v523 + v596 + v628 + v632 + v667 + v683 + v684 + v764 - 1.0*v788 - tr_231_1",
		"- 1.0*v809 - 1.0*v815 - 1.0*v824 + v883 + v884 + v938 + v952 + v953 - 1.0*v959 - 1.0*v968 + v978 + v986- tr_231_2",
		"tr_231_1 + tr_231_2","v18 - 1.0*v32 + v44 + v50 + v84 + v106 + v121 + v133 - 0.25*v150 + v163 + v185 - 1.0*v234 + v490 - 1.0*v504 - 1.0*v507 + v508 - 1.0*v509 - 1.0*v510 - 1.0*v511 + v512 + v513 + v514 + 2.0*v515 - 1.0*v517 + v518 + v519 + v520 + v546 - 1.0*v597 + v611 + v614 - tr_388_1",
		"- 1.0*v647 - 1.0*v792 + v803 + v807 + v835 + v871 - 1.0*v880 + v887 + v935 + v943 + v951 - 1.0*v979 + v1026 - 1.0*v1049 + v1062- tr_388_2",
		"tr_388_1 + tr_388_2","v6 + v7 - 1.0*v8 - 1.0*v17 - 1.0*v19 - 1.0*v41 - 1.0*v48 - 1.0*v51 - 1.0*v69 - 1.0*v71 - 1.0*v73 - 1.0*v74 - 1.0*v86 - 1.0*v89 - 1.0*v90 - 1.0*v91 - 1.0*v92 + v94 - 1.0*v103 - 1.0*v104 - 1.0*v108 - 1.0*v109 - 1.0*v112 - 1.0*v118 - 1.0*v120 - 1.0*v121 - tr_424_1",
		"- 1.0*v123 - 1.0*v134 - 1.0*v139 + v141 - 1.0*v142 - 1.0*v143 - 1.0*v144 + v145 + v146 - 45.5608*v150 + 5.0*v151 + 6.0*v152 + 6.0*v153 + 7.0*v154 + 7.0*v155 + 8.0*v156 + 2.0*v158 - 1.0*v159 - 1.0*v160 - 1.0*v161 - 1.0*v163 - 1.0*v164 - 1.0*v166 - 1.0*v175 - tr_424_2",
		" 2.0*v177 + v180 - 1.0*v182 - 1.0*v183 - 1.0*v185 - 1.0*v189 - 1.0*v191 - 1.0*v192 + v193 + v194 - 1.0*v195 - 1.0*v201 - 1.0*v202 - 1.0*v209 - 1.0*v213 - 1.0*v214 - 1.0*v219 + v223 + v224 + 2.0*v232 - 1.0*v239 - 1.0*v241 + v243 + v244 - 1.0*v250 - tr_424_3",
		"- 3.0*v251 + v253 + v255 + v257 - 1.0*v259 - 1.0*v260 - 1.0*v270 - 1.0*v275 + v278 + v283 - 6.0*v431 - 7.0*v432 - 8.0*v433 - 1.0*v434 - 1.0*v436 - 1.0*v443 - 1.0*v444 - 1.0*v453 - 1.0*v456 - 1.0*v460 + v467 - 1.0*v469 + v472 + v473 - 1.0*v477 - 1.0*v482 - tr_424_4",
		"- 1.0*v488 + v491 + v497 - 1.0*v505 - 1.0*v511 - 1.0*v512 - 1.0*v513 - 1.0*v518 - 1.0*v524 - 1.0*v526 - 1.0*v539 + v541 - 1.0*v544 - 1.0*v546 - 1.0*v550 - 1.0*v551 - 1.0*v552 - 1.0*v553 - 1.0*v554 - 1.0*v555 - 1.0*v559 - 1.0*v563 - 3.0*v564 - tr_424_5",
		"- 1.0*v565 + v571 - 1.0*v574 - 1.0*v576 - 1.0*v579 - 1.0*v580 - 1.0*v581 - 1.0*v583 - 1.0*v584 - 1.0*v585 - 1.0*v602 - 1.0*v606 + v612 + v613 - 1.0*v615 - 1.0*v617 - 1.0*v618 + v624 + v626 - 1.0*v627 - 1.0*v628 - 1.0*v635 - 1.0*v636 - 1.0*v638 - 1.0*v640 - tr_424_6",
		"- 1.0*v641 - 1.0*v642 - 1.0*v648 - 1.0*v652 - 1.0*v653 - 1.0*v654 - 1.0*v660 - 1.0*v667 - 1.0*v668 - 1.0*v669 - 1.0*v670 - 1.0*v671 - 1.0*v672 + v681 - 1.0*v683 + v688 + v691 - 1.0*v695 - 1.0*v696 - 1.0*v698 - 1.0*v700 - 1.0*v701 - 1.0*v702 - 1.0*v703 - tr_424_7",
		"- 1.0*v704 - 1.0*v705 - 1.0*v706 + v714 - 1.0*v717 - 1.0*v718 - 1.0*v719 - 1.0*v724 - 1.0*v726 - 1.0*v735 - 1.0*v738 - 1.0*v752 - 1.0*v753 - 1.0*v755 - 1.0*v756 + v761 + v762 - 1.0*v765 - 1.0*v766 - 1.0*v767 - 1.0*v768 - 1.0*v769 - 1.0*v770 - 1.0*v771 - tr_424_8",
		"- 1.0*v772 - 1.0*v773 - 1.0*v774 - 1.0*v775 - 1.0*v776 - 1.0*v777 - 1.0*v778 - 1.0*v779 - 1.0*v780 - 1.0*v781 - 1.0*v782 - 1.0*v783 - 1.0*v784 - 1.0*v785 + 2.0*v786 - 1.0*v799 - 1.0*v804 - 2.0*v807 - 1.0*v811 + 2.0*v817 - 1.0*v818 - 1.0*v819 - tr_424_9",
		"- 1.0*v829 - 1.0*v830 - 1.0*v833 - 1.0*v837 - 1.0*v839 - 1.0*v840 - 1.0*v841 - 1.0*v843 - 1.0*v847 - 1.0*v848 + 2.0*v850 - 1.0*v851 + v859 + 3.0*v860 - 1.0*v863 - 1.0*v868 - 1.0*v870 - 1.0*v871 - 1.0*v874 - 1.0*v881 - 1.0*v888 - 1.0*v897 - 1.0*v901 - tr_424_10",
		" 2.0*v905 + 2.0*v907 - 1.0*v913 + v918 + v919 + v920 + v921 + v922 + v923 + v924 + v925 - 1.0*v928 - 2.0*v930 - 1.0*v931 - 1.0*v934 - 1.0*v936 - 1.0*v943 - 1.0*v944 - 1.0*v954 - 1.0*v957 - 1.0*v958 + v960 - 1.0*v961 - 3.0*v970 - 1.0*v971 + v974 - 1.0*v977 - tr_424_11",
		" v982 - 1.0*v986 - 1.0*v989 - 1.0*v993 - 1.0*v994 + v997 + v1000 + v1002 - 1.0*v1012 - 1.0*v1013 - 1.0*v1015 - 1.0*v1018 + v1019 + v1020 - 1.0*v1024 - 1.0*v1030 - 1.0*v1039 - 1.0*v1043 - 1.0*v1045 - 1.0*v1047 + v1051 - 1.0*v1061 - 1.0*v1063 - 1.0*v1074- tr_424_12",
		"tr_424_1 + tr_424_2 + tr_424_3 + tr_424_4 + tr_424_5 + tr_424_6 + tr_424_7 + tr_424_8 + tr_424_9 + tr_424_10 + tr_424_11 + tr_424_12",
		"v16 - 1.0*v3 - 1.0*v4 - 1.0*v5 - 1.0*v6 - 1.0*v7 - 1.0*v9 - 1.0*v2 + 2.0*v19 + v20 + v23 + v24 - 1.0*v26 + v27 + v32 - 1.0*v33 - 1.0*v35 + v36 + v38 + v45 + v47 - 1.0*v48 + v49 - 1.0*v51 + v52 - 2.0*v53 - 1.0*v57 + v59 + v60 + v61 + v62 + v63 + v65 - tr_427_1",
		" 2.0*v68 + v72 + v76 + v79 + v80 + v86 + v87 - 1.0*v88 + 2.0*v89 + 2.0*v90 + v92 + v93 + v100 + v102 + 4.0*v103 + v106 - 1.0*v107 + 2.0*v108 + 2.0*v109 - 1.0*v110 + v112 + v113 - 1.0*v114 + v117 + v118 + v119 + v121 + v122 + v123 + v124 - 1.0*v125 + v126 - tr_427_2",
		" v134 + v135 + 2.0*v136 + 3.0*v137 + v138 + v139 + 3.0*v141 + 2.0*v142 + 2.0*v143 - 1.0*v145 - 1.0*v146 + 2.0*v147 + v149 + 45.560349999999999681676854379475*v150 - 14.0*v151 - 17.0*v152 - 16.0*v153 - 20.0*v154 - 19.0*v155 - 22.0*v156 + v157 + v160 - tr_427_3",
		" 2.0*v162 + 2.0*v163 + 2.0*v164 + v165 + v166 + v167 - 1.0*v171 - 2.0*v177 + v182 - 1.0*v183 + v184 + 2.0*v185 + v186 - 3.0*v187 + v188 + v190 + v192 - 2.0*v193 - 2.5*v194 - 1.0*v195 + v196 + v197 + v198 - 1.0*v202 + v204 + v205 + v206 - 1.0*v207 - tr_427_4",
		" v209 - 1.0*v210 + v211 + 3.0*v212 - 1.0*v213 - 1.0*v214 + v215 + v216 + v217 + v218 + v222 + v227 + v229 - 1.0*v231 + v232 - 1.0*v233 + v235 + v239 + v240 - 1.0*v241 + v242 + v246 - 1.0*v247 - 1.0*v248 - 1.0*v249 + 2.0*v250 + 6.0*v251 - 1.0*v253 - tr_427_5",
		" v254 + v260 - 1.0*v261 + v262 - 1.0*v263 + v265 + v267 + v269 + v270 - 1.0*v271 - 1.0*v272 + v273 + v274 + 2.0*v275 + v276 + 6.0*v284 + v286 + 6.0*v431 + 7.0*v432 + 8.0*v433 + v434 + v438 + 2.0*v440 - 3.0*v441 - 3.0*v442 + v443 - 1.0*v445 - 1.0*v446 - tr_427_6",
		" v450 + v453 + v455 + 2.0*v457 + 3.0*v458 + v459 - 1.0*v461 + v463 + v467 - 1.0*v468 + v470 + v474 + v475 + v476 - 1.0*v479 + v480 - 1.0*v481 + v482 + v483 + v485 + v486 + v487 + 2.0*v488 + v494 + v498 + v499 + v500 + v502 - 1.0*v503 + v504 + v505 - tr_427_7",
		" v506 + v509 - 1.0*v510 + v511 - 1.0*v515 - 1.0*v516 + v518 + v519 - 1.0*v521 + v524 + v525 + v526 + v528 + v529 - 1.0*v531 - 1.0*v532 + v533 + v538 + v539 + v540 - 1.0*v542 + v543 - 2.0*v545 + 2.0*v546 + v548 - 1.0*v549 + 2.0*v550 + v551 + v552 - tr_427_8",
		" v553 + v554 + v555 + v557 + v558 + v560 - 1.0*v561 + v562 + 2.0*v564 - 1.0*v565 + v568 + v573 + v574 + v575 + v577 + v578 + 3.0*v579 + v581 + v582 + 2.0*v583 + 2.0*v584 + v586 + v588 + v590 - 1.0*v592 - 1.0*v593 + v594 + v595 - 2.0*v599 - 2.0*v600 - tr_427_9",
		"- 2.0*v601 - 1.0*v608 - 1.0*v609 + v610 + v611 - 1.0*v613 + v615 + v616 + v618 + v619 + v620 + v621 + v622 - 1.0*v624 + v625 + v628 - 1.0*v629 - 1.0*v630 - 1.0*v631 - 1.0*v632 - 2.0*v633 - 1.0*v637 + v638 + v639 + 2.0*v641 + 2.0*v642 - 1.0*v643 + v644 - tr_427_10",
		" v645 + v648 + v649 + v651 + v652 + v653 + v654 + 10.0*v658 - 1.0*v659 + v660 + v661 + v664 + v665 - 1.0*v666 + v667 + v668 + v669 + v670 + v671 + v672 + 2.0*v674 + 3.0*v675 - 1.0*v676 + v679 + v683 + v685 - 1.0*v691 + v693 - 1.0*v694 + v696 + v698 - tr_427_11",
		"- 1.0*v710 + v715 + v716 + v720 - 1.0*v721 + v723 + 2.0*v726 - 1.0*v728 - 1.0*v729 - 4.5*v730 - 3.0*v731 - 3.8*v732 - 1.0*v733 + v734 + v736 + v739 + 3.0*v740 + 2.0*v741 - 1.0*v751 + v753 + v755 - 1.0*v757 + v758 - 2.0*v759 + v760 - 2.0*v761 - 2.0*v762 - tr_427_12",
		" v776 + v777 + v778 + v779 + v780 + v781 + v782 + v783 - 5.0*v786 + v789 + v790 + v793 + v794 - 1.0*v795 - 1.0*v797 - 1.0*v800 - 1.0*v802 + v804 - 1.0*v806 + v807 - 2.0*v808 + v810 + 2.0*v813 + v817 - 1.0*v820 + v821 + v822 + v823 + v826 + v829 - tr_427_13",
		" v834 + v836 + v837 + v838 + v839 + v840 + v841 + v845 + v848 + v850 + v851 - 1.0*v852 + v857 - 1.0*v859 - 1.0*v861 + v862 + 2.0*v863 + v864 + v865 + 2.0*v866 + v869 + v870 + v871 + v873 + v874 + v875 + v877 - 1.0*v879 + v882 - 1.0*v885 + v888 + v889 - tr_427_14",
		" v898 + v899 + v900 - 1.0*v902 + v904 + v905 + v906 + v909 + v910 + v913 + v915 + v916 - 2.0*v930 + v932 - 1.0*v937 + v941 + 2.0*v944 + v946 + 3.0*v947 - 1.0*v948 + v949 + v950 + v952 + v953 + v954 + v955 - 2.0*v956 + 2.0*v957 + 2.0*v958 + v961 + 2.0*v962 - tr_427_15",
		" 3.0*v963 - 1.0*v964 + 5.0*v970 + v971 + v972 + v976 + v977 + v978 - 1.0*v981 + v983 + 2.0*v985 + v987 + v988 + v989 + v991 + v994 + v995 + v997 - 1.0*v1000 - 1.0*v1002 + v1004 + v1007 - 1.0*v1009 - 1.0*v1011 + v1014 + v1022 - 1.0*v1023 + v1024 - tr_427_16",
		" v1025 + v1027 + v1028 + v1029 + 3.0*v1030 - 1.0*v1034 + v1035 + v1036 + v1037 - 1.0*v1038 + v1039 + 3.0*v1043 - 2.0*v1045 + v1046 + v1050 - 4.0*v1052 + v1054 + v1055 + v1058 + v1059 + v1060 + 2.0*v1061 + v1063 + v1064 + v1068 + v1070 + v1073 + v1074 + v1075- tr_427_17",
		"tr_427_1 + tr_427_2 + tr_427_3 + tr_427_4 + tr_427_5 + tr_427_6 + tr_427_7 + tr_427_8 + tr_427_9 + tr_427_10 + tr_427_11 + tr_427_12 + tr_427_13 + tr_427_14 + tr_427_15 + tr_427_16 + tr_427_17",
		"2.0*v193 - 1.0*v23 - 1.0*v38 - 1.0*v47 - 1.0*v52 - 1.0*v60 - 1.0*v61 - 1.0*v79 - 1.0*v87 - 1.0*v93 - 1.0*v113 - 1.0*v124 - 1.0*v135 - 2.0*v136 - 3.0*v137 - 4.0*v141 - 1.0*v149 - 1.0*v157 - 1.0*v167 - 1.0*v184 - 1.0*v188 - 1.0*v20 + 2.5*v194 - 1.0*v197 - tr_428_1",
		"- 1.0*v198 - 1.0*v204 - 1.0*v206 - 1.0*v215 - 1.0*v217 - 1.0*v222 - 1.0*v246 - 1.0*v265 - 1.0*v269 - 1.0*v274 - 1.0*v286 - 1.0*v359 + 2.0*v441 + 2.0*v442 - 1.0*v455 - 2.0*v457 - 3.0*v458 - 1.0*v474 - 1.0*v475 - 1.0*v480 - 1.0*v483 + v493 - 1.0*v494 - tr_428_2",
		"- 1.0*v498 - 1.0*v500 - 1.0*v502 - 1.0*v519 - 1.0*v525 - 1.0*v533 - 1.0*v540 - 1.0*v558 - 1.0*v568 - 1.0*v573 - 1.0*v575 - 1.0*v582 - 1.0*v590 + 2.0*v599 + 2.0*v600 + 2.0*v601 - 1.0*v610 - 1.0*v616 - 1.0*v619 - 1.0*v621 - 1.0*v622 - 1.0*v639 - 1.0*v644 - tr_428_3",
		"- 1.0*v649 - 1.0*v661 - 1.0*v664 - 2.0*v674 - 3.0*v675 - 1.0*v693 + 2.0*v727 + 3.5*v730 + 2.0*v731 + 2.8*v732 - 1.0*v739 - 3.0*v740 - 2.0*v741 - 1.0*v760 + 2.0*v761 + 2.0*v762 - 1.0*v790 - 1.0*v836 - 1.0*v838 - 1.0*v862 - 1.0*v875 - 1.0*v889 - 1.0*v904 - tr_428_4",
		"- 1.0*v916 - 1.0*v941 - 2.0*v962 - 3.0*v963 + v964 - 2.0*v985 - 1.0*v987 - 1.0*v988 - 1.0*v995 - 1.0*v1001 - 1.0*v1003 - 1.0*v1022 - 1.0*v1025 - 1.0*v1027 - 1.0*v1054 - 1.0*v1055 - 1.0*v1059 - 1.0*v1060 - 1.0*v1064 - 1.0*v1068 - 1.0*v1070 - 1.0*v1075- tr_428_5",
		"tr_428_1 + tr_428_2 + tr_428_3 + tr_428_4 + tr_428_5","v2 + v4 + v6 + v7 - 1.0*v19 - 1.0*v24 + 2.0*v53 - 1.0*v78 + v88 - 1.0*v89 - 1.0*v90 - 1.0*v142 + v145 - 0.00215*v150 + v171 - 1.0*v227 - 1.0*v229 - 1.0*v240 + v248 + v253 + v261 - 1.0*v275 - 6.0*v431 - 7.0*v432 - 8.0*v433 - 1.0*v434 - 1.0*v485 - 1.0*v488 - tr_536_1",
		"- 1.0*v506 - 1.0*v528 - 1.0*v530 + v531 - 2.0*v579 + v592 + v608 - 1.0*v618 + v624 - 1.0*v625 - 1.0*v641 - 1.0*v642 + v643 - 1.0*v645 - 1.0*v665 - 1.0*v679 - 1.0*v685 - 1.0*v689 + v721 - 1.0*v726 + v728 + v729 + v730 + v731 + v732 + v733 - 1.0*v734 - tr_536_2",
		" v735 + v736 - 1.0*v737 + v751 + 3.0*v786 - 1.0*v807 - 1.0*v815 - 1.0*v817 - 1.0*v821 - 1.0*v826 - 1.0*v858 + v861 - 1.0*v932 - 1.0*v944 - 1.0*v946 - 1.0*v957 - 1.0*v972 + v985 - 1.0*v991 + v1023 - 2.0*v1030 - 2.0*v1043- tr_536_3",
		"tr_536_1 + tr_536_2 + tr_536_3","v19 - 1.0*v4 - 1.0*v6 - 1.0*v7 - 1.0*v2 + v24 - 2.0*v53 + v78 - 1.0*v88 + v89 + v90 + v142 - 1.0*v145 - 0.00005*v150 - 1.0*v171 + v227 + v229 + v240 - 1.0*v248 - 1.0*v253 - 1.0*v261 + v275 + 6.0*v431 + 7.0*v432 + 8.0*v433 + v434 + v485 + v488 + v506 - tr_538_1",
		" v528 + v530 - 1.0*v531 + 2.0*v579 - 1.0*v592 - 1.0*v608 + v618 - 1.0*v624 + v625 + v641 + v642 - 1.0*v643 + v645 + v665 + v679 + v685 + v689 - 1.0*v721 - 1.0*v728 - 1.0*v729 - 1.0*v730 - 1.0*v731 - 1.0*v732 - 1.0*v733 + v737 - 3.0*v786 + v807 + v815 - tr_538_2",
		" v817 + v821 + v826 + v858 - 1.0*v861 + v932 + v944 + v946 + v957 + v972 - 1.0*v985 + v991 - 1.0*v1023 + 2.0*v1030 + 2.0*v1043- tr_538_3",
		"tr_538_1 + tr_538_2 + tr_538_3","v3 + v5 + v9 - 1.0*v72 + v110 - 1.0*v119 - 1.0*v143 + v146 - 0.00013*v150 + 10.0*v151 + 12.0*v152 + 11.0*v153 + 14.0*v154 + 13.0*v155 + 15.0*v156 + v231 + v233 + v247 + v249 + v263 + v271 - 1.0*v463 + v468 - 1.0*v470 - 1.0*v511 + v515 + v516 + v532 - tr_539_1",
		" v545 - 1.0*v547 + v549 + v561 + v593 - 1.0*v594 - 1.0*v604 + v609 + v629 + v630 + v633 - 1.0*v690 - 1.0*v720 + v734 - 1.0*v735 + v737 + v808 + v948 - 1.0*v958 - 3.0*v970 + v981 - 1.0*v985 + v1011 + v1038- tr_539_2",
		"tr_539_1 + tr_539_2","v72 - 1.0*v5 - 1.0*v9 - 1.0*v3 - 1.0*v110 + v119 + v143 - 1.0*v146 - 0.0004*v150 - 10.0*v151 - 12.0*v152 - 11.0*v153 - 14.0*v154 - 13.0*v155 - 15.0*v156 - 1.0*v231 - 1.0*v233 - 1.0*v247 - 1.0*v249 - 1.0*v263 - 1.0*v271 + v463 - 1.0*v468 + v470 + v511 - tr_540_1",
		"- 1.0*v515 - 1.0*v516 - 1.0*v532 - 1.0*v545 + v547 - 1.0*v549 - 1.0*v561 - 1.0*v593 + v594 + v604 - 1.0*v609 - 1.0*v629 - 1.0*v630 - 1.0*v633 + v690 + v720 - 1.0*v737 - 1.0*v808 - 1.0*v948 + v958 + 3.0*v970 - 1.0*v981 + v985 - 1.0*v1011 - 1.0*v1038- tr_540_2",
		"tr_540_1 + tr_540_2","v48 + v51 + v120 - 1.0*v122 + v132 - 1.0*v162 + v183 + v187 + v189 + v191 + v195 + v201 + v202 + v213 + v214 + v241 + v285 + v469 - 1.0*v504 + v511 + v512 + v530 + v545 + v565 + 4.0*v585 - 1.0*v736 + v750 + v752 + v756 + v786 + v819 + v897 + 2.0*v930 - tr_542_1",
		" v939 + v940 + v992 + v1018 + 2.0*v1045- tr_542_2","tr_542_1 + tr_542_2","v8 + v27 + v28 + v68 - 1.0*v72 + v76 + v80 + v86 + v112 + v118 - 1.0*v119 + v123 + v126 + v134 + v139 - 1.0*v141 + v144 + 45.562800000000002853539626812562*v150 + v159 + v160 + v161 + v163 + v166 + v169 + v185 + v192 + v209 + v212 + v219 + v234 + v245 - tr_589_1",
		" v250 + v251 + v259 - 1.0*v268 + v436 + v443 - 2.0*v454 + v460 + v468 - 2.0*v471 + v482 - 1.0*v485 + v487 - 1.0*v495 + v504 + v505 + v509 + v518 + v524 + v526 - 1.0*v527 + v544 + v560 + v562 + v580 + v581 + v602 + v615 + v635 + v636 + v638 + v648 - tr_589_2",
		" v660 + v668 + v669 + v670 + v671 + v672 + v676 - 2.0*v678 + v695 + v696 + v698 + v699 + v700 - 1.0*v707 - 1.0*v708 - 1.0*v709 + v735 + v738 + v765 + v766 + v767 + v768 + v769 + v770 + v771 + v772 + v773 + v774 + v775 + v789 + v804 + v811 + v817 + v818 - tr_589_3",
		" v830 + v833 + 2.0*v837 + v838 + v843 + 2.0*v848 + v851 + v863 + v865 + v866 + v869 + v871 + v874 + v878 + v881 - 1.0*v883 - 1.0*v884 + v888 - 1.0*v890 - 1.0*v891 - 1.0*v892 - 1.0*v893 - 1.0*v894 - 1.0*v895 - 1.0*v896 + v901 - 1.0*v903 + v905 + v907 - tr_589_4",
		" v913 + v928 + v931 + v936 + v954 + v961 + v968 + v971 + v977 + v989 + v993 + v994 - 1.0*v1005 + v1013 + v1024 + v1029 + v1033 + v1036 + v1037 + v1039 + v1046 + v1063 + v1074- tr_589_5",
		"tr_589_1 + tr_589_2 + tr_589_3 + tr_589_4 + tr_589_5","v11 + v12 + v13 + v14 + v15 + v26 + v46 + v58 + v64 + v105 + v117 + v121 + v122 + v140 + 0.7302*v150 + v159 + v161 + v210 + v228 + v235 + v242 + v252 + v260 + v270 + v431 + v432 + v433 + v446 + v461 + v479 + v481 + v503 + v513 + v517 + v542 + v546 + v556 - tr_599_1",
		" v564 + v566 + v572 + v576 + v598 + v634 + v694 + v695 + v736 + v738 + v751 + v757 + v759 + v776 + v777 + v778 + v779 + v780 + v781 + v782 + v783 + 5.0*v791 - 1.0*v805 + v809 + v810 - 1.0*v848 + v857 + v870 + v885 + v931 + v937 + v959 + v997 + v1009 - tr_599_2",
		" v1034 + 8.0*v1040 + v1053 + v1069- tr_599_3","tr_599_1 + tr_599_2 + tr_599_3","v30 - 1.0*v33 - 2.0*v35 + v37 + v39 + v49 + v83 + v84 + v85 + v106 + v170 + v189 + v191 + v201 + v220 + v225 - 1.0*v232 - 1.0*v272 + v277 + v451 + v452 + v478 + v484 + v496 + v501 + v587 + v606 + v645 + v646 + v662 + v663 + v673 + v680 + v682 + v689 - tr_623_1",
		" v690 + v713 - 1.0*v815 - 1.0*v824 - 1.0*v847 - 1.0*v863 + v902 + v904 + v933 + v939 + v940 + v945 + v969 + v1017 + v1018 + v1065- tr_623_2",
		"tr_623_1 + tr_623_2" };
	const char* inequalities[q] = { "v2","v3","v4","v5","v6","v7","v8","v11","v12","v13","v14",
		"v15","v16","v17","v18","v19","v20","v21","v24","v26","v28","v29","v30","v31","v32","v33",
		"v35","v36","v37","v38","v39","v41","v42","v45","v46","v48","v49","v50","v51","v58","v59",
		"v60","v62","v63","v64","v65","v68","v69","v70","v71","v74","v76","v78","v82","v83","v85",
		"v86","v89","v90","v91","v92","v94","v95","v96","v97","v98","v99","v100","v102","v103",
		"v104","v105","v106","v108","v109","v110","v112","v114","v117","v118","v120","v121","v122",
		"v123","v125","v126","v128","v129","v130","v131","v132","v134","v135","v136","v137","v138",
		"v140","v142","v143","v144","v145","v146","v148","v150","v151","v152","v153","v154","v155",
		"v156","v157","v158","v160","v162","v163","v164","v165","v166","v168","v169","v170","v171",
		"v172","v173","v175","v177","v181","v182","v183","v184","v185","v187","v188","v189", //"v186",
		"v190","v191","v192","v193","v194","v195","v196","v197","v201","v202","v204","v205","v207",
		"v209","v211","v213","v214","v215","v216","v218","v219","v222","v223","v224","v225","v229",
		"v230","v231","v232","v234","v235","v236","v237","v238","v240","v241","v242","v243","v245",
		"v246","v247","v248","v249","v250","v251","v252","v253","v254","v255","v256","v257","v258",
		"v259","v260","v261","v262","v263","v264","v267","v269","v270","v271","v272","v273","v276",
		"v277","v278","v279","v280","v281","v282","v284","v285","v287","v288","v289","v290","v291",
		"v292","v293","v294","v295","v296","v297","v298","v299","v300","v301","v302","v303","v304",
		"v305","v306","v307","v308","v309","v310","v311","v312","v313","v314","v316","v317","v318",
		"v319","v320","v321","v322","v323","v324","v325","v326","v327","v328","v329","v331","v332",
		"v333","v334","v335","v336","v337","v338","v339","v340","v341","v342","v343",//"v344 + 100",
		"v345","v346","v347","v348","v349","v350","v351","v352","v353","v354","v355","v356","v357",
		"v360","v361","v362","v363","v364","v365","v366","v368","v369","v370","v371","v372","v373",
		"v374","v375","v376","v377","v378","v379","v380","v381","v382","v383","v384","v386","v387",
		"v389","v390","v391","v393","v394","v395","v397","v398","v399","v400","v401",//,"v392 + 20"
		"v402","v403","v404","v405","v406","v408","v409","v410","v411","v412","v413","v414","v415",
		"v416","v417","v418","v419","v420","v421","v422","v423","v424","v425","v426","v427","v428",
		"v431","v432","v433","v434","v436","v438","v440","v441","v442","v443","v444","v445",//"-v429",
		"v446","v448","v449","v450","v451","v452","v453","v454","v457","v458","v459","v460","v461",
		"v462","v464","v465","v466","v467","v468","v469","v471","v472","v473","v477","v478","v481",
		"v482","v483","v484","v487","v488","v489","v490","v491","v493","v495","v496","v497","v499",
		"v501","v502","v503","v504","v505","v507","v509","v510","v512","v513","v515","v516","v517",
		"v518","v520","v521","v523","v524","v526","v527","v528","v529","v530","v531","v532","v534",
		"v535","v536","v538","v539","v541","v542","v543","v544","v545","v546","v547","v548","v549",
		"v550","v551","v552","v553","v554","v555","v556","v557","v558","v559","v560","v562","v563",
		"v564","v565","v566","v568","v572","v575","v576","v577","v578","v579","v580","v581","v583",
		"v584","v585","v586","v587","v588","v589","v592","v593","v595","v596","v597","v598","v599",
		"v600","v601","v602","v605","v606","v607","v609","v611","v612","v613","v615","v618","v620",
		"v621","v623","v624","v625","v628","v629","v630","v631","v632","v633","v634","v635","v636",
		"v637","v638","v640","v642","v646","v647","v648","v650","v651","v652","v653","v654","v655",
		"v656","v657","v658","v659","v660","v662","v663","v666","v667","v668","v669","v670","v671",
		"v672","v673","v674","v675","v676","v678","v680","v681","v683","v686","v687","v688","v689",
		"v690","v691","v692","v693","v694","v695","v696","v697","v698","v699","v700","v701","v702",
		"v703","v704","v705","v706","v710","v712","v713","v714","v715","v716","v717","v718","v721",
		"v723","v724","v725","v726","v727","v728","v729","v730","v731","v732","v733","v734","v735",
		"v736","v737","v738","v740","v741","v751","v752","v753","v754","v755","v756","v757","v758",
		"v759","v761","v762","v763","v764","v765","v766","v767","v768","v769","v770","v771","v772",
		"v773","v774","v775","v776","v777","v778","v779","v780","v781","v782","v783","v784","v785",
		"v786","v788","v790","v791","v793","v794","v795","v796","v797","v798","v799","v800","v801",
		"v802","v803","v804","v806","v807","v808","v809","v810","v811","v812","v813","v814","v815",
		"v817","v818","v819","v822","v823","v824","v826","v829","v830","v833","v837","v839","v840",
		"v841","v843","v844","v845","v846","v847","v848","v850","v851","v852","v853","v854","v857",
		"v858","v859","v860","v861","v863","v864","v866","v867","v868","v870","v871","v872","v873",
		"v874","v876","v879","v880","v881","v883","v885","v887","v888","v897","v898","v899","v900",
		"v901","v902","v905","v906","v907","v908","v909","v910","v912","v913","v915","v916","v918",
		"v919","v920","v921","v922","v923","v924","v925","v928","v929","v930","v931","v933","v934",
		"v936","v939","v940","v942","v943","v944","v945","v946","v947","v949","v950","v951","v952",
		"v953","v954","v955","v956","v957","v958","v959","v960","v961","v962","v963","v964","v965",
		"v969","v971","v974","v976","v977","v978","v979","v980","v981","v982","v983","v985","v986",
		"v987","v989","v991","v992","v993","v994","v996","v997","v1000","v1001","v1002","v1003",
		"v1004","v1006","v1009","v1011","v1012","v1013","v1014","v1015","v1016","v1017","v1019",
		"v1020","v1021","v1023","v1024","v1025","v1028","v1029","v1030","v1031","v1033","v1034",
		"v1035","v1036","v1037","v1038","v1039","v1040","v1042","v1043","v1045","v1046","v1047",
		"v1049","v1050","v1051","v1052","v1053","v1054","v1058","v1059","v1061","v1063","v1065",
		"v1066","v1068","v1069","v1072","v1073","v1074","v1075","-v344","-v392","v344+b1","v429+b2","v392+b3","-v429" };

	std::vector<double> Qdiag;
	if (createQP) {
		Qdiag.resize(n + p);
		for (int i = 0; i < n + p; ++i) {
			if (i < p) {
				Qdiag[i] = 1e-12;
			}
			else {
				Qdiag[i] = 1e-6;
			}
		}
	}
	int info[2];

	return parseDFBAmodel(params,
		variables,
		objective,
		equalities,
		inequalities,
		p,
		m,
		n,
		q,
		Qdiag.data(),
		Qdiag.size(),
		info);
};
void* create_object_Ecoli(int createQP, std::vector<double>& b0, std::vector<double>& bend) {
	std::vector<double> b0_ = { 10.4982,0.002,13.6364 };
	std::vector<double> bend_ = { 0.001,5.15021,13.6364 };
	b0.clear();
	b0 = b0_;
	bend.clear();
	bend = bend_;
	return create_object_Ecoli(createQP);
}
