#ifndef DaeoToolboxAdditional_H_
#define DaeoToolboxAdditional_H_

#include "DaeoToolbox.hpp"
#include <vector>
#include <tuple>
#include <queue>
#include <iostream>


// class for sparse matrix in triplet form
class sparseTripletMatrix {
	//	friend void basicSolution::initSparseMatrix(sparseTripletMatrix sTM);
public:
	std::vector<int> iRow;
	std::vector<int> jCol;
	std::vector<double> values;
	int nz = 0; //number of non-zero elements in sparse matrix
	int nrow = 0; //number of rows of sparse matrix
	int ncol = 0; //number of columns of sparse matrix
	const char* name;
public:
	sparseTripletMatrix(const char* inputName) { name = inputName; };
	sparseTripletMatrix() { name = "No name given"; };
	void addEntry(int row, int col, double val) {
		iRow.push_back(row);
		jCol.push_back(col);
		values.push_back(val);
		nz++;
		if (row > nrow - 1) { nrow = row + 1; };
		if (col > ncol - 1) { ncol = col + 1; };
	}
	int getNrow() { return nrow; }
	int getNcol() { return ncol; }
	void setNrow(int rows) { if (rows > nrow) nrow = rows; }
	void setNcol(int cols) { if (cols > ncol) ncol = cols; }
	int getNz() { return nz; }
	void resetMatrix() { iRow.clear(); jCol.clear(); values.clear(); nz = 0; nrow = 0; ncol = 0; }
	std::tuple<int, int, double> returnEntry(int i) { return std::make_tuple(iRow[i], jCol[i], values[i]); }
	void printMatrix() {
		std::cout << "Matrix " << name << " has dimensions " << nrow << " x " << ncol << " and " << nz << " non-zeros." << std::endl;
		for (int i = 0; i < nz; i++) {
			std::cout << "irow = " << iRow[i] << ", icol = " << jCol[i] << ", ival = " << values[i] << std::endl;
		}
	}
	int* get_Ti() { return iRow.data(); }
	int* get_Tj() { return jCol.data(); }
	double* get_Tx() { return values.data(); }
	~sparseTripletMatrix() {};
};

#endif