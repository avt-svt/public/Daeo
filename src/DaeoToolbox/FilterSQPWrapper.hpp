/**
* @file FilterSQPWrapper.hpp
*
* ========================================================================\n
* &copy; Lehrstuhl fuer Prozesstechnik, RWTH Aachen                       \n
* ========================================================================\n
* NPSOLWrapper - Part of DyOS                                             \n
* ========================================================================\n
* This file contains the class definitions of the FilterSQPWrapper class  \n
* ========================================================================\n
* @author Mohammadsobhan Moazemi Goodarzi, Fady Assassa
* @date 27.11.2012
*/
#pragma once

#include<vector>
#include <fstream>
//#include <boost/shared_ptr.hpp>
#include "AdeNlp.hpp"
#include <memory>



class TripletMatrix {
public:
    int m;
    int n;
    int nz;
    int *p;
    int *i;
    double *x;
    TripletMatrix(int mm, int nn, int nnz) {
        m=mm;
        n=nn;
        nz=nnz;
        p=new int[nz];
        i = new int[nz];
        x = new double[nz];
    for (std::size_t j = 0; j < nz; ++j) {
      p[j]=0;
      i[j]=0;
      x[j]=0.;
    }
    }
    ~TripletMatrix() {
        delete[] x;
        delete[] i ;
        delete[] p;
    }
};


class FilterSqpStat {
public:
    FilterSqpStat(int* istat, double* rstat) : m_Istat(istat), m_Rstat(rstat) {}
private:
    int* m_Istat; // integer space for solution statistics
    double *m_Rstat; // real space for solution statistics
public:
    void PrintToFile(const char* fileName) const {
        std::ofstream file(fileName);
        for(int i=0; i< 14; i++)
            file << m_Istat[i] << std::endl;
        for(int i=0; i< 7; i++)
            file << m_Rstat[i] << std::endl;
        file.close();
    }
};



/**
* @class FilterSQPWrapper
*/
class FilterSQPWrapper //: public GenericOptimizerWithOptimizationProblem
{
protected:

  static std::fstream m_staticFileReader;

  std::vector<int> m_intWorkspace;
  std::vector<double> m_realWorkspace;

  // Fortran file handles: <=0 : no output
  //                         6 : stdout
  //                       all other values: filename specified by user
  // handles of output files
  int m_printFileHandleFilterSQP;
  int m_summaryFileHandleFilterSQP;

  static void confun(double *x, int *n, int *m, double *c, double *a, int *la,
                     double *user, int *iuser, int *flag);
  static void objfun(double *x, int *n, double *f, double *user, int *iuser, int *flag);
  static void gradient(int *n, int *m, int *mxa, double *x, double *a, int *la,
                       int *maxa, double *user, int *iuser, int *flag);
  static void hessian(double *x, int *n, int *m, int *phase, double *lam, double *ws, int *lws,
                      double *user, int *iuser, int *l_hess, int *li_hess, int *flag);
  static void Wdotd(int *, double*, double*, int* , double*); //n, d, ws, lws, v

  //utility functions
  static void openLogFile();
  static void closeLogFile();



  int maxRealWS(const int n,
                const int m,
                const int mlp,
                const int maxf,
                const int kmax)const;

  int maxIntWS(const int n,
               const int m,
               const int mlp,
               const int kmax) const;
public:
  FilterSQPWrapper();
  ~FilterSQPWrapper(){}
  int solve(AdeNlp* nlp);
public:
    AdeNlp* Nlp;
    TripletMatrix *JacAsTriplet;
    TripletMatrix * HessianAsTriplet;
};
