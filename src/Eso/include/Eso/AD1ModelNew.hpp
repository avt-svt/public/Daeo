#ifndef AD1_MODEL_NEW_HPP
#define AD1_MODEL_NEW_HPP

#include "Eso/EqGroup.hpp"
#include "ad/ad.hpp"
#include "Eigen/Core"
#include <type_traits>



template<template<typename> class ModelTemplate, typename RealType>
class AD1ModelNew
{
public:
  template<int I>
  static constexpr inline int numEquations(EqGroup<I> eqGroup) {
    return TangentModel::numEquations(eqGroup);
  }
  template<int K>
  static constexpr inline int numVariables(VarGroup<K> varGroup) {
    return TangentModel::numVariables(varGroup);
  }


  template<int I>
  inline RealType eval(EqGroup<I> , int equationIndex) const {
    return ad::value(tangentModel.eval(EqGroup<I>{},equationIndex));
  }

  template<int I>
  inline RealType evalTangent(EqGroup<I> , int equationIndex) const {
    return ad::derivative(tangentModel.eval(EqGroup<I>{},equationIndex));
  }


  template<int K>
  inline void setVariable(VarGroup<K> varGroup, const int variableIndex, const RealType& value){
    tangentModel.setVariable(varGroup,variableIndex,value);
  }

  template<int K>
  inline RealType getVariable(VarGroup<K> varGroup, const int variableIndex) const {
    return ad::value(tangentModel.getVariable(varGroup,variableIndex));
  }

  template<int K>
  inline RealType getInitialValue(VarGroup<K> varGroup, const int variableIndex) const {
    return ad::value(tangentModel.getInitialValue(varGroup,variableIndex));
  }

  template<int K>
  inline void setTangent(VarGroup<K> varGroup, const int varIndex, const RealType& value){
    TangentType variable = tangentModel.getVariable(varGroup,varIndex);
    ad::derivative(variable) = value;
    tangentModel.setVariable(varGroup,varIndex,variable);
  }

  template<int K>
  inline RealType getTangent(VarGroup<K> varGroup, const int varIndex) const {
    return ad::derivative(tangentModel.getVariable(varGroup,varIndex));
  }


  template<int I, typename  V>
  void  evalAll(EqGroup<I> , V& residuals)
  {
    for(int i=0; i < numEquations(EqGroup<I>{});++i){
      residuals[i] = eval(EqGroup<I>{},i);
    }
  }

private:

  using TangentType = typename ad::gt1s<RealType>::type;
  using TangentModel = ModelTemplate<TangentType>;
  TangentModel tangentModel;
};



#endif // AD1_MODEL_NEW_HPP
