within ;
model MichaelisMenten2
  Real x1(start = 0.7, fixed = true);
  Real x2(start = 0.3, fixed = true);
  parameter Real k1 = 0.001;
  parameter Real k2 = 0.0001;
  parameter Real k3 = 0.1;
  parameter Real k4 = 200;
equation
  der(x1) = -k1*x1*(k4-x2) + k2*x2;
  der(x2) = k1*x1*(k4-x2) - (k3+k2)*x2;
end MichaelisMenten2;
