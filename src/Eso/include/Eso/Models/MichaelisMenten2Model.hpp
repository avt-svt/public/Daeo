﻿#ifndef MICHAELISMENTEN2_MODEL_HPP
#define MICHAELISMENTEN2_MODEL_HPP

#include "Eso/StandardGroups.hpp"
#include <vector>
#include <cmath>

template<typename RealType>
class MichaelisMenten2Model {
public:
  static constexpr inline int numEquations(const DiffEqns) { return 2;}
  static constexpr inline int numEquations(const AlgEqns) { return 0;}
  static constexpr inline int numEquations(const AllEqns) { return 2;}
  static constexpr inline int numVariables(DiffVars) { return 2;}
  static constexpr inline int numVariables(AlgVars) { return 0;}
  static constexpr inline int numVariables(TimeVar) { return 0;}
  static constexpr inline int numVariables(ParamVars) { return 4;}
  static constexpr inline int numVariables(AllVars) {return 8;}
  static constexpr inline int numVariables(StateVars) { return 2; }
  static constexpr inline int numVariables(DerDiffVars) {return 2; }

  inline RealType eval(AllEqns, EqIndex<0>) const {
    using std::pow;
    using std::pow;
    return (-p(0)*x(0)*(p(3)-x(1))+p(1)*x(1)) - (der_x(0));
  }
  inline RealType eval(AllEqns, EqIndex<1>) const {
    return (p(0)*x(0)*(p(3)-x(1))-(p(2)+p(1))*x(1)) - (der_x(1));
  }


// Boiler plate code starts here
private:
	static constexpr int pseudoLastEquationIndex = numEquations(AllEqns{}); //hack to help Visual Studio
public:
  inline RealType eval(AllEqns, EqIndex<pseudoLastEquationIndex>) const {
    return static_cast<RealType>(0.);
  }
  template <int J>
  inline RealType eval(DiffEqns,EqIndex<J>) const {
    return eval(AllEqns{}, EqIndex<J>{});
  }

  template <int J>
  inline RealType eval(AlgEqns,EqIndex<J>) const {
    return eval(AllEqns{}, EqIndex<J+numEquations(DiffEqns{})>{});
  }

  template<int K>
  inline void setVariable(VarGroup<K> varGroup, const int index, const RealType& value){
    variables[index+variableIndexOffset(varGroup)] = value;
  }

  template<int K>
  inline RealType getVariable(VarGroup<K> varGroup, const int index) const {
    return variables[index+variableIndexOffset(varGroup)];
  }

  template<int K>
  inline RealType getInitialValue(VarGroup<K> varGroup, const int index) const {
    return initialValues[index+variableIndexOffset(varGroup)];
  }

private:
  inline RealType x(const int i) const { return getVariable(DiffVars{},i); }
  inline RealType y(const int i) const { return getVariable(AlgVars{},i); }
  inline RealType der_x(const int i) const { return getVariable(DerDiffVars{},i); }
  inline RealType p(const int i) const { return getVariable(ParamVars{},i); }
  inline const RealType c(const int i) const {return  constVariables[i];}
  inline RealType time() const { return getVariable(TimeVar{},0); }
  static constexpr inline int variableIndexOffset(AllVars){ return 0;}
  static constexpr inline int variableIndexOffset(DiffVars){ return variableIndexOffset(AllVars{});}
  static constexpr inline int variableIndexOffset(AlgVars){ return numVariables(DiffVars{}) + variableIndexOffset(DiffVars{});}
  static constexpr inline int variableIndexOffset(TimeVar){ return numVariables(AlgVars{}) + variableIndexOffset(AlgVars{});}
  static constexpr inline int variableIndexOffset(ParamVars){ return numVariables(TimeVar{}) + variableIndexOffset(TimeVar{});}
  static constexpr inline int variableIndexOffset(DerDiffVars){ return numVariables(ParamVars{}) + variableIndexOffset(ParamVars{}); }
  static constexpr inline int variableIndexOffset(StateVars){ return variableIndexOffset(DiffVars{});}
  std::vector<RealType> variables = getInitialValues();
  const std::vector<RealType> constVariables = getConstVariables();
  const std::vector<RealType> initialValues = getInitialValues();
  static const std::vector<RealType> getInitialValues();
  static const std::vector<RealType> getConstVariables();
};
// Boiler plate code ends here


template<typename RealType>
const std::vector<RealType> MichaelisMenten2Model<RealType>::getInitialValues(){
  return {
    static_cast<RealType>(500.), // x1
    static_cast<RealType>(0.1), // x2
    static_cast<RealType>(0.001), // k1
    static_cast<RealType>(0.0001), // k2
    static_cast<RealType>(0.1), // k3
    static_cast<RealType>(200.), // k4
    static_cast<RealType>(0.), // der(x1)
    static_cast<RealType>(0.) // der(x2)
  };
}

template<typename RealType>
const std::vector<RealType> MichaelisMenten2Model<RealType>::getConstVariables(){
  return {};
}



#endif // MICHAELISMENTEN2_MODEL_HPP
