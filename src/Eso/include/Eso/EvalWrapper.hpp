#ifndef EVALWRAPPER_HPP
#define EVALWRAPPER_HPP


#include "Eso/EqGroup.hpp"
#include "ad/ad.hpp"
#include "Eigen/Core"
#include <type_traits>

template<template<typename> class Model,typename RealType>
class EvalWrapper : public Model<RealType>
{
public:
  template<int I>
  static constexpr inline int numEquations(EqGroup<I> eqGroup) {
    return Model<RealType>::numEquations(eqGroup);
  }
  template<int K>
  static constexpr inline int numVariables(VarGroup<K> varGroup) {
    return Model<RealType>::numVariables(varGroup);
  }
  template<int I>
  inline RealType eval(EqGroup<I> , int equationIndex) const {
    return Eval<I,0>{static_cast<const Model<RealType>&>(*this)}.eval(equationIndex);
  }

  template<int I, typename  V>
  void  evalAll(EqGroup<I> , V& residuals)
  {
    Eval<I,0>{static_cast<const Model<RealType>&>(*this)}.evalAll(residuals);
  }

  template<int I, class EquationIndices, class Residuals>
  inline void evalBlock(EqGroup<I> eqGroup,const EquationIndices& indices, Residuals& residuals)
  {
      int index=0;
      for(auto i : indices){
          residuals[index++] = this->eval(eqGroup,i);
      }
  }


  template<int K, class V>
  inline void setVariables(VarGroup<K> varGroup, const V& values){
    constexpr int numVars = Model<RealType>::numVariables(VarGroup<K>{});
    for(int i=0; i <numVars;++i)
      static_cast<Model<RealType>*>(this)->setVariable(varGroup,i,values[i]);
  }

  template<int K, class Indices, class Values>
  inline void setVariables(VarGroup<K> varGroup, const Indices& indices, const Values& values)
  {
//      using std::cbegin;
//      using std::cend;
      int index = 0;
      for(auto p = indices.cbegin(); p != indices.end(); ++p,++index)
      {
        static_cast<Model<RealType>*>(this)->setVariable(varGroup,*p,values[index]);
      }
  }

  template<int K>
  inline void setVariable(VarGroup<K> varGroup, int variableIndex, const RealType& variableValue)
  {
    static_cast<Model<RealType>*>(this)->setVariable(varGroup,variableIndex,variableValue);
  }

  template<int K>
  inline RealType getVariable(VarGroup<K> varGroup, const int variableIndex) const {
    return static_cast<const Model<RealType>*>(this)->getVariable(varGroup,variableIndex);
  }

  template<int K>
  inline RealType getInitialValue(VarGroup<K> varGroup, const int variableIndex) const {
    return static_cast<const Model<RealType>*>(this)->getInitialValue(varGroup,variableIndex);
  }

  template<int I, int J>
  inline RealType eval(EqGroup<I> eqGroup, EqIndex<J> eqIndex) const {
    return static_cast<const Model<RealType>*>(this)->eval(eqGroup,eqIndex);
  }

private:
  struct Empty {
    Empty(const Model<RealType>& ) {}

    template<typename V>
    void evalAll(V& ) const { }

    RealType eval(int) const  { return RealType{};}
  };

  template<int I, int J>
  struct Eval {
  public:
    Eval(const Model<RealType>&  model) : model(model) {}
    inline RealType eval(int equationNumber){
      if(equationNumber == J) {
        return model.eval(EqGroup<I>{},EqIndex<J>{});
      }
      else
      {
        using NextEval = typename std::conditional<J+1 < numEquations(EqGroup<I>{}),Eval<I,J+1>,Empty>::type;
        NextEval    nextEval{model};
        return nextEval.eval(equationNumber);
      }
    }

    template<typename V>
    inline void evalAll(V& residuals) const {
      residuals[J] = model.eval(EqGroup<I>{},EqIndex<J>{});
      using NextEval = typename std::conditional<J+1 < numEquations(EqGroup<I>{}),Eval<I,J+1>,Empty>::type;
      NextEval    nextEval{model};
      nextEval.evalAll(residuals);
    }
  private:
     const Model<RealType>&  model;
  };

};

#endif // EVALWRAPPER_HPP
