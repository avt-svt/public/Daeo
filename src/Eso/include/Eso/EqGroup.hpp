#ifndef EQ_GROUP_HPP
#define EQ_GROUP_HPP

template<int I> struct EqGroup { static constexpr int value = I;};
template<int I> struct EqIndex { static constexpr int value = I;};
template<int I> struct VarGroup { static constexpr int value = I;};

#endif // EQ_GROUP_HPP
