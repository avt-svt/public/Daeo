#ifndef FIRST_ORDER_ESO_HPP
#define FIRST_ORDER_ESO_HPP

#include "Eso/EqGroup.hpp"
#include "Eso/StandardGroups.hpp"
#include "Yasp/Yasp.hpp"
#include "Eigen/Core"
#include "Eigen/SparseCore"

template<template<typename> class ModelType, typename RealType>
class FirstOrderEso
{

public:
  template<int K>
  inline void setVariable(VarGroup<K> varGroup, int variableIndex, const RealType& variableValue)
  {
    model.setVariable(varGroup,variableIndex,variableValue);
  }

  template<int K>
  inline RealType getVariable(VarGroup<K> varGroup, int variableIndex)
  {
    return model.getVariable(varGroup,variableIndex);
  }


  template<int K>
  inline RealType getInitialValue(VarGroup<K> varGroup, int variableIndex) const
  {
    return model.getInitialValue(varGroup,variableIndex);
  }

  template<int I>
  static constexpr inline int numEquations(EqGroup<I> eqGroup) {
    return ModelType<RealType>::numEquations(eqGroup);
  }

  template<int K>
  static constexpr inline int numVariables(VarGroup<K> varGroup) {
    return ModelType<RealType>::numVariables(varGroup);
  }

  template<int K, class V>
  inline void setVariables(VarGroup<K> varGroup, const V& values){
    constexpr int numVars = ModelType<RealType>::numVariables(VarGroup<K>{});
    for(int i=0; i <numVars;++i)
      setVariable(varGroup,i,values[i]);
  }

  template<int K, class V>
  inline void getVariables(VarGroup<K> varGroup, V& values){
    constexpr int numVars = ModelType<RealType>::numVariables(VarGroup<K>{});
    for(int i=0; i <numVars;++i)
      values[i] = getVariable(varGroup,i);
  }

  template<int K, class V>
  inline void getInitialValues(VarGroup<K> varGroup, V& values) const {
    constexpr int numVars = ModelType<RealType>::numVariables(VarGroup<K>{});
    for(int i=0; i <numVars;++i)
      values[i] = getInitialValue(varGroup,i);
  }

  template<int K, class Indices, class Values>
  inline void setVariables(VarGroup<K> varGroup, const Indices& indices, const Values& values)
  {
    //      using std::cbegin;
    //      using std::cend;
    int index = 0;
    for(auto p = indices.cbegin(); p != indices.cend(); ++p,++index)
    {
      setVariable(varGroup,*p,values[index]);
    }
  }


  template<int K, class Indices, class Values>
  inline void getVariables(VarGroup<K> varGroup, const Indices& indices,Values& values)
  {
    int index = 0;
    for(auto p = indices.cbegin(); p != indices.cend(); ++p,++index)
    {
      values[index] = getVariable(varGroup,*p);
    }
  }

  template<int I>
  inline RealType eval(EqGroup<I> eqGroup,int equationIndex)
  {
    return model.eval(eqGroup,equationIndex);
  }

  template<int I, class EquationIndices, class Residuals>
  inline void evalBlock(EqGroup<I> eqGroup,const EquationIndices& indices, Residuals& residuals)
  {
    int index=0;
    for(auto i : indices){
      residuals[index++] = model.eval(eqGroup,i);
    }
  }

  template<int I, typename  V>
  inline void  evalAll(EqGroup<I> eqGroup, V& residuals)
  {
    model.evalAll(eqGroup,residuals);
  }


  template<int I, int K>
  inline RealType evalDerivative(EqGroup<I> eqGroup, int equationIndex, VarGroup<K> varGroup, int variableIndex){
    model.setTangent(varGroup,variableIndex,RealType{1.0});
    RealType result = model.evalTangent(eqGroup,equationIndex);
    model.setTangent(varGroup,variableIndex,RealType{0.0});
    return result;
  }

  template<int I, int K, typename _Scalar, int _Rows, int _Cols, int _Options,
           int _MaxRows, int _MaxCols>
  inline void evalJacobianPattern(EqGroup<I>, VarGroup<K> ,
                                  Eigen::Matrix< _Scalar, _Rows, _Cols, _Options,
                                  _MaxRows, _MaxCols >& jacobian)
  {
    constexpr int numEqns = numEquations(EqGroup<I>{}); // number of equations
    constexpr int numVars = numVariables(VarGroup<K>{}); // number of variables
    jacobian.resize(numEqns,numVars);
  }

  template<int I, int K, typename _Scalar, int _Rows, int _Cols, int _Options,
           int _MaxRows, int _MaxCols>
  inline void evalJacobianValues(EqGroup<I> eqGroup, VarGroup<K> varGroup,
                                 Eigen::Matrix< _Scalar, _Rows, _Cols, _Options,
                                 _MaxRows, _MaxCols >& jacobian)
  {
    constexpr int numEqns = numEquations(EqGroup<I>{}); // number of equations
    constexpr int numVars = numVariables(VarGroup<K>{}); // number of variables
    for(int varIndex=0; varIndex < numVars; ++varIndex){
      for(int eqIndex=0; eqIndex < numEqns;++eqIndex) {
        jacobian(eqIndex,varIndex) =  evalDerivative(eqGroup,eqIndex,varGroup,varIndex);
      }
    }
  }


  template<int I, int K,
           class EquationIndices,
           class VariableIndices,
           typename Jacobian>
  void evalBlockJacobian(EqGroup<I> eqGroup, VarGroup<K> varGroup, const EquationIndices& equationIndices,
                         const VariableIndices& stateIndices,
                         Jacobian& jacobian)
  {
    //    using std::cbegin;
    //    using std::cend;
    int jcol =0;
    for(auto j = stateIndices.cbegin(); j != stateIndices.cend(); ++j,++jcol) {
      int irow = 0;
      for(auto i = equationIndices.cbegin(); i != equationIndices.cend();++i,++irow)
        jacobian(irow,jcol) = evalDerivative(eqGroup,*i,varGroup,*j);
    }
  }


  template<int I, int K, typename _Scalar>
  inline void evalJacobianValues(EqGroup<I> eqGroup, VarGroup<K> varGroup,
                                 Eigen::SparseMatrix<_Scalar>& jacobian)
  {
    const int numVars = jacobian.cols(); // number of variables

    _Scalar *jacobianValues = jacobian.valuePtr();
    auto *rowIndices  = jacobian.innerIndexPtr();
    auto *colPointers = jacobian.outerIndexPtr();

    for(int varIndex=0; varIndex < numVars; ++varIndex){
      for(int i=colPointers[varIndex]; i < colPointers[varIndex+1];++i ) {
        auto eqIndex = rowIndices[i];
        jacobianValues[i] = evalDerivative(eqGroup,eqIndex,varGroup,varIndex);
      }
    }
  }


  template<int I, int K, typename _Scalar>
  inline void evalJacobianPattern(EqGroup<I> eqGroup, VarGroup<K> varGroup,
                                  Eigen::SparseMatrix<_Scalar>& jacobian)
  {

    constexpr int numEqns = numEquations(EqGroup<I>{}); // number of equations
    constexpr int numVars = numVariables(VarGroup<K>{}); // number of variables

    for(int i=0; i < numVars;++i){
      sparsityPatternModel.setVariable(varGroup,i,Yasp::create(i));
    }


    Eigen::Matrix<Yasp,numEqns,1> residuals;
    evalAllPattern(eqGroup,residuals);

    size_t nnz=0;
    for(int i=0; i < numEqns; ++i){
      nnz += residuals[i].nz.size();
    }

    using TripletType = Eigen::Triplet<_Scalar>;
    std::vector<TripletType> triplets;
    triplets.reserve(nnz);

    // second iteration to fill entris
    for(int irow=0; irow < numEqns; irow++){
      for(auto icol : residuals[irow].nz) {
        triplets.emplace_back(TripletType{irow,icol,_Scalar{0.0}});
      }
    }

    // reset jacobian patterns to avoid conflicts with other components
    for(int i=0; i < numVars;++i){
      sparsityPatternModel.setVariable(varGroup,i,Yasp{});
    }

    jacobian.resize(numEqns,numVars);
    jacobian.setFromTriplets(triplets.begin(),triplets.end());
    jacobian.makeCompressed();

  }

  template<int I, int K, typename _Scalar>
  inline void evalTransposedJacobianPattern(EqGroup<I>, VarGroup<K>,
                                            Eigen::SparseMatrix<_Scalar>& transposedJacobian){
    Eigen::SparseMatrix<_Scalar> jacobian;
    evalJacobianPattern(EqGroup<I>{},VarGroup<K>{},jacobian);
    transposedJacobian = jacobian.transpose();
    transposedJacobian.makeCompressed();
    const int numNonZeros = transposedJacobian.nonZeros();
    workAdjoints.reserve(numNonZeros);
  }

   template<int I, int K, typename _Scalar>
  inline void evalTransposedJacobianValues(EqGroup<I>, VarGroup<K>,
                                           Eigen::SparseMatrix<_Scalar>& transposedJacobian)
  {
    setAdjointMode(VarGroup<K>{});
    const int numNonZeros = transposedJacobian.nonZeros();
    model.evalAllAdjoints(EqGroup<I>{},workAdjoints);
    RealType *valuePtr = transposedJacobian.valuePtr();
    for(int i=0; i < numNonZeros; ++i){
      valuePtr[i] = workAdjoints[static_cast<size_t>(i)];
    }

  }

  template<int I>
  inline void setAdjointMode(VarGroup<I>){
    model.setAdjointMode(VarGroup<I>::value);
  }


private:
  template<int I, typename  V>
  inline void  evalAllPattern(EqGroup<I> eqGroup, V& residuals)
  {
    sparsityPatternModel.evalAll(eqGroup, residuals);
  }

  ModelType<RealType> model;
  std::vector<RealType> workAdjoints;
  ModelType<Yasp>  sparsityPatternModel;
};

#endif // FIRST_ORDER_ESO_HPP
