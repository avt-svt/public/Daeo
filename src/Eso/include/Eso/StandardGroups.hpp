#ifndef STANDARD_GROUPS_HPP
#define STANDARD_GROUPS_HPP

#include "Eso/EqGroup.hpp"

using AllVars = VarGroup<0>;
using StateTimeParamVars = VarGroup<1>;
using StateTimeVars = VarGroup<2>;
using StateVars = VarGroup<3>;
using DiffVars = VarGroup<4>;
using AlgVars = VarGroup<5>;
using TimeVar = VarGroup<6>;
using ParamVars = VarGroup<7>;
using DerDiffVars = VarGroup<8>;

using AllEqns = EqGroup<0>;
using DiffEqns = EqGroup<1>;
using AlgEqns = EqGroup<2>;


#endif // STANDARD_GROUPS_HPP
