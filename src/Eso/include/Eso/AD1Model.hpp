#ifndef AD1_MODEL_HPP
#define AD1_MODEL_HPP

#include "Eso/EqGroup.hpp"
#include "ad/ad.hpp"
#include "Eigen/Core"
#include <type_traits>



template<template<typename> class ModelTemplate, typename RealType>
class AD1Model
{
public:
  template<int I>
  static constexpr inline int numEquations(EqGroup<I> eqGroup) {
    return TangentModel::numEquations(eqGroup);
  }
  template<int K>
  static constexpr inline int numVariables(VarGroup<K> varGroup) {
    return TangentModel::numVariables(varGroup);
  }


  template<int I>
  inline RealType eval(EqGroup<I> , int equationIndex) const {
    return Eval<I,0>{*this}.eval(equationIndex);
  }

  template<int I>
  inline RealType evalTangent(EqGroup<I> , int equationIndex) const {
    return Eval<I,0>{*this}.evalTangent(equationIndex);
  }


  template<int K>
  inline void setVariable(VarGroup<K> varGroup, const int variableIndex, const RealType& value){
    tangentModel.setVariable(varGroup,variableIndex,value);
  }

  template<int K>
  inline RealType getVariable(VarGroup<K> varGroup, const int variableIndex) const {
    return ad::value(tangentModel.getVariable(varGroup,variableIndex));
  }

  template<int K>
  inline RealType getInitialValue(VarGroup<K> varGroup, const int variableIndex) const {
    return ad::value(tangentModel.getInitialValue(varGroup,variableIndex));
  }

  template<int K>
  inline void setTangent(VarGroup<K> varGroup, const int varIndex, const RealType& value){
    TangentType variable = tangentModel.getVariable(varGroup,varIndex);
    ad::derivative(variable) = value;
    tangentModel.setVariable(varGroup,varIndex,variable);
  }

  template<int K>
  inline RealType getTangent(VarGroup<K> varGroup, const int varIndex) const {
    return ad::derivative(tangentModel.getVariable(varGroup,varIndex));
  }


  template<int I, typename  V>
  void  evalAll(EqGroup<I> , V& residuals)
  {
    Eval<I,0>{*this}.evalAll(residuals);
  }



private:
  using TangentType = typename ad::gt1s<RealType>::type;
  using TangentModel = ModelTemplate<TangentType>;
  struct Empty {
    Empty(const AD1Model<ModelTemplate,RealType>&) {}
    template<typename V>
    void evalAll(V& ) const { }
    RealType eval(int) const  { return RealType{};}
    RealType evalTangent(int) const  { return RealType{};}
  };

  template<int I, int J>
  struct Eval {
  public:
    Eval(const AD1Model<ModelTemplate,RealType>& tangentModel) : tangentModel(tangentModel) {}
    inline RealType eval(int equationNumber){
      if(equationNumber == J) {
        return tangentModel.eval(EqGroup<I>{},EqIndex<J>{});
      }
      else
      {
        using NextEval = typename std::conditional<J+1 < numEquations(EqGroup<I>{}),Eval<I,J+1>,Empty>::type;
        NextEval    nextEval{tangentModel};
        return nextEval.eval(equationNumber);
      }
    }
    inline RealType evalTangent(int equationNumber){
      if(equationNumber == J) {
        return tangentModel.evalTangent(EqGroup<I>{},EqIndex<J>{});
      }
      else
      {
        using NextEval = typename std::conditional<J+1 < numEquations(EqGroup<I>{}),Eval<I,J+1>,Empty>::type;
        NextEval    nextEval{tangentModel};
        return nextEval.evalTangent(equationNumber);
      }
    }

    template<typename V>
    inline void evalAll(V& residuals) const {
      residuals[J] = tangentModel.eval(EqGroup<I>{},EqIndex<J>{});
      using NextEval = typename std::conditional<J+1 < numEquations(EqGroup<I>{}),Eval<I,J+1>,Empty>::type;
      NextEval    nextEval{tangentModel};
      nextEval.evalAll(residuals);
    }
  private:
    const AD1Model<ModelTemplate,RealType>& tangentModel;
  };

  template<int I, int J>
  inline RealType evalTangent(EqGroup<I> eqGroup, EqIndex<J> eqIndex) const {
    return ad::derivative(tangentModel.eval(eqGroup,eqIndex));
  }

  template<int I, int J>
  inline RealType eval(EqGroup<I> eqGroup, EqIndex<J> eqIndex) const {
    return ad::value(tangentModel.eval(eqGroup,eqIndex));
  }

  TangentModel tangentModel;
};



#endif // AD1_MODEL_HPP
