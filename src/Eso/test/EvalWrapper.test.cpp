#include "Eso/EvalWrapper.hpp"
#include "gtest/gtest.h"
#include "Eso/Models/SimpleModel.hpp"
#include "Eso/Models/VanDerPolModel.hpp"


template<typename RealType>
using VanDerPolWrapper = EvalWrapper<VanDerPolModel,RealType>;

TEST(EvalWrapper, canEval)
{
  using RealType = double;
  VanDerPolWrapper<RealType> eso;
  eso.setVariables(VarGroup<0>{},std::vector<int>{0,1,2},std::array<RealType,3>{1.1,2.2,3.3});
  eso.setVariables(VarGroup<1>{},std::array<RealType,3>{0.,0.,0.});
  eso.setVariables(VarGroup<2>{},std::array<RealType,2>{1.,1.});
  auto residual = eso.eval(EqGroup<0>{},0);
  EXPECT_NEAR(2.2,residual,1e-7);
  std::array<RealType,3> residuals;
  eso.evalAll(EqGroup<0>{},residuals);
  EXPECT_NEAR(2.2,residuals[0],1e-7);
  EXPECT_NEAR(7.05,residuals[2],1e-7);
}

TEST(EvalWrapper,CanEvaluateResiduals)
{
  using RealType = double;
  VanDerPolWrapper<RealType> eso;
  eso.setVariables(VarGroup<0>{},std::array<RealType,3>{1.0,1.0,1.5});
  eso.setVariables(VarGroup<1>{},std::array<RealType,3>{0.5,0.5,1.5});
  eso.setVariables(VarGroup<2>{},std::array<RealType,3>{1.0,2.0});
  std::array<RealType,3> residuals;
  eso.evalAll(EqGroup<0>{},residuals);
  EXPECT_NEAR(0.5,residuals[0],1e-7);
  EXPECT_NEAR(0.5,residuals[1],1e-7);
  EXPECT_NEAR(4.5,residuals[2],1e-7);
  residuals = std::array<RealType,3>{17,17,17};
  eso.evalBlock(EqGroup<0>{},std::array<int,2>{0,2},residuals);
  EXPECT_NEAR(0.5,residuals[0],1e-7);
  EXPECT_NEAR(4.5,residuals[1],1e-7);
  EXPECT_NEAR(17.0,residuals[2],1e-7);

}
