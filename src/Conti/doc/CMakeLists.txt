find_package(Doxygen)
find_package(LATEX)

if(DOXYGEN_FOUND)

    if(NOT DOXYGEN_DOT_FOUND)
      message(WARNING "Graphviz doesn't seem to be installed. Doxygen will not be able to generate graphs. Consider installing this package.")
    endif(NOT DOXYGEN_DOT_FOUND)
    if(LATEX_FOUND)
        set(GENERATE_LATEX YES)
    else()
        set(GENERATE_LATEX NO)
    endif()

    configure_file(documentation-config.doxygen.in ${CMAKE_CURRENT_BINARY_DIR}/documentation-config.doxygen @ONLY)
    add_custom_target(doc${PROJECT_NAME}
        ${DOXYGEN_EXECUTABLE} ${CMAKE_CURRENT_BINARY_DIR}/documentation-config.doxygen
        WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
        COMMENT "Generating API documentation using doxygen for ${PROJECT_NAME}" VERBATIM
        SOURCES documentation-config.doxygen.in 00_welcome.dox 10_user-guide.dox 20_technical-overview.dox)

    set(INSTALL_DOC_DIR ${CMAKE_BINARY_DIR}/doc/${PROJECT_NAME})


    install(DIRECTORY ${INSTALL_DOC_DIR}/html DESTINATION doc/${PROJECT_NAME}/html COMPONENT doc${PROJECT_NAME})

    if(NOT TARGET doc)
        add_custom_target(doc)
    endif()
    add_dependencies(doc doc${PROJECT_NAME})


    if(LATEX_FOUND)
        if(UNIX)
            set(MAKE_COMMAND $(MAKE))
        elseif(WIN32)
            set(MAKE_COMMAND make.bat)
        else()
            set(MAKE_COMMAND make)
        endif()

        add_custom_target(pdf${PROJECT_NAME} ${MAKE_COMMAND}
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/doc/${PROJECT_NAME}/latex)

        add_custom_command(TARGET pdf${PROJECT_NAME} POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy_if_different
            ARGS refman.pdf ${PROJECT_NAME}.pdf
            WORKING_DIRECTORY ${CMAKE_BINARY_DIR}/doc/${PROJECT_NAME}/latex)

        add_dependencies(pdf${PROJECT_NAME} doc${PROJECT_NAME})
        add_dependencies(doc pdf${PROJECT_NAME})

        install(FILES ${CMAKE_BINARY_DIR}/doc/${PROJECT_NAME}/latex/${PROJECT_NAME}.pdf
            DESTINATION doc/${PROJECT_NAME})
    endif()

endif(DOXYGEN_FOUND)
