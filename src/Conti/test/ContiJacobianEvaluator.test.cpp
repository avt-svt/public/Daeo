#include <iostream>
#include <iterator>
#include <string>
#include <algorithm>
#include "Eigen/Dense"
#include "Eigen/Sparse"
#include <vector>
#include <array>
#include "Conti/ContiUtil.hpp"
#include <type_traits>
#include "More.hpp"
#include "Conti/ContiJacobianEvaluator.hpp"
#include <gtest/gtest.h>



TEST(Conti, CanUseJacobianEvaluator)
{
    constexpr int N=15;
    using value_type = double;
    using domain_type = Eigen::Matrix<value_type,Eigen::Dynamic,1>;
    //    using domain_type = std::vector<float>;
    //    using domain_type = std::array<value_type,N+1>;
    using range_type = Eigen::Matrix<value_type,Eigen::Dynamic,1>;
    //   using range_type = std::array<value_type,N>;
    using jacobian_type = Eigen::Matrix<value_type,Eigen::Dynamic,Eigen::Dynamic>;
    More<value_type,domain_type,range_type> more;
    domain_type x;
    Conti::resize(x,N+1);
    for(unsigned i = 0; i < 10;++i){
        x[i] = 1.0;
    }
    for(unsigned i = 10; i < N; ++i) {
        x[i] = 0.02;
    }
    x[N] = 0;
    more.Initialize(x);
    jacobian_type J(N,N+1);
    EXPECT_NO_THROW(more(J,x,Conti::EvalJacobian()));

    std::cout << J << std::endl;


}
