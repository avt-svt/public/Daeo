#include <iostream>
#include <iterator>
#include <string>
#include <algorithm>
#include "Eigen/Dense"
#include "More.hpp"
#include <vector>
#include "Conti/ContiSolver.hpp"
#include <gtest/gtest.h>
#include "ad/ad.hpp"
#include "EigenADSupport.hpp"
#include "mpreal/MprealWrapper.hpp"


using namespace Eigen;

template<typename value_type>
std::ostream & operator <<
(std::ostream & s,
 const std::vector<value_type>& v)
{
    for(const value_type& i: v) s << i;
        return s;
}


TEST(Conti, CanSolveMoreWithArbitraryPrecision)
{
    constexpr int N=15;
    using value_type = mpfr::mpreal;
    // Required precision of computations in decimal digits
    // Play with it to check different precisions
    const int digits = 50;
    // Setup default precision for all subsequent computations
    // MPFR accepts precision in bits - so we do the conversion
    mpfr::mpreal::set_default_prec(mpfr::digits2bits(digits));

    using domain_type = Eigen::Matrix<value_type,Eigen::Dynamic,1>;
    //    using domain_type = std::vector<value_type>;
    //    using domain_type = std::array<value_type,N+1>;
    using range_type = Eigen::Matrix<value_type,Eigen::Dynamic,1>;
    //   using range_type = std::array<value_type,N>;
    using jacobian_type = Eigen::Matrix<value_type,Eigen::Dynamic,Eigen::Dynamic>;
    using qrsolver_type = Eigen::ColPivHouseholderQR<jacobian_type>;
    More<value_type,domain_type,range_type> more;
    domain_type x;
    Conti::resize(x,N+1);
    for(unsigned i = 0; i < 10;++i){
        x[i] = 0.9999;
    }
    for(unsigned i = 10; i < N; ++i) {
        x[i] = 0.02;
    }
    x[N] = 0;
    more.Initialize(x);

    Conti::Options<value_type> options;
    options.MaximalResidualNorm = "1e-300";
    Conti::Solver<value_type,domain_type,range_type,jacobian_type,qrsolver_type> solver(options);
        const bool success = solver.Solve(x,more);



    EXPECT_TRUE(success);


//    if(success) {
//      std::cout.precision(digits);
//      range_type F;
//      Conti::resize(F,N);
//      more.evalF(F,x);
//      std::cout << "Residual is" << "\n";
//      for(int i=0; i <N;++i){
//          std::cout << F[i] << "\n";
//        }

//      std::cout << "Solution vector is"  "\n";
//      for(int i=0; i <=N;++i){
//          std::cout << x[i] << "\n";
//        }

//    }

     mpfr_free_cache(); // needed to free caches of mpfr library


}



TEST(Mpreal, TestToFindMemoryLeak)
{
  const int digits = 50;
  mpfr::mpreal::set_default_prec(mpfr::digits2bits(digits));
  mpfr::mpreal x=0.1;
  auto y = cos(x);
  mpfr_free_cache(); // needed to free caches of mpfr library



}
