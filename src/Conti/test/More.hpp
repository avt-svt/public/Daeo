#ifndef MORE_HPP
#define MORE_HPP


#include "Conti/Conti.hpp"
#include "Conti/ContiUtil.hpp"
#include "Eso/Models/MoreModel.hpp"
#include "Eso/FirstOrderEso.hpp"
#include "Eso/AD1Model.hpp"
#include "Eso/AlgebraicEsoView.hpp"
#include "Eigen/Core"
#include "Eigen/SparseCore"
#include <type_traits>
#include <vector>

template<typename RealType>
using TangentMoreModel = AD1Model<MoreModel,RealType>;

template<typename value_type = double,
         typename domain_type = Eigen::Matrix<value_type,Eigen::Dynamic,1>,
         typename range_type = Eigen::Matrix<value_type,Eigen::Dynamic,1> >
struct More
{

    template<typename value_type2 = double,
             typename domain_type2 = Eigen::Matrix<value_type,Eigen::Dynamic,1>,
             typename range_type2 = Eigen::Matrix<value_type,Eigen::Dynamic,1> >
    void operator()(range_type2& H, const domain_type2& u, const Conti::EvalFunction evalFunction);

    template <typename value_type2 = double,
              typename domain_type2 = Eigen::Matrix<value_type2,Eigen::Dynamic,1>,
            typename jacobian_type = Eigen::Matrix<value_type2,Eigen::Dynamic,Eigen::Dynamic> >
    void operator()(jacobian_type& J, const domain_type2& u, const Conti::EvalJacobian evalJacobian);


public:
    domain_type getInitialValues() const;
    void Initialize(const domain_type &InitialValues);
private:
    domain_type m_InitialValues;
    range_type m_InitialF;
    FirstOrderEso<TangentMoreModel,value_type> eso1;
};

template<typename value_type, typename domain_type , typename range_type>
domain_type More<value_type,domain_type, range_type>::getInitialValues() const
{
    return m_InitialValues;
}

template<typename value_type, typename domain_type , typename range_type>
void More<value_type,domain_type, range_type>::Initialize(const domain_type &InitialValues)
{
    m_InitialValues = InitialValues;
    const int N = Conti::size(InitialValues)-1;
    Conti::resize(m_InitialF,N);
    eso1.setVariables(VarGroup<0>{},InitialValues);
    eso1.evalAll(EqGroup<0>{}, m_InitialF);

}

template<typename value_type, typename domain_type, typename range_type>
template<typename value_type2, typename domain_type2 , typename range_type2>
void More<value_type, domain_type,range_type>::operator()(range_type2& H, const domain_type2& u, const Conti::EvalFunction /* evalFunction */)
{
    const int  N =eso1.numEquations(EqGroup<0>{});
    eso1.setVariables(VarGroup<0>{},u);
    eso1.evalAll(EqGroup<0>{}, H);

    for(unsigned i = 0; i < N; ++i)
    {
        H[i] = H[i] + (u[N] - 1) * m_InitialF[i];
    }
}


template<typename value_type, typename domain_type , typename range_type>
template <typename value_type2, typename domain_type2, typename jacobian_type>
void More<value_type,domain_type,range_type>::operator()(jacobian_type& J, const domain_type2& u, const Conti::EvalJacobian /* evalJacobian */)
{
    eso1.setVariables(VarGroup<0>{},u);
    eso1.evalJacobianValues(EqGroup<0>{},VarGroup<0>{},J);
    const int  N =eso1.numEquations(EqGroup<0>{});
    for(int i=0; i < N; ++i)
      J(i,N) = m_InitialF[i];
}



#endif // MORE_HPP

