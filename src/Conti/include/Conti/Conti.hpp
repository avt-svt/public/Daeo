#ifndef CONTI_HPP
#define CONTI_HPP

namespace Conti {

/*! \brief Dummy class for use with Functor to eval the Jacobian H'(u)*/
class EvalJacobian {};

/*! \brief Dummy class for use with Functor to eval the function H(u) */
class EvalFunction {};

}

#endif // CONTI_HPP
