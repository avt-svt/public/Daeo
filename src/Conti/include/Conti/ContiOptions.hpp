#ifndef CONTI_OPTIONS_HPP
#define CONTI_OPTIONS_HPP

namespace Conti
{

/*! \brief Options for continuation solver
*
* Options for  a simple predictor corrector continuation method
* according to the book "Introduction to Numerical Continuation Mehods"
* form Eugene L. Allgower and Kurt Georg. Please see page 266 of
* the SIAM CLASSICS edition (c) 2003.
*/
template<typename value_type>
struct Options {
    Options(value_type initialSteplength = 0.03,
            value_type minimalSteplength = 1e-5,
            value_type toleranceCorrectorLoop = 1e-4,
            value_type maximalContractionFactor = 0.6,
            value_type maximalDistanceToCurve = 0.4,
            value_type maximalNominalAngle = 0.2,
            value_type maximalResidualNorm = 1e-7) :
        InitialSteplength( initialSteplength),
        MinimalSteplength( minimalSteplength),
        ToleranceCorrectorLoop ( toleranceCorrectorLoop),
        MaximalContractionFactor (maximalContractionFactor ),
        MaximalDistanceToCurve ( maximalDistanceToCurve),
        MaximalResidualNorm (maximalResidualNorm),
        DesiredContractionFactor(maximalContractionFactor/value_type(4)),
        DesiredDistanceToCurve(maximalDistanceToCurve/value_type(4)) {}
    value_type InitialSteplength; /*!< initial steplength h */
    value_type MinimalSteplength; /*!< minimal stepsize hmin */
    value_type ToleranceCorrectorLoop; /*!< tolerance for corrector loop tol */
    value_type MaximalContractionFactor; /*!< maximal contraction factor contr */
    value_type MaximalDistanceToCurve; /*!< maximal distance to curve dmax */
    value_type MaximalResidualNorm;
    value_type DesiredContractionFactor;
    value_type DesiredDistanceToCurve;
    value_type DesiredAngleTangents = static_cast<value_type>(0.05) ;
    value_type ContractionFactorGuard = 1e-14; /*!< if satifisfied, innerIteration is successful */

    unsigned MaximalInnerIterations = 10u;
    unsigned MaximalOuterIterations = 200u;
};

};


#endif // CONTI_OPTIONS_HPP
