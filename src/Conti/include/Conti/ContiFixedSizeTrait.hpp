#ifndef CONTI_FIXED_SIZE_TRAIT_HPP
#define CONTI_FIXED_SIZE_TRAIT_HPP

#include <Eigen/Dense>
#include <Eigen/Sparse>

namespace Conti {

template <typename T>
struct IsFixedSize
{
    static const bool value = false;
};

template <typename value_type,
          int RowsAtCompileTime,
          int ColsAtCompileTime>
struct IsFixedSize<Eigen::Matrix<
        value_type,
        RowsAtCompileTime,
        ColsAtCompileTime> >
{
    static const bool value = (RowsAtCompileTime > 0) && (ColsAtCompileTime > 0);
};

template<typename value_type, int N, template<typename> class qrsolver_type>
struct IsFixedSize<qrsolver_type<Eigen::Matrix<value_type,N,N+1> > >
{
     static const bool value = true;
};

template<typename MatrixType>
struct IsSparseMatrix
{
    static const bool value = false;
};

template<typename value_type>
struct IsSparseMatrix<Eigen::SparseMatrix<value_type> >
{
    static const bool value = true;
};

template<class MatrixType>
struct IsSparseSolver
{
    static const bool value = false;
};

template<typename MatrixType, typename OrderingType>
struct IsSparseSolver<Eigen::SparseQR<MatrixType,OrderingType> > {
    static const bool value = true;
};

}

#endif // CONTI_FIXED_SIZE_TRAIT_HPP
