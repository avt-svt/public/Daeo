#ifndef CONTI_UTIL_HPP
#define CONTI_UTIL_HPP

#include <array>
#include <type_traits>
#include <Eigen/Dense>
#include "Conti/Conti.hpp"
#include "Conti/ContiFixedSizeTrait.hpp"
#include "Conti/ContiMinMax.hpp"


namespace Conti {


template <typename value_type,
          int RowsAtCompileTime,
          int ColsAtCompileTime>
typename std::enable_if<IsFixedSize<Eigen::Matrix<value_type,
                             RowsAtCompileTime,
                             ColsAtCompileTime> >::value,
                 EIGEN_DEFAULT_DENSE_INDEX_TYPE>::type
inline constexpr size(const Eigen::Matrix<value_type,RowsAtCompileTime,
                                          ColsAtCompileTime>& )
{
    return RowsAtCompileTime * ColsAtCompileTime;
}


template <class T>
inline constexpr auto size(const T& v) -> decltype(v.size())
{
    return v.size();
}



template <class state_type>
inline void resize(state_type& out, const int N)
{
    out.resize(N);
}

template <class state_type>
typename std::enable_if<IsFixedSize<state_type>::value,void>::type
inline resize(state_type& , const int )
{}

template <class matrix_type>
inline void resize(matrix_type& out, const int M, const int N)
{
    out.resize(M,N);
}


template <class matrix_type>
typename std::enable_if<IsFixedSize<matrix_type>::value,void>::type
inline resize(matrix_type& , const int , const int ) {}


template <typename T> int sgn(T val) {
    return (T(0) < val) - (val < T(0));
}

template<typename state_type>
auto ScalarProduct(const state_type& v, const state_type& w) -> decltype(v[0]*w[0])
{
    auto scalarProduct = v[0]*w[0];
    for(int i = 1; i < size(v);++i){
        scalarProduct += v[i]*w[i];
    }
    return scalarProduct;
}

template<typename state_type>
auto TwoNorm(const state_type& v) -> decltype (v[0]*v[0])
{
    return sqrt(ScalarProduct(v,v));
}


template <typename value_type>
value_type AbsoluteValue(const value_type a)
{
    return (a >= 0) ? a : (-a);
}

template <typename jacobian_type>
void ComputeQR(Eigen::ColPivHouseholderQR<jacobian_type>& qrsolver, const jacobian_type& jacobian)
{
    qrsolver.compute(jacobian);
}


template<typename jacobian_type, typename OrderingType>
void ComputeQR(Eigen::SparseQR<jacobian_type,OrderingType>& qrsolver, const jacobian_type& jacobian)
{
    qrsolver.analyzePattern(jacobian);
    qrsolver.factorize(jacobian);
}

template <typename domain_type, typename qrsolver_type>
inline void ApplyPermutationOnTheLeft(domain_type& tangent, const qrsolver_type& qr)
{
    qr.colsPermutation().applyThisOnTheLeft(tangent);
}

template <typename domain_type, typename jacobian_type>
inline void ApplyPermutationOnTheLeft(domain_type& , const Eigen::HouseholderQR<jacobian_type>& )
{}



template<typename domain_type, typename qrsolver_type>
inline typename std::enable_if<(!Conti::IsFixedSize<domain_type>::value ||!Conti::IsFixedSize<qrsolver_type>::value) && !Conti::IsSparseSolver<qrsolver_type>::value,int>::type ComputeTangentBlockSolve(domain_type& tangent, const qrsolver_type& qr)
{
    const int N = size(tangent)-1;
    tangent.block(0,0,N,1) = qr.matrixQR().topLeftCorner(N,N).template triangularView<Eigen::Upper>().solve(qr.matrixQR().col(N));
    return N;
}

template<typename value_type, typename qrsolver_type>
inline typename std::enable_if<Conti::IsSparseSolver<qrsolver_type>::value,int>::type ComputeTangentBlockSolve(Eigen::Matrix<value_type,Eigen::Dynamic,1>& tangent, const qrsolver_type& qr)
{
    const int N = size(tangent)-1;
    tangent.block(0,0,N,1);
    Eigen::Matrix<value_type,Eigen::Dynamic,1> rhs(N);
    rhs = qr.matrixR().col(N);
    tangent.block(0,0,N,1) = qr.matrixR().topLeftCorner(N,N).template triangularView<Eigen::Upper>().solve(rhs);
    return N;
}

template<typename domain_type, typename qrsolver_type>
inline const typename std::enable_if<Conti::IsFixedSize<domain_type>::value && Conti::IsFixedSize<qrsolver_type>::value,int>::type ComputeTangentBlockSolve(domain_type& tangent, const qrsolver_type& qr)
{

    tangent.template block<size(tangent)-1,1>(0,0) = qr.matrixQR().template topLeftCorner<size(tangent)-1,size(tangent)-1>().template triangularView<Eigen::Upper>().solve(qr.matrixQR().col(size(tangent)-1));
    return size(tangent)-1;
}


template<typename domain_type, typename qrsolver_type>
inline void ComputeTangent(domain_type& tangent, const qrsolver_type& qr)
{
    const int N = ComputeTangentBlockSolve(tangent,qr);
    tangent[N] = -1;
    ApplyPermutationOnTheLeft(tangent,qr);
    auto fac = 1/TwoNorm(tangent);
    tangent = fac *tangent;
}

template<typename domain_type, typename range_type, typename qrsolver_type>
inline void ComputeCorrector(domain_type& corrector, const range_type& b, const qrsolver_type& qr)
{
    corrector = qr.solve(b);
}


template <typename value_type>
inline value_type DeterminantQ(const Eigen::HouseholderQR<Eigen::Matrix<value_type,Eigen::Dynamic,Eigen::Dynamic>>& qr)
{
    const auto& hCoeffs = qr.hCoeffs();
    int nnz =  0;
    for (int i=0; i < hCoeffs.size();++i)
    {
        if (hCoeffs(i) != 0) ++nnz;
    }
    value_type det = (nnz % 2 == 0)? 1 : -1;
    return det;
}

template <typename value_type>
inline value_type DeterminantR(const Eigen::HouseholderQR<Eigen::Matrix<value_type,Eigen::Dynamic,Eigen::Dynamic>>& qr)
{
    const auto& QR = qr.matrixQR();
    const int N = std::min(QR.rows(),QR.cols());
    value_type det = 1;
    for (int i = 0; i < N; ++i)
    {
        det *= QR(i,i);
    }
    return det;
}


template <typename jacobian_type>
void SetZero (jacobian_type& )
{
   throw std::runtime_error("setZero has to be specialized");
}

template<typename _Scalar, int _Rows, int _Cols, int _Options, int _MaxRows, int _MaxCols>
inline void SetZero(Eigen::Matrix< _Scalar, _Rows, _Cols, _Options, _MaxRows, _MaxCols >& J)
{
    J.setZero();
}



}  // end namespace Conti


#endif // CONTI_UTIL_HPP
