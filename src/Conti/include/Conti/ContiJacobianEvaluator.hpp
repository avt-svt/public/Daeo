#ifndef CONTI_JACOBIAN_EVALUATOR_HPP
#define CONTI_JACOBIAN_EVALUATOR_HPP

#include <type_traits>

#include <ad/ad.hpp>
#include <Eigen/Dense>
#include <Eigen/Sparse>

#include "Conti/Conti.hpp"
#include "Conti/ContiUtil.hpp"
#include "Yasp/Yasp.hpp"
#include <set>



namespace  Conti {

template<typename value_type = double,
         typename domain_type = Eigen::Matrix<value_type,Eigen::Dynamic,1>,
         typename range_type = Eigen::Matrix<value_type,Eigen::Dynamic,1>>
class DenseJacobianEvaluator
{
public:
    DenseJacobianEvaluator() : m_AreSizesAdjusted(false) {}
    template<typename jacobian_type, typename H_functor>
    void ComputeJacobian(jacobian_type& J, const domain_type& u, H_functor& H);
private:
    void AdjustSizes(const domain_type& u)
    {
        if(!m_AreSizesAdjusted)
        {
            const int N1 = size(u);
            m_N = N1-1;
            resize(m_t1_u,N1);
            resize(m_t1_H,m_N);
            m_AreSizesAdjusted = true;
        }
    }

private:
    std::vector<typename ad::gt1s<value_type>::type> m_t1_u;
    std::vector<typename ad::gt1s<value_type>::type> m_t1_H;
    int m_N;
    bool m_AreSizesAdjusted;

};



template<typename ValueType,
         typename DomainType,
         typename RangeType>
template<typename JacobianType, typename HFunctor>
void DenseJacobianEvaluator<ValueType,DomainType,RangeType>::ComputeJacobian(JacobianType& J, const DomainType& u, HFunctor& H)
{
    SetZero(J);
    AdjustSizes(u);
    // Initialize AD variables

    for(int j = 0; j < m_N+1; ++j) {
        ad::derivative(m_t1_u[j]) = 1;
        H(m_t1_H,m_t1_u, Conti::EvalFunction());
        ad::derivative(m_t1_u[j]) = 0;
        for(int i = 0; i < m_N; ++i) {
            J(i,j) = ad::derivative(m_t1_H[i]);
        }
    }
}


template<typename value_type = double,
         typename domain_type = Eigen::Matrix<value_type,Eigen::Dynamic,1>,
         typename range_type = Eigen::Matrix<value_type,Eigen::Dynamic,1>>
class SparseJacobianEvaluator
{
public:
    SparseJacobianEvaluator() : m_nnz(0), m_N(0), m_AreSizesAdjusted(false), m_IsComputedSparsityPattern(false)
	{
	}

    template<typename H_functor>
    void ComputeJacobian(Eigen::SparseMatrix<value_type>& J, const domain_type& u, H_functor& H);

private:
    void AdjustSizes(const domain_type& u)
    {
        if(!m_AreSizesAdjusted)
        {
            const int n1 = size(u);
            m_N = n1-1;
            resize(m_t1_u,n1);
            resize(m_t1_H,m_N);
            m_p.resize(m_N+2,0);
            m_AreSizesAdjusted = true;
            m_nnz =0;
        }
    }

    template<typename H_functor>
    void ComputeSparsityPattern(Eigen::SparseMatrix<value_type>& J, const domain_type& u, H_functor& H);

private:
    std::vector<typename ad::gt1s<value_type>::type> m_t1_u;
    std::vector<typename ad::gt1s<value_type>::type> m_t1_H;
    std::vector<int> m_p;  /*!< column pointers of compressed column storage format*/
    std::vector<int> m_i;  /*!< row indices of compressed column storage format */
    int m_nnz;  /*!< number of nonzeros of compressed column storage format */
    int m_N; /*!< dimension of range, domain has dimension m_N+1 */
    bool m_AreSizesAdjusted;
    bool m_IsComputedSparsityPattern;
};




template<typename value_type,
         typename domain_type,
         typename range_type>
template<typename H_functor>
void SparseJacobianEvaluator<value_type,domain_type,range_type>::ComputeJacobian(Eigen::SparseMatrix<value_type>& J, const domain_type& u, H_functor& H)
{
    ComputeSparsityPattern(J,u,H);
    // Initialize AD variables
    for(int i = 0; i < m_N+1; ++i) {
        ad::value(m_t1_u[i]) = u[i];
        ad::derivative(m_t1_u[i]) = 0;
    }
    value_type * valuePtr = J.valuePtr();
    int index = 0;
    for(int j = 0; j < m_N+1; ++j) {
        ad::derivative(m_t1_u[j]) = 1;
        H(m_t1_H,m_t1_u, Conti::EvalFunction());
        ad::derivative(m_t1_u[j]) = 0;

        for(int i = m_p[j]; i < m_p[j+1]; ++i) {
            const int rowIndex = m_i[i];
            valuePtr[index] = ad::derivative(m_t1_H[rowIndex]);
            ++index;
        }
    }

}



template<typename value_type,
         typename domain_type,
         typename range_type>
template<typename H_functor>
void SparseJacobianEvaluator<value_type,domain_type,range_type>::ComputeSparsityPattern(
        Eigen::SparseMatrix<value_type>& J, const domain_type& u, H_functor& H)
{
    if(!m_IsComputedSparsityPattern)
    {
        AdjustSizes(u);
        std::vector<Yasp> yasp_u(m_N+1);
        std::vector<Yasp> yasp_H(m_N);

        int indepCounter=0;
        for (int i=0; i<m_N+1; i++) {
          yasp_u[i]=1;
          yasp_u[i].nz.insert(indepCounter++);
        }
        H(yasp_H,yasp_u,Conti::EvalFunction());

        // first iteration to compute number of column entries
        m_nnz=0;
        std::vector<int> w(m_N+2,0);
        for(int i=0; i < m_N; i++){
          std::set<int>::const_iterator pp;
          for (pp=yasp_H[i].nz.begin(); pp!=yasp_H[i].nz.end(); ++pp) {
            ++w[1 + *pp];
            ++m_nnz;
          }
        }

        // cumulative sum
        for(int i=2; i<=m_N+1 ; ++i)
          w[i] = w[i] + w[i-1];

        // pass to column pointers
        for (int i = 0; i <= m_N+1; ++i)
        {
            m_p[i] = w[i];
        }

        // prepare row indices vector
        m_i.resize(m_nnz,0);


        // second iteration to fill entris
        int index = 0;
        for(int i=0; i < m_N; i++){
          std::set<int>::const_iterator pp;
          for (pp=yasp_H[i].nz.begin(); pp!=yasp_H[i].nz.end(); pp++) {
            unsigned index = w[*pp]; ++w[*pp];
            m_i[index] =  i;
          }
        }

        J.resize(m_N,m_N+1);
        std::vector<Eigen::Triplet<value_type> > triplets;
        triplets.reserve(m_nnz);


        for(int j = 0; j < m_N+1; ++j){
            for(int i = m_p[j]; i < m_p[j+1]; ++i) {
                triplets.emplace_back(m_i[i],j,1.0);
            }
        }

        J.setFromTriplets(triplets.begin(),triplets.end());
        J.makeCompressed();

        m_IsComputedSparsityPattern = true;
    }
}



}  // end namespace Conti

#endif // CONTI_JACOBIAN_EVALUATOR_HPP
