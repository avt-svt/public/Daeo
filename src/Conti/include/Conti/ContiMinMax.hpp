#ifndef CONTI_MIN_MAX_HPP
#define CONTI_MIN_MAX_HPP

#include <type_traits>
#include <algorithm>

namespace Conti {
template<typename T>
inline T Minimum(const T& t) {
    return t;
}

template<typename T, typename ...P>
typename std::common_type<T,P...>::type
Minimum(const T& t, const P& ...p) {
    using res_type = typename std::common_type<T,P...>::type;
    return std::min(res_type(t),res_type(Minimum(p...)));
}

template<typename T>
inline T Maximum(const T& t) {
    return t;
}

template<typename T, typename ...P>
typename std::common_type<T,P...>::type
Maximum(const T& t, const P& ...p) {
    using res_type = typename std::common_type<T,P...>::type;
    return std::max(res_type(t),res_type(Maximum(p...)));
}
}

#endif // CONTI_MIN_MAX_HPP
