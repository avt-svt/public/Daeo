#ifndef CONTI_SOLVER_HPP
#define CONTI_SOLVER_HPP

#include <Eigen/Dense>
#include "Conti/Conti.hpp"
#include "Conti/ContiOptions.hpp"
#include "Conti/ContiUtil.hpp"
#include <limits>
#include <iostream>

namespace Conti{

template<typename value_type,
         typename domain_type = Eigen::Matrix<value_type,Eigen::Dynamic,1>,
         typename range_type = Eigen::Matrix<value_type,Eigen::Dynamic,1>,
         typename jacobian_type = Eigen::Matrix<value_type,Eigen::Dynamic,Eigen::Dynamic>,
         typename qrsolver_type = Eigen::ColPivHouseholderQR<jacobian_type> >
class Solver
{
public:
    Solver(const Options<value_type>& options = Options<value_type>()) : m_N(0), m_Options(options) {}
    template<typename functor_type>
    bool Solve(domain_type& x, functor_type& H);
    Options<value_type> GetOptions() const;
    void SetOptions(const Options<value_type> &Options);

private:
    void AdjustSizes(const domain_type& x);
    void PrepareSolver(const domain_type& x);

    template<typename functor_type>
    bool InnerIteration(functor_type& H);

    void ComputeNewStepSize();

    template<typename functor_type>
    bool HasConverged(functor_type& H);

    void FlipTangentDirectionIfRequired();

private:
    domain_type m_x;
    domain_type m_u;
    range_type m_H;
    value_type m_h;
    domain_type m_tangent_old;
    domain_type m_tangent;
    domain_type m_corrector;
    jacobian_type m_A;
    value_type m_ContractionFactor;
    value_type m_dist_old;
    value_type m_dist;
    value_type m_DistanceToCurve;
    int m_TangentDirection;
    int m_N;
    Options<value_type> m_Options;
    qrsolver_type m_QrSolver;
    unsigned m_NumOuterIterations;
    unsigned m_NumInnerIterations;
    bool m_DoNewton;


};

template<typename value_type,
         typename domain_type,
         typename range_type,
         typename jacobian_type,
         typename qrsolver_type>
void Solver<value_type,domain_type,range_type,jacobian_type,qrsolver_type>::FlipTangentDirectionIfRequired()
{
    if(ScalarProduct(m_tangent,m_tangent_old) < 0) {
        m_TangentDirection = -m_TangentDirection;
    }
}

template<typename value_type,
         typename domain_type,
         typename range_type,
         typename jacobian_type,
         typename qrsolver_type>
Options<value_type> Solver<value_type,domain_type,range_type,jacobian_type,qrsolver_type>::GetOptions() const
{
    return m_Options;
}

template<typename value_type,
         typename domain_type,
         typename range_type,
         typename jacobian_type,
         typename qrsolver_type>
void Solver<value_type,domain_type,range_type,jacobian_type,qrsolver_type>::SetOptions(const Options<value_type> &Options)
{
    m_Options = Options;
}

template<typename value_type,
         typename domain_type,
         typename range_type,
         typename jacobian_type,
         typename qrsolver_type>
void Solver<value_type,domain_type,range_type,jacobian_type,qrsolver_type>::AdjustSizes(const domain_type& x)
{
    const int N1 = size(x);
    const int N = N1-1;
    if(m_N != N)
    {
        m_N = N;
        resize(m_x,N1);
        resize(m_u,N1);
        resize(m_H,N);
        resize(m_tangent,N1);
        resize(m_tangent_old,N1);
        resize(m_corrector,N1);
        resize(m_A,N,N1);
    }
}

template<typename value_type,
         typename domain_type,
         typename range_type,
         typename jacobian_type,
         typename qrsolver_type>
void Solver<value_type,domain_type,range_type,jacobian_type,qrsolver_type>::PrepareSolver(const domain_type& x)
{
    AdjustSizes(x);
    m_x = x;
    m_h = m_Options.InitialSteplength;
    m_DoNewton = false;
    m_NumOuterIterations = 0;
}


template<typename value_type,
         typename domain_type,
         typename range_type,
         typename jacobian_type,
         typename qrsolver_type>
template<typename functor_type>
bool Solver<value_type,domain_type,range_type,jacobian_type,qrsolver_type>::HasConverged(functor_type& H)
{
    using std::numeric_limits;
    if(m_DoNewton) {
        const value_type epsilon = std::numeric_limits<value_type>::epsilon();
        const value_type myepsilon = 10 * epsilon;
        if(AbsoluteValue(m_x[m_N]-1) < myepsilon) {
            H(m_H,m_x,Conti::EvalFunction());
            if ( TwoNorm(m_H) <= m_Options.MaximalResidualNorm ) {
               return true;
            }
        }
    }
    return false;
}




template<typename value_type,
         typename domain_type,
         typename range_type,
         typename jacobian_type,
         typename qrsolver_type>
template<typename functor_type>
bool Solver<value_type,domain_type,range_type,jacobian_type,qrsolver_type>::InnerIteration(
        functor_type& H)
{
    ++m_NumInnerIterations;
    if (m_NumInnerIterations > m_Options.MaximalInnerIterations) {
        return false;
    }
    H(m_H,m_u,EvalFunction());
    ComputeCorrector(m_corrector,m_H,m_QrSolver);
    m_dist = TwoNorm(m_corrector);
    m_u = m_u - m_corrector;
    if(m_NumInnerIterations == 1) m_DistanceToCurve = m_dist;
    if(m_NumInnerIterations == 2) {
      // guard against imediate convergence
      if( m_dist / TwoNorm(m_u) < m_Options.ContractionFactorGuard) {
        m_ContractionFactor = m_Options.DesiredContractionFactor / static_cast<value_type>(100);
      }
      else {
         m_ContractionFactor = m_dist / m_dist_old;
      }

    }
    m_dist_old = m_dist;
    if(m_dist > m_Options.MaximalDistanceToCurve) {
        return false;
    }
    else if ((m_NumInnerIterations == 2) && (m_ContractionFactor > m_Options.MaximalContractionFactor)){
        return false;
    }
    else if(m_NumInnerIterations == m_Options.MaximalInnerIterations &&
                          m_dist >= m_Options.ToleranceCorrectorLoop) {
        return false;
    }
    else {
        return true;
    }

}


template<typename value_type,
         typename domain_type,
         typename range_type,
         typename jacobian_type,
         typename qrsolver_type>
void Solver<value_type,domain_type,range_type,jacobian_type,qrsolver_type>::ComputeNewStepSize()
{
    if (m_x[m_N] > 1) {
        m_DoNewton = true;
    }

    if (!m_DoNewton) {
        value_type angleTangents = AbsoluteValue(acos(ScalarProduct(m_tangent,m_tangent_old)));
        value_type fac1 = sqrt(m_ContractionFactor/m_Options.DesiredContractionFactor);
        value_type fac2 = sqrt(m_DistanceToCurve / m_Options.DesiredDistanceToCurve);
        value_type fac3 = angleTangents / m_Options.DesiredAngleTangents;
        value_type fac = Maximum(fac1,fac2,fac3);
        fac = Maximum(Minimum(fac,value_type(2)),value_type(1)/value_type(2));
        m_h /= fac;
        if(m_x[m_N]+m_TangentDirection*m_h*m_tangent[m_N] > 1) {
            m_DoNewton = true;
        }
    }

    if (m_DoNewton)
    {
        m_TangentDirection = 1;
        m_h = - (m_x[m_N] - 1)/m_tangent[m_N];
    }

}


template<typename value_type,
         typename domain_type,
         typename range_type,
         typename jacobian_type,
         typename qrsolver_type>
template<typename functor_type>
bool Solver<value_type,domain_type,range_type,jacobian_type,qrsolver_type>::Solve(domain_type& x, functor_type& H)
{
    PrepareSolver(x);
    H(m_A,m_x,EvalJacobian());
    ComputeQR(m_QrSolver,m_A);
    ComputeTangent(m_tangent,m_QrSolver);
    m_TangentDirection = sgn(m_tangent[m_N]);
    do
    {
        ++m_NumOuterIterations;
        m_u = m_x + ((m_TangentDirection * m_h) * m_tangent);
        H(m_A,m_x,EvalJacobian());
        ComputeQR(m_QrSolver,m_A);
        m_NumInnerIterations = 0;
        bool successInnerIteration  = false;
        do
        {
            successInnerIteration = InnerIteration(H);
            if(!successInnerIteration) {
                break;
            }
        }
        while((m_dist >= m_Options.ToleranceCorrectorLoop || m_NumInnerIterations < 2));
        if (successInnerIteration) {
            m_x = m_u;
            if(HasConverged(H)) {
                x = m_x;
                return true;
            }
            else {
                m_tangent_old = m_tangent;
                ComputeTangent(m_tangent,m_QrSolver);
                FlipTangentDirectionIfRequired();
                ComputeNewStepSize();
            }
        }
        else
        { // reduce step size
            m_h /= 2;
            if (m_h <= std::numeric_limits<value_type>::epsilon()) {
                return false;
            }
        }
    }
    while (m_NumOuterIterations <= m_Options.MaximalOuterIterations);
    return false;
}

}
#endif // CONTI_SOLVER_HPP
