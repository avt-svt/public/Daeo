
// Generated from //mac/Home/Documents/repo/Daeo/src/DaeoExprParser/DaeoExpr.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "DaeoExprParser.h"


namespace sm {

/**
 * This interface defines an abstract listener for a parse tree produced by DaeoExprParser.
 */
class  DaeoExprListener : public antlr4::tree::ParseTreeListener {
public:

  virtual void enterArithmetic_expression(DaeoExprParser::Arithmetic_expressionContext *ctx) = 0;
  virtual void exitArithmetic_expression(DaeoExprParser::Arithmetic_expressionContext *ctx) = 0;

  virtual void enterAdd_op(DaeoExprParser::Add_opContext *ctx) = 0;
  virtual void exitAdd_op(DaeoExprParser::Add_opContext *ctx) = 0;

  virtual void enterTerm(DaeoExprParser::TermContext *ctx) = 0;
  virtual void exitTerm(DaeoExprParser::TermContext *ctx) = 0;

  virtual void enterMul_op(DaeoExprParser::Mul_opContext *ctx) = 0;
  virtual void exitMul_op(DaeoExprParser::Mul_opContext *ctx) = 0;

  virtual void enterPrimary_unsigned_number(DaeoExprParser::Primary_unsigned_numberContext *ctx) = 0;
  virtual void exitPrimary_unsigned_number(DaeoExprParser::Primary_unsigned_numberContext *ctx) = 0;

  virtual void enterPrimary_name(DaeoExprParser::Primary_nameContext *ctx) = 0;
  virtual void exitPrimary_name(DaeoExprParser::Primary_nameContext *ctx) = 0;

  virtual void enterPrimary_parens_arithmetic_expression(DaeoExprParser::Primary_parens_arithmetic_expressionContext *ctx) = 0;
  virtual void exitPrimary_parens_arithmetic_expression(DaeoExprParser::Primary_parens_arithmetic_expressionContext *ctx) = 0;

  virtual void enterName(DaeoExprParser::NameContext *ctx) = 0;
  virtual void exitName(DaeoExprParser::NameContext *ctx) = 0;


};

}  // namespace sm
