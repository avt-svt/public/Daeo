
// Generated from //mac/Home/Documents/repo/Daeo/src/DaeoExprParser/DaeoExpr.g4 by ANTLR 4.7.2


#include "DaeoExprListener.h"
#include "DaeoExprVisitor.h"

#include "DaeoExprParser.h"


using namespace antlrcpp;
using namespace sm;
using namespace antlr4;

DaeoExprParser::DaeoExprParser(TokenStream *input) : Parser(input) {
  _interpreter = new atn::ParserATNSimulator(this, _atn, _decisionToDFA, _sharedContextCache);
}

DaeoExprParser::~DaeoExprParser() {
  delete _interpreter;
}

std::string DaeoExprParser::getGrammarFileName() const {
  return "DaeoExpr.g4";
}

const std::vector<std::string>& DaeoExprParser::getRuleNames() const {
  return _ruleNames;
}

dfa::Vocabulary& DaeoExprParser::getVocabulary() const {
  return _vocabulary;
}


//----------------- Arithmetic_expressionContext ------------------------------------------------------------------

DaeoExprParser::Arithmetic_expressionContext::Arithmetic_expressionContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<DaeoExprParser::TermContext *> DaeoExprParser::Arithmetic_expressionContext::term() {
  return getRuleContexts<DaeoExprParser::TermContext>();
}

DaeoExprParser::TermContext* DaeoExprParser::Arithmetic_expressionContext::term(size_t i) {
  return getRuleContext<DaeoExprParser::TermContext>(i);
}

std::vector<DaeoExprParser::Add_opContext *> DaeoExprParser::Arithmetic_expressionContext::add_op() {
  return getRuleContexts<DaeoExprParser::Add_opContext>();
}

DaeoExprParser::Add_opContext* DaeoExprParser::Arithmetic_expressionContext::add_op(size_t i) {
  return getRuleContext<DaeoExprParser::Add_opContext>(i);
}


size_t DaeoExprParser::Arithmetic_expressionContext::getRuleIndex() const {
  return DaeoExprParser::RuleArithmetic_expression;
}

void DaeoExprParser::Arithmetic_expressionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<DaeoExprListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterArithmetic_expression(this);
}

void DaeoExprParser::Arithmetic_expressionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<DaeoExprListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitArithmetic_expression(this);
}


antlrcpp::Any DaeoExprParser::Arithmetic_expressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<DaeoExprVisitor*>(visitor))
    return parserVisitor->visitArithmetic_expression(this);
  else
    return visitor->visitChildren(this);
}

DaeoExprParser::Arithmetic_expressionContext* DaeoExprParser::arithmetic_expression() {
  Arithmetic_expressionContext *_localctx = _tracker.createInstance<Arithmetic_expressionContext>(_ctx, getState());
  enterRule(_localctx, 0, DaeoExprParser::RuleArithmetic_expression);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(13);
    _errHandler->sync(this);

    _la = _input->LA(1);
    if (_la == DaeoExprParser::ADD

    || _la == DaeoExprParser::SUB) {
      setState(12);
      add_op();
    }
    setState(15);
    term();
    setState(21);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == DaeoExprParser::ADD

    || _la == DaeoExprParser::SUB) {
      setState(16);
      add_op();
      setState(17);
      term();
      setState(23);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Add_opContext ------------------------------------------------------------------

DaeoExprParser::Add_opContext::Add_opContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* DaeoExprParser::Add_opContext::ADD() {
  return getToken(DaeoExprParser::ADD, 0);
}

tree::TerminalNode* DaeoExprParser::Add_opContext::SUB() {
  return getToken(DaeoExprParser::SUB, 0);
}


size_t DaeoExprParser::Add_opContext::getRuleIndex() const {
  return DaeoExprParser::RuleAdd_op;
}

void DaeoExprParser::Add_opContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<DaeoExprListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterAdd_op(this);
}

void DaeoExprParser::Add_opContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<DaeoExprListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitAdd_op(this);
}


antlrcpp::Any DaeoExprParser::Add_opContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<DaeoExprVisitor*>(visitor))
    return parserVisitor->visitAdd_op(this);
  else
    return visitor->visitChildren(this);
}

DaeoExprParser::Add_opContext* DaeoExprParser::add_op() {
  Add_opContext *_localctx = _tracker.createInstance<Add_opContext>(_ctx, getState());
  enterRule(_localctx, 2, DaeoExprParser::RuleAdd_op);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(26);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case DaeoExprParser::ADD: {
        enterOuterAlt(_localctx, 1);
        setState(24);
        dynamic_cast<Add_opContext *>(_localctx)->op = match(DaeoExprParser::ADD);
        break;
      }

      case DaeoExprParser::SUB: {
        enterOuterAlt(_localctx, 2);
        setState(25);
        dynamic_cast<Add_opContext *>(_localctx)->op = match(DaeoExprParser::SUB);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- TermContext ------------------------------------------------------------------

DaeoExprParser::TermContext::TermContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

std::vector<DaeoExprParser::PrimaryContext *> DaeoExprParser::TermContext::primary() {
  return getRuleContexts<DaeoExprParser::PrimaryContext>();
}

DaeoExprParser::PrimaryContext* DaeoExprParser::TermContext::primary(size_t i) {
  return getRuleContext<DaeoExprParser::PrimaryContext>(i);
}

std::vector<DaeoExprParser::Mul_opContext *> DaeoExprParser::TermContext::mul_op() {
  return getRuleContexts<DaeoExprParser::Mul_opContext>();
}

DaeoExprParser::Mul_opContext* DaeoExprParser::TermContext::mul_op(size_t i) {
  return getRuleContext<DaeoExprParser::Mul_opContext>(i);
}


size_t DaeoExprParser::TermContext::getRuleIndex() const {
  return DaeoExprParser::RuleTerm;
}

void DaeoExprParser::TermContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<DaeoExprListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterTerm(this);
}

void DaeoExprParser::TermContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<DaeoExprListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitTerm(this);
}


antlrcpp::Any DaeoExprParser::TermContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<DaeoExprVisitor*>(visitor))
    return parserVisitor->visitTerm(this);
  else
    return visitor->visitChildren(this);
}

DaeoExprParser::TermContext* DaeoExprParser::term() {
  TermContext *_localctx = _tracker.createInstance<TermContext>(_ctx, getState());
  enterRule(_localctx, 4, DaeoExprParser::RuleTerm);
  size_t _la = 0;

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(28);
    primary();
    setState(34);
    _errHandler->sync(this);
    _la = _input->LA(1);
    while (_la == DaeoExprParser::MUL

    || _la == DaeoExprParser::DIV) {
      setState(29);
      mul_op();
      setState(30);
      primary();
      setState(36);
      _errHandler->sync(this);
      _la = _input->LA(1);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- Mul_opContext ------------------------------------------------------------------

DaeoExprParser::Mul_opContext::Mul_opContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* DaeoExprParser::Mul_opContext::MUL() {
  return getToken(DaeoExprParser::MUL, 0);
}

tree::TerminalNode* DaeoExprParser::Mul_opContext::DIV() {
  return getToken(DaeoExprParser::DIV, 0);
}


size_t DaeoExprParser::Mul_opContext::getRuleIndex() const {
  return DaeoExprParser::RuleMul_op;
}

void DaeoExprParser::Mul_opContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<DaeoExprListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterMul_op(this);
}

void DaeoExprParser::Mul_opContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<DaeoExprListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitMul_op(this);
}


antlrcpp::Any DaeoExprParser::Mul_opContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<DaeoExprVisitor*>(visitor))
    return parserVisitor->visitMul_op(this);
  else
    return visitor->visitChildren(this);
}

DaeoExprParser::Mul_opContext* DaeoExprParser::mul_op() {
  Mul_opContext *_localctx = _tracker.createInstance<Mul_opContext>(_ctx, getState());
  enterRule(_localctx, 6, DaeoExprParser::RuleMul_op);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(39);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case DaeoExprParser::MUL: {
        enterOuterAlt(_localctx, 1);
        setState(37);
        dynamic_cast<Mul_opContext *>(_localctx)->op = match(DaeoExprParser::MUL);
        break;
      }

      case DaeoExprParser::DIV: {
        enterOuterAlt(_localctx, 2);
        setState(38);
        dynamic_cast<Mul_opContext *>(_localctx)->op = match(DaeoExprParser::DIV);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- PrimaryContext ------------------------------------------------------------------

DaeoExprParser::PrimaryContext::PrimaryContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}


size_t DaeoExprParser::PrimaryContext::getRuleIndex() const {
  return DaeoExprParser::RulePrimary;
}

void DaeoExprParser::PrimaryContext::copyFrom(PrimaryContext *ctx) {
  ParserRuleContext::copyFrom(ctx);
}

//----------------- Primary_unsigned_numberContext ------------------------------------------------------------------

tree::TerminalNode* DaeoExprParser::Primary_unsigned_numberContext::UNSIGNED_NUMBER() {
  return getToken(DaeoExprParser::UNSIGNED_NUMBER, 0);
}

DaeoExprParser::Primary_unsigned_numberContext::Primary_unsigned_numberContext(PrimaryContext *ctx) { copyFrom(ctx); }

void DaeoExprParser::Primary_unsigned_numberContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<DaeoExprListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterPrimary_unsigned_number(this);
}
void DaeoExprParser::Primary_unsigned_numberContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<DaeoExprListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitPrimary_unsigned_number(this);
}

antlrcpp::Any DaeoExprParser::Primary_unsigned_numberContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<DaeoExprVisitor*>(visitor))
    return parserVisitor->visitPrimary_unsigned_number(this);
  else
    return visitor->visitChildren(this);
}
//----------------- Primary_parens_arithmetic_expressionContext ------------------------------------------------------------------

DaeoExprParser::Arithmetic_expressionContext* DaeoExprParser::Primary_parens_arithmetic_expressionContext::arithmetic_expression() {
  return getRuleContext<DaeoExprParser::Arithmetic_expressionContext>(0);
}

DaeoExprParser::Primary_parens_arithmetic_expressionContext::Primary_parens_arithmetic_expressionContext(PrimaryContext *ctx) { copyFrom(ctx); }

void DaeoExprParser::Primary_parens_arithmetic_expressionContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<DaeoExprListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterPrimary_parens_arithmetic_expression(this);
}
void DaeoExprParser::Primary_parens_arithmetic_expressionContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<DaeoExprListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitPrimary_parens_arithmetic_expression(this);
}

antlrcpp::Any DaeoExprParser::Primary_parens_arithmetic_expressionContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<DaeoExprVisitor*>(visitor))
    return parserVisitor->visitPrimary_parens_arithmetic_expression(this);
  else
    return visitor->visitChildren(this);
}
//----------------- Primary_nameContext ------------------------------------------------------------------

DaeoExprParser::NameContext* DaeoExprParser::Primary_nameContext::name() {
  return getRuleContext<DaeoExprParser::NameContext>(0);
}

DaeoExprParser::Primary_nameContext::Primary_nameContext(PrimaryContext *ctx) { copyFrom(ctx); }

void DaeoExprParser::Primary_nameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<DaeoExprListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterPrimary_name(this);
}
void DaeoExprParser::Primary_nameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<DaeoExprListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitPrimary_name(this);
}

antlrcpp::Any DaeoExprParser::Primary_nameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<DaeoExprVisitor*>(visitor))
    return parserVisitor->visitPrimary_name(this);
  else
    return visitor->visitChildren(this);
}
DaeoExprParser::PrimaryContext* DaeoExprParser::primary() {
  PrimaryContext *_localctx = _tracker.createInstance<PrimaryContext>(_ctx, getState());
  enterRule(_localctx, 8, DaeoExprParser::RulePrimary);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    setState(47);
    _errHandler->sync(this);
    switch (_input->LA(1)) {
      case DaeoExprParser::UNSIGNED_NUMBER: {
        _localctx = dynamic_cast<PrimaryContext *>(_tracker.createInstance<DaeoExprParser::Primary_unsigned_numberContext>(_localctx));
        enterOuterAlt(_localctx, 1);
        setState(41);
        match(DaeoExprParser::UNSIGNED_NUMBER);
        break;
      }

      case DaeoExprParser::IDENT: {
        _localctx = dynamic_cast<PrimaryContext *>(_tracker.createInstance<DaeoExprParser::Primary_nameContext>(_localctx));
        enterOuterAlt(_localctx, 2);
        setState(42);
        name();
        break;
      }

      case DaeoExprParser::T__0: {
        _localctx = dynamic_cast<PrimaryContext *>(_tracker.createInstance<DaeoExprParser::Primary_parens_arithmetic_expressionContext>(_localctx));
        enterOuterAlt(_localctx, 3);
        setState(43);
        match(DaeoExprParser::T__0);
        setState(44);
        arithmetic_expression();
        setState(45);
        match(DaeoExprParser::T__1);
        break;
      }

    default:
      throw NoViableAltException(this);
    }
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

//----------------- NameContext ------------------------------------------------------------------

DaeoExprParser::NameContext::NameContext(ParserRuleContext *parent, size_t invokingState)
  : ParserRuleContext(parent, invokingState) {
}

tree::TerminalNode* DaeoExprParser::NameContext::IDENT() {
  return getToken(DaeoExprParser::IDENT, 0);
}


size_t DaeoExprParser::NameContext::getRuleIndex() const {
  return DaeoExprParser::RuleName;
}

void DaeoExprParser::NameContext::enterRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<DaeoExprListener *>(listener);
  if (parserListener != nullptr)
    parserListener->enterName(this);
}

void DaeoExprParser::NameContext::exitRule(tree::ParseTreeListener *listener) {
  auto parserListener = dynamic_cast<DaeoExprListener *>(listener);
  if (parserListener != nullptr)
    parserListener->exitName(this);
}


antlrcpp::Any DaeoExprParser::NameContext::accept(tree::ParseTreeVisitor *visitor) {
  if (auto parserVisitor = dynamic_cast<DaeoExprVisitor*>(visitor))
    return parserVisitor->visitName(this);
  else
    return visitor->visitChildren(this);
}

DaeoExprParser::NameContext* DaeoExprParser::name() {
  NameContext *_localctx = _tracker.createInstance<NameContext>(_ctx, getState());
  enterRule(_localctx, 10, DaeoExprParser::RuleName);

  auto onExit = finally([=] {
    exitRule();
  });
  try {
    enterOuterAlt(_localctx, 1);
    setState(49);
    match(DaeoExprParser::IDENT);
   
  }
  catch (RecognitionException &e) {
    _errHandler->reportError(this, e);
    _localctx->exception = std::current_exception();
    _errHandler->recover(this, _localctx->exception);
  }

  return _localctx;
}

// Static vars and initialization.
std::vector<dfa::DFA> DaeoExprParser::_decisionToDFA;
atn::PredictionContextCache DaeoExprParser::_sharedContextCache;

// We own the ATN which in turn owns the ATN states.
atn::ATN DaeoExprParser::_atn;
std::vector<uint16_t> DaeoExprParser::_serializedATN;

std::vector<std::string> DaeoExprParser::_ruleNames = {
  "arithmetic_expression", "add_op", "term", "mul_op", "primary", "name"
};

std::vector<std::string> DaeoExprParser::_literalNames = {
  "", "'('", "')'", "'*'", "'/'", "'+'", "'-'"
};

std::vector<std::string> DaeoExprParser::_symbolicNames = {
  "", "", "", "MUL", "DIV", "ADD", "SUB", "IDENT", "STRING", "UNSIGNED_NUMBER", 
  "WS"
};

dfa::Vocabulary DaeoExprParser::_vocabulary(_literalNames, _symbolicNames);

std::vector<std::string> DaeoExprParser::_tokenNames;

DaeoExprParser::Initializer::Initializer() {
	for (size_t i = 0; i < _symbolicNames.size(); ++i) {
		std::string name = _vocabulary.getLiteralName(i);
		if (name.empty()) {
			name = _vocabulary.getSymbolicName(i);
		}

		if (name.empty()) {
			_tokenNames.push_back("<INVALID>");
		} else {
      _tokenNames.push_back(name);
    }
	}

  _serializedATN = {
    0x3, 0x608b, 0xa72a, 0x8133, 0xb9ed, 0x417c, 0x3be7, 0x7786, 0x5964, 
    0x3, 0xc, 0x36, 0x4, 0x2, 0x9, 0x2, 0x4, 0x3, 0x9, 0x3, 0x4, 0x4, 0x9, 
    0x4, 0x4, 0x5, 0x9, 0x5, 0x4, 0x6, 0x9, 0x6, 0x4, 0x7, 0x9, 0x7, 0x3, 
    0x2, 0x5, 0x2, 0x10, 0xa, 0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 0x3, 0x2, 
    0x7, 0x2, 0x16, 0xa, 0x2, 0xc, 0x2, 0xe, 0x2, 0x19, 0xb, 0x2, 0x3, 0x3, 
    0x3, 0x3, 0x5, 0x3, 0x1d, 0xa, 0x3, 0x3, 0x4, 0x3, 0x4, 0x3, 0x4, 0x3, 
    0x4, 0x7, 0x4, 0x23, 0xa, 0x4, 0xc, 0x4, 0xe, 0x4, 0x26, 0xb, 0x4, 0x3, 
    0x5, 0x3, 0x5, 0x5, 0x5, 0x2a, 0xa, 0x5, 0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 
    0x3, 0x6, 0x3, 0x6, 0x3, 0x6, 0x5, 0x6, 0x32, 0xa, 0x6, 0x3, 0x7, 0x3, 
    0x7, 0x3, 0x7, 0x2, 0x2, 0x8, 0x2, 0x4, 0x6, 0x8, 0xa, 0xc, 0x2, 0x2, 
    0x2, 0x36, 0x2, 0xf, 0x3, 0x2, 0x2, 0x2, 0x4, 0x1c, 0x3, 0x2, 0x2, 0x2, 
    0x6, 0x1e, 0x3, 0x2, 0x2, 0x2, 0x8, 0x29, 0x3, 0x2, 0x2, 0x2, 0xa, 0x31, 
    0x3, 0x2, 0x2, 0x2, 0xc, 0x33, 0x3, 0x2, 0x2, 0x2, 0xe, 0x10, 0x5, 0x4, 
    0x3, 0x2, 0xf, 0xe, 0x3, 0x2, 0x2, 0x2, 0xf, 0x10, 0x3, 0x2, 0x2, 0x2, 
    0x10, 0x11, 0x3, 0x2, 0x2, 0x2, 0x11, 0x17, 0x5, 0x6, 0x4, 0x2, 0x12, 
    0x13, 0x5, 0x4, 0x3, 0x2, 0x13, 0x14, 0x5, 0x6, 0x4, 0x2, 0x14, 0x16, 
    0x3, 0x2, 0x2, 0x2, 0x15, 0x12, 0x3, 0x2, 0x2, 0x2, 0x16, 0x19, 0x3, 
    0x2, 0x2, 0x2, 0x17, 0x15, 0x3, 0x2, 0x2, 0x2, 0x17, 0x18, 0x3, 0x2, 
    0x2, 0x2, 0x18, 0x3, 0x3, 0x2, 0x2, 0x2, 0x19, 0x17, 0x3, 0x2, 0x2, 
    0x2, 0x1a, 0x1d, 0x7, 0x7, 0x2, 0x2, 0x1b, 0x1d, 0x7, 0x8, 0x2, 0x2, 
    0x1c, 0x1a, 0x3, 0x2, 0x2, 0x2, 0x1c, 0x1b, 0x3, 0x2, 0x2, 0x2, 0x1d, 
    0x5, 0x3, 0x2, 0x2, 0x2, 0x1e, 0x24, 0x5, 0xa, 0x6, 0x2, 0x1f, 0x20, 
    0x5, 0x8, 0x5, 0x2, 0x20, 0x21, 0x5, 0xa, 0x6, 0x2, 0x21, 0x23, 0x3, 
    0x2, 0x2, 0x2, 0x22, 0x1f, 0x3, 0x2, 0x2, 0x2, 0x23, 0x26, 0x3, 0x2, 
    0x2, 0x2, 0x24, 0x22, 0x3, 0x2, 0x2, 0x2, 0x24, 0x25, 0x3, 0x2, 0x2, 
    0x2, 0x25, 0x7, 0x3, 0x2, 0x2, 0x2, 0x26, 0x24, 0x3, 0x2, 0x2, 0x2, 
    0x27, 0x2a, 0x7, 0x5, 0x2, 0x2, 0x28, 0x2a, 0x7, 0x6, 0x2, 0x2, 0x29, 
    0x27, 0x3, 0x2, 0x2, 0x2, 0x29, 0x28, 0x3, 0x2, 0x2, 0x2, 0x2a, 0x9, 
    0x3, 0x2, 0x2, 0x2, 0x2b, 0x32, 0x7, 0xb, 0x2, 0x2, 0x2c, 0x32, 0x5, 
    0xc, 0x7, 0x2, 0x2d, 0x2e, 0x7, 0x3, 0x2, 0x2, 0x2e, 0x2f, 0x5, 0x2, 
    0x2, 0x2, 0x2f, 0x30, 0x7, 0x4, 0x2, 0x2, 0x30, 0x32, 0x3, 0x2, 0x2, 
    0x2, 0x31, 0x2b, 0x3, 0x2, 0x2, 0x2, 0x31, 0x2c, 0x3, 0x2, 0x2, 0x2, 
    0x31, 0x2d, 0x3, 0x2, 0x2, 0x2, 0x32, 0xb, 0x3, 0x2, 0x2, 0x2, 0x33, 
    0x34, 0x7, 0x9, 0x2, 0x2, 0x34, 0xd, 0x3, 0x2, 0x2, 0x2, 0x8, 0xf, 0x17, 
    0x1c, 0x24, 0x29, 0x31, 
  };

  atn::ATNDeserializer deserializer;
  _atn = deserializer.deserialize(_serializedATN);

  size_t count = _atn.getNumberOfDecisions();
  _decisionToDFA.reserve(count);
  for (size_t i = 0; i < count; i++) { 
    _decisionToDFA.emplace_back(_atn.getDecisionState(i), i);
  }
}

DaeoExprParser::Initializer DaeoExprParser::_init;
