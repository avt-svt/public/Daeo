
// Generated from //mac/Home/Documents/repo/Daeo/src/DaeoExprParser/DaeoExpr.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "DaeoExprListener.h"


namespace sm {

/**
 * This class provides an empty implementation of DaeoExprListener,
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
class  DaeoExprBaseListener : public DaeoExprListener {
public:

  virtual void enterArithmetic_expression(DaeoExprParser::Arithmetic_expressionContext * /*ctx*/) override { }
  virtual void exitArithmetic_expression(DaeoExprParser::Arithmetic_expressionContext * /*ctx*/) override { }

  virtual void enterAdd_op(DaeoExprParser::Add_opContext * /*ctx*/) override { }
  virtual void exitAdd_op(DaeoExprParser::Add_opContext * /*ctx*/) override { }

  virtual void enterTerm(DaeoExprParser::TermContext * /*ctx*/) override { }
  virtual void exitTerm(DaeoExprParser::TermContext * /*ctx*/) override { }

  virtual void enterMul_op(DaeoExprParser::Mul_opContext * /*ctx*/) override { }
  virtual void exitMul_op(DaeoExprParser::Mul_opContext * /*ctx*/) override { }

  virtual void enterPrimary_unsigned_number(DaeoExprParser::Primary_unsigned_numberContext * /*ctx*/) override { }
  virtual void exitPrimary_unsigned_number(DaeoExprParser::Primary_unsigned_numberContext * /*ctx*/) override { }

  virtual void enterPrimary_name(DaeoExprParser::Primary_nameContext * /*ctx*/) override { }
  virtual void exitPrimary_name(DaeoExprParser::Primary_nameContext * /*ctx*/) override { }

  virtual void enterPrimary_parens_arithmetic_expression(DaeoExprParser::Primary_parens_arithmetic_expressionContext * /*ctx*/) override { }
  virtual void exitPrimary_parens_arithmetic_expression(DaeoExprParser::Primary_parens_arithmetic_expressionContext * /*ctx*/) override { }

  virtual void enterName(DaeoExprParser::NameContext * /*ctx*/) override { }
  virtual void exitName(DaeoExprParser::NameContext * /*ctx*/) override { }


  virtual void enterEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void exitEveryRule(antlr4::ParserRuleContext * /*ctx*/) override { }
  virtual void visitTerminal(antlr4::tree::TerminalNode * /*node*/) override { }
  virtual void visitErrorNode(antlr4::tree::ErrorNode * /*node*/) override { }

};

}  // namespace sm
