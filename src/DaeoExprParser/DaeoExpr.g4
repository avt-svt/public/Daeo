grammar DaeoExpr;

arithmetic_expression
   : (add_op)? term (add_op term)*
   ;

add_op
   : op='+'
   | op='-'
   ;

term
   : primary (mul_op primary)*
   ;

mul_op
   : op='*'
   | op='/'
   ;


primary
   : UNSIGNED_NUMBER                # primary_unsigned_number
   | name                           # primary_name
   | '(' arithmetic_expression ')'  # primary_parens_arithmetic_expression
   ;

name
   : IDENT
   ;

// identfier according to flat Modelica definition

MUL : '*' ;
DIV : '/' ;
ADD : '+' ;
SUB : '-' ;

IDENT
   : NONDIGIT (DIGIT | NONDIGIT | '.' | '[' | ']' )* // raha: added alterantives '.', '[' and ']'
   ;

fragment Q_IDENT
   : '\'' (Q_CHAR | S_ESCAPE) (Q_CHAR | S_ESCAPE)* '\''
   ;


fragment S_CHAR
   : ~ ["\\]
   ;

fragment NONDIGIT
   : '_' | 'a' .. 'z' | 'A' .. 'Z'
   ;

fragment DOT
   : '.'
   ;

STRING
   : '"' ( S_CHAR | S_ESCAPE )* '"'
   ;


fragment Q_CHAR
   : NONDIGIT | DIGIT | '!' | '#' | '$' | '%' | '&' | '(' | ')' | '*' | '+' | ',' | '-' | '.' | '/' | ':' | ';' | '<' | '>' | '=' | '?' | '@' | '[' | ']' | '^' | '{' | '}' | '|' | '~'
   ;


fragment S_ESCAPE
   : '\\' ('�' | '\'' | '"' | '?' | '\\' | 'a' | 'b' | 'f' | 'n' | 'r' | 't' | 'v')
   ;


fragment DIGIT
   : '0' .. '9'
   ;

fragment UNSIGNED_INTEGER
   : DIGIT (DIGIT)*
   ;

UNSIGNED_NUMBER
   : UNSIGNED_INTEGER ('.' (UNSIGNED_INTEGER)?)? (('e' | 'E') ('+' | '-')? UNSIGNED_INTEGER)?
   ;

WS
   : [ \r\n\t] + -> channel (HIDDEN)
   ;
