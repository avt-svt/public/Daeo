#ifndef DaeoExprTESTUTIL_H
#define DaeoExprTESTUTIL_H

#include "antlr4-runtime-wrapper.h"
#include "DaeoExprParser.h"
#include "DaeoExprLexer.h"
#include <memory>


struct DaeoExprTestUtil
{
  static const char* getCorrectSimpleModel();
  static const char* getIncorrectSimpleModel();
  antlr4::tree::ParseTree* getParseTree(const char* model);
private:
  std::shared_ptr<sm::DaeoExprParser> parser;
  std::shared_ptr<sm::DaeoExprLexer> lexer;
  std::shared_ptr<antlr4::ANTLRInputStream> input;
  std::shared_ptr<antlr4::CommonTokenStream> tokens;
};

#endif // DaeoExprTESTUTIL_H
