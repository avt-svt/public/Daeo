#include "antlr4-runtime-wrapper.h"
#include "DaeoExprLexer.h"
#include "DaeoExprParser.h"
#include "gtest/gtest.h"
#include <memory>
#include "DaeoExprTestUtil.h"



TEST(DaeoExprParser, CanParseCorrectSimpleModel)
{
  EXPECT_NO_THROW({

                    const char *model = DaeoExprTestUtil::getCorrectSimpleModel();
                    DaeoExprTestUtil util;
                    auto tree = util.getParseTree(model);

                  });
}

TEST(DaeoExprParser, ThrowsExceptionForIncorrectSimpleModel)
{
  EXPECT_THROW(
  const char *model = DaeoExprTestUtil::getIncorrectSimpleModel();
  DaeoExprTestUtil util;
  auto tree = util.getParseTree(model);,std::exception);
}


