#include "DaeoExprTestUtil.h"
#include "DaeoExprLexer.h"
#include "DaeoExprParser.h"



const char* DaeoExprTestUtil::getCorrectSimpleModel()
{
  static const char* model =
      "v_upt - v_max*A_ex /(A_ex + k_Aex)\n";

  return model;
}


const char* DaeoExprTestUtil::getIncorrectSimpleModel()
{
  static const char *model =
      "v_upt -- v_max*A_ex /(A_ex + k_Aex)";
  return model;
}

antlr4::tree::ParseTree* DaeoExprTestUtil::getParseTree(const char *model)
{
  input.reset(new antlr4::ANTLRInputStream(model));
  lexer.reset(new sm::DaeoExprLexer(input.get()));
  tokens.reset(new antlr4::CommonTokenStream(lexer.get()));
  parser.reset(new sm::DaeoExprParser(tokens.get()));
  parser->getInterpreter<antlr4::atn::ParserATNSimulator>()->setPredictionMode(antlr4::atn::PredictionMode::SLL);
  parser->removeErrorListeners();
  parser->setErrorHandler(std::make_shared<antlr4::BailErrorStrategy>());
  return parser->arithmetic_expression();

}



