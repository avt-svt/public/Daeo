
// Generated from //mac/Home/Documents/repo/Daeo/src/DaeoExprParser/DaeoExpr.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "DaeoExprVisitor.h"


namespace sm {

/**
 * This class provides an empty implementation of DaeoExprVisitor, which can be
 * extended to create a visitor which only needs to handle a subset of the available methods.
 */
class  DaeoExprBaseVisitor : public DaeoExprVisitor {
public:

  virtual antlrcpp::Any visitArithmetic_expression(DaeoExprParser::Arithmetic_expressionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitAdd_op(DaeoExprParser::Add_opContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitTerm(DaeoExprParser::TermContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitMul_op(DaeoExprParser::Mul_opContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitPrimary_unsigned_number(DaeoExprParser::Primary_unsigned_numberContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitPrimary_name(DaeoExprParser::Primary_nameContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitPrimary_parens_arithmetic_expression(DaeoExprParser::Primary_parens_arithmetic_expressionContext *ctx) override {
    return visitChildren(ctx);
  }

  virtual antlrcpp::Any visitName(DaeoExprParser::NameContext *ctx) override {
    return visitChildren(ctx);
  }


};

}  // namespace sm
