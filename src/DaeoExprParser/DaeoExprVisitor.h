
// Generated from //mac/Home/Documents/repo/Daeo/src/DaeoExprParser/DaeoExpr.g4 by ANTLR 4.7.2

#pragma once


#include "antlr4-runtime.h"
#include "DaeoExprParser.h"


namespace sm {

/**
 * This class defines an abstract visitor for a parse tree
 * produced by DaeoExprParser.
 */
class  DaeoExprVisitor : public antlr4::tree::AbstractParseTreeVisitor {
public:

  /**
   * Visit parse trees produced by DaeoExprParser.
   */
    virtual antlrcpp::Any visitArithmetic_expression(DaeoExprParser::Arithmetic_expressionContext *context) = 0;

    virtual antlrcpp::Any visitAdd_op(DaeoExprParser::Add_opContext *context) = 0;

    virtual antlrcpp::Any visitTerm(DaeoExprParser::TermContext *context) = 0;

    virtual antlrcpp::Any visitMul_op(DaeoExprParser::Mul_opContext *context) = 0;

    virtual antlrcpp::Any visitPrimary_unsigned_number(DaeoExprParser::Primary_unsigned_numberContext *context) = 0;

    virtual antlrcpp::Any visitPrimary_name(DaeoExprParser::Primary_nameContext *context) = 0;

    virtual antlrcpp::Any visitPrimary_parens_arithmetic_expression(DaeoExprParser::Primary_parens_arithmetic_expressionContext *context) = 0;

    virtual antlrcpp::Any visitName(DaeoExprParser::NameContext *context) = 0;


};

}  // namespace sm
