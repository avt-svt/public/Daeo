within ;
package Daeo
  package Solver
    class MyDaeoObject
      extends ExternalObject;
      function constructor
        input String params[:];
        // DAEO parameters (description)
        input String variables[:];
        // DAEO variables (description)
        input String objective;
        // DAEO objective (description)
        input String equalities[:];
        // DAEO equalities (description)
        input String inequalities[:];
        input Real Qdiag[:];
        input Integer nQdiag;
        // DAEO inequalities (>=0)
        //input Integer n_y;
        output MyDaeoObject dfba;
      protected
        Integer info[2];
      external"C" dfba = parseDFBAmodel(
              params,
              variables,
              objective,
              equalities,
              inequalities,
              size(params, 1),
              size(equalities, 1),
              size(variables, 1),
              size(inequalities, 1),
              Qdiag,
              size(Qdiag,1),
              info) annotation (Library={"DaeoToolbox"});
        // assert(info[1] == 0,
        //   "DAEO toolbox: Parsing error, please check NLP formulation");
      end constructor;

      function destructor "Release storage of table"
        input MyDaeoObject dfba;
      external"C" closeDFBAmodel(dfba) annotation (Library={"DaeoToolbox"});
      end destructor;
    end MyDaeoObject;

    function updateActiveSet
      input Daeo.Solver.MyDaeoObject dfba;
      input Real b[:];
      output Integer info[2];
    external"C" updateBasicSet(
          dfba,
          b,
          info) annotation (Library={"DaeoToolbox"});
    end updateActiveSet;

    function updateActiveSetNew
      input Integer pre_eventCounter;
      input Daeo.Solver.MyDaeoObject dfba;
      input Real daeoIn[:];
      output Integer eventCounter;
    algorithm
      updateActiveSet(dfba,daeoIn);
      eventCounter :=pre_eventCounter + 1;
    end updateActiveSetNew;

    function solveDirect
      input MyDaeoObject dfba;
      input Real b[:];
      input Integer n_x;
      output Real x[n_x];
      output Integer info[2];
    external"C" solveDirect(
          dfba,
          b,
          x,
          info) annotation (Library={"DaeoToolbox"});
      //"cplex1280","DfbaCplex","concert","ilocplex"
    end solveDirect;

    function solveDirectSigma
      import DaeoDfba = Daeo;
      input DaeoDfba.Solver.MyDaeoObject dfba;
      input Real b[:];
      input Integer n_x;
      input Integer n_q;
      output Real x[n_x];
      output Real sigma[n_q];
      output Integer info[2];
    external"C" solveDirectSigma(
          dfba,
          b,
          x,
          sigma,
          info) annotation (Library={"DaeoToolbox"});
      //"cplex1280","DfbaCplex","concert","ilocplex"
    end solveDirectSigma;

    function solveActiveSet
      import DaeoDfba = Daeo;
      input DaeoDfba.Solver.MyDaeoObject dfba;
      input Real b[:];
      input Integer n_x;
      input Integer n_q;
      output Real x[n_x];
      output Real sigma[n_q];
      output Integer info[2];
    external"C" solveActiveSet(
          dfba,
          b,
          x,
          sigma,
          info) annotation (Library={"DaeoToolbox"});//,derivative=dsolveActiveSet_dt);

      //"cplex1280","DfbaCplex","concert","ilocplex"
    end solveActiveSet;

    function solveActiveSetDer
      import DaeoDfba = Daeo;
      input DaeoDfba.Solver.MyDaeoObject dfba;
      input Real b[:];
      input Integer n_x;
      input Integer n_q;
      output Real x[n_x];
      output Real sigma[n_q];
      output Integer info[2];
    external"C" solveActiveSet(
        dfba,
        b,
        x,
        sigma,
        info) annotation (Library={"DaeoToolbox"},derivative=Derivatives.derivative_dx_dsigma);
    end solveActiveSetDer;

    package Derivatives
      function derivative_dx_dsigma
        input MyDaeoObject dfba;
        input Real b[:];
        input Integer n_x;
        input Integer n_q;
        input Real db[:];
        output Real dx[n_x];
        output Real dsigma[n_q];
      protected
        Integer info[2];
      external"C" getDer_dxdb_new(
            dfba,
            db,
            dx,
            dsigma,
            info) annotation (Library={"DaeoToolbox"});
      end derivative_dx_dsigma;

    end Derivatives;

    class NonlinearDaeoObject
      extends ExternalObject;
      function constructor
        input String params[:];
        // DAEO parameters (description)
        input String variables[:];
        // DAEO variables (description)
        input String objective;
        // DAEO objective (description)
        input String equalities[:];
        // DAEO equalities (description)
        input String inequalities[:];
        // DAEO inequalities (>=0)
        //input Integer n_y;
        output NonlinearDaeoObject daeo;
      protected
        Integer info[2];
      external"C" daeo = parsenNonlinearDAEOmodel(
              params,
              variables,
              objective,
              equalities,
              inequalities,
              size(params, 1),
              size(equalities, 1),
              size(variables, 1),
              size(inequalities, 1),
              info) annotation (Library={"DaeoToolbox"});
        // assert(info[1] == 0,
        //   "DAEO toolbox: Parsing error, please check NLP formulation");
      end constructor;

      function destructor "Release storage of table"
        input NonlinearDaeoObject daeo;
      external"C" closeNonlinearDAEO(daeo) annotation (Library={"DaeoToolbox"});
      end destructor;
    end NonlinearDaeoObject;

    function solveNonlinearActiveSet
      import DaeoDfba = Daeo;
      input DaeoDfba.Solver.NonlinearDaeoObject daeo;
      input Real x[:];
      input Integer n_y;
      input Integer n_q;
      output Real y[n_y];
      output Real sigma[n_q];
      output Integer info[2];
    external"C" solveNonlinearActiveSet(
          daeo,
          x,
          y,
          sigma,
          info) annotation (Library={"DaeoToolbox"});
    end solveNonlinearActiveSet;

    function updateNonlinearActiveSet
      import DaeoDfba = Daeo;
      input DaeoDfba.Solver.NonlinearDaeoObject daeo;
      input Real x[:];
      output Integer info[2];
    external"C" updateNonlinearActiveSet(
          daeo,
          x,
          info) annotation (Library={"DaeoToolbox"});
    end updateNonlinearActiveSet;

    function changeActiveSet
      input Real sigma[:];
      input Boolean mode_old[:];
      input Real eps=0;
      output Boolean mode_new[size(sigma,1)];
    algorithm
      for i in 1:size(sigma,1) loop
        if (sigma[i] < eps) then
          mode_new[i] := not
                            (mode_old[i]);
        end if;
      end for;
    end changeActiveSet;

    function min_ind
      input Real[:] vec;
      output Integer min_ind;
    protected
      Real[:] vec_sorted;
      Integer[:] ind;
    algorithm
      (vec_sorted,ind) := Modelica.Math.Vectors.sort(vec);
      min_ind :=ind[1];
    end min_ind;
  end Solver;

  package Nonlinear_Example

    model SmallNonlinearExample_DAEO
     import Modelica.Constants.pi;
     parameter Real p1=1, p2=3, p3=1;
     constant Integer n=2 "number of DAEO variables";
     constant Integer p=3 "number of DAEO inputs";
     constant Integer m=1 "number of DAEO equalities";
     constant Integer q=2 "number of DAEO inequalities";
     constant String params[p]={"x","p2","p3"} "DAEO inputs";
     Real daeoIn[p] "DAEO input values";
     constant String variables[n]={"y","z"} "DAEO variables";
     constant String objective="(-x + 2 * y + p3)^2"
     "DAEO objective function (to be minimized)";
     constant String equalities[m]={"y - z"}
     "DAEO equality constraints";
     constant String inequalities[q]={"y","p2-y"}
     "DAEO inequality constraints";
     parameter Solver.NonlinearDaeoObject daeo=
      Solver.NonlinearDaeoObject(params,
      variables,
      objective,
      equalities,
      inequalities) "Nonlinear external DAEO object";
     parameter Real tolEvent=1e-6;
     Real y_d(start=4, fixed=true) "Differential variable";
     Real y_a(start=1) "Algebraic variable";
     Real sigma[q] "DAEO switching function values";
     Real daeoOut[n] "DAEO solution vector";
     Integer info[2];
     Integer eventCounter(start=0, fixed=true);
    equation
     der(y_d) = 8*pi*cos(time*(2*pi));
     daeoIn[1] = y_d;
     daeoIn[2] = p2;
     daeoIn[3] = p3;
     (daeoOut,sigma,info) = Solver.solveNonlinearActiveSet(
      daeo,
      daeoIn,
      n,
      q);
     when min(sigma) < -tolEvent then
      Solver.updateNonlinearActiveSet(daeo, daeoIn);
      eventCounter = pre(eventCounter) + 1;
     end when;
     y_a = daeoOut[1];
       annotation (experiment(
          StopTime=1,
          Interval=0.01,
          Tolerance=1e-12));
    end SmallNonlinearExample_DAEO;

    model SmallNonlinearExample_nDAE
     import Modelica.Constants.pi;
     constant Integer m=0 "Number of DAEO equalities";
     constant Integer q=2 "Number of DAEO inequalities";
     parameter Real p1=1;
     parameter Real p2=3;
     parameter Real p3=1;
     parameter Real tolEvent=1e-6;
     Real y_d(start=4, fixed=true) "Differential variable";
     Real y_a(start=1) "Algebraic variable";
     Real sigma[q] "DAEO switching function values";
     Real lambda[q] "Lagrangian multiplier";
     Real gineq[q] "Inequalities of embedded NLP";
     Boolean isactive_ineq[q](start={false,false});
    equation
     der(y_d) = 8*pi*cos(time*(2*pi));
     gineq[1] = y_a;
     gineq[2] = p2 - y_a;
     4*(-y_d + 2*y_a + p3) - lambda[1] + lambda[2] = 0;
     for i in 1:q loop
      if (pre(isactive_ineq[i])) then
       gineq[i] = 0;
       sigma[i] = lambda[i];
      else
       lambda[i] = 0;
       sigma[i] = gineq[i];
      end if;
     end for;
     when (min(sigma) < -tolEvent) then
      isactive_ineq = Solver.changeActiveSet(
       sigma,
       pre(isactive_ineq));
     end when;
      annotation (experiment(
          StopTime=1,
          Interval=0.01,
          Tolerance=1e-12));
    end SmallNonlinearExample_nDAE;

  end Nonlinear_Example;

  package DFBA_Examples

    package CorynebacteriumGlutamicum
      record ParameterSet
        //BioSc forum values
        parameter Real vmax_g=4.5;
        parameter Real K_g=15;
        parameter Real vmax_x=4;
        parameter Real K_x=15;
        parameter Real vmax_o2=7.54;
        parameter Real K_o2=0.001;
        parameter Real klaO2=20;
        parameter Real C_O2_sat=1.03;
        //parameter needed for xylonate production
        parameter Real ndh_ub=M;
        //11.7
        parameter Real xdy_ub=M;
        parameter Real upt_max = 4.5;
        parameter Real xi_ub=1e6;
        parameter Real xdh_ub=1e6;
        parameter Real xr_ub=1e6;
        parameter Real yagE_ub=1e6;
        parameter Real kdy_ub=1e6;
        constant Real M = 1e6;

        parameter Real MM_GLC = 0.18016;//g/mmol
        parameter Real MM_XYL = 0.15013;//g/mmol
        parameter Real MM_XLT = 0.16613;//g/mmol

        parameter Real tolEvent = 2e-9;

        parameter Real C_threshold=-10 "when reached, simulation is stopped";
        parameter Real eps=1e-5
          "when extracellular concentration of differential equation is below, der(x) = 0";

          // parameter and variables needed to handle O2 balance
        parameter Real C_O2_const=0.3*C_O2_sat;
      end ParameterSet;

      partial model partial_CG_metabolicModel_Linear
        // Here, DAEO related variable declarations
        constant Integer n=68 "number of DAEO variables";
        constant Integer p=9 "number of DAEO inputs";
        constant Integer m=57 "number of DAEO equalities";
        constant Integer q=50 "number of DAEO inequalities";
        constant String params[p]={"b_1","b_2","b_3","upt_max","xdh_ub",
            "kdy_ub","xi_ub","yagE_ub","xr_ub"} "DAEO inputs";
        Real daeoIn[p] "DAEO input values";
        constant String variables[n]={"icd","odhA","sucD","sdhCAB_m","fumC",
            "mqo_mdh","gltA","pgi","pyk","pdh","aceB","aceA","odx","pyc","mez",
            "pckG","ppc","pfkA","fda","gapA","eno","zwf","opcA","gnd","rpe",
            "rpi","tkt_1","tal","tkt_2","tpiA","fbp","CO2_t","bmsynth","SO3_t",
            "NH3_t","bc1_aa3","O2_t","ksh","Xyl_t","GLC_t_PEP","pgm","pgk",
            "acnB","acnA","SUCC_t","ndh","cyto_bd","ATPase","xdh","xls","xdy",
            "kdy","dGLC_t","dXyl_t","dO2_t","xi","xylB","yagE","aldA","glcD",
            "xr","xldh","pta","ackA","pqo","AC_t","ldh","LAC_t"}
          "DAEO variables";
        constant String objective="2*dGLC_t - bmsynth + 2*dXyl_t"
          "DAEO objective function";
        // DAEO constraints with bounds
        constant String equalities[m]={"acnB - 1.0*aceA - 1.0*icd",
            "icd - 1.224*bmsynth + ksh - 1.0*odhA","odhA - 1.0*sucD",
            "sdhCAB_m - 1.0*fumC","aceB + fumC - 1.0*mez - 1.0*mqo_mdh",
            "mqo_mdh - 1.0*gltA - 1.68*bmsynth - 1.0*odx - 1.0*pckG + ppc + pyc",
            "aceA - 1.0*SUCC_t - 1.0*sdhCAB_m + sucD",
            "pdh - 3.177*bmsynth - 1.0*gltA - 1.0*aceB - 1.0*pta",
            "GLC_t_PEP - 2.604*bmsynth - 1.0*ldh + mez + odx - 1.0*pdh - 1.0*pqo - 1.0*pyc + pyk + yagE",
            "eno - 0.652*bmsynth - 1.0*GLC_t_PEP + pckG - 1.0*ppc - 1.0*pyk",
            "GLC_t_PEP - 0.205*bmsynth - 1.0*pgi - 1.0*zwf",
            "aceA - 1.0*aceB + glcD",
            "fbp - 0.308*bmsynth - 1.0*pfkA + pgi + tal + tkt_2",
            "pfkA - 1.0*fda - 1.0*fbp",
            "fda - 0.129*bmsynth - 1.0*gapA - 1.0*tal + tkt_1 + tkt_2 + tpiA",
            "gnd - 1.0*rpe - 1.0*rpi","zwf - 1.0*opcA","opcA - 1.0*gnd",
            "rpe - 1.0*tkt_1 - 1.0*tkt_2 + xylB",
            "rpi - 0.879*bmsynth - 1.0*tkt_1","tkt_1 - 1.0*tal",
            "tal - 0.268*bmsynth - 1.0*tkt_2","fda - 1.0*tpiA",
            "bmsynth - 1.0*CO2_t + gnd + icd + mez + odhA + odx + pckG + pdh - 1.0*ppc - 1.0*pyc",
            "pgm - 1.0*eno",
            "ATPase - 1.0*Xyl_t + ackA - 17.002*bmsynth - 1.0*pckG - 1.0*pfkA + pgk - 1.0*pyc + pyk + sucD - 1.0*xylB",
            "aldA + 3.111*bmsynth + gapA + ksh - 1.0*ldh - 1.0*ndh + odhA + pdh + xdh + xldh",
            "glcD - 16.429*bmsynth + gnd + icd + mez - 1.0*xr + zwf",
            "2.0*bc1_aa3 + 2.0*cyto_bd - 1.0*mqo_mdh - 1.0*ndh - 1.0*pqo - 1.0*sdhCAB_m",
            "mqo_mdh - 2.0*cyto_bd - 2.0*bc1_aa3 + ndh + pqo + sdhCAB_m",
            "NH3_t - 6.231*bmsynth","O2_t - 1.0*bc1_aa3 - 1.0*cyto_bd",
            "Xyl_t - 1.0*xdh - 1.0*xi - 1.0*xr","pgk - 1.295*bmsynth - 1.0*pgm",
            "gapA - 1.0*pgk","SO3_t - 0.233*bmsynth","gltA - 1.0*acnA",
            "acnA - 1.0*acnB",
            "4.0*ATPase - 12.0*bc1_aa3 - 4.0*cyto_bd + 2.0*sdhCAB_m",
            "kdy - 1.0*ksh","xdy - 1.0*kdy - 1.0*yagE","xls - 1.0*xdy",
            "xdh - 1.0*xls","GLC_t_PEP - 1.0*b_1 + dGLC_t",
            "Xyl_t - 1.0*b_2 + dXyl_t","O2_t - 1.0*b_3 + dO2_t",
            "xi + xldh - 1.0*xylB","yagE - 1.0*aldA","aldA - 1.0*glcD",
            "xr - 1.0*xldh","pta - 1.0*ackA","ackA - 1.0*AC_t + pqo",
            "ldh - 1.0*LAC_t","mez","odx","ppc","pqo"}
          "DAEO equality constraints";
        constant String inequalities[q]={"icd","odhA","sucD","gltA","pyk","pdh",
            "aceB","aceA","pyc","pckG","pfkA","zwf","opcA","gnd","fbp",
            "bmsynth","SO3_t","NH3_t","bc1_aa3","O2_t","ksh","Xyl_t",
            "GLC_t_PEP","SUCC_t","ndh","cyto_bd","ATPase","xdh","xls","xdy",
            "kdy","dGLC_t","dXyl_t","dO2_t","xi","xylB","yagE","aldA","glcD",
            "xr","xldh","AC_t","ldh","LAC_t",
            "upt_max - 1.0*Xyl_t - 1.0*GLC_t_PEP","xdh_ub - 1.0*xdh",
            "kdy_ub - 1.0*kdy","xi_ub - 1.0*xi","yagE_ub - 1.0*yagE",
            "xr_ub - 1.0*xr"} "DAEO inequality constraints";
        parameter Real Gdiag[0]=zeros(0)
          "Diagonal entries of quadratic objective matrix G";
        parameter Solver.MyDaeoObject dfba=Solver.MyDaeoObject(
                  params,
                  variables,
                  objective,
                  equalities,
                  inequalities,
                  Gdiag,
                  size(Gdiag, 1)) "Linear external DAEO object"
          annotation (fixed=true);

        // outputs of DAEO
        Real daeoOut[n] "DAEO solution vector";
        Real sigma[q] "DAEO switching function values";
        // names of fluxes to be mapped to solution vector
        Real icd;
        Real odhA;
        Real sucD;
        Real sdhCAB_m;
        Real fumC;
        Real mqo_mdh;
        Real gltA;
        Real pgi;
        Real pyk;
        Real pdh;
        Real aceB;
        Real aceA;
        Real odx;
        Real pyc;
        Real mez;
        Real pckG;
        Real ppc;
        Real pfkA;
        Real fda;
        Real gapA;
        Real eno;
        Real zwf;
        Real opcA;
        Real gnd;
        Real rpe;
        Real rpi;
        Real tkt_1;
        Real tal;
        Real tkt_2;
        Real tpiA;
        Real fbp;
        Real CO2_t;
        Real bmsynth;
        Real SO3_t;
        Real NH3_t;
        Real bc1_aa3;
        Real O2_t;
        Real ksh;
        Real Xyl_t;
        Real GLC_t_PEP;
        Real pgm;
        Real pgk;
        Real acnB;
        Real acnA;
        Real SUCC_t;
        Real ndh;
        Real cyto_bd;
        Real ATPase;
        Real xdh;
        Real xls;
        Real xdy;
        Real kdy;
        Real dGLC_t;
        Real dXyl_t;
        Real dO2_t;
        Real xi;
        Real xylB;
        Real yagE;
        Real aldA;
        Real glcD;
        Real xr;
        Real xldh;
        Real pta;
        Real ackA;
        Real pqo;
        Real AC_t;
        Real ldh;
        Real LAC_t;

      equation
        // mapping of fluxes to their respective entries in solution vector
        icd = daeoOut[1];
        odhA = daeoOut[2];
        sucD = daeoOut[3];
        sdhCAB_m = daeoOut[4];
        fumC = daeoOut[5];
        mqo_mdh = daeoOut[6];
        gltA = daeoOut[7];
        pgi = daeoOut[8];
        pyk = daeoOut[9];
        pdh = daeoOut[10];
        aceB = daeoOut[11];
        aceA = daeoOut[12];
        odx = daeoOut[13];
        pyc = daeoOut[14];
        mez = daeoOut[15];
        pckG = daeoOut[16];
        ppc = daeoOut[17];
        pfkA = daeoOut[18];
        fda = daeoOut[19];
        gapA = daeoOut[20];
        eno = daeoOut[21];
        zwf = daeoOut[22];
        opcA = daeoOut[23];
        gnd = daeoOut[24];
        rpe = daeoOut[25];
        rpi = daeoOut[26];
        tkt_1 = daeoOut[27];
        tal = daeoOut[28];
        tkt_2 = daeoOut[29];
        tpiA = daeoOut[30];
        fbp = daeoOut[31];
        CO2_t = daeoOut[32];
        bmsynth = daeoOut[33];
        SO3_t = daeoOut[34];
        NH3_t = daeoOut[35];
        bc1_aa3 = daeoOut[36];
        O2_t = daeoOut[37];
        ksh = daeoOut[38];
        Xyl_t = daeoOut[39];
        GLC_t_PEP = daeoOut[40];
        pgm = daeoOut[41];
        pgk = daeoOut[42];
        acnB = daeoOut[43];
        acnA = daeoOut[44];
        SUCC_t = daeoOut[45];
        ndh = daeoOut[46];
        cyto_bd = daeoOut[47];
        ATPase = daeoOut[48];
        xdh = daeoOut[49];
        xls = daeoOut[50];
        xdy = daeoOut[51];
        kdy = daeoOut[52];
        dGLC_t = daeoOut[53];
        dXyl_t = daeoOut[54];
        dO2_t = daeoOut[55];
        xi = daeoOut[56];
        xylB = daeoOut[57];
        yagE = daeoOut[58];
        aldA = daeoOut[59];
        glcD = daeoOut[60];
        xr = daeoOut[61];
        xldh = daeoOut[62];
        pta = daeoOut[63];
        ackA = daeoOut[64];
        pqo = daeoOut[65];
        AC_t = daeoOut[66];
        ldh = daeoOut[67];
        LAC_t = daeoOut[68];
        annotation (experiment(
            StopTime=30,
            Interval=0.1,
            Tolerance=1e-006));
      end partial_CG_metabolicModel_Linear;

      partial model partial_CG_metabolicModel_Quadratic
        // Here, DAEO related variable declarations
        constant Integer n=68 "number of DAEO variables";
        constant Integer p=9 "number of DAEO inputs";
        constant Integer m=57 "number of DAEO equalities";
        constant Integer q=50 "number of DAEO inequalities";
        constant String params[p]={"b_1", "b_2", "b_3", "upt_max", "xdh_ub", "kdy_ub", "xi_ub", "yagE_ub", "xr_ub"} "DAEO inputs";
        Real daeoIn[p] "DAEO input values";
        constant String variables[n]={"icd", "odhA", "sucD", "sdhCAB_m", "fumC", "mqo_mdh", "gltA", "pgi", "pyk", "pdh", "aceB", "aceA", "odx", "pyc", "mez", "pckG", "ppc", "pfkA", "fda", "gapA", "eno", "zwf", "opcA", "gnd", "rpe", "rpi", "tkt_1", "tal", "tkt_2", "tpiA", "fbp", "CO2_t", "bmsynth", "SO3_t", "NH3_t", "bc1_aa3", "O2_t", "ksh", "Xyl_t", "GLC_t_PEP", "pgm", "pgk", "acnB", "acnA", "SUCC_t", "ndh", "cyto_bd", "ATPase", "xdh", "xls", "xdy", "kdy", "dGLC_t", "dXyl_t", "dO2_t", "xi", "xylB", "yagE", "aldA", "glcD", "xr", "xldh", "pta", "ackA", "pqo", "AC_t", "ldh", "LAC_t"} "DAEO variables";
        constant String objective="2*dGLC_t - bmsynth + 2*dXyl_t" "DAEO objective function";
        // DAEO constraints with bounds
        constant String equalities[m]={"acnB - 1.0*aceA - 1.0*icd", "icd - 1.224*bmsynth + ksh - 1.0*odhA", "odhA - 1.0*sucD", "sdhCAB_m - 1.0*fumC", "aceB + fumC - 1.0*mez - 1.0*mqo_mdh", "mqo_mdh - 1.0*gltA - 1.68*bmsynth - 1.0*odx - 1.0*pckG + ppc + pyc", "aceA - 1.0*SUCC_t - 1.0*sdhCAB_m + sucD", "pdh - 3.177*bmsynth - 1.0*gltA - 1.0*aceB - 1.0*pta", "GLC_t_PEP - 2.604*bmsynth - 1.0*ldh + mez + odx - 1.0*pdh - 1.0*pqo - 1.0*pyc + pyk + yagE", "eno - 0.652*bmsynth - 1.0*GLC_t_PEP + pckG - 1.0*ppc - 1.0*pyk", "GLC_t_PEP - 0.205*bmsynth - 1.0*pgi - 1.0*zwf", "aceA - 1.0*aceB + glcD", "fbp - 0.308*bmsynth - 1.0*pfkA + pgi + tal + tkt_2", "pfkA - 1.0*fda - 1.0*fbp", "fda - 0.129*bmsynth - 1.0*gapA - 1.0*tal + tkt_1 + tkt_2 + tpiA", "gnd - 1.0*rpe - 1.0*rpi", "zwf - 1.0*opcA", "opcA - 1.0*gnd", "rpe - 1.0*tkt_1 - 1.0*tkt_2 + xylB", "rpi - 0.879*bmsynth - 1.0*tkt_1", "tkt_1 - 1.0*tal", "tal - 0.268*bmsynth - 1.0*tkt_2", "fda - 1.0*tpiA", "bmsynth - 1.0*CO2_t + gnd + icd + mez + odhA + odx + pckG + pdh - 1.0*ppc - 1.0*pyc", "pgm - 1.0*eno", "ATPase - 1.0*Xyl_t + ackA - 17.002*bmsynth - 1.0*pckG - 1.0*pfkA + pgk - 1.0*pyc + pyk + sucD - 1.0*xylB", "aldA + 3.111*bmsynth + gapA + ksh - 1.0*ldh - 1.0*ndh + odhA + pdh + xdh + xldh", "glcD - 16.429*bmsynth + gnd + icd + mez - 1.0*xr + zwf", "2.0*bc1_aa3 + 2.0*cyto_bd - 1.0*mqo_mdh - 1.0*ndh - 1.0*pqo - 1.0*sdhCAB_m", "mqo_mdh - 2.0*cyto_bd - 2.0*bc1_aa3 + ndh + pqo + sdhCAB_m", "NH3_t - 6.231*bmsynth", "O2_t - 1.0*bc1_aa3 - 1.0*cyto_bd", "Xyl_t - 1.0*xdh - 1.0*xi - 1.0*xr", "pgk - 1.295*bmsynth - 1.0*pgm", "gapA - 1.0*pgk", "SO3_t - 0.233*bmsynth", "gltA - 1.0*acnA", "acnA - 1.0*acnB", "4.0*ATPase - 12.0*bc1_aa3 - 4.0*cyto_bd + 2.0*sdhCAB_m", "kdy - 1.0*ksh", "xdy - 1.0*kdy - 1.0*yagE", "xls - 1.0*xdy", "xdh - 1.0*xls", "GLC_t_PEP - 1.0*b_1 + dGLC_t", "Xyl_t - 1.0*b_2 + dXyl_t", "O2_t - 1.0*b_3 + dO2_t", "xi + xldh - 1.0*xylB", "yagE - 1.0*aldA", "aldA - 1.0*glcD", "xr - 1.0*xldh", "pta - 1.0*ackA", "ackA - 1.0*AC_t + pqo", "ldh - 1.0*LAC_t", "mez", "odx", "ppc", "pqo"} "DAEO equality constraints";
        constant String inequalities[q]={"icd", "odhA", "sucD", "gltA", "pyk", "pdh", "aceB", "aceA", "pyc", "pckG", "pfkA", "zwf", "opcA", "gnd", "fbp", "bmsynth", "SO3_t", "NH3_t", "bc1_aa3", "O2_t", "ksh", "Xyl_t", "GLC_t_PEP", "SUCC_t", "ndh", "cyto_bd", "ATPase", "xdh", "xls", "xdy", "kdy", "dGLC_t", "dXyl_t", "dO2_t", "xi", "xylB", "yagE", "aldA", "glcD", "xr", "xldh", "AC_t", "ldh", "LAC_t", "upt_max - 1.0*Xyl_t - 1.0*GLC_t_PEP", "xdh_ub - 1.0*xdh", "kdy_ub - 1.0*kdy", "xi_ub - 1.0*xi", "yagE_ub - 1.0*yagE", "xr_ub - 1.0*xr"} "DAEO inequality constraints";
        parameter Real Gdiag[n+p](fixed=false) "Diagonal entries of quadratic objective matrix G";
        parameter Solver.MyDaeoObject dfba=Solver.MyDaeoObject(
            params,
            variables,
            objective,
            equalities,
            inequalities,
            Gdiag,
            size(Gdiag, 1)) "Linear external DAEO object" annotation (fixed=true);

        // outputs of DAEO
        Real daeoOut[n] "DAEO solution vector";
        Real sigma[q] "DAEO switching function values";
        // names of fluxes to be mapped to solution vector
        Real icd;
        Real odhA;
        Real sucD;
        Real sdhCAB_m;
        Real fumC;
        Real mqo_mdh;
        Real gltA;
        Real pgi;
        Real pyk;
        Real pdh;
        Real aceB;
        Real aceA;
        Real odx;
        Real pyc;
        Real mez;
        Real pckG;
        Real ppc;
        Real pfkA;
        Real fda;
        Real gapA;
        Real eno;
        Real zwf;
        Real opcA;
        Real gnd;
        Real rpe;
        Real rpi;
        Real tkt_1;
        Real tal;
        Real tkt_2;
        Real tpiA;
        Real fbp;
        Real CO2_t;
        Real bmsynth;
        Real SO3_t;
        Real NH3_t;
        Real bc1_aa3;
        Real O2_t;
        Real ksh;
        Real Xyl_t;
        Real GLC_t_PEP;
        Real pgm;
        Real pgk;
        Real acnB;
        Real acnA;
        Real SUCC_t;
        Real ndh;
        Real cyto_bd;
        Real ATPase;
        Real xdh;
        Real xls;
        Real xdy;
        Real kdy;
        Real dGLC_t;
        Real dXyl_t;
        Real dO2_t;
        Real xi;
        Real xylB;
        Real yagE;
        Real aldA;
        Real glcD;
        Real xr;
        Real xldh;
        Real pta;
        Real ackA;
        Real pqo;
        Real AC_t;
        Real ldh;
        Real LAC_t;
      initial algorithm
         for i in 1:p loop
           Gdiag[i]:=1e-12;
         end for;
         for i in p+1:p+n loop
           Gdiag[i]:=1e-6;
         end for;
      equation
        // mapping of fluxes to their respective entries in solution vector
        icd = daeoOut[1];
        odhA = daeoOut[2];
        sucD = daeoOut[3];
        sdhCAB_m = daeoOut[4];
        fumC = daeoOut[5];
        mqo_mdh = daeoOut[6];
        gltA = daeoOut[7];
        pgi = daeoOut[8];
        pyk = daeoOut[9];
        pdh = daeoOut[10];
        aceB = daeoOut[11];
        aceA = daeoOut[12];
        odx = daeoOut[13];
        pyc = daeoOut[14];
        mez = daeoOut[15];
        pckG = daeoOut[16];
        ppc = daeoOut[17];
        pfkA = daeoOut[18];
        fda = daeoOut[19];
        gapA = daeoOut[20];
        eno = daeoOut[21];
        zwf = daeoOut[22];
        opcA = daeoOut[23];
        gnd = daeoOut[24];
        rpe = daeoOut[25];
        rpi = daeoOut[26];
        tkt_1 = daeoOut[27];
        tal = daeoOut[28];
        tkt_2 = daeoOut[29];
        tpiA = daeoOut[30];
        fbp = daeoOut[31];
        CO2_t = daeoOut[32];
        bmsynth = daeoOut[33];
        SO3_t = daeoOut[34];
        NH3_t = daeoOut[35];
        bc1_aa3 = daeoOut[36];
        O2_t = daeoOut[37];
        ksh = daeoOut[38];
        Xyl_t = daeoOut[39];
        GLC_t_PEP = daeoOut[40];
        pgm = daeoOut[41];
        pgk = daeoOut[42];
        acnB = daeoOut[43];
        acnA = daeoOut[44];
        SUCC_t = daeoOut[45];
        ndh = daeoOut[46];
        cyto_bd = daeoOut[47];
        ATPase = daeoOut[48];
        xdh = daeoOut[49];
        xls = daeoOut[50];
        xdy = daeoOut[51];
        kdy = daeoOut[52];
        dGLC_t = daeoOut[53];
        dXyl_t = daeoOut[54];
        dO2_t = daeoOut[55];
        xi = daeoOut[56];
        xylB = daeoOut[57];
        yagE = daeoOut[58];
        aldA = daeoOut[59];
        glcD = daeoOut[60];
        xr = daeoOut[61];
        xldh = daeoOut[62];
        pta = daeoOut[63];
        ackA = daeoOut[64];
        pqo = daeoOut[65];
        AC_t = daeoOut[66];
        ldh = daeoOut[67];
        LAC_t = daeoOut[68];
        annotation (experiment(
            StopTime=30,
            Interval=0.1,
            Tolerance=1e-006));
      end partial_CG_metabolicModel_Quadratic;

      model main_CG_Direct
        extends partial_CG_metabolicModel_Linear;

        // kinetic parameters for CG example
        Daeo.DFBA_Examples.CorynebacteriumGlutamicum.ParameterSet CG_params;

        // parameters for calculation of initial values of differential states
        parameter Real X_start=0.1;
        parameter Real C_C_total=1000
          "Total moles of carbon per liter at beginning of batch [mol C/L]";
        parameter Real phi_Xyl=0.1
          "Ratio of carbon atoms in xylose vs (xylose+glucose)";

        // definition of differential variables
        Real X(start=X_start, fixed=true);
        Real C_Glc(start=(1 - phi_Xyl)*C_C_total/6, fixed=true);
        Real C_Xyl(start=phi_Xyl*C_C_total/5, fixed=true);
        Real C_Suc(start=0.000000, fixed=true);
        Real C_Lac(start=0.000000, fixed=true);
        Real C_Ace(start=0.000000, fixed=true);

        // additional quantities of interest
        Real Yield "Yield w.r.t. biomass formation";
        Real STY "Space-time yield w.r.t. biomass formation";
        Real Titer "Biomass titer";
        Real C_total "Concentration of carbon during simulation [mol C/L]";
      equation
        // condition to terminate simulation when carbon sources are depleted
      //   when (C_total < CG_params.C_threshold) then
      //     terminate("Carbon sources are depleted");
      //   end when;

         // DAEO input definition
        daeoIn[1] = if noEvent(C_Glc < CG_params.eps) then 0 else (C_Glc*CG_params.vmax_g)/(C_Glc +  CG_params.K_g);
        daeoIn[2] = if noEvent(C_Xyl < CG_params.eps) then 0 else (C_Xyl*CG_params.vmax_x)/(C_Xyl +  CG_params.K_x);
        daeoIn[3] = CG_params.vmax_o2;
        daeoIn[4] = CG_params.upt_max;
        daeoIn[5] = CG_params.xdh_ub;
        daeoIn[6] = CG_params.kdy_ub;
        daeoIn[7] = CG_params.xi_ub;
        daeoIn[8] = CG_params.yagE_ub;
        daeoIn[9] = CG_params.xr_ub;

        // daeo calculations
        (daeoOut,sigma) =Solver.solveDirectSigma(
          dfba,
          daeoIn,
          n,
          q);

        // differential equations
        der(X) = bmsynth*X;
        der(C_Glc) = if noEvent(C_Glc < CG_params.eps) then 0 else -(GLC_t_PEP*X);
        der(C_Xyl) = if noEvent(C_Xyl < CG_params.eps) then 0 else -(Xyl_t*X);
        der(C_Suc) = (SUCC_t*X);
        der(C_Lac) = LAC_t*X;
        der(C_Ace) = AC_t*X;

        // calculation of additional quantities of interest
        C_total = 6*C_Glc + 5*C_Xyl;
        Titer = X;
        //added small eps in denominator to maintain well-posed equation at t=0
        STY = X/(time + CG_params.eps);
        Yield = X/C_C_total;
        annotation (experiment(
            StopTime=12,
            Interval=0.1,
            Tolerance=1e-12));
      end main_CG_Direct;

      model main_CG_Direct_Quadratic
        extends partial_CG_metabolicModel_Quadratic;

        // kinetic parameters for CG example
        Daeo.DFBA_Examples.CorynebacteriumGlutamicum.ParameterSet CG_params;

        // parameters for calculation of initial values of differential states
        parameter Real X_start=0.1;
        parameter Real C_C_total=1000
          "Total moles of carbon per liter at beginning of batch [mol C/L]";
        parameter Real phi_Xyl=0.1
          "Ratio of carbon atoms in xylose vs (xylose+glucose)";

        // definition of differential variables
        Real X(start=X_start, fixed=true);
        Real C_Glc(start=(1 - phi_Xyl)*C_C_total/6, fixed=true);
        Real C_Xyl(start=phi_Xyl*C_C_total/5, fixed=true);
        Real C_Suc(start=0.000000, fixed=true);
        Real C_Lac(start=0.000000, fixed=true);
        Real C_Ace(start=0.000000, fixed=true);

        // additional quantities of interest
        Real Yield "Yield w.r.t. biomass formation";
        Real STY "Space-time yield w.r.t. biomass formation";
        Real Titer "Biomass titer";
        Real C_total "Concentration of carbon during simulation [mol C/L]";
      equation
        // condition to terminate simulation when carbon sources are depleted
      //   when (C_total < CG_params.C_threshold) then
      //     terminate("Carbon sources are depleted");
      //   end when;

         // DAEO input definition
        daeoIn[1] = if noEvent(C_Glc < CG_params.eps) then 0 else (C_Glc*CG_params.vmax_g)/(C_Glc +  CG_params.K_g);
        daeoIn[2] = if noEvent(C_Xyl < CG_params.eps) then 0 else (C_Xyl*CG_params.vmax_x)/(C_Xyl +  CG_params.K_x);
        daeoIn[3] = CG_params.vmax_o2;
        daeoIn[4] = CG_params.upt_max;
        daeoIn[5] = CG_params.xdh_ub;
        daeoIn[6] = CG_params.kdy_ub;
        daeoIn[7] = CG_params.xi_ub;
        daeoIn[8] = CG_params.yagE_ub;
        daeoIn[9] = CG_params.xr_ub;

        // daeo calculations
        (daeoOut,sigma) =Solver.solveDirectSigma(
          dfba,
          daeoIn,
          n,
          q);

        // differential equations
        der(X) = bmsynth*X;
        der(C_Glc) = if noEvent(C_Glc < CG_params.eps) then 0 else -(GLC_t_PEP*X);
        der(C_Xyl) = if noEvent(C_Xyl < CG_params.eps) then 0 else -(Xyl_t*X);
        der(C_Suc) = (SUCC_t*X);
        der(C_Lac) = LAC_t*X;
        der(C_Ace) = AC_t*X;

        // calculation of additional quantities of interest
        C_total = 6*C_Glc + 5*C_Xyl;
        Titer = X;
        //added small eps in denominator to maintain well-posed equation at t=0
        STY = X/(time + CG_params.eps);
        Yield = X/C_C_total;
        annotation (experiment(
            StopTime=12,
            Interval=0.1,
            Tolerance=1e-12));
      end main_CG_Direct_Quadratic;

      model main_CG_ActiveSet
        extends partial_CG_metabolicModel_Linear;

        // kinetic parameters for CG example
        Daeo.DFBA_Examples.CorynebacteriumGlutamicum.ParameterSet CG_params;

        // parameters for calculation of initial values of differential states
        parameter Real X_start=0.1;
        parameter Real C_C_total=1000
          "Total moles of carbon per liter at beginning of batch [mol C/L]";
        parameter Real phi_Xyl=0.1
          "Ratio of carbon atoms in xylose vs (xylose+glucose)";

        // definition of differential variables
        Real X(start=X_start, fixed=true);
        Real C_Glc(start=(1 - phi_Xyl)*C_C_total/6, fixed=true);
        Real C_Xyl(start=phi_Xyl*C_C_total/5, fixed=true);
        Real C_Suc(start=0.000000, fixed=true);
        Real C_Lac(start=0.000000, fixed=true);
        Real C_Ace(start=0.000000, fixed=true);

        // additional quantities of interest
        Real Yield "Yield w.r.t. biomass formation";
        Real STY "Space-time yield w.r.t. biomass formation";
        Real Titer "Biomass titer";
        Real C_total "Concentration of carbon during simulation [mol C/L]";
        // for active set approach
        Real minsigma;
        Integer eventCounter(start=0, fixed=true);
      equation
        // condition to terminate simulation when carbon sources are depleted
      //   when (C_total < CG_params.C_threshold) then
      //     terminate("Carbon sources are depleted");
      //   end when;

         // DAEO input definition
        daeoIn[1] = if noEvent(C_Glc < CG_params.eps) then 0 else (C_Glc*CG_params.vmax_g)/(C_Glc +  CG_params.K_g);
        daeoIn[2] = if noEvent(C_Xyl < CG_params.eps) then 0 else (C_Xyl*CG_params.vmax_x)/(C_Xyl +  CG_params.K_x);
        daeoIn[3] = CG_params.vmax_o2;
        daeoIn[4] = CG_params.upt_max;
        daeoIn[5] = CG_params.xdh_ub;
        daeoIn[6] = CG_params.kdy_ub;
        daeoIn[7] = CG_params.xi_ub;
        daeoIn[8] = CG_params.yagE_ub;
        daeoIn[9] = CG_params.xr_ub;

        // daeo calculations
        (daeoOut,sigma) = Solver.solveActiveSet(
                dfba,
                daeoIn,
                n,
                q);
        minsigma = min(sigma);
        when min(sigma) < -CG_params.tolEvent then
          eventCounter=Solver.updateActiveSetNew(pre(eventCounter),dfba,daeoIn);
        end when;

        // differential equations
        der(X) = bmsynth*X;
        der(C_Glc) = if noEvent(C_Glc < CG_params.eps) then 0 else -(GLC_t_PEP*X);
        der(C_Xyl) = if noEvent(C_Xyl < CG_params.eps) then 0 else -(Xyl_t*X);
        der(C_Suc) = (SUCC_t*X);
        der(C_Lac) = LAC_t*X;
        der(C_Ace) = AC_t*X;

        // calculation of additional quantities of interest
        C_total = 6*C_Glc + 5*C_Xyl;
        Titer = X;
        //added small eps in denominator to maintain well-posed equation at t=0
        STY = X/(time + CG_params.eps);
        Yield = X/C_C_total;
        annotation (experiment(
            StopTime=12,
            Interval=0.1,
            Tolerance=1e-12));
      end main_CG_ActiveSet;

      model main_CG_ActiveSet_Der
        extends partial_CG_metabolicModel_Linear;

        // kinetic parameters for CG example
        Daeo.DFBA_Examples.CorynebacteriumGlutamicum.ParameterSet CG_params;

        // parameters for calculation of initial values of differential states
        parameter Real X_start=0.1;
        parameter Real C_C_total=1000
          "Total moles of carbon per liter at beginning of batch [mol C/L]";
        parameter Real phi_Xyl=0.1
          "Ratio of carbon atoms in xylose vs (xylose+glucose)";

        // definition of differential variables
        Real X(start=X_start, fixed=true);
        Real C_Glc(start=(1 - phi_Xyl)*C_C_total/6, fixed=true);
        Real C_Xyl(start=phi_Xyl*C_C_total/5, fixed=true);
        Real C_Suc(start=0.000000, fixed=true);
        Real C_Lac(start=0.000000, fixed=true);
        Real C_Ace(start=0.000000, fixed=true);

        // additional quantities of interest
        Real Yield "Yield w.r.t. biomass formation";
        Real STY "Space-time yield w.r.t. biomass formation";
        Real Titer "Biomass titer";
        Real C_total "Concentration of carbon during simulation [mol C/L]";
        // for active set approach
        Real minsigma;
        Integer eventCounter(start=0, fixed=true);
      equation
        // condition to terminate simulation when carbon sources are depleted
      //   when (C_total < CG_params.C_threshold) then
      //     terminate("Carbon sources are depleted");
      //   end when;

         // DAEO input definition
        daeoIn[1] = if noEvent(C_Glc < CG_params.eps) then 0 else (C_Glc*CG_params.vmax_g)/(C_Glc +  CG_params.K_g);
        daeoIn[2] = if noEvent(C_Xyl < CG_params.eps) then 0 else (C_Xyl*CG_params.vmax_x)/(C_Xyl +  CG_params.K_x);
        daeoIn[3] = CG_params.vmax_o2;
        daeoIn[4] = CG_params.upt_max;
        daeoIn[5] = CG_params.xdh_ub;
        daeoIn[6] = CG_params.kdy_ub;
        daeoIn[7] = CG_params.xi_ub;
        daeoIn[8] = CG_params.yagE_ub;
        daeoIn[9] = CG_params.xr_ub;

        // daeo calculations
        (daeoOut,sigma) = Solver.solveActiveSetDer(
                dfba,
                daeoIn,
                n,
                q);
        minsigma = min(sigma);
        when min(sigma) < -CG_params.tolEvent then
          eventCounter=Solver.updateActiveSetNew(pre(eventCounter),dfba,daeoIn);
        end when;

        // differential equations
        der(X) = bmsynth*X;
        der(C_Glc) = if noEvent(C_Glc < CG_params.eps) then 0 else -(GLC_t_PEP*X);
        der(C_Xyl) = if noEvent(C_Xyl < CG_params.eps) then 0 else -(Xyl_t*X);
        der(C_Suc) = (SUCC_t*X);
        der(C_Lac) = LAC_t*X;
        der(C_Ace) = AC_t*X;

        // calculation of additional quantities of interest
        C_total = 6*C_Glc + 5*C_Xyl;
        Titer = X;
        //added small eps in denominator to maintain well-posed equation at t=0
        STY = X/(time + CG_params.eps);
        Yield = X/C_C_total;
        annotation (experiment(
            StopTime=12,
            Interval=0.1,
            Tolerance=1e-12));
      end main_CG_ActiveSet_Der;

      model main_CG_ActiveSet_Quadratic
        extends partial_CG_metabolicModel_Quadratic;

        // kinetic parameters for CG example
        Daeo.DFBA_Examples.CorynebacteriumGlutamicum.ParameterSet CG_params;

        // parameters for calculation of initial values of differential states
        parameter Real X_start=0.1;
        parameter Real C_C_total=1000
          "Total moles of carbon per liter at beginning of batch [mol C/L]";
        parameter Real phi_Xyl=0.1
          "Ratio of carbon atoms in xylose vs (xylose+glucose)";

        // definition of differential variables
        Real X(start=X_start, fixed=true);
        Real C_Glc(start=(1 - phi_Xyl)*C_C_total/6, fixed=true);
        Real C_Xyl(start=phi_Xyl*C_C_total/5, fixed=true);
        Real C_Suc(start=0.000000, fixed=true);
        Real C_Lac(start=0.000000, fixed=true);
        Real C_Ace(start=0.000000, fixed=true);

        // additional quantities of interest
        Real Yield "Yield w.r.t. biomass formation";
        Real STY "Space-time yield w.r.t. biomass formation";
        Real Titer "Biomass titer";
        Real C_total "Concentration of carbon during simulation [mol C/L]";
        // for active set approach
        Real minsigma;
        Integer eventCounter(start=0, fixed=true);
      equation
        // condition to terminate simulation when carbon sources are depleted
      //   when (C_total < CG_params.C_threshold) then
      //     terminate("Carbon sources are depleted");
      //   end when;

         // DAEO input definition
        daeoIn[1] = if noEvent(C_Glc < CG_params.eps) then 0 else (C_Glc*CG_params.vmax_g)/(C_Glc +  CG_params.K_g);
        daeoIn[2] = if noEvent(C_Xyl < CG_params.eps) then 0 else (C_Xyl*CG_params.vmax_x)/(C_Xyl +  CG_params.K_x);
        daeoIn[3] = CG_params.vmax_o2;
        daeoIn[4] = CG_params.upt_max;
        daeoIn[5] = CG_params.xdh_ub;
        daeoIn[6] = CG_params.kdy_ub;
        daeoIn[7] = CG_params.xi_ub;
        daeoIn[8] = CG_params.yagE_ub;
        daeoIn[9] = CG_params.xr_ub;

        // daeo calculations
        (daeoOut,sigma) = Solver.solveActiveSet(
                dfba,
                daeoIn,
                n,
                q);
        minsigma = min(sigma);
        when min(sigma) < -CG_params.tolEvent then
          //Solver.updateActiveSet(dfba, daeoIn);
          eventCounter=Solver.updateActiveSetNew(pre(eventCounter),dfba,daeoIn);
          //eventCounter = pre(eventCounter) + 1;
        end when;

        // differential equations
        der(X) = bmsynth*X;
        der(C_Glc) = if noEvent(C_Glc < CG_params.eps) then 0 else -(GLC_t_PEP*X);
        der(C_Xyl) = if noEvent(C_Xyl < CG_params.eps) then 0 else -(Xyl_t*X);
        der(C_Suc) = (SUCC_t*X);
        der(C_Lac) = LAC_t*X;
        der(C_Ace) = AC_t*X;

        // calculation of additional quantities of interest
        C_total = 6*C_Glc + 5*C_Xyl;
        Titer = X;
        //added small eps in denominator to maintain well-posed equation at t=0
        STY = X/(time + CG_params.eps);
        Yield = X/C_C_total;
        annotation (experiment(
            StopTime=12,
            Interval=0.1,
            Tolerance=1e-12));
      end main_CG_ActiveSet_Quadratic;

      model main_CG_ActiveSet_Quadratic_Der
        extends partial_CG_metabolicModel_Quadratic;

        // kinetic parameters for CG example
        Daeo.DFBA_Examples.CorynebacteriumGlutamicum.ParameterSet CG_params;

        // parameters for calculation of initial values of differential states
        parameter Real X_start=0.1;
        parameter Real C_C_total=1000
          "Total moles of carbon per liter at beginning of batch [mol C/L]";
        parameter Real phi_Xyl=0.1
          "Ratio of carbon atoms in xylose vs (xylose+glucose)";

        // definition of differential variables
        Real X(start=X_start, fixed=true);
        Real C_Glc(start=(1 - phi_Xyl)*C_C_total/6, fixed=true);
        Real C_Xyl(start=phi_Xyl*C_C_total/5, fixed=true);
        Real C_Suc(start=0.000000, fixed=true);
        Real C_Lac(start=0.000000, fixed=true);
        Real C_Ace(start=0.000000, fixed=true);

        // additional quantities of interest
        Real Yield "Yield w.r.t. biomass formation";
        Real STY "Space-time yield w.r.t. biomass formation";
        Real Titer "Biomass titer";
        Real C_total "Concentration of carbon during simulation [mol C/L]";
        // for active set approach
        Real minsigma;
      //  Integer eventCounter(start=0, fixed=true);
      equation
        // condition to terminate simulation when carbon sources are depleted
      //   when (C_total < CG_params.C_threshold) then
      //     terminate("Carbon sources are depleted");
      //   end when;

         // DAEO input definition
        daeoIn[1] = if noEvent(C_Glc < CG_params.eps) then 0 else (C_Glc*CG_params.vmax_g)/(C_Glc +  CG_params.K_g);
        daeoIn[2] = if noEvent(C_Xyl < CG_params.eps) then 0 else (C_Xyl*CG_params.vmax_x)/(C_Xyl +  CG_params.K_x);
        daeoIn[3] = CG_params.vmax_o2;
        daeoIn[4] = CG_params.upt_max;
        daeoIn[5] = CG_params.xdh_ub;
        daeoIn[6] = CG_params.kdy_ub;
        daeoIn[7] = CG_params.xi_ub;
        daeoIn[8] = CG_params.yagE_ub;
        daeoIn[9] = CG_params.xr_ub;

        // daeo calculations
        (daeoOut,sigma) = Solver.solveActiveSetDer(
                dfba,
                daeoIn,
                n,
                q);
        minsigma = min(sigma);
      //   when min(sigma) < -CG_params.tolEvent then
      //     //Solver.updateActiveSet(dfba, daeoIn);
      //     eventCounter=Solver.updateActiveSetNew(pre(eventCounter),dfba,daeoIn);
      //     //eventCounter = pre(eventCounter) + 1;
      //   end when;

        // differential equations
        der(X) = bmsynth*X;
        der(C_Glc) = if noEvent(C_Glc < CG_params.eps) then 0 else -(GLC_t_PEP*X);
        der(C_Xyl) = if noEvent(C_Xyl < CG_params.eps) then 0 else -(Xyl_t*X);
        der(C_Suc) = (SUCC_t*X);
        der(C_Lac) = LAC_t*X;
        der(C_Ace) = AC_t*X;

        // calculation of additional quantities of interest
        C_total = 6*C_Glc + 5*C_Xyl;
        Titer = X;
        //added small eps in denominator to maintain well-posed equation at t=0
        STY = X/(time + CG_params.eps);
        Yield = X/C_C_total;
        annotation (experiment(
            StopTime=12,
            Interval=0.1,
            Tolerance=1e-12));
      end main_CG_ActiveSet_Quadratic_Der;

      package CPU_comparison

        model run_Case_Study_LP
          parameter Integer i=2;
          parameter Real Y0_matrix[:,:]=[    0.1791,  100.7166,   33.9415;
            0.6603,   67.9341,   45.9382;
            0.1075,  165.6415,  186.5116;
            0.2096,   87.6245,  169.0709;
            0.5875,  128.7336,    6.0327;
            0.3955,   59.5665,   66.6117;
            0.9631,   40.8325,  116.3365;
            0.8814,  134.1685,   97.3553;
            0.7535,    9.6956,  139.8971;
            0.4714,   16.9341,  136.4593];
          parameter Real X_start=Y0_matrix[i,1];
          parameter Real C_Glc_start=Y0_matrix[i,2];
          parameter Real C_Xyl_start=Y0_matrix[i,3];
          CorynebacteriumGlutamicum.main_CG_ActiveSet ActiveSet(
            X(start=X_start),
            C_Glc(start=C_Glc_start),
            C_Xyl(start=C_Xyl_start));
          annotation (experiment(
              StopTime=15,
              Interval=0.01,
              Tolerance=1e-12));
        end run_Case_Study_LP;

        model run_Case_Study_QP
          parameter Integer i=1;
          parameter Real Y0_matrix[:,:]=[    0.1791,  100.7166,   33.9415;
            0.6603,   67.9341,   45.9382;
            0.1075,  165.6415,  186.5116;
            0.2096,   87.6245,  169.0709;
            0.5875,  128.7336,    6.0327;
            0.3955,   59.5665,   66.6117;
            0.9631,   40.8325,  116.3365;
            0.8814,  134.1685,   97.3553;
            0.7535,    9.6956,  139.8971;
            0.4714,   16.9341,  136.4593];
          parameter Real X_start=Y0_matrix[i,1];
          parameter Real C_Glc_start=Y0_matrix[i,2];
          parameter Real C_Xyl_start=Y0_matrix[i,3];
          CorynebacteriumGlutamicum.main_CG_ActiveSet_Quadratic ActiveSet(
            X(start=X_start),
            C_Glc(start=C_Glc_start),
            C_Xyl(start=C_Xyl_start));
          annotation (experiment(
              StopTime=15,
              Interval=0.01,
              Tolerance=1e-12));
        end run_Case_Study_QP;
      end CPU_comparison;
    end CorynebacteriumGlutamicum;

    package EscherichiaColi_iJR904
      record ParameterSet
        parameter Real vmax_g=10.5 "mmol/g/h";
        parameter Real K_g=0.0027 "g/L";
        parameter Real K_ie=20 "g/L";
        parameter Real vmax_x=6 "mmol/g/h";
        parameter Real K_x=0.0165 "g/L";
        parameter Real vmax_o2=15 "mmol/g/h";
        parameter Real K_o2=0.024 "g/L";
        parameter Real K_ig=0.005 "g/L";
        parameter Real tolEvent = 2e-9;
        parameter Real C_threshold=0.1;

        parameter Real MW_Glc = 180.1559/1000;
        parameter Real MW_Xyl = 150.13/1000;
        parameter Real MW_O2 = 16/1000;
        parameter Real MW_Ethanol = 46.06844/1000;
      end ParameterSet;

      partial model partial_Ecoli_metabolicModel_Linear
        // Here, DAEO related variable declarations
        constant Integer n=1152 "number of DAEO variables";
        constant Integer p=3 "number of DAEO inputs";
        constant Integer m=839 "number of DAEO equalities";
        constant Integer q=823 "number of DAEO inequalities";
        constant String params[p]={"b1","b2","b3"} "DAEO input variables";
        Real daeoIn[p] "DAEO input values (need to be provided by user)";
        constant String variables[n]={"v1","v2","v3","v4","v5","v6","v7","v8",
            "v9","v10","v11","v12","v13","v14","v15","v16","v17","v18","v19",
            "v20","v21","v22","v23","v24","v25","v26","v27","v28","v29","v30",
            "v31","v32","v33","v34","v35","v36","v37","v38","v39","v40","v41",
            "v42","v43","v44","v45","v46","v47","v48","v49","v50","v51","v52",
            "v53","v54","v55","v56","v57","v58","v59","v60","v61","v62","v63",
            "v64","v65","v66","v67","v68","v69","v70","v71","v72","v73","v74",
            "v75","v76","v77","v78","v79","v80","v81","v82","v83","v84","v85",
            "v86","v87","v88","v89","v90","v91","v92","v93","v94","v95","v96",
            "v97","v98","v99","v100","v101","v102","v103","v104","v105","v106",
            "v107","v108","v109","v110","v111","v112","v113","v114","v115",
            "v116","v117","v118","v119","v120","v121","v122","v123","v124",
            "v125","v126","v127","v128","v129","v130","v131","v132","v133",
            "v134","v135","v136","v137","v138","v139","v140","v141","v142",
            "v143","v144","v145","v146","v147","v148","v149","v150","v151",
            "v152","v153","v154","v155","v156","v157","v158","v159","v160",
            "v161","v162","v163","v164","v165","v166","v167","v168","v169",
            "v170","v171","v172","v173","v174","v175","v176","v177","v178",
            "v179","v180","v181","v182","v183","v184","v185","v186","v187",
            "v188","v189","v190","v191","v192","v193","v194","v195","v196",
            "v197","v198","v199","v200","v201","v202","v203","v204","v205",
            "v206","v207","v208","v209","v210","v211","v212","v213","v214",
            "v215","v216","v217","v218","v219","v220","v221","v222","v223",
            "v224","v225","v226","v227","v228","v229","v230","v231","v232",
            "v233","v234","v235","v236","v237","v238","v239","v240","v241",
            "v242","v243","v244","v245","v246","v247","v248","v249","v250",
            "v251","v252","v253","v254","v255","v256","v257","v258","v259",
            "v260","v261","v262","v263","v264","v265","v266","v267","v268",
            "v269","v270","v271","v272","v273","v274","v275","v276","v277",
            "v278","v279","v280","v281","v282","v283","v284","v285","v286",
            "v287","v288","v289","v290","v291","v292","v293","v294","v295",
            "v296","v297","v298","v299","v300","v301","v302","v303","v304",
            "v305","v306","v307","v308","v309","v310","v311","v312","v313",
            "v314","v315","v316","v317","v318","v319","v320","v321","v322",
            "v323","v324","v325","v326","v327","v328","v329","v330","v331",
            "v332","v333","v334","v335","v336","v337","v338","v339","v340",
            "v341","v342","v343","v344","v345","v346","v347","v348","v349",
            "v350","v351","v352","v353","v354","v355","v356","v357","v358",
            "v359","v360","v361","v362","v363","v364","v365","v366","v367",
            "v368","v369","v370","v371","v372","v373","v374","v375","v376",
            "v377","v378","v379","v380","v381","v382","v383","v384","v385",
            "v386","v387","v388","v389","v390","v391","v392","v393","v394",
            "v395","v396","v397","v398","v399","v400","v401","v402","v403",
            "v404","v405","v406","v407","v408","v409","v410","v411","v412",
            "v413","v414","v415","v416","v417","v418","v419","v420","v421",
            "v422","v423","v424","v425","v426","v427","v428","v429","v430",
            "v431","v432","v433","v434","v435","v436","v437","v438","v439",
            "v440","v441","v442","v443","v444","v445","v446","v447","v448",
            "v449","v450","v451","v452","v453","v454","v455","v456","v457",
            "v458","v459","v460","v461","v462","v463","v464","v465","v466",
            "v467","v468","v469","v470","v471","v472","v473","v474","v475",
            "v476","v477","v478","v479","v480","v481","v482","v483","v484",
            "v485","v486","v487","v488","v489","v490","v491","v492","v493",
            "v494","v495","v496","v497","v498","v499","v500","v501","v502",
            "v503","v504","v505","v506","v507","v508","v509","v510","v511",
            "v512","v513","v514","v515","v516","v517","v518","v519","v520",
            "v521","v522","v523","v524","v525","v526","v527","v528","v529",
            "v530","v531","v532","v533","v534","v535","v536","v537","v538",
            "v539","v540","v541","v542","v543","v544","v545","v546","v547",
            "v548","v549","v550","v551","v552","v553","v554","v555","v556",
            "v557","v558","v559","v560","v561","v562","v563","v564","v565",
            "v566","v567","v568","v569","v570","v571","v572","v573","v574",
            "v575","v576","v577","v578","v579","v580","v581","v582","v583",
            "v584","v585","v586","v587","v588","v589","v590","v591","v592",
            "v593","v594","v595","v596","v597","v598","v599","v600","v601",
            "v602","v603","v604","v605","v606","v607","v608","v609","v610",
            "v611","v612","v613","v614","v615","v616","v617","v618","v619",
            "v620","v621","v622","v623","v624","v625","v626","v627","v628",
            "v629","v630","v631","v632","v633","v634","v635","v636","v637",
            "v638","v639","v640","v641","v642","v643","v644","v645","v646",
            "v647","v648","v649","v650","v651","v652","v653","v654","v655",
            "v656","v657","v658","v659","v660","v661","v662","v663","v664",
            "v665","v666","v667","v668","v669","v670","v671","v672","v673",
            "v674","v675","v676","v677","v678","v679","v680","v681","v682",
            "v683","v684","v685","v686","v687","v688","v689","v690","v691",
            "v692","v693","v694","v695","v696","v697","v698","v699","v700",
            "v701","v702","v703","v704","v705","v706","v707","v708","v709",
            "v710","v711","v712","v713","v714","v715","v716","v717","v718",
            "v719","v720","v721","v722","v723","v724","v725","v726","v727",
            "v728","v729","v730","v731","v732","v733","v734","v735","v736",
            "v737","v738","v739","v740","v741","v742","v743","v744","v745",
            "v746","v747","v748","v749","v750","v751","v752","v753","v754",
            "v755","v756","v757","v758","v759","v760","v761","v762","v763",
            "v764","v765","v766","v767","v768","v769","v770","v771","v772",
            "v773","v774","v775","v776","v777","v778","v779","v780","v781",
            "v782","v783","v784","v785","v786","v787","v788","v789","v790",
            "v791","v792","v793","v794","v795","v796","v797","v798","v799",
            "v800","v801","v802","v803","v804","v805","v806","v807","v808",
            "v809","v810","v811","v812","v813","v814","v815","v816","v817",
            "v818","v819","v820","v821","v822","v823","v824","v825","v826",
            "v827","v828","v829","v830","v831","v832","v833","v834","v835",
            "v836","v837","v838","v839","v840","v841","v842","v843","v844",
            "v845","v846","v847","v848","v849","v850","v851","v852","v853",
            "v854","v855","v856","v857","v858","v859","v860","v861","v862",
            "v863","v864","v865","v866","v867","v868","v869","v870","v871",
            "v872","v873","v874","v875","v876","v877","v878","v879","v880",
            "v881","v882","v883","v884","v885","v886","v887","v888","v889",
            "v890","v891","v892","v893","v894","v895","v896","v897","v898",
            "v899","v900","v901","v902","v903","v904","v905","v906","v907",
            "v908","v909","v910","v911","v912","v913","v914","v915","v916",
            "v917","v918","v919","v920","v921","v922","v923","v924","v925",
            "v926","v927","v928","v929","v930","v931","v932","v933","v934",
            "v935","v936","v937","v938","v939","v940","v941","v942","v943",
            "v944","v945","v946","v947","v948","v949","v950","v951","v952",
            "v953","v954","v955","v956","v957","v958","v959","v960","v961",
            "v962","v963","v964","v965","v966","v967","v968","v969","v970",
            "v971","v972","v973","v974","v975","v976","v977","v978","v979",
            "v980","v981","v982","v983","v984","v985","v986","v987","v988",
            "v989","v990","v991","v992","v993","v994","v995","v996","v997",
            "v998","v999","v1000","v1001","v1002","v1003","v1004","v1005",
            "v1006","v1007","v1008","v1009","v1010","v1011","v1012","v1013",
            "v1014","v1015","v1016","v1017","v1018","v1019","v1020","v1021",
            "v1022","v1023","v1024","v1025","v1026","v1027","v1028","v1029",
            "v1030","v1031","v1032","v1033","v1034","v1035","v1036","v1037",
            "v1038","v1039","v1040","v1041","v1042","v1043","v1044","v1045",
            "v1046","v1047","v1048","v1049","v1050","v1051","v1052","v1053",
            "v1054","v1055","v1056","v1057","v1058","v1059","v1060","v1061",
            "v1062","v1063","v1064","v1065","v1066","v1067","v1068","v1069",
            "v1070","v1071","v1072","v1073","v1074","v1075","tr_126_1",
            "tr_151_1","tr_151_2","tr_151_3","tr_151_4","tr_151_5","tr_180_1",
            "tr_200_1","tr_200_2","tr_200_3","tr_200_4","tr_200_5","tr_200_6",
            "tr_200_7","tr_200_8","tr_229_1","tr_229_2","tr_231_1","tr_231_2",
            "tr_388_1","tr_388_2","tr_424_1","tr_424_2","tr_424_3","tr_424_4",
            "tr_424_5","tr_424_6","tr_424_7","tr_424_8","tr_424_9","tr_424_10",
            "tr_424_11","tr_424_12","tr_427_1","tr_427_2","tr_427_3","tr_427_4",
            "tr_427_5","tr_427_6","tr_427_7","tr_427_8","tr_427_9","tr_427_10",
            "tr_427_11","tr_427_12","tr_427_13","tr_427_14","tr_427_15",
            "tr_427_16","tr_427_17","tr_428_1","tr_428_2","tr_428_3","tr_428_4",
            "tr_428_5","tr_536_1","tr_536_2","tr_536_3","tr_538_1","tr_538_2",
            "tr_538_3","tr_539_1","tr_539_2","tr_540_1","tr_540_2","tr_542_1",
            "tr_542_2","tr_589_1","tr_589_2","tr_589_3","tr_589_4","tr_589_5",
            "tr_599_1","tr_599_2","tr_599_3","tr_623_1","tr_623_2"}
          "DAEO variables";
        constant String objective="-v150" "DAEO objective function";
        constant String equalities[m]={"v719 - 1.0*v453 - 1.0*v486 - 1.0*v75",
            "0.02*v811 - 0.02*v205 + 0.02*v820","v1 + v643",
            "- 1.0*v1 - 1.0*v287","v485 + v828","v659 - 1.0*v157",
            "v157 - 1.0*v288","v467 - 1.0*v807 - 1.0*v808 + v873",
            "v606 - 1.0*v227","v227 - 1.0*v228","v228 - 3.0*v284",
            "v232 - 1.0*v231","v629 - 1.0*v223","v630 - 1.0*v224","-1.0*v261",
            "v869 - 1.0*v67","- 1.0*v247 - 1.0*v248 - 1.0*v249",
            "v564 - 1.0*v241","v934 - 1.0*v208",
            "v208 - 1.0*v207 + v209 - 1.0*v1029","- 1.0*v209 - 1.0*v289",
            "v33 - 1.0*v630","v991 - 1.0*v523","v867 - 1.0*v613",
            "v219 - 1.0*v245","v218 - 1.0*v277 + v278",
            "v94 + v217 - 1.0*v218 + v714","- 1.0*v217 - 1.0*v290",
            "v216 - 1.0*v220","v473 - 1.0*v216","- 1.0*v2 - 1.0*v3",
            "v247 - 1.0*v5 - 1.0*v4","v717 - 1.0*v263",
            "v235 - 1.0*v102 + v257 + v258 + v449 - 1.0*v466 - 1.0*v536 - 1.0*v601 - 1.0*v732 - 1.0*v733 + v1002 + v1003",
            "v466 - 1.0*v258 - 1.0*v449 - 1.0*v257 + v536 + v601 + v732 + v733 - 1.0*v1002 - 1.0*v1003",
            "v268 - 1.0*v856 + v891 + v893 + v895 + v1005","v856 - 1.0*v264",
            "v496 + v521 + v591 - 1.0*v1023","v626 - 1.0*v627",
            "v250 + v251 - 1.0*v1049","v844 - 1.0*v1009","v681 - 1.0*v701",
            "v683 - 1.0*v681","v271 - 1.0*v694","v692 - 1.0*v691",
            "v992 - 1.0*v788 - 1.0*v33","v801 - 1.0*v793","v798 - 1.0*v794",
            "v796 - 1.0*v254","v794 - 1.0*v796","v793 - 1.0*v798",
            "v800 - 1.0*v801","v165 - 1.0*v692","- 1.0*v283 - 1.0*v831",
            "-1.0*v830","v945 - 1.0*v960","v858 + v1026",
            "- 1.0*v625 - 1.0*v626","v627 + v628","v625 - 1.0*v795","v261",
            "-1.0*v637","v245 - 1.0*v244","v244 - 1.0*v948","v573 - 1.0*v6",
            "- 1.0*v291 - 1.0*v573","v633 - 1.0*v1028 - 1.0*v1032",
            "v590 - 1.0*v7","- 1.0*v292 - 1.0*v590",
            "v613 - 1.0*v1019 - 1.0*v1021",
            "v223 - 1.0*v628 - 1.0*v717 + v1062 - 1.0*v1065","v224 + v614",
            "v572 - 1.0*v800","v529 - 1.0*v826 - 1.0*v828 + v831",
            "v826 - 1.0*v880","v878 - 1.0*v169",
            "v19 - 1.0*v18 + v20 - 1.0*v508 + v510","v508 - 1.0*v293 - 1.0*v20",
            "v887 - 1.0*v19","v49 - 1.0*v242","v50 - 1.0*v49","-1.0*v586",
            "v103 + v586 - 1.0*v844","v694 - 1.0*v165","v799 - 1.0*v587","v997",
            "v170 - 1.0*v572","v8","-1.0*v577","v795 - 1.0*v647",
            "v577 + v997 - 1.0*v1009","v119 + v127","v275 - 1.0*v821",
            "v845 - 1.0*v857","v857 - 1.0*v852","v843 - 1.0*v907 + v908",
            "- 1.0*v77 - 1.0*v869","v462 - 2.0*v850","v110 - 1.0*v843",
            "v241 - 1.0*v110","v76 + v77",
            "v248 - 1.0*v9 + v249 - 1.0*v608 - 1.0*v609",
            "v472 - 1.0*v496 + v497","v723 - 1.0*v722","v722 - 1.0*v688",
            "v955 - 1.0*v718","v721 - 1.0*v697 - 0.05*v150","v718 - 1.0*v723",
            "v758 - 1.0*v928","v236 - 1.0*v588","v588 - 1.0*v242",
            "v548 - 1.0*v547 - 1.0*v278 + v829","v470 - 1.0*v829",
            "v107 - 1.0*v101",
            "v45 - 1.0*v12 - 1.0*v13 - 1.0*v14 - 1.0*v15 - 1.0*v40 - 1.0*v11 + 4.0*v151 + 5.0*v152 + 5.0*v153 + 6.0*v154 + 6.0*v155 + 7.0*v156 + v279 + v280 + v281 + v282 + v631 + v633 - 1.0*v684 + 2.0*v814 + v1028 + v1032",
            "v952","v976","v21 + v22 + v434","v40 - 1.0*v631 + v666",
            "v21 - 1.0*v34 + v41 - 1.0*v46 + v47 + v69 + v90 + v148 + v172 + v190 + v724 + v847 + v1047",
            "- 1.0*v47 - 1.0*v294","v23 - 1.0*v21","- 1.0*v23 - 1.0*v295",
            "v25 - 1.0*v24 - 1.0*v90 + v264 + v285 + v587 + v990",
            "- 1.0*v25 - 1.0*v296","v31 + v72","v44 - 1.0*v72 - 1.0*v724",
            "v459 - 1.0*v1034","v30 - 1.0*v69 + v99","- 1.0*v30 - 1.0*v297",
            "v32 - 1.0*v31","v39 - 1.0*v100","- 1.0*v37 - 1.0*v298",
            "v37 - 1.0*v99 + v100","v38 - 1.0*v39","- 1.0*v38 - 1.0*v299",
            "-1.0*v42","v42","- 1.0*v41 - 1.0*v44","v938 - 1.0*v190",
            "v631 - 1.0*v152 - 1.0*v153 - 1.0*v154 - 1.0*v155 - 1.0*v156 - 1.0*v151 + v632",
            "v34 + v884",
            "v52 - 1.0*v51 - 1.0*v64 + v74 + v104 + v718 + v890 + v891",
            "- 1.0*v52 - 1.0*v300",
            "v60 - 1.0*v59 - 1.0*v48 + v61 + v73 + v773 - 1.0*v890",
            "- 1.0*v60 - 1.0*v61 - 1.0*v301","v159 - 1.0*v62","v62 - 1.0*v26",
            "v63 + v161","v503 - 1.0*v499","v542 - 1.0*v70","v70 - 3.0*v658",
            "v26 - 1.0*v63","v114 - 1.0*v71",
            "0.02*v841 - 0.02*v656 - 0.02*v654",
            "0.02*v840 - 0.02*v655 - 0.02*v653",
            "0.02*v839 - 0.02*v657 - 0.02*v652",
            "v42 - 1.0*v73 - 1.0*v74 + v102 + v254 + v793 + v794 + 2.0*v1050",
            "v563 - 1.0*v260","v67 - 1.0*v75 + v611",
            "v866 - 1.0*v103 - 1.0*v76",
            "v79 - 1.0*v44 - 1.0*v78 - 1.0*v18 - 1.0*v84 - 1.0*v133 + v511 - 1.0*v515 + v597 + v604 - 1.0*v614 + v647 + v792 - 1.0*v803 - 1.0*v806 - 1.0*v835 + v880 - 1.0*v887 - 1.0*v935 - 1.0*v951 - 1.0*v976 + v979 - 1.0*v1026 + v1049 - 1.0*v1062",
            "- 1.0*v79 - 1.0*v302","v125 - 1.0*v810",
            "v81 - 2.0*v80 + v82 - 1.0*v83 - 1.0*v201 + v206",
            "- 1.0*v206 - 1.0*v303",
            "v86 - 1.0*v82 - 1.0*v84 - 1.0*v85 - 1.0*v81 + v87 - 1.0*v107 + v147 - 0.488*v150 + v997 - 1.0*v1037 - 1.0*v1065",
            "- 1.0*v86 - 1.0*v87 - 1.0*v304","v80 - 1.0*v1046","v35 - 1.0*v629",
            "v93 - 1.0*v92","- 1.0*v93 - 1.0*v305","v92 - 1.0*v91",
            "- 1.0*v94 - 1.0*v972",
            "v695 - 1.0*v57 - 1.0*v101 - 1.0*v102 - 1.0*v254 - 1.0*v42 - 1.0*v793 - 1.0*v794 - 2.0*v1050",
            "v57 - 1.0*v955","v101","v727 - 1.0*v306","v106 - 1.0*v105",
            "-1.0*v108","-1.0*v109","0.02*v655 + 0.02*v656 + 0.02*v657",
            "-1.0*v45","v931 - 1.0*v65","v10 - 1.0*v636",
            "v112 - 1.0*v111 + v113","- 1.0*v112 - 1.0*v113 - 1.0*v307",
            "-1.0*v17","v115 - 1.0*v114 + v116 + v118 - 1.0*v138 - 0.281*v150",
            "- 1.0*v115 - 1.0*v118 - 1.0*v308","v117 - 1.0*v116",
            "v121 - 1.0*v120 + v122 + v123 + v124 - 0.229*v150",
            "- 1.0*v123 - 1.0*v124 - 1.0*v309",
            "v120 - 1.0*v117 - 1.0*v68 - 1.0*v121 - 1.0*v122 - 1.0*v125 - 1.0*v126 - 1.0*v127 - 1.0*v128 - 1.0*v129 - 1.0*v130 - 1.0*v131 - 1.0*v132 - 1.0*v133 + v134 + v135 + v136 + v137 - 0.229*v150 - 1.0*v869",
            "- 1.0*v134 - 1.0*v135 - 1.0*v136 - 1.0*v137 - 1.0*v310",
            "v594 - 1.0*v232 - 1.0*v119","-1.0*v178","- 1.0*v142 - 1.0*v143",
            "v148 - 1.0*v434","v145 + v146 + v147","- 1.0*v145 - 1.0*v146",
            "v149 - 1.0*v148","- 1.0*v149 - 1.0*v311","v58","v126 + v239",
            "-1.0*v159","v160 - 1.0*v161","- 1.0*v160 - 1.0*v312",
            "v162 - 1.0*v126 + v163 - 1.0*v789",
            "v199 + 2.0*v658 - 1.0*v744 - 1.0*v920",
            "0.02*v210 - 0.02*v164 - 0.02*v834 - 0.02*v882","v820 - 2.0*v658",
            "v861 - 1.0*v240","v171 - 1.0*v229","v166 + v167 + v551",
            "- 1.0*v166 - 1.0*v167 - 1.0*v313",
            "v169 - 1.0*v106 - 1.0*v168 - 1.0*v50 - 1.0*v170 - 1.0*v605",
            "-1.0*v171","v173 - 1.0*v172 - 1.0*v43 + v182",
            "- 1.0*v173 - 1.0*v314","v789 - 1.0*v117",
            "v634 - 3.0*v658 - 1.0*v715 - 1.0*v716",
            "0.02*v174 - 0.00012899999999999999140964934696285*v150",
            "v164 - 1.0*v175 + v196 - 1.0*v199 + 3.0*v658 + v692 + v715 + v716 - 1.0*v770 + v779 - 1.0*v820 + v834 + v857 + v882",
            "- 1.0*v176 - 1.0*v315","v1052 - 1.0*v177",
            "v181 - 1.0*v179 - 1.0*v178","- 1.0*v181 - 1.0*v316",
            "v178 + v179 - 1.0*v180","v175 - 1.0*v183 + v184",
            "- 1.0*v184 - 1.0*v317","v179","v180 - 1.0*v179",
            "v185 - 0.126*v150 - 1.0*v210 - 1.0*v634 - 1.0*v694 + v744 - 1.0*v779 - 1.0*v857 - 1.0*v924",
            "-1.0*v186","v188 - 1.0*v187","- 1.0*v188 - 1.0*v318",
            "v190 - 0.087*v150 - 1.0*v189 - 1.0*v147 + v192 - 1.0*v509 - 1.0*v857 - 1.0*v950 - 1.0*v997",
            "- 1.0*v192 - 1.0*v319","v950 - 1.0*v191",
            "v197 - 1.0*v196 - 1.0*v195 + v198 + v770",
            "- 1.0*v197 - 1.0*v198 - 1.0*v320",
            "v204 - 1.0*v202 + v772 - 1.0*v891","- 1.0*v204 - 1.0*v321",
            "v203 - 1.0*v749 + v918","v780 - 1.0*v772 - 1.0*v203",
            "v101 - 1.0*v212","v749 - 0.0247*v150 - 1.0*v780 + v922",
            "v211 - 1.0*v907","v68 - 1.0*v66","v200 - 1.0*v748 + v920",
            "v778 - 1.0*v769 - 1.0*v200",
            "v748 - 1.0*v213 - 0.0254*v150 - 1.0*v778 + v924",
            "v215 - 1.0*v214 + v769","- 1.0*v215 - 1.0*v322",
            "v151 - 1.0*v279 - 1.0*v633","v221 - 1.0*v746 + v919",
            "v776 - 1.0*v774 - 1.0*v221","v222 + v774 + v784 - 1.0*v893",
            "- 1.0*v222 - 1.0*v323",
            "v746 - 0.0254*v150 - 1.0*v776 - 1.0*v784 + v923",
            "v226 - 1.0*v225 + v430 + v528","- 1.0*v226 - 1.0*v324",
            "v225 + v435 + v439 + v463 + v464 + v465 + v466 - 1.0*v699 - 1.0*v905 + v917 + v984 - 1.0*v1010",
            "v6 + v229 - 1.0*v230","v234 - 1.0*v233 + v1006","v764 - 1.0*v235",
            "v259 - 1.0*v236","- 1.0*v237 - 1.0*v238 - 1.0*v239",
            "v260 - 1.0*v259","v7 + v240 - 1.0*v589","v242 - 1.0*v234",
            "v912 - 1.0*v243","v202 + v246 - 1.0*v895","- 1.0*v246 - 1.0*v325",
            "v688 - 1.0*v251 - 1.0*v250","-1.0*v758","v907 - 2.0*v908",
            "v253 - 1.0*v252 + v623","v255 + v257","v256 + v258 - 1.0*v326",
            "- 1.0*v255 - 1.0*v257","- 1.0*v256 - 1.0*v258 - 1.0*v327",
            "v757 - 1.0*v736","v885 - 1.0*v262","v1050 - 1.0*v946",
            "v212 - 1.0*v147","v978 - 1.0*v16","v979 - 1.0*v978",
            "v982 - 1.0*v980 - 1.0*v979","v980 - 1.0*v981",
            "v16 + v266 - 1.0*v745","v461 - 1.0*v982","v981",
            "v782 - 1.0*v771 - 1.0*v266 + v1004 + v1006",
            "v745 - 1.0*v461 - 0.0247*v150 - 1.0*v782",
            "v921 - 1.0*v747 + v1057",
            "v267 + v270 - 1.0*v765 - 1.0*v1006 - 1.0*v1057",
            "v214 - 1.0*v267 - 1.0*v268 + v269 + v765","- 1.0*v269 - 1.0*v328",
            "v213 - 1.0*v270 + v747 + v925",
            "v272 - 1.0*v271 + v273 - 1.0*v817 - 1.0*v997","-1.0*v273",
            "v973 - 1.0*v275 - 1.0*v219 - 1.0*v999","v276","v611 - 1.0*v612",
            "v284","v552 - 1.0*v285","v53 + v286","- 1.0*v286 - 1.0*v329",
            "v451 - 1.0*v450",
            "v436 - 1.0*v430 + v452 + v469 - 1.0*v490 + v665 + v677 - 1.0*v822 + v827 + v932 + v973 + v999",
            "v446 - 1.0*v201 - 6.0*v431 - 7.0*v432 - 8.0*v433 - 1.0*v434 - 0.00001*v150 - 1.0*v873 - 1.0*v965 + v966",
            "v201 + 6.0*v431 + 7.0*v432 + 8.0*v433 + v434 + v873 + v965 - 1.0*v966",
            "v438 - 1.0*v439","v437 - 1.0*v438",
            "v450 - 1.0*v436 - 1.0*v435 + v822","v443 - 1.0*v440 - 1.0*v947",
            "- 1.0*v330 - 1.0*v443","v486 + v487 - 1.0*v871","v906 - 1.0*v446",
            "2.0*v103 + v211 + v250 + v251 - 1.0*v441 - 1.0*v442 - 1.0*v445 + v447 + v453 - 1.0*v487 + v563 + v564 + v788 + v824",
            "- 1.0*v331 - 1.0*v447","v871 - 1.0*v866","v75 + v617",
            "v556 - 1.0*v576 - 1.0*v791 - 1.0*v1040","v444 - 1.0*v1072",
            "- 1.0*v332 - 1.0*v451 - 1.0*v452","v569 + v679","v454",
            "- 1.0*v333 - 1.0*v454","v455 - 1.0*v437","- 1.0*v334 - 1.0*v455",
            "v66 + v67 + v116 - 1.0*v130 + v132 - 1.0*v448 - 1.0*v449 - 1.0*v456 + v457 + v458 + v584 + v965 + v967",
            "- 1.0*v335 - 1.0*v457 - 1.0*v458 - 1.0*v967",
            "v495 - 1.0*v461 - 1.0*v479 - 1.0*v481 - 1.0*v460 - 1.0*v503 + v707 + v708 + v709 - 1.0*v832 + v1044",
            "v220 + v264 - 1.0*v272 + v277 + v430 + v435 - 1.0*v485 - 1.0*v973 + v984 + v998 + v999 + v1010 + v1019 + v1021",
            "v654 - 1.0*v551 + v656","v653 - 1.0*v552 + v655",
            "v652 - 1.0*v554 + v657","-1.0*v555","-1.0*v553",
            "v17 + v444 - 1.0*v470 + v471 + v501 + v578 - 1.0*v827 + v832 + v1012 - 1.0*v1014",
            "- 1.0*v336 - 1.0*v471","v476 - 1.0*v1044",
            "v477 - 1.0*v476 + v482 + v483 + v640",
            "- 1.0*v337 - 1.0*v482 - 1.0*v483","v475 - 1.0*v472",
            "- 1.0*v338 - 1.0*v475","v474 - 1.0*v473","- 1.0*v339 - 1.0*v474",
            "v478 - 1.0*v506","- 1.0*v340 - 1.0*v478","v480 - 1.0*v570",
            "- 1.0*v341 - 1.0*v480","- 1.0*v459 - 1.0*v825",
            "v69 - 1.0*v469 + v484 + v490 + v825","- 1.0*v342 - 1.0*v484",
            "v865 - 1.0*v487 - 1.0*v486","v178 - 1.0*v181","v181 - 1.0*v343",
            "v236 - 1.0*v488",
            "v55 + v68 + v196 + v492 + 2.0*v550 - 1.0*v676 - 1.0*v742 - 1.0*v919 + v931 + v1058",
            "v541 - 1.0*v489","v549","v676 - 1.0*v541","v489 - 1.0*v549",
            "v95 + v96 + v97 + v98 + v460 + v477 + v502 - 1.0*v578 + v640 + v702 + v703 + v704 + v705 + v706 + v1012 + 2.0*v1015 + v1072",
            "2.0*v1016 - 1.0*v493 - 1.0*v501 - 1.0*v502 - 1.0*v344",
            "v2 + v3 + v9 + v494 - 1.0*v548","v493 - 1.0*v345 - 1.0*v494",
            "v498 - 1.0*v497","- 1.0*v346 - 1.0*v498","v500 - 1.0*v569",
            "- 1.0*v347 - 1.0*v500",
            "v504 - 1.0*v106 - 1.0*v121 - 0.25*v150 - 1.0*v163 - 1.0*v185 - 1.0*v490 - 1.0*v50 + v505 - 1.0*v512 - 1.0*v513 - 1.0*v515 - 1.0*v546 - 1.0*v611 - 1.0*v871",
            "- 1.0*v348 - 1.0*v505","v516 - 1.0*v462","v507 - 1.0*v468",
            "v468 - 1.0*v467 + v724 + v803","- 1.0*v514 - 1.0*v1036",
            "- 1.0*v349 - 1.0*v508 - 1.0*v518 - 1.0*v519 - 1.0*v520",
            "v509 - 1.0*v562","v517 - 1.0*v516",
            "v534 - 1.0*v531 - 1.0*v532 - 2.0*v521 + v535 + v536 + v607 - 1.0*v667 + v1045",
            "v491 - 0.582*v150 + v523 - 1.0*v530 + v540 - 1.0*v562 - 1.0*v865 + v990",
            "- 1.0*v350 - 1.0*v540","v522 - 1.0*v88","- 1.0*v351 - 1.0*v522",
            "v142 + v143 + v524 + v525","- 1.0*v352 - 1.0*v524 - 1.0*v525",
            "v526 - 1.0*v464 - 1.0*v465 - 1.0*v466 - 1.0*v463 + v527 + v538 + v551 + v552 + v553 + v554 + v555 - 1.0*v814 - 1.0*v834",
            "- 1.0*v353 - 1.0*v526 - 1.0*v527","v592 - 1.0*v529 + v593 + v1023",
            "v88 + v174 - 1.0*v528 - 1.0*v537 - 1.0*v538 + v554",
            "v537 - 1.0*v354",
            "v488 + v531 + v532 + v533 - 1.0*v534 - 1.0*v535 - 1.0*v536 + v830",
            "- 1.0*v355 - 1.0*v533","v499 - 1.0*v495 - 0.154*v150",
            "v543 - 1.0*v544","v544 - 1.0*v542","v929 - 1.0*v543",
            "v63 - 1.0*v492 - 1.0*v545 + v546 + v557 + v566 - 1.0*v775 + v777",
            "-1.0*v550","v252 - 1.0*v556",
            "v558 - 1.0*v557 + v775 + v785 - 1.0*v892","- 1.0*v356 - 1.0*v558",
            "-1.0*v561","v539 + v559 - 1.0*v560 + 2.0*v561 + v562 - 1.0*v650",
            "v742 - 1.0*v55 - 1.0*v68 - 0.203*v150 - 1.0*v196 - 1.0*v563 - 1.0*v564 - 1.0*v26 - 1.0*v777 - 1.0*v785 - 1.0*v923 - 1.0*v931 - 1.0*v1058",
            "v560 - 1.0*v559","v567 - 1.0*v566 - 1.0*v565 + v568 + v892 + v893",
            "- 1.0*v357 - 1.0*v567 - 1.0*v568",
            "v445 - 1.0*v599 - 1.0*v600 - 1.0*v601",
            "v691 - 1.0*v624 - 1.0*v253",
            "v131 - 2.0*v158 + v816 + v819 + v897 + v956",
            "v256 + v258 - 1.0*v358 - 1.0*v493 - 1.0*v571 - 1.0*v727 + v1001 + v1003 - 1.0*v1016",
            "v189 - 1.0*v190 - 1.0*v970",
            "v574 - 1.0*v76 - 1.0*v163 - 1.0*v187 - 1.0*v27",
            "v73 + v191 - 1.0*v697 + v912",
            "v575 - 1.0*v432 - 1.0*v13 + 0.36*v652 + 0.36*v653 + 0.36*v654 + 0.36*v839 + 0.36*v840 + 0.36*v841",
            "- 1.0*v360 - 1.0*v575",
            "0.07*v652 - 1.0*v14 + 0.07*v653 + 0.07*v654 + 0.07*v839 + 0.07*v840 + 0.07*v841",
            "v14 + v155 - 1.0*v281 - 0.14*v814","v576",
            "v579 - 0.09*v150 + v581 + v582","- 1.0*v361 - 1.0*v581 - 1.0*v582",
            "v597 - 1.0*v580","v580 - 1.0*v579","v589 - 1.0*v583",
            "v230 - 1.0*v584","v585 - 1.0*v1051","v243",
            "- 1.0*v594 - 1.0*v595 - 1.0*v596",
            "- 1.0*v591 - 1.0*v592 - 1.0*v593","v17",
            "v51 - 1.0*v598 + v603 + v894 + v895","- 1.0*v362 - 1.0*v603",
            "v128 + v129 + v130 + v131 - 1.0*v905","v605 - 1.0*v606 - 1.0*v945",
            "v43 - 1.0*v604 - 1.0*v607","v4 + v5 + v608 + v609 + v610",
            "- 1.0*v363 - 1.0*v610","v56","v615 - 1.0*v614 - 0.276*v150 + v616",
            "- 1.0*v364 - 1.0*v615 - 1.0*v616","v612 - 1.0*v597",
            "v545 - 1.0*v68 + v598 - 1.0*v617 - 1.0*v618 + v620 - 1.0*v767",
            "v619 + v1018 - 1.0*v1020 + v1021","- 1.0*v365 - 1.0*v619",
            "v555 + v700","v48 - 1.0*v620 + v621 + v622 + v767 - 1.0*v894",
            "- 1.0*v366 - 1.0*v621 - 1.0*v622",
            "v624 - 1.0*v556 - 1.0*v623 - 1.0*v252 - 5.0*v791 - 8.0*v1040",
            "-1.0*v56","v638 + v639","- 1.0*v367 - 1.0*v638 - 1.0*v639",
            "v279 - 1.0*v280","v716 - 1.0*v281 - 1.0*v279","v281 - 1.0*v282",
            "v636 - 1.0*v635","v635 - 1.0*v634","v715 - 1.0*v716",
            "v274 + v539 - 1.0*v645 - 1.0*v646","- 1.0*v274 - 1.0*v368",
            "v641 + v642 - 1.0*v662 - 1.0*v663 + v664","- 1.0*v369 - 1.0*v664",
            "v439 - 1.0*v641 - 1.0*v642 - 1.0*v643 + v917","v644 - 1.0*v640",
            "- 1.0*v370 - 1.0*v644","v647 - 0.428*v150 + v648 + v649",
            "- 1.0*v371 - 1.0*v648 - 1.0*v649","v650 - 1.0*v539",
            "v280 - 1.0*v658","v282","v983 - 1.0*v715","v651 - 1.0*v983",
            "v1061 - 1.0*v651","v658 - 0.0084*v150",
            "v157 - 0.326*v150 + v207 - 1.0*v659 + v660 + v661",
            "- 1.0*v157 - 1.0*v372 - 1.0*v660 - 1.0*v661",
            "v684 - 5.0*v152 - 5.0*v153 - 6.0*v154 - 6.0*v155 - 7.0*v156 - 1.0*v631 - 1.0*v632 - 1.0*v633 - 1.0*v666 - 4.0*v151",
            "v456 + v667 + v674 + v675 - 1.0*v685 - 1.0*v686 - 1.0*v687 - 1.0*v689 - 1.0*v690",
            "- 1.0*v373 - 1.0*v674 - 1.0*v675","v27 - 1.0*v684","v673",
            "v672 - 1.0*v96 - 1.0*v97 - 1.0*v98 - 1.0*v95 + v702",
            "- 1.0*v374 - 1.0*v672 - 1.0*v673","v98 - 1.0*v706 - 1.0*v709",
            "v97 - 1.0*v98 + v668 - 1.0*v705 + v706 - 1.0*v708 + v709",
            "- 1.0*v375 - 1.0*v668",
            "v96 - 1.0*v97 + v669 - 1.0*v704 + v705 - 1.0*v707 + v708",
            "- 1.0*v376 - 1.0*v669","v670 - 1.0*v95 - 1.0*v702 + v703",
            "- 1.0*v377 - 1.0*v670",
            "v95 - 1.0*v96 + v671 - 1.0*v703 + v704 + v707",
            "- 1.0*v378 - 1.0*v671","- 1.0*v676 - 1.0*v842",
            "v678 - 1.0*v677 + v680 + v842","- 1.0*v379 - 1.0*v678",
            "- 1.0*v380 - 1.0*v680","- 1.0*v679 - 1.0*v714","v693 - 1.0*v477",
            "- 1.0*v381 - 1.0*v693","v696","- 1.0*v382 - 1.0*v696",
            "v697 - 1.0*v695 - 0.146*v150 + v698 + v1049",
            "- 1.0*v383 - 1.0*v698","v720 - 1.0*v719","-1.0*v700",
            "v701 - 1.0*v682",
            "v491 + v530 - 1.0*v717 - 1.0*v720 - 1.0*v721 - 1.0*v1006",
            "v712 - 1.0*v711","v711 - 1.0*v710","v713 - 1.0*v665",
            "- 1.0*v384 - 1.0*v713",
            "v129 + v238 - 1.0*v255 - 1.0*v256 + v442 - 1.0*v448 + v465 + v535 + v600 + v663 + v687 + v728 + v731 - 1.0*v762 - 1.0*v1000 - 1.0*v1001",
            "v102 - 1.0*v129 - 1.0*v238 + v255 + v256 - 1.0*v442 + v448 - 1.0*v465 - 1.0*v535 - 1.0*v600 - 1.0*v663 - 1.0*v687 - 1.0*v728 - 1.0*v731 + v762 + v1000 + v1001",
            "v699 - 1.0*v650","v11 + v152 - 1.0*v280 - 1.0*v282 - 0.04*v814",
            "v953",
            "v520 - 1.0*v739 - 2.0*v740 - 1.0*v741 + v846 + v876 + v942 + v996",
            "v739 - 1.0*v520 - 1.0*v385 + 2.0*v740 + v741 - 1.0*v846 - 1.0*v876 - 1.0*v942 - 1.0*v996",
            "v725 - 1.0*v738 + v756 + v758","- 1.0*v386 - 1.0*v725",
            "- 1.0*v387 - 1.0*v727","v753 + v755 - 1.0*v756",
            "- 1.0*v388 - 1.0*v750","v738 + v752 - 1.0*v757 - 1.0*v758 + v759",
            "v726 - 1.0*v751 - 1.0*v752 - 1.0*v753 + v754",
            "v727 - 1.0*v389 - 1.0*v754 - 1.0*v755",
            "v760 + v761 + v762 - 1.0*v763 - 1.0*v786",
            "v763 - 1.0*v760 - 1.0*v390","v763 - 1.0*v762 - 1.0*v761",
            "- 1.0*v391 - 1.0*v763",
            "v158 - 1.0*v7 - 1.0*v131 - 1.0*v6 - 1.0*v171 - 1.0*v177 - 0.5*v193 - 0.5*v194 - 1.0*v230 - 1.0*v250 - 1.0*v589 + v787 - 0.5*v796 - 0.5*v798 - 0.5*v801 - 1.0*v816 - 1.0*v819 - 1.5*v860 - 1.0*v861 - 1.0*v897 + v956 - 1.0*v976",
            "- 1.0*v392 - 1.0*v787","-2.0*v956",
            "v133 + v172 - 1.0*v182 - 1.0*v683 + v685 + v686 + v687 + v851 - 1.0*v853 + v974",
            "v790 - 1.0*v433","- 1.0*v393 - 1.0*v790",
            "0.5*v652 - 1.0*v15 + 0.5*v653 + 0.5*v654 + 0.5*v839 + 0.5*v840 + 0.5*v841",
            "v791 - 1.0*v572 - 1.0*v235","v15 + v156 - 1.0*v814",
            "v821 - 1.0*v792","v583 + v584 - 1.0*v799",
            "v41 - 1.0*v115 - 1.0*v789 - 1.0*v802 - 1.0*v803 + v804 - 1.0*v886",
            "v115 - 1.0*v394 - 1.0*v804 + v886","- 1.0*v797 - 1.0*v805",
            "v237 + v238 + v805",
            "0.02*v164 + 0.02*v205 - 0.02*v210 - 0.02*v811 + 0.02*v814",
            "v89 - 1.0*v809","v819 - 1.0*v89","v13 + v154 - 0.72*v814",
            "v852 - 1.0*v885","v263 - 1.0*v810","v45 - 1.0*v144 + v813",
            "v65 - 1.0*v813","-0.02*v841","v817 - 1.0*v816 - 1.0*v818 + v900",
            "0.02*v879 - 0.02*v820 - 0.02*v840 - 0.0019350000000000000879851747015437*v150",
            "-1.0*v819",
            "v283 - 1.0*v37 - 1.0*v219 - 1.0*v225 - 1.0*v30 - 1.0*v451 - 1.0*v452 - 1.0*v478 - 1.0*v484 - 1.0*v501 - 1.0*v636 - 1.0*v673 - 1.0*v680 - 1.0*v713 - 1.0*v851 + v853 + v863 - 1.0*v878 - 1.0*v902 - 1.0*v933 - 1.0*v969 - 1.0*v1017 - 1.0*v1033",
            "v864 - 0.0276*v150",
            "0.02*v833 - 0.04*v174 - 0.02*v655 - 0.02*v656 - 0.02*v657 - 0.000464*v150 - 0.02*v839",
            "0.02*v834 - 0.02*v833","v809","v836 - 1.0*v835 - 0.176*v150",
            "- 1.0*v395 - 1.0*v836","v440 - 1.0*v576","v595 - 1.0*v993",
            "v835 + v859","v792 - 1.0*v8 - 1.0*v817",
            "2.0*v454 - 1.0*v396 + 2.0*v471 + v527 + 2.0*v678 - 1.0*v837 - 1.0*v838",
            "-1.0*v107","v810 - 1.0*v845 + v846","- 1.0*v397 - 1.0*v846",
            "v849 - 1.0*v28 + v854","v883 - 1.0*v849","v850 - 4.0*v585",
            "v28 - 1.0*v683 + v710 + v788 - 1.0*v854 - 1.0*v883",
            "v168 - 1.0*v858 - 1.0*v859","v860 - 1.0*v440","v177 - 1.0*v860",
            "v784 + v785","v862 - 1.0*v861","- 1.0*v398 - 1.0*v862",
            "v513 - 1.0*v865","v105 - 1.0*v867","v870 - 1.0*v868",
            "v140 - 1.0*v870","v868 - 1.0*v872","v872 - 1.0*v611",
            "v808 - 0.21*v150 - 1.0*v873 + v874 + v875 + v876",
            "- 1.0*v399 - 1.0*v874 - 1.0*v875 - 1.0*v876",
            "v805 - 1.0*v105 - 1.0*v140 - 1.0*v513 - 1.0*v566 - 1.0*v598 - 1.0*v738 - 1.0*v759 - 1.0*v64 + v877 - 1.0*v1053 - 1.0*v1069",
            "0.02*v882 - 0.02*v879 - 0.000051999999999999996799868867691785*v150",
            "v880 - 1.0*v881",
            "v71 - 0.035*v150 + v802 + v886 - 1.0*v887 + v888 + v889 - 1.0*v955",
            "- 1.0*v400 - 1.0*v886 - 1.0*v888 - 1.0*v889",
            "v83 + v85 - 1.0*v602 - 1.0*v897 + v898","v602 - 1.0*v898",
            "v816 - 1.0*v85 - 1.0*v83 + v897 + v899 - 1.0*v901",
            "v901 - 1.0*v899","v818 - 1.0*v900","- 1.0*v401 - 1.0*v904",
            "v193 - 1.0*v128 + v194 - 1.0*v237 - 1.0*v441 - 1.0*v464 - 1.0*v493 - 1.0*v534 - 1.0*v599 - 1.0*v646 - 1.0*v662 - 1.0*v686 - 1.0*v729 - 1.0*v730 + v761 - 1.0*v847 - 1.0*v966",
            "v128 - 1.0*v193 - 1.0*v194 + v237 + v254 + v441 + v464 + v493 + v534 + v599 + v646 + v662 + v686 + v729 + v730 - 1.0*v761 + v847 + v966",
            "v905 - 1.0*v759","v890 - 1.0*v855 + v892 + v894 + v896 + v903",
            "v104 + v175 + v753 + v755 + v855 - 1.0*v877 + v909 - 1.0*v927 - 1.0*v998",
            "v111 - 1.0*v910","v928 - 1.0*v63","v74 - 1.0*v912",
            "v913 - 1.0*v909","- 1.0*v402 - 1.0*v913","v908 - 1.0*v906",
            "v915 - 1.0*v917","v914 - 1.0*v915","v916 - 1.0*v914",
            "- 1.0*v403 - 1.0*v916",
            "v547 - 1.0*v211 - 1.0*v10 - 1.0*v926 + v927",
            "v910 - 1.0*v911 + v1066","v998 - 1.0*v973 - 1.0*v929",
            "v933 - 1.0*v932","- 1.0*v404 - 1.0*v933","v959 - 1.0*v764",
            "v946 - 1.0*v947","-1.0*v936","v936","v265 - 1.0*v939",
            "- 1.0*v265 - 1.0*v405",
            "v553 - 1.0*v491 - 0.205*v150 + v881 - 1.0*v882 - 1.0*v937 - 1.0*v938 - 1.0*v940 + v941 + v942 - 1.0*v1019 - 1.0*v1020",
            "- 1.0*v406 - 1.0*v941 - 1.0*v942","v937 - 3.0*v284","v947",
            "v949 - 1.0*v878","v948 - 1.0*v949","- 1.0*v934 - 1.0*v935",
            "v935 + v986","v186 + v813 + v970 + v976","v971 - 1.0*v931",
            "- 1.0*v407 - 1.0*v971",
            "v559 - 0.007*v150 - 1.0*v560 - 1.0*v952 - 1.0*v953 + v954 + v955",
            "- 1.0*v408 - 1.0*v954","v806 - 1.0*v945","v969 - 1.0*v444",
            "v138 - 1.0*v930","v960 - 1.0*v959",
            "v130 - 1.0*v173 + v448 + v449 + v583 + v607 + v682 - 1.0*v854 + v934 + v943 + v950 + v957 + v958 + v961 + v962 + v963 - 1.0*v964 - 1.0*v965 - 1.0*v967 - 1.0*v968 - 1.0*v975 + v976",
            "v173 - 1.0*v409 - 1.0*v961 - 1.0*v962 - 1.0*v963 + v964 + v967 + v975",
            "v78 - 1.0*v138 - 0.0000030000000000000000760025722912339*v150 - 1.0*v596 - 1.0*v712 + v854 + v968 - 1.0*v986",
            "v944 - 1.0*v943","v951 - 1.0*v944","v596 - 1.0*v950",
            "v930 - 1.0*v951","- 1.0*v410 - 1.0*v969",
            "v18 - 1.0*v957 - 1.0*v958","v506 - 1.0*v823","v823 - 1.0*v984",
            "v570 + v972","v975 - 1.0*v974","- 1.0*v411 - 1.0*v975",
            "v977 - 1.0*v976","- 1.0*v412 - 1.0*v977","v186",
            "v12 + v153 - 0.1*v814","v231 - 1.0*v986",
            "v75 + v233 + v453 + v486 - 1.0*v491 - 1.0*v530 + v697 + v717",
            "v989 - 1.0*v1007","- 1.0*v413 - 1.0*v989",
            "v1007 - 1.0*v1008 + v1009","v945 - 1.0*v806 + v1008",
            "v993 - 1.0*v990 - 1.0*v991 - 1.0*v992 - 0.241*v150 + v994 + v995 + v996",
            "- 1.0*v414 - 1.0*v994 - 1.0*v995 - 1.0*v996","v1005",
            "v771 + v987 + v988 - 1.0*v1004 - 1.0*v1005",
            "- 1.0*v415 - 1.0*v987 - 1.0*v988","v1000 + v1002",
            "v1001 - 1.0*v416 + v1003","- 1.0*v1000 - 1.0*v1002",
            "- 1.0*v417 - 1.0*v1001 - 1.0*v1003",
            "v813 + v918 + v919 + v920 + v921 + v922 + v923 + v924 + v925 - 1.0*v1011",
            "v1011 - 1.0*v918 - 1.0*v919 - 1.0*v920 - 1.0*v921 - 1.0*v922 - 1.0*v923 - 1.0*v924 - 1.0*v925 - 1.0*v813",
            "v1014 - 1.0*v1013 - 1.0*v1012 + v1017","v1013 - 1.0*v1015",
            "- 1.0*v418 - 1.0*v1016 - 1.0*v1017","v516 - 1.0*v517",
            "v1019 - 1.0*v1018 - 0.054*v150 + v1020 + v1022",
            "- 1.0*v419 - 1.0*v1022","v1024 - 1.0*v186",
            "- 1.0*v420 - 1.0*v1024",
            "0.02*v652 - 1.0*v431 - 1.0*v11 + 0.02*v653 + 0.02*v654 + 0.02*v839 + 0.02*v840 + 0.02*v841 + v1025",
            "- 1.0*v421 - 1.0*v1025",
            "0.05*v652 - 1.0*v12 + 0.05*v653 + 0.05*v654 + 0.05*v839 + 0.05*v840 + 0.05*v841",
            "v1027 - 1.0*v997 - 1.0*v1026 - 0.131*v150",
            "- 1.0*v422 - 1.0*v1027","v1028 - 1.0*v651 - 1.0*v1061",
            "v1032 - 1.0*v1047","v1047 - 1.0*v1028","v1035 - 1.0*v864",
            "v1033 - 1.0*v1038",
            "v1034 - 1.0*v1031 - 1.0*v1032 - 1.0*v1033 - 1.0*v29 - 1.0*v1035",
            "v1031 - 1.0*v1030","v1030 - 1.0*v36","v812 - 1.0*v1035",
            "v1037 - 1.0*v1036","v1036 - 1.0*v1029","v1038 - 1.0*v1037",
            "v276 + v864 - 1.0*v1039 + v1040","v1039 - 1.0*v812 - 1.0*v29",
            "v36 + v651 + 2.0*v658 - 1.0*v743 - 1.0*v921 + v1014 + v1035 + v1048",
            "v479 - 0.003*v150 + v481 - 2.0*v658 - 1.0*v1014 - 1.0*v1041 - 1.0*v1043 - 1.0*v1044",
            "v1041 - 1.0*v1042 + v1044","v1042","v1043","v1029 - 1.0*v1046",
            "v1046 - 1.0*v812",
            "v29 - 1.0*v768 + v783 + v797 + v812 - 1.0*v1048 + v1053 + v1058 + v1061",
            "v29 - 1.0*v36","v36 - 1.0*v16","v16 - 1.0*v276",
            "v1051 - 1.0*v1050 - 1.0*v1052",
            "v183 + v268 + v903 - 1.0*v1053 + v1054 + v1055",
            "- 1.0*v423 - 1.0*v1054 - 1.0*v1055","v91 - 1.0*v1045",
            "v71 + v91 + v1056","- 1.0*v424 - 1.0*v1056",
            "v195 + v768 - 1.0*v903 - 1.0*v1058 + v1059 + v1060",
            "- 1.0*v425 - 1.0*v1059 - 1.0*v1060",
            "v743 - 1.0*v185 - 1.0*v479 - 1.0*v481 - 0.136*v150 - 1.0*v783 - 1.0*v925 - 1.0*v1034",
            "v1063 - 1.0*v1062 - 0.402*v150 + v1064 + v1065",
            "- 1.0*v426 - 1.0*v1063 - 1.0*v1064",
            "v565 + v896 + v1067 + v1068 - 1.0*v1069",
            "- 1.0*v427 - 1.0*v1067 - 1.0*v1068",
            "v618 - 1.0*v546 - 1.0*v766 + v1069","v766 - 1.0*v896 + v1070",
            "- 1.0*v428 - 1.0*v1070",
            "v911 + v926 - 1.0*v998 - 1.0*v999 + v1073","v637 - 1.0*v1066",
            "v1074 - 1.0*v1071 + v1075","- 1.0*v429 - 1.0*v1074 - 1.0*v1075",
            "v1071 - 1.0*v1073","v139 - 38/5",
            "v24 - 1.0*v21 - 2.0*v22 - tr_126_1 - 1.0*v27 - 1.0*v32 - 1.0*v40 + v46 - 1.0*v53 - 1.0*v148 - 0.00005*v150 - 1.0*v182 + 7.0*v431 + 8.0*v432 + 9.0*v433 - 1.0*v459 + v523 - 1.0*v628 - 1.0*v632 - 1.0*v667 + v815 + v824 - 1.0*v884 - 1.0*v938 - 1.0*v952 - 1.0*v953 - 1.0*v978",
            "tr_126_1",
            "v27 - tr_151_1 + v28 + v31 + v34 + 2.0*v54 + v55 + v56 + v59 + v62 + v65 + v76 + v80 + v86 + v100 + 2.0*v108 + v109 + v112 + v118 + v123 + v127 + v134 + v139 - 1.0*v141 + 45.5608*v150 + v160 + v162 + 2.0*v163 + v165 + v166 + v185 + v192 + v199 + v200 + v203 + v205",
            "v209 - tr_151_2 + v212 + v216 + v218 + v221 + v234 + v262 + v266 + v267 + v273 + v438 + v443 + v450 + v476 + v482 + v487 + v492 + v499 + v504 + v505 + v507 + v509 + v518 + v524 + v526 + v529 + v538 + v543 + v548 + v557 + v560 + v562 + v577 + v578 + v581",
            "v586 - tr_151_3 + v595 + v615 + v620 + v638 + v648 + 3.0*v658 + v660 + v668 + v669 + v670 + v671 + v672 + v696 + v698 + v723 + v734 + v738 + v742 + v743 + v744 + v745 + v746 + v747 + v748 + v749 + v804 + v822 + v823 + v828 + v837 + v844 + v845 - 1.0*v849 + v853",
            "v865 - tr_151_4 + v866 + v869 + v871 + v874 + v888 + v898 + v899 + v900 - 1.0*v902 + v906 + v909 + v910 + v913 + v915 - 1.0*v918 + v949 + v954 + v961 + v968 + v971 + v977 + v983 + v989 + v994 + v1004 + v1007 + v1008 + v1024 + v1029 + v1036 + v1037 + v1046",
            "v1048 - tr_151_5 + v1057 + v1063 + v1073 + v1074",
            "tr_151_1 + tr_151_2 + tr_151_3 + tr_151_4 + tr_151_5",
            "v11 - tr_180_1 + v12 + v13 + v14 + v15 + v46 - 1.0*v54 - 1.0*v55 - 1.0*v56 + v59 + v64 + v66 - 1.0*v104 + v117 + v121 + v122 + v144 - 0.001*v150 + 6.0*v284 + v431 + v432 + v433 + v517 + v546 + v588 + v726 + v736 - 1.0*v773 + v781 + v809 + v810 + v863 + v877 + v936 + v959 + v997",
            "tr_180_1",
            "v109 - 1.0*v11 - 1.0*v12 - 1.0*v13 - 1.0*v14 - 1.0*v15 - 1.0*v27 - 1.0*v28 - 1.0*v31 - 1.0*v34 - 1.0*v46 - 1.0*v54 - 1.0*v58 - 1.0*v59 - 1.0*v62 - 1.0*v65 - 1.0*v76 - 1.0*v80 - 1.0*v86 - 1.0*v100 - tr_200_1 - 1.0*v112 - 1.0*v117 - 1.0*v118 - 1.0*v121 - 1.0*v122",
            "v141 - 1.0*v123 - 1.0*v127 - 1.0*v134 - 1.0*v139 - 1.0*v140 - tr_200_2 - 45.73179999999999978399500832893*v150 - 1.0*v159 - 1.0*v160 - 1.0*v161 - 1.0*v162 - 2.0*v163 - 1.0*v165 - 1.0*v166 - 1.0*v185 - 1.0*v192 - 1.0*v199 - 1.0*v200 - 1.0*v203 - 1.0*v205 - 1.0*v209",
            "- tr_200_3 - 1.0*v212 - 1.0*v216 - 1.0*v218 - 1.0*v221 - 1.0*v228 - 1.0*v234 - 1.0*v262 - 1.0*v266 - 1.0*v267 - 1.0*v273 - 1.0*v431 - 1.0*v432 - 1.0*v433 - 1.0*v438 - 1.0*v443 - 1.0*v446 - 1.0*v450 - 1.0*v476 - 1.0*v482 - 1.0*v487 - 1.0*v492 - 1.0*v503",
            "- tr_200_4 - 1.0*v504 - 1.0*v505 - 1.0*v507 - 1.0*v509 - 1.0*v517 - 1.0*v518 - 1.0*v524 - 1.0*v526 - 1.0*v529 - 1.0*v538 - 1.0*v542 - 1.0*v543 - 1.0*v546 - 1.0*v548 - 1.0*v557 - 1.0*v560 - 1.0*v562 - 1.0*v577 - 1.0*v578 - 1.0*v581 - 1.0*v586 - 1.0*v588 - 1.0*v595",
            "- tr_200_5 - 1.0*v615 - 1.0*v620 - 1.0*v638 - 1.0*v648 - 1.0*v660 - 1.0*v668 - 1.0*v669 - 1.0*v670 - 1.0*v671 - 1.0*v672 - 1.0*v695 - 1.0*v696 - 1.0*v698 - 1.0*v723 - 1.0*v734 - 1.0*v736 - 1.0*v738 - 1.0*v742 - 1.0*v743 - 1.0*v744 - 1.0*v745 - 1.0*v746 - 1.0*v747",
            "v849 - 1.0*v748 - 1.0*v749 - 1.0*v751 - 1.0*v757 - 1.0*v781 - 1.0*v804 - 1.0*v809 - 1.0*v810 - 1.0*v822 - 1.0*v823 - 1.0*v828 - 1.0*v837 - 1.0*v844 - 1.0*v845 - tr_200_6 - 1.0*v853 - 1.0*v863 - 1.0*v865 - 1.0*v866 - 1.0*v869 - 1.0*v871 - 1.0*v874 - 1.0*v877",
            "v902 - 1.0*v885 - 1.0*v888 - 1.0*v898 - 1.0*v899 - 1.0*v900 - tr_200_7 - 1.0*v906 - 1.0*v909 - 1.0*v910 - 1.0*v913 - 1.0*v915 - 1.0*v922 - 1.0*v931 - 1.0*v936 - 1.0*v937 - 1.0*v949 - 1.0*v954 - 1.0*v959 - 1.0*v961 - 1.0*v968 - 1.0*v971 - 1.0*v977 - 1.0*v983",
            "- tr_200_8 - 1.0*v989 - 1.0*v994 - 1.0*v997 - 1.0*v1004 - 1.0*v1007 - 1.0*v1008 - 1.0*v1024 - 1.0*v1029 - 1.0*v1036 - 1.0*v1037 - 1.0*v1046 - 1.0*v1048 - 1.0*v1057 - 1.0*v1063 - 1.0*v1073 - 1.0*v1074",
            "tr_200_1 + tr_200_2 + tr_200_3 + tr_200_4 + tr_200_5 + tr_200_6 + tr_200_7 + tr_200_8",
            "v33 - tr_229_1 + v35 + v57 + v78 + v107 + v114 + v125 + 4.0*v151 + 5.0*v152 + 5.0*v153 + 6.0*v154 + 6.0*v155 + 7.0*v156 - 1.0*v162 + v176 + 2.0*v177 + 2.0*v187 + v207 - 1.0*v212 + v235 + v272 + v441 + v442 + v445 + v510 + v521 + v530 + v547 - 1.0*v574 + v604 + v613",
            "v631 - tr_229_2 + v632 + v633 + v637 + v659 + v666 + v689 + v690 + v710 + v759 + v795 + v797 + v800 + v802 + v806 + v815 + v817 + v847 - 1.0*v851 + v852 + v853 + v858 + v859 + v879 + v930 + v976 + v997 + v1045 + 4.0*v1052",
            "tr_229_1 + tr_229_2",
            "v22 - tr_231_1 - 1.0*v24 - 1.0*v28 + v32 + v40 - 1.0*v45 - 1.0*v46 + v53 - 1.0*v78 + v107 + v138 - 0.0000060000000000000001520051445824677*v150 + v182 + v262 - 7.0*v431 - 8.0*v432 - 9.0*v433 + v459 - 1.0*v523 + v596 + v628 + v632 + v667 + v683 + v684 + v764 - 1.0*v788",
            "v883 - 1.0*v809 - 1.0*v815 - 1.0*v824 - tr_231_2 + v884 + v938 + v952 + v953 - 1.0*v959 - 1.0*v968 + v978 + v986",
            "tr_231_1 + tr_231_2",
            "v18 - tr_388_1 - 1.0*v32 + v44 + v50 + v84 + v106 + v121 + v133 - 0.25*v150 + v163 + v185 - 1.0*v234 + v490 - 1.0*v504 - 1.0*v507 + v508 - 1.0*v509 - 1.0*v510 - 1.0*v511 + v512 + v513 + v514 + 2.0*v515 - 1.0*v517 + v518 + v519 + v520 + v546 - 1.0*v597 + v611 + v614",
            "v803 - 1.0*v647 - 1.0*v792 - tr_388_2 + v807 + v835 + v871 - 1.0*v880 + v887 + v935 + v943 + v951 - 1.0*v979 + v1026 - 1.0*v1049 + v1062",
            "tr_388_1 + tr_388_2",
            "v6 - tr_424_1 + v7 - 1.0*v8 - 1.0*v17 - 1.0*v19 - 1.0*v41 - 1.0*v48 - 1.0*v51 - 1.0*v69 - 1.0*v71 - 1.0*v73 - 1.0*v74 - 1.0*v86 - 1.0*v89 - 1.0*v90 - 1.0*v91 - 1.0*v92 + v94 - 1.0*v103 - 1.0*v104 - 1.0*v108 - 1.0*v109 - 1.0*v112 - 1.0*v118 - 1.0*v120 - 1.0*v121",
            "v141 - 1.0*v123 - 1.0*v134 - 1.0*v139 - tr_424_2 - 1.0*v142 - 1.0*v143 - 1.0*v144 + v145 + v146 - 45.5608*v150 + 5.0*v151 + 6.0*v152 + 6.0*v153 + 7.0*v154 + 7.0*v155 + 8.0*v156 + 2.0*v158 - 1.0*v159 - 1.0*v160 - 1.0*v161 - 1.0*v163 - 1.0*v164 - 1.0*v166 - 1.0*v175",
            "2.0*v177 - tr_424_3 + v180 - 1.0*v182 - 1.0*v183 - 1.0*v185 - 1.0*v189 - 1.0*v191 - 1.0*v192 + v193 + v194 - 1.0*v195 - 1.0*v201 - 1.0*v202 - 1.0*v209 - 1.0*v213 - 1.0*v214 - 1.0*v219 + v223 + v224 + 2.0*v232 - 1.0*v239 - 1.0*v241 + v243 + v244 - 1.0*v250",
            "v253 - 3.0*v251 - tr_424_4 + v255 + v257 - 1.0*v259 - 1.0*v260 - 1.0*v270 - 1.0*v275 + v278 + v283 - 6.0*v431 - 7.0*v432 - 8.0*v433 - 1.0*v434 - 1.0*v436 - 1.0*v443 - 1.0*v444 - 1.0*v453 - 1.0*v456 - 1.0*v460 + v467 - 1.0*v469 + v472 + v473 - 1.0*v477 - 1.0*v482",
            "v491 - 1.0*v488 - tr_424_5 + v497 - 1.0*v505 - 1.0*v511 - 1.0*v512 - 1.0*v513 - 1.0*v518 - 1.0*v524 - 1.0*v526 - 1.0*v539 + v541 - 1.0*v544 - 1.0*v546 - 1.0*v550 - 1.0*v551 - 1.0*v552 - 1.0*v553 - 1.0*v554 - 1.0*v555 - 1.0*v559 - 1.0*v563 - 3.0*v564",
            "v571 - 1.0*v565 - tr_424_6 - 1.0*v574 - 1.0*v576 - 1.0*v579 - 1.0*v580 - 1.0*v581 - 1.0*v583 - 1.0*v584 - 1.0*v585 - 1.0*v602 - 1.0*v606 + v612 + v613 - 1.0*v615 - 1.0*v617 - 1.0*v618 + v624 + v626 - 1.0*v627 - 1.0*v628 - 1.0*v635 - 1.0*v636 - 1.0*v638 - 1.0*v640",
            "v681 - 1.0*v641 - 1.0*v642 - 1.0*v648 - 1.0*v652 - 1.0*v653 - 1.0*v654 - 1.0*v660 - 1.0*v667 - 1.0*v668 - 1.0*v669 - 1.0*v670 - 1.0*v671 - 1.0*v672 - tr_424_7 - 1.0*v683 + v688 + v691 - 1.0*v695 - 1.0*v696 - 1.0*v698 - 1.0*v700 - 1.0*v701 - 1.0*v702 - 1.0*v703",
            "v714 - 1.0*v704 - 1.0*v705 - 1.0*v706 - tr_424_8 - 1.0*v717 - 1.0*v718 - 1.0*v719 - 1.0*v724 - 1.0*v726 - 1.0*v735 - 1.0*v738 - 1.0*v752 - 1.0*v753 - 1.0*v755 - 1.0*v756 + v761 + v762 - 1.0*v765 - 1.0*v766 - 1.0*v767 - 1.0*v768 - 1.0*v769 - 1.0*v770 - 1.0*v771",
            "2.0*v786 - 1.0*v772 - 1.0*v773 - 1.0*v774 - 1.0*v775 - 1.0*v776 - 1.0*v777 - 1.0*v778 - 1.0*v779 - 1.0*v780 - 1.0*v781 - 1.0*v782 - 1.0*v783 - 1.0*v784 - 1.0*v785 - tr_424_9 - 1.0*v799 - 1.0*v804 - 2.0*v807 - 1.0*v811 + 2.0*v817 - 1.0*v818 - 1.0*v819",
            "2.0*v850 - 1.0*v829 - 1.0*v830 - 1.0*v833 - 1.0*v837 - 1.0*v839 - 1.0*v840 - 1.0*v841 - 1.0*v843 - 1.0*v847 - 1.0*v848 - tr_424_10 - 1.0*v851 + v859 + 3.0*v860 - 1.0*v863 - 1.0*v868 - 1.0*v870 - 1.0*v871 - 1.0*v874 - 1.0*v881 - 1.0*v888 - 1.0*v897 - 1.0*v901",
            "2.0*v905 - tr_424_11 + 2.0*v907 - 1.0*v913 + v918 + v919 + v920 + v921 + v922 + v923 + v924 + v925 - 1.0*v928 - 2.0*v930 - 1.0*v931 - 1.0*v934 - 1.0*v936 - 1.0*v943 - 1.0*v944 - 1.0*v954 - 1.0*v957 - 1.0*v958 + v960 - 1.0*v961 - 3.0*v970 - 1.0*v971 + v974 - 1.0*v977",
            "v982 - tr_424_12 - 1.0*v986 - 1.0*v989 - 1.0*v993 - 1.0*v994 + v997 + v1000 + v1002 - 1.0*v1012 - 1.0*v1013 - 1.0*v1015 - 1.0*v1018 + v1019 + v1020 - 1.0*v1024 - 1.0*v1030 - 1.0*v1039 - 1.0*v1043 - 1.0*v1045 - 1.0*v1047 + v1051 - 1.0*v1061 - 1.0*v1063 - 1.0*v1074",
            "tr_424_1 + tr_424_2 + tr_424_3 + tr_424_4 + tr_424_5 + tr_424_6 + tr_424_7 + tr_424_8 + tr_424_9 + tr_424_10 + tr_424_11 + tr_424_12",
            "v16 - 1.0*v2 - 1.0*v3 - 1.0*v4 - 1.0*v5 - 1.0*v6 - 1.0*v7 - 1.0*v9 - tr_427_1 + 2.0*v19 + v20 + v23 + v24 - 1.0*v26 + v27 + v32 - 1.0*v33 - 1.0*v35 + v36 + v38 + v45 + v47 - 1.0*v48 + v49 - 1.0*v51 + v52 - 2.0*v53 - 1.0*v57 + v59 + v60 + v61 + v62 + v63 + v65",
            "2.0*v68 - tr_427_2 + v72 + v76 + v79 + v80 + v86 + v87 - 1.0*v88 + 2.0*v89 + 2.0*v90 + v92 + v93 + v100 + v102 + 4.0*v103 + v106 - 1.0*v107 + 2.0*v108 + 2.0*v109 - 1.0*v110 + v112 + v113 - 1.0*v114 + v117 + v118 + v119 + v121 + v122 + v123 + v124 - 1.0*v125 + v126",
            "v134 - tr_427_3 + v135 + 2.0*v136 + 3.0*v137 + v138 + v139 + 3.0*v141 + 2.0*v142 + 2.0*v143 - 1.0*v145 - 1.0*v146 + 2.0*v147 + v149 + 45.560349999999999681676854379475*v150 - 14.0*v151 - 17.0*v152 - 16.0*v153 - 20.0*v154 - 19.0*v155 - 22.0*v156 + v157 + v160",
            "2.0*v162 - tr_427_4 + 2.0*v163 + 2.0*v164 + v165 + v166 + v167 - 1.0*v171 - 2.0*v177 + v182 - 1.0*v183 + v184 + 2.0*v185 + v186 - 3.0*v187 + v188 + v190 + v192 - 2.0*v193 - 2.5*v194 - 1.0*v195 + v196 + v197 + v198 - 1.0*v202 + v204 + v205 + v206 - 1.0*v207",
            "v209 - tr_427_5 - 1.0*v210 + v211 + 3.0*v212 - 1.0*v213 - 1.0*v214 + v215 + v216 + v217 + v218 + v222 + v227 + v229 - 1.0*v231 + v232 - 1.0*v233 + v235 + v239 + v240 - 1.0*v241 + v242 + v246 - 1.0*v247 - 1.0*v248 - 1.0*v249 + 2.0*v250 + 6.0*v251 - 1.0*v253",
            "v254 - tr_427_6 + v260 - 1.0*v261 + v262 - 1.0*v263 + v265 + v267 + v269 + v270 - 1.0*v271 - 1.0*v272 + v273 + v274 + 2.0*v275 + v276 + 6.0*v284 + v286 + 6.0*v431 + 7.0*v432 + 8.0*v433 + v434 + v438 + 2.0*v440 - 3.0*v441 - 3.0*v442 + v443 - 1.0*v445 - 1.0*v446",
            "v450 - tr_427_7 + v453 + v455 + 2.0*v457 + 3.0*v458 + v459 - 1.0*v461 + v463 + v467 - 1.0*v468 + v470 + v474 + v475 + v476 - 1.0*v479 + v480 - 1.0*v481 + v482 + v483 + v485 + v486 + v487 + 2.0*v488 + v494 + v498 + v499 + v500 + v502 - 1.0*v503 + v504 + v505",
            "v506 - tr_427_8 + v509 - 1.0*v510 + v511 - 1.0*v515 - 1.0*v516 + v518 + v519 - 1.0*v521 + v524 + v525 + v526 + v528 + v529 - 1.0*v531 - 1.0*v532 + v533 + v538 + v539 + v540 - 1.0*v542 + v543 - 2.0*v545 + 2.0*v546 + v548 - 1.0*v549 + 2.0*v550 + v551 + v552",
            "v553 - tr_427_9 + v554 + v555 + v557 + v558 + v560 - 1.0*v561 + v562 + 2.0*v564 - 1.0*v565 + v568 + v573 + v574 + v575 + v577 + v578 + 3.0*v579 + v581 + v582 + 2.0*v583 + 2.0*v584 + v586 + v588 + v590 - 1.0*v592 - 1.0*v593 + v594 + v595 - 2.0*v599 - 2.0*v600",
            "v610 - 2.0*v601 - 1.0*v608 - 1.0*v609 - tr_427_10 + v611 - 1.0*v613 + v615 + v616 + v618 + v619 + v620 + v621 + v622 - 1.0*v624 + v625 + v628 - 1.0*v629 - 1.0*v630 - 1.0*v631 - 1.0*v632 - 2.0*v633 - 1.0*v637 + v638 + v639 + 2.0*v641 + 2.0*v642 - 1.0*v643 + v644",
            "v645 - tr_427_11 + v648 + v649 + v651 + v652 + v653 + v654 + 10.0*v658 - 1.0*v659 + v660 + v661 + v664 + v665 - 1.0*v666 + v667 + v668 + v669 + v670 + v671 + v672 + 2.0*v674 + 3.0*v675 - 1.0*v676 + v679 + v683 + v685 - 1.0*v691 + v693 - 1.0*v694 + v696 + v698",
            "v715 - 1.0*v710 - tr_427_12 + v716 + v720 - 1.0*v721 + v723 + 2.0*v726 - 1.0*v728 - 1.0*v729 - 4.5*v730 - 3.0*v731 - 3.8*v732 - 1.0*v733 + v734 + v736 + v739 + 3.0*v740 + 2.0*v741 - 1.0*v751 + v753 + v755 - 1.0*v757 + v758 - 2.0*v759 + v760 - 2.0*v761 - 2.0*v762",
            "v776 - tr_427_13 + v777 + v778 + v779 + v780 + v781 + v782 + v783 - 5.0*v786 + v789 + v790 + v793 + v794 - 1.0*v795 - 1.0*v797 - 1.0*v800 - 1.0*v802 + v804 - 1.0*v806 + v807 - 2.0*v808 + v810 + 2.0*v813 + v817 - 1.0*v820 + v821 + v822 + v823 + v826 + v829",
            "v834 - tr_427_14 + v836 + v837 + v838 + v839 + v840 + v841 + v845 + v848 + v850 + v851 - 1.0*v852 + v857 - 1.0*v859 - 1.0*v861 + v862 + 2.0*v863 + v864 + v865 + 2.0*v866 + v869 + v870 + v871 + v873 + v874 + v875 + v877 - 1.0*v879 + v882 - 1.0*v885 + v888 + v889",
            "v898 - tr_427_15 + v899 + v900 - 1.0*v902 + v904 + v905 + v906 + v909 + v910 + v913 + v915 + v916 - 2.0*v930 + v932 - 1.0*v937 + v941 + 2.0*v944 + v946 + 3.0*v947 - 1.0*v948 + v949 + v950 + v952 + v953 + v954 + v955 - 2.0*v956 + 2.0*v957 + 2.0*v958 + v961 + 2.0*v962",
            "3.0*v963 - tr_427_16 - 1.0*v964 + 5.0*v970 + v971 + v972 + v976 + v977 + v978 - 1.0*v981 + v983 + 2.0*v985 + v987 + v988 + v989 + v991 + v994 + v995 + v997 - 1.0*v1000 - 1.0*v1002 + v1004 + v1007 - 1.0*v1009 - 1.0*v1011 + v1014 + v1022 - 1.0*v1023 + v1024",
            "v1025 - tr_427_17 + v1027 + v1028 + v1029 + 3.0*v1030 - 1.0*v1034 + v1035 + v1036 + v1037 - 1.0*v1038 + v1039 + 3.0*v1043 - 2.0*v1045 + v1046 + v1050 - 4.0*v1052 + v1054 + v1055 + v1058 + v1059 + v1060 + 2.0*v1061 + v1063 + v1064 + v1068 + v1070 + v1073 + v1074 + v1075",
            "tr_427_1 + tr_427_2 + tr_427_3 + tr_427_4 + tr_427_5 + tr_427_6 + tr_427_7 + tr_427_8 + tr_427_9 + tr_427_10 + tr_427_11 + tr_427_12 + tr_427_13 + tr_427_14 + tr_427_15 + tr_427_16 + tr_427_17",
            "2.0*v193 - 1.0*v20 - 1.0*v23 - 1.0*v38 - 1.0*v47 - 1.0*v52 - 1.0*v60 - 1.0*v61 - 1.0*v79 - 1.0*v87 - 1.0*v93 - 1.0*v113 - 1.0*v124 - 1.0*v135 - 2.0*v136 - 3.0*v137 - 4.0*v141 - 1.0*v149 - 1.0*v157 - 1.0*v167 - 1.0*v184 - 1.0*v188 - tr_428_1 + 2.5*v194 - 1.0*v197",
            "2.0*v441 - 1.0*v198 - 1.0*v204 - 1.0*v206 - 1.0*v215 - 1.0*v217 - 1.0*v222 - 1.0*v246 - 1.0*v265 - 1.0*v269 - 1.0*v274 - 1.0*v286 - 1.0*v359 - tr_428_2 + 2.0*v442 - 1.0*v455 - 2.0*v457 - 3.0*v458 - 1.0*v474 - 1.0*v475 - 1.0*v480 - 1.0*v483 + v493 - 1.0*v494",
            "2.0*v599 - 1.0*v498 - 1.0*v500 - 1.0*v502 - 1.0*v519 - 1.0*v525 - 1.0*v533 - 1.0*v540 - 1.0*v558 - 1.0*v568 - 1.0*v573 - 1.0*v575 - 1.0*v582 - 1.0*v590 - tr_428_3 + 2.0*v600 + 2.0*v601 - 1.0*v610 - 1.0*v616 - 1.0*v619 - 1.0*v621 - 1.0*v622 - 1.0*v639 - 1.0*v644",
            "2.0*v727 - 1.0*v649 - 1.0*v661 - 1.0*v664 - 2.0*v674 - 3.0*v675 - 1.0*v693 - tr_428_4 + 3.5*v730 + 2.0*v731 + 2.8*v732 - 1.0*v739 - 3.0*v740 - 2.0*v741 - 1.0*v760 + 2.0*v761 + 2.0*v762 - 1.0*v790 - 1.0*v836 - 1.0*v838 - 1.0*v862 - 1.0*v875 - 1.0*v889 - 1.0*v904",
            "v964 - 1.0*v916 - 1.0*v941 - 2.0*v962 - 3.0*v963 - tr_428_5 - 2.0*v985 - 1.0*v987 - 1.0*v988 - 1.0*v995 - 1.0*v1001 - 1.0*v1003 - 1.0*v1022 - 1.0*v1025 - 1.0*v1027 - 1.0*v1054 - 1.0*v1055 - 1.0*v1059 - 1.0*v1060 - 1.0*v1064 - 1.0*v1068 - 1.0*v1070 - 1.0*v1075",
            "tr_428_1 + tr_428_2 + tr_428_3 + tr_428_4 + tr_428_5",
            "v2 - tr_536_1 + v4 + v6 + v7 - 1.0*v19 - 1.0*v24 + 2.0*v53 - 1.0*v78 + v88 - 1.0*v89 - 1.0*v90 - 1.0*v142 + v145 - 0.00215*v150 + v171 - 1.0*v227 - 1.0*v229 - 1.0*v240 + v248 + v253 + v261 - 1.0*v275 - 6.0*v431 - 7.0*v432 - 8.0*v433 - 1.0*v434 - 1.0*v485 - 1.0*v488",
            "v531 - 1.0*v506 - 1.0*v528 - 1.0*v530 - tr_536_2 - 2.0*v579 + v592 + v608 - 1.0*v618 + v624 - 1.0*v625 - 1.0*v641 - 1.0*v642 + v643 - 1.0*v645 - 1.0*v665 - 1.0*v679 - 1.0*v685 - 1.0*v689 + v721 - 1.0*v726 + v728 + v729 + v730 + v731 + v732 + v733 - 1.0*v734",
            "v735 - tr_536_3 + v736 - 1.0*v737 + v751 + 3.0*v786 - 1.0*v807 - 1.0*v815 - 1.0*v817 - 1.0*v821 - 1.0*v826 - 1.0*v858 + v861 - 1.0*v932 - 1.0*v944 - 1.0*v946 - 1.0*v957 - 1.0*v972 + v985 - 1.0*v991 + v1023 - 2.0*v1030 - 2.0*v1043",
            "tr_536_1 + tr_536_2 + tr_536_3",
            "v19 - 1.0*v2 - 1.0*v4 - 1.0*v6 - 1.0*v7 - tr_538_1 + v24 - 2.0*v53 + v78 - 1.0*v88 + v89 + v90 + v142 - 1.0*v145 - 0.00005*v150 - 1.0*v171 + v227 + v229 + v240 - 1.0*v248 - 1.0*v253 - 1.0*v261 + v275 + 6.0*v431 + 7.0*v432 + 8.0*v433 + v434 + v485 + v488 + v506",
            "v528 - tr_538_2 + v530 - 1.0*v531 + 2.0*v579 - 1.0*v592 - 1.0*v608 + v618 - 1.0*v624 + v625 + v641 + v642 - 1.0*v643 + v645 + v665 + v679 + v685 + v689 - 1.0*v721 - 1.0*v728 - 1.0*v729 - 1.0*v730 - 1.0*v731 - 1.0*v732 - 1.0*v733 + v737 - 3.0*v786 + v807 + v815",
            "v817 - tr_538_3 + v821 + v826 + v858 - 1.0*v861 + v932 + v944 + v946 + v957 + v972 - 1.0*v985 + v991 - 1.0*v1023 + 2.0*v1030 + 2.0*v1043",
            "tr_538_1 + tr_538_2 + tr_538_3",
            "v3 - tr_539_1 + v5 + v9 - 1.0*v72 + v110 - 1.0*v119 - 1.0*v143 + v146 - 0.00013*v150 + 10.0*v151 + 12.0*v152 + 11.0*v153 + 14.0*v154 + 13.0*v155 + 15.0*v156 + v231 + v233 + v247 + v249 + v263 + v271 - 1.0*v463 + v468 - 1.0*v470 - 1.0*v511 + v515 + v516 + v532",
            "v545 - tr_539_2 - 1.0*v547 + v549 + v561 + v593 - 1.0*v594 - 1.0*v604 + v609 + v629 + v630 + v633 - 1.0*v690 - 1.0*v720 + v734 - 1.0*v735 + v737 + v808 + v948 - 1.0*v958 - 3.0*v970 + v981 - 1.0*v985 + v1011 + v1038",
            "tr_539_1 + tr_539_2",
            "v72 - 1.0*v3 - 1.0*v5 - 1.0*v9 - tr_540_1 - 1.0*v110 + v119 + v143 - 1.0*v146 - 0.0004*v150 - 10.0*v151 - 12.0*v152 - 11.0*v153 - 14.0*v154 - 13.0*v155 - 15.0*v156 - 1.0*v231 - 1.0*v233 - 1.0*v247 - 1.0*v249 - 1.0*v263 - 1.0*v271 + v463 - 1.0*v468 + v470 + v511",
            "v547 - 1.0*v515 - 1.0*v516 - 1.0*v532 - 1.0*v545 - tr_540_2 - 1.0*v549 - 1.0*v561 - 1.0*v593 + v594 + v604 - 1.0*v609 - 1.0*v629 - 1.0*v630 - 1.0*v633 + v690 + v720 - 1.0*v737 - 1.0*v808 - 1.0*v948 + v958 + 3.0*v970 - 1.0*v981 + v985 - 1.0*v1011 - 1.0*v1038",
            "tr_540_1 + tr_540_2",
            "v48 - tr_542_1 + v51 + v120 - 1.0*v122 + v132 - 1.0*v162 + v183 + v187 + v189 + v191 + v195 + v201 + v202 + v213 + v214 + v241 + v285 + v469 - 1.0*v504 + v511 + v512 + v530 + v545 + v565 + 4.0*v585 - 1.0*v736 + v750 + v752 + v756 + v786 + v819 + v897 + 2.0*v930",
            "v939 - tr_542_2 + v940 + v992 + v1018 + 2.0*v1045",
            "tr_542_1 + tr_542_2",
            "v8 - tr_589_1 + v27 + v28 + v68 - 1.0*v72 + v76 + v80 + v86 + v112 + v118 - 1.0*v119 + v123 + v126 + v134 + v139 - 1.0*v141 + v144 + 45.562800000000002853539626812562*v150 + v159 + v160 + v161 + v163 + v166 + v169 + v185 + v192 + v209 + v212 + v219 + v234 + v245",
            "v250 - tr_589_2 + v251 + v259 - 1.0*v268 + v436 + v443 - 2.0*v454 + v460 + v468 - 2.0*v471 + v482 - 1.0*v485 + v487 - 1.0*v495 + v504 + v505 + v509 + v518 + v524 + v526 - 1.0*v527 + v544 + v560 + v562 + v580 + v581 + v602 + v615 + v635 + v636 + v638 + v648",
            "v660 - tr_589_3 + v668 + v669 + v670 + v671 + v672 + v676 - 2.0*v678 + v695 + v696 + v698 + v699 + v700 - 1.0*v707 - 1.0*v708 - 1.0*v709 + v735 + v738 + v765 + v766 + v767 + v768 + v769 + v770 + v771 + v772 + v773 + v774 + v775 + v789 + v804 + v811 + v817 + v818",
            "v830 - tr_589_4 + v833 + 2.0*v837 + v838 + v843 + 2.0*v848 + v851 + v863 + v865 + v866 + v869 + v871 + v874 + v878 + v881 - 1.0*v883 - 1.0*v884 + v888 - 1.0*v890 - 1.0*v891 - 1.0*v892 - 1.0*v893 - 1.0*v894 - 1.0*v895 - 1.0*v896 + v901 - 1.0*v903 + v905 + v907",
            "v913 - tr_589_5 + v928 + v931 + v936 + v954 + v961 + v968 + v971 + v977 + v989 + v993 + v994 - 1.0*v1005 + v1013 + v1024 + v1029 + v1033 + v1036 + v1037 + v1039 + v1046 + v1063 + v1074",
            "tr_589_1 + tr_589_2 + tr_589_3 + tr_589_4 + tr_589_5",
            "v11 - tr_599_1 + v12 + v13 + v14 + v15 + v26 + v46 + v58 + v64 + v105 + v117 + v121 + v122 + v140 + 0.7302*v150 + v159 + v161 + v210 + v228 + v235 + v242 + v252 + v260 + v270 + v431 + v432 + v433 + v446 + v461 + v479 + v481 + v503 + v513 + v517 + v542 + v546 + v556",
            "v564 - tr_599_2 + v566 + v572 + v576 + v598 + v634 + v694 + v695 + v736 + v738 + v751 + v757 + v759 + v776 + v777 + v778 + v779 + v780 + v781 + v782 + v783 + 5.0*v791 - 1.0*v805 + v809 + v810 - 1.0*v848 + v857 + v870 + v885 + v931 + v937 + v959 + v997 + v1009",
            "v1034 - tr_599_3 + 8.0*v1040 + v1053 + v1069",
            "tr_599_1 + tr_599_2 + tr_599_3",
            "v30 - tr_623_1 - 1.0*v33 - 2.0*v35 + v37 + v39 + v49 + v83 + v84 + v85 + v106 + v170 + v189 + v191 + v201 + v220 + v225 - 1.0*v232 - 1.0*v272 + v277 + v451 + v452 + v478 + v484 + v496 + v501 + v587 + v606 + v645 + v646 + v662 + v663 + v673 + v680 + v682 + v689",
            "v690 - tr_623_2 + v713 - 1.0*v815 - 1.0*v824 - 1.0*v847 - 1.0*v863 + v902 + v904 + v933 + v939 + v940 + v945 + v969 + v1017 + v1018 + v1065",
            "tr_623_1 + tr_623_2"} "DAEO equality constraints";
        constant String inequalities[q]={"-1.0*v344","-1.0*v392","b1 + v344",
            "b2 + v429","b3 + v392","-1.0*v429","v2","v3","v4","v5","v6","v7",
            "v8","v11","v12","v13","v14","v15","v16","v17","v18","v19","v20",
            "v21","v24","v26","v28","v29","v30","v31","v32","v33","v35","v36",
            "v37","v38","v39","v41","v42","v45","v46","v48","v49","v50","v51",
            "v58","v59","v60","v62","v63","v64","v65","v68","v69","v70","v71",
            "v74","v76","v78","v82","v83","v85","v86","v89","v90","v91","v92",
            "v94","v95","v96","v97","v98","v99","v100","v102","v103","v104",
            "v105","v106","v108","v109","v110","v112","v114","v117","v118",
            "v120","v121","v122","v123","v125","v126","v128","v129","v130",
            "v131","v132","v134","v135","v136","v137","v138","v140","v142",
            "v143","v144","v145","v146","v148","v150","v151","v152","v153",
            "v154","v155","v156","v157","v158","v160","v162","v163","v164",
            "v165","v166","v168","v169","v170","v171","v172","v173","v175",
            "v177","v181","v182","v183","v184","v185","v186","v187","v188",
            "v189","v190","v191","v192","v193","v194","v195","v196","v197",
            "v201","v202","v204","v205","v207","v209","v211","v213","v214",
            "v215","v216","v218","v219","v222","v223","v224","v225","v229",
            "v230","v231","v232","v234","v235","v236","v237","v238","v240",
            "v241","v242","v243","v245","v246","v247","v248","v249","v250",
            "v251","v252","v253","v254","v255","v256","v257","v258","v259",
            "v260","v261","v262","v263","v264","v267","v269","v270","v271",
            "v272","v273","v276","v277","v278","v279","v280","v281","v282",
            "v284","v285","v287","v288","v289","v290","v291","v292","v293",
            "v294","v295","v296","v297","v298","v299","v300","v301","v302",
            "v303","v304","v305","v306","v307","v308","v309","v310","v311",
            "v312","v313","v314","v316","v317","v318","v319","v320","v321",
            "v322","v323","v324","v325","v326","v327","v328","v329","v331",
            "v332","v333","v334","v335","v336","v337","v338","v339","v340",
            "v341","v342","v343","v345","v346","v347","v348","v349","v350",
            "v351","v352","v353","v354","v355","v356","v357","v360","v361",
            "v362","v363","v364","v365","v366","v368","v369","v370","v371",
            "v372","v373","v374","v375","v376","v377","v378","v379","v380",
            "v381","v382","v383","v384","v386","v387","v389","v390","v391",
            "v393","v394","v395","v397","v398","v399","v400","v401","v402",
            "v403","v404","v405","v406","v408","v409","v410","v411","v412",
            "v413","v414","v415","v416","v417","v418","v419","v420","v421",
            "v422","v423","v424","v425","v426","v427","v428","v431","v432",
            "v433","v434","v436","v438","v440","v441","v442","v443","v444",
            "v445","v446","v448","v449","v450","v451","v452","v453","v454",
            "v457","v458","v459","v460","v461","v462","v464","v465","v466",
            "v467","v468","v469","v471","v472","v473","v477","v478","v481",
            "v482","v483","v484","v487","v488","v489","v490","v491","v493",
            "v495","v496","v497","v499","v501","v502","v503","v504","v505",
            "v507","v509","v510","v512","v513","v515","v516","v517","v518",
            "v520","v521","v523","v524","v526","v527","v528","v529","v530",
            "v531","v532","v534","v535","v536","v538","v539","v541","v542",
            "v543","v544","v545","v546","v547","v548","v549","v550","v551",
            "v552","v553","v554","v555","v556","v557","v558","v559","v560",
            "v562","v563","v564","v565","v566","v568","v572","v575","v576",
            "v577","v578","v579","v580","v581","v583","v584","v585","v586",
            "v587","v588","v589","v592","v593","v595","v596","v597","v598",
            "v599","v600","v601","v602","v605","v606","v607","v609","v611",
            "v612","v613","v615","v618","v620","v621","v623","v624","v625",
            "v628","v629","v630","v631","v632","v633","v634","v635","v636",
            "v637","v638","v640","v642","v646","v647","v648","v650","v651",
            "v652","v653","v654","v655","v656","v657","v658","v659","v660",
            "v662","v663","v666","v667","v668","v669","v670","v671","v672",
            "v673","v674","v675","v676","v678","v680","v681","v683","v686",
            "v687","v688","v689","v690","v691","v692","v693","v694","v695",
            "v696","v697","v698","v699","v700","v701","v702","v703","v704",
            "v705","v706","v710","v712","v713","v714","v715","v716","v717",
            "v718","v721","v723","v724","v725","v726","v727","v728","v729",
            "v730","v731","v732","v733","v734","v735","v736","v737","v738",
            "v740","v741","v751","v752","v753","v754","v755","v756","v757",
            "v758","v759","v761","v762","v763","v764","v765","v766","v767",
            "v768","v769","v770","v771","v772","v773","v774","v775","v776",
            "v777","v778","v779","v780","v781","v782","v783","v784","v785",
            "v786","v788","v790","v791","v793","v794","v795","v796","v797",
            "v798","v799","v800","v801","v802","v803","v804","v806","v807",
            "v808","v809","v810","v811","v812","v813","v814","v815","v817",
            "v818","v819","v822","v823","v824","v826","v829","v830","v833",
            "v837","v839","v840","v841","v843","v844","v845","v846","v847",
            "v848","v850","v851","v852","v853","v854","v857","v858","v859",
            "v860","v861","v863","v864","v866","v867","v868","v870","v871",
            "v872","v873","v874","v876","v879","v880","v881","v883","v885",
            "v887","v888","v897","v898","v899","v900","v901","v902","v905",
            "v906","v907","v908","v909","v910","v912","v913","v915","v916",
            "v918","v919","v920","v921","v922","v923","v924","v925","v928",
            "v929","v930","v931","v933","v934","v936","v939","v940","v942",
            "v943","v944","v945","v946","v947","v949","v950","v951","v952",
            "v953","v954","v955","v956","v957","v958","v959","v960","v961",
            "v962","v963","v964","v965","v969","v971","v974","v976","v977",
            "v978","v979","v980","v981","v982","v983","v985","v986","v987",
            "v989","v991","v992","v993","v994","v996","v997","v1000","v1001",
            "v1002","v1003","v1004","v1006","v1009","v1011","v1012","v1013",
            "v1014","v1015","v1016","v1017","v1019","v1020","v1021","v1023",
            "v1024","v1025","v1028","v1029","v1030","v1031","v1033","v1034",
            "v1035","v1036","v1037","v1038","v1039","v1040","v1042","v1043",
            "v1045","v1046","v1047","v1049","v1050","v1051","v1052","v1053",
            "v1054","v1058","v1059","v1061","v1063","v1065","v1066","v1068",
            "v1069","v1072","v1073","v1074","v1075"}
          "DAEO inequality constraints";
        parameter Real Gdiag[0]=zeros(0)
          "Diagonal entries of quadratic objective matrix G";
        parameter Solver.MyDaeoObject dfba=Solver.MyDaeoObject(
                  params,
                  variables,
                  objective,
                  equalities,
                  inequalities,
                  Gdiag,
                  size(Gdiag, 1)) "Linear external DAEO object"
          annotation (fixed=true);

        // outputs of DAEO
        Real daeoOut[n] "DAEO solution vector";
        Real sigma[q] "DAEO switching function values";
        // names of fluxes to be mapped to solution vector
        Real v1;
        Real v2;
        Real v3;
        Real v4;
        Real v5;
        Real v6;
        Real v7;
        Real v8;
        Real v9;
        Real v10;
        Real v11;
        Real v12;
        Real v13;
        Real v14;
        Real v15;
        Real v16;
        Real v17;
        Real v18;
        Real v19;
        Real v20;
        Real v21;
        Real v22;
        Real v23;
        Real v24;
        Real v25;
        Real v26;
        Real v27;
        Real v28;
        Real v29;
        Real v30;
        Real v31;
        Real v32;
        Real v33;
        Real v34;
        Real v35;
        Real v36;
        Real v37;
        Real v38;
        Real v39;
        Real v40;
        Real v41;
        Real v42;
        Real v43;
        Real v44;
        Real v45;
        Real v46;
        Real v47;
        Real v48;
        Real v49;
        Real v50;
        Real v51;
        Real v52;
        Real v53;
        Real v54;
        Real v55;
        Real v56;
        Real v57;
        Real v58;
        Real v59;
        Real v60;
        Real v61;
        Real v62;
        Real v63;
        Real v64;
        Real v65;
        Real v66;
        Real v67;
        Real v68;
        Real v69;
        Real v70;
        Real v71;
        Real v72;
        Real v73;
        Real v74;
        Real v75;
        Real v76;
        Real v77;
        Real v78;
        Real v79;
        Real v80;
        Real v81;
        Real v82;
        Real v83;
        Real v84;
        Real v85;
        Real v86;
        Real v87;
        Real v88;
        Real v89;
        Real v90;
        Real v91;
        Real v92;
        Real v93;
        Real v94;
        Real v95;
        Real v96;
        Real v97;
        Real v98;
        Real v99;
        Real v100;
        Real v101;
        Real v102;
        Real v103;
        Real v104;
        Real v105;
        Real v106;
        Real v107;
        Real v108;
        Real v109;
        Real v110;
        Real v111;
        Real v112;
        Real v113;
        Real v114;
        Real v115;
        Real v116;
        Real v117;
        Real v118;
        Real v119;
        Real v120;
        Real v121;
        Real v122;
        Real v123;
        Real v124;
        Real v125;
        Real v126;
        Real v127;
        Real v128;
        Real v129;
        Real v130;
        Real v131;
        Real v132;
        Real v133;
        Real v134;
        Real v135;
        Real v136;
        Real v137;
        Real v138;
        Real v139;
        Real v140;
        Real v141;
        Real v142;
        Real v143;
        Real v144;
        Real v145;
        Real v146;
        Real v147;
        Real v148;
        Real v149;
        Real v150;
        Real v151;
        Real v152;
        Real v153;
        Real v154;
        Real v155;
        Real v156;
        Real v157;
        Real v158;
        Real v159;
        Real v160;
        Real v161;
        Real v162;
        Real v163;
        Real v164;
        Real v165;
        Real v166;
        Real v167;
        Real v168;
        Real v169;
        Real v170;
        Real v171;
        Real v172;
        Real v173;
        Real v174;
        Real v175;
        Real v176;
        Real v177;
        Real v178;
        Real v179;
        Real v180;
        Real v181;
        Real v182;
        Real v183;
        Real v184;
        Real v185;
        Real v186;
        Real v187;
        Real v188;
        Real v189;
        Real v190;
        Real v191;
        Real v192;
        Real v193;
        Real v194;
        Real v195;
        Real v196;
        Real v197;
        Real v198;
        Real v199;
        Real v200;
        Real v201;
        Real v202;
        Real v203;
        Real v204;
        Real v205;
        Real v206;
        Real v207;
        Real v208;
        Real v209;
        Real v210;
        Real v211;
        Real v212;
        Real v213;
        Real v214;
        Real v215;
        Real v216;
        Real v217;
        Real v218;
        Real v219;
        Real v220;
        Real v221;
        Real v222;
        Real v223;
        Real v224;
        Real v225;
        Real v226;
        Real v227;
        Real v228;
        Real v229;
        Real v230;
        Real v231;
        Real v232;
        Real v233;
        Real v234;
        Real v235;
        Real v236;
        Real v237;
        Real v238;
        Real v239;
        Real v240;
        Real v241;
        Real v242;
        Real v243;
        Real v244;
        Real v245;
        Real v246;
        Real v247;
        Real v248;
        Real v249;
        Real v250;
        Real v251;
        Real v252;
        Real v253;
        Real v254;
        Real v255;
        Real v256;
        Real v257;
        Real v258;
        Real v259;
        Real v260;
        Real v261;
        Real v262;
        Real v263;
        Real v264;
        Real v265;
        Real v266;
        Real v267;
        Real v268;
        Real v269;
        Real v270;
        Real v271;
        Real v272;
        Real v273;
        Real v274;
        Real v275;
        Real v276;
        Real v277;
        Real v278;
        Real v279;
        Real v280;
        Real v281;
        Real v282;
        Real v283;
        Real v284;
        Real v285;
        Real v286;
        Real v287;
        Real v288;
        Real v289;
        Real v290;
        Real v291;
        Real v292;
        Real v293;
        Real v294;
        Real v295;
        Real v296;
        Real v297;
        Real v298;
        Real v299;
        Real v300;
        Real v301;
        Real v302;
        Real v303;
        Real v304;
        Real v305;
        Real v306;
        Real v307;
        Real v308;
        Real v309;
        Real v310;
        Real v311;
        Real v312;
        Real v313;
        Real v314;
        Real v315;
        Real v316;
        Real v317;
        Real v318;
        Real v319;
        Real v320;
        Real v321;
        Real v322;
        Real v323;
        Real v324;
        Real v325;
        Real v326;
        Real v327;
        Real v328;
        Real v329;
        Real v330;
        Real v331;
        Real v332;
        Real v333;
        Real v334;
        Real v335;
        Real v336;
        Real v337;
        Real v338;
        Real v339;
        Real v340;
        Real v341;
        Real v342;
        Real v343;
        Real v344;
        Real v345;
        Real v346;
        Real v347;
        Real v348;
        Real v349;
        Real v350;
        Real v351;
        Real v352;
        Real v353;
        Real v354;
        Real v355;
        Real v356;
        Real v357;
        Real v358;
        Real v359;
        Real v360;
        Real v361;
        Real v362;
        Real v363;
        Real v364;
        Real v365;
        Real v366;
        Real v367;
        Real v368;
        Real v369;
        Real v370;
        Real v371;
        Real v372;
        Real v373;
        Real v374;
        Real v375;
        Real v376;
        Real v377;
        Real v378;
        Real v379;
        Real v380;
        Real v381;
        Real v382;
        Real v383;
        Real v384;
        Real v385;
        Real v386;
        Real v387;
        Real v388;
        Real v389;
        Real v390;
        Real v391;
        Real v392;
        Real v393;
        Real v394;
        Real v395;
        Real v396;
        Real v397;
        Real v398;
        Real v399;
        Real v400;
        Real v401;
        Real v402;
        Real v403;
        Real v404;
        Real v405;
        Real v406;
        Real v407;
        Real v408;
        Real v409;
        Real v410;
        Real v411;
        Real v412;
        Real v413;
        Real v414;
        Real v415;
        Real v416;
        Real v417;
        Real v418;
        Real v419;
        Real v420;
        Real v421;
        Real v422;
        Real v423;
        Real v424;
        Real v425;
        Real v426;
        Real v427;
        Real v428;
        Real v429;
        Real v430;
        Real v431;
        Real v432;
        Real v433;
        Real v434;
        Real v435;
        Real v436;
        Real v437;
        Real v438;
        Real v439;
        Real v440;
        Real v441;
        Real v442;
        Real v443;
        Real v444;
        Real v445;
        Real v446;
        Real v447;
        Real v448;
        Real v449;
        Real v450;
        Real v451;
        Real v452;
        Real v453;
        Real v454;
        Real v455;
        Real v456;
        Real v457;
        Real v458;
        Real v459;
        Real v460;
        Real v461;
        Real v462;
        Real v463;
        Real v464;
        Real v465;
        Real v466;
        Real v467;
        Real v468;
        Real v469;
        Real v470;
        Real v471;
        Real v472;
        Real v473;
        Real v474;
        Real v475;
        Real v476;
        Real v477;
        Real v478;
        Real v479;
        Real v480;
        Real v481;
        Real v482;
        Real v483;
        Real v484;
        Real v485;
        Real v486;
        Real v487;
        Real v488;
        Real v489;
        Real v490;
        Real v491;
        Real v492;
        Real v493;
        Real v494;
        Real v495;
        Real v496;
        Real v497;
        Real v498;
        Real v499;
        Real v500;
        Real v501;
        Real v502;
        Real v503;
        Real v504;
        Real v505;
        Real v506;
        Real v507;
        Real v508;
        Real v509;
        Real v510;
        Real v511;
        Real v512;
        Real v513;
        Real v514;
        Real v515;
        Real v516;
        Real v517;
        Real v518;
        Real v519;
        Real v520;
        Real v521;
        Real v522;
        Real v523;
        Real v524;
        Real v525;
        Real v526;
        Real v527;
        Real v528;
        Real v529;
        Real v530;
        Real v531;
        Real v532;
        Real v533;
        Real v534;
        Real v535;
        Real v536;
        Real v537;
        Real v538;
        Real v539;
        Real v540;
        Real v541;
        Real v542;
        Real v543;
        Real v544;
        Real v545;
        Real v546;
        Real v547;
        Real v548;
        Real v549;
        Real v550;
        Real v551;
        Real v552;
        Real v553;
        Real v554;
        Real v555;
        Real v556;
        Real v557;
        Real v558;
        Real v559;
        Real v560;
        Real v561;
        Real v562;
        Real v563;
        Real v564;
        Real v565;
        Real v566;
        Real v567;
        Real v568;
        Real v569;
        Real v570;
        Real v571;
        Real v572;
        Real v573;
        Real v574;
        Real v575;
        Real v576;
        Real v577;
        Real v578;
        Real v579;
        Real v580;
        Real v581;
        Real v582;
        Real v583;
        Real v584;
        Real v585;
        Real v586;
        Real v587;
        Real v588;
        Real v589;
        Real v590;
        Real v591;
        Real v592;
        Real v593;
        Real v594;
        Real v595;
        Real v596;
        Real v597;
        Real v598;
        Real v599;
        Real v600;
        Real v601;
        Real v602;
        Real v603;
        Real v604;
        Real v605;
        Real v606;
        Real v607;
        Real v608;
        Real v609;
        Real v610;
        Real v611;
        Real v612;
        Real v613;
        Real v614;
        Real v615;
        Real v616;
        Real v617;
        Real v618;
        Real v619;
        Real v620;
        Real v621;
        Real v622;
        Real v623;
        Real v624;
        Real v625;
        Real v626;
        Real v627;
        Real v628;
        Real v629;
        Real v630;
        Real v631;
        Real v632;
        Real v633;
        Real v634;
        Real v635;
        Real v636;
        Real v637;
        Real v638;
        Real v639;
        Real v640;
        Real v641;
        Real v642;
        Real v643;
        Real v644;
        Real v645;
        Real v646;
        Real v647;
        Real v648;
        Real v649;
        Real v650;
        Real v651;
        Real v652;
        Real v653;
        Real v654;
        Real v655;
        Real v656;
        Real v657;
        Real v658;
        Real v659;
        Real v660;
        Real v661;
        Real v662;
        Real v663;
        Real v664;
        Real v665;
        Real v666;
        Real v667;
        Real v668;
        Real v669;
        Real v670;
        Real v671;
        Real v672;
        Real v673;
        Real v674;
        Real v675;
        Real v676;
        Real v677;
        Real v678;
        Real v679;
        Real v680;
        Real v681;
        Real v682;
        Real v683;
        Real v684;
        Real v685;
        Real v686;
        Real v687;
        Real v688;
        Real v689;
        Real v690;
        Real v691;
        Real v692;
        Real v693;
        Real v694;
        Real v695;
        Real v696;
        Real v697;
        Real v698;
        Real v699;
        Real v700;
        Real v701;
        Real v702;
        Real v703;
        Real v704;
        Real v705;
        Real v706;
        Real v707;
        Real v708;
        Real v709;
        Real v710;
        Real v711;
        Real v712;
        Real v713;
        Real v714;
        Real v715;
        Real v716;
        Real v717;
        Real v718;
        Real v719;
        Real v720;
        Real v721;
        Real v722;
        Real v723;
        Real v724;
        Real v725;
        Real v726;
        Real v727;
        Real v728;
        Real v729;
        Real v730;
        Real v731;
        Real v732;
        Real v733;
        Real v734;
        Real v735;
        Real v736;
        Real v737;
        Real v738;
        Real v739;
        Real v740;
        Real v741;
        Real v742;
        Real v743;
        Real v744;
        Real v745;
        Real v746;
        Real v747;
        Real v748;
        Real v749;
        Real v750;
        Real v751;
        Real v752;
        Real v753;
        Real v754;
        Real v755;
        Real v756;
        Real v757;
        Real v758;
        Real v759;
        Real v760;
        Real v761;
        Real v762;
        Real v763;
        Real v764;
        Real v765;
        Real v766;
        Real v767;
        Real v768;
        Real v769;
        Real v770;
        Real v771;
        Real v772;
        Real v773;
        Real v774;
        Real v775;
        Real v776;
        Real v777;
        Real v778;
        Real v779;
        Real v780;
        Real v781;
        Real v782;
        Real v783;
        Real v784;
        Real v785;
        Real v786;
        Real v787;
        Real v788;
        Real v789;
        Real v790;
        Real v791;
        Real v792;
        Real v793;
        Real v794;
        Real v795;
        Real v796;
        Real v797;
        Real v798;
        Real v799;
        Real v800;
        Real v801;
        Real v802;
        Real v803;
        Real v804;
        Real v805;
        Real v806;
        Real v807;
        Real v808;
        Real v809;
        Real v810;
        Real v811;
        Real v812;
        Real v813;
        Real v814;
        Real v815;
        Real v816;
        Real v817;
        Real v818;
        Real v819;
        Real v820;
        Real v821;
        Real v822;
        Real v823;
        Real v824;
        Real v825;
        Real v826;
        Real v827;
        Real v828;
        Real v829;
        Real v830;
        Real v831;
        Real v832;
        Real v833;
        Real v834;
        Real v835;
        Real v836;
        Real v837;
        Real v838;
        Real v839;
        Real v840;
        Real v841;
        Real v842;
        Real v843;
        Real v844;
        Real v845;
        Real v846;
        Real v847;
        Real v848;
        Real v849;
        Real v850;
        Real v851;
        Real v852;
        Real v853;
        Real v854;
        Real v855;
        Real v856;
        Real v857;
        Real v858;
        Real v859;
        Real v860;
        Real v861;
        Real v862;
        Real v863;
        Real v864;
        Real v865;
        Real v866;
        Real v867;
        Real v868;
        Real v869;
        Real v870;
        Real v871;
        Real v872;
        Real v873;
        Real v874;
        Real v875;
        Real v876;
        Real v877;
        Real v878;
        Real v879;
        Real v880;
        Real v881;
        Real v882;
        Real v883;
        Real v884;
        Real v885;
        Real v886;
        Real v887;
        Real v888;
        Real v889;
        Real v890;
        Real v891;
        Real v892;
        Real v893;
        Real v894;
        Real v895;
        Real v896;
        Real v897;
        Real v898;
        Real v899;
        Real v900;
        Real v901;
        Real v902;
        Real v903;
        Real v904;
        Real v905;
        Real v906;
        Real v907;
        Real v908;
        Real v909;
        Real v910;
        Real v911;
        Real v912;
        Real v913;
        Real v914;
        Real v915;
        Real v916;
        Real v917;
        Real v918;
        Real v919;
        Real v920;
        Real v921;
        Real v922;
        Real v923;
        Real v924;
        Real v925;
        Real v926;
        Real v927;
        Real v928;
        Real v929;
        Real v930;
        Real v931;
        Real v932;
        Real v933;
        Real v934;
        Real v935;
        Real v936;
        Real v937;
        Real v938;
        Real v939;
        Real v940;
        Real v941;
        Real v942;
        Real v943;
        Real v944;
        Real v945;
        Real v946;
        Real v947;
        Real v948;
        Real v949;
        Real v950;
        Real v951;
        Real v952;
        Real v953;
        Real v954;
        Real v955;
        Real v956;
        Real v957;
        Real v958;
        Real v959;
        Real v960;
        Real v961;
        Real v962;
        Real v963;
        Real v964;
        Real v965;
        Real v966;
        Real v967;
        Real v968;
        Real v969;
        Real v970;
        Real v971;
        Real v972;
        Real v973;
        Real v974;
        Real v975;
        Real v976;
        Real v977;
        Real v978;
        Real v979;
        Real v980;
        Real v981;
        Real v982;
        Real v983;
        Real v984;
        Real v985;
        Real v986;
        Real v987;
        Real v988;
        Real v989;
        Real v990;
        Real v991;
        Real v992;
        Real v993;
        Real v994;
        Real v995;
        Real v996;
        Real v997;
        Real v998;
        Real v999;
        Real v1000;
        Real v1001;
        Real v1002;
        Real v1003;
        Real v1004;
        Real v1005;
        Real v1006;
        Real v1007;
        Real v1008;
        Real v1009;
        Real v1010;
        Real v1011;
        Real v1012;
        Real v1013;
        Real v1014;
        Real v1015;
        Real v1016;
        Real v1017;
        Real v1018;
        Real v1019;
        Real v1020;
        Real v1021;
        Real v1022;
        Real v1023;
        Real v1024;
        Real v1025;
        Real v1026;
        Real v1027;
        Real v1028;
        Real v1029;
        Real v1030;
        Real v1031;
        Real v1032;
        Real v1033;
        Real v1034;
        Real v1035;
        Real v1036;
        Real v1037;
        Real v1038;
        Real v1039;
        Real v1040;
        Real v1041;
        Real v1042;
        Real v1043;
        Real v1044;
        Real v1045;
        Real v1046;
        Real v1047;
        Real v1048;
        Real v1049;
        Real v1050;
        Real v1051;
        Real v1052;
        Real v1053;
        Real v1054;
        Real v1055;
        Real v1056;
        Real v1057;
        Real v1058;
        Real v1059;
        Real v1060;
        Real v1061;
        Real v1062;
        Real v1063;
        Real v1064;
        Real v1065;
        Real v1066;
        Real v1067;
        Real v1068;
        Real v1069;
        Real v1070;
        Real v1071;
        Real v1072;
        Real v1073;
        Real v1074;
        Real v1075;

      equation
        // mapping of fluxes to their respective entries in solution vector
        v1 = daeoOut[1];
        v2 = daeoOut[2];
        v3 = daeoOut[3];
        v4 = daeoOut[4];
        v5 = daeoOut[5];
        v6 = daeoOut[6];
        v7 = daeoOut[7];
        v8 = daeoOut[8];
        v9 = daeoOut[9];
        v10 = daeoOut[10];
        v11 = daeoOut[11];
        v12 = daeoOut[12];
        v13 = daeoOut[13];
        v14 = daeoOut[14];
        v15 = daeoOut[15];
        v16 = daeoOut[16];
        v17 = daeoOut[17];
        v18 = daeoOut[18];
        v19 = daeoOut[19];
        v20 = daeoOut[20];
        v21 = daeoOut[21];
        v22 = daeoOut[22];
        v23 = daeoOut[23];
        v24 = daeoOut[24];
        v25 = daeoOut[25];
        v26 = daeoOut[26];
        v27 = daeoOut[27];
        v28 = daeoOut[28];
        v29 = daeoOut[29];
        v30 = daeoOut[30];
        v31 = daeoOut[31];
        v32 = daeoOut[32];
        v33 = daeoOut[33];
        v34 = daeoOut[34];
        v35 = daeoOut[35];
        v36 = daeoOut[36];
        v37 = daeoOut[37];
        v38 = daeoOut[38];
        v39 = daeoOut[39];
        v40 = daeoOut[40];
        v41 = daeoOut[41];
        v42 = daeoOut[42];
        v43 = daeoOut[43];
        v44 = daeoOut[44];
        v45 = daeoOut[45];
        v46 = daeoOut[46];
        v47 = daeoOut[47];
        v48 = daeoOut[48];
        v49 = daeoOut[49];
        v50 = daeoOut[50];
        v51 = daeoOut[51];
        v52 = daeoOut[52];
        v53 = daeoOut[53];
        v54 = daeoOut[54];
        v55 = daeoOut[55];
        v56 = daeoOut[56];
        v57 = daeoOut[57];
        v58 = daeoOut[58];
        v59 = daeoOut[59];
        v60 = daeoOut[60];
        v61 = daeoOut[61];
        v62 = daeoOut[62];
        v63 = daeoOut[63];
        v64 = daeoOut[64];
        v65 = daeoOut[65];
        v66 = daeoOut[66];
        v67 = daeoOut[67];
        v68 = daeoOut[68];
        v69 = daeoOut[69];
        v70 = daeoOut[70];
        v71 = daeoOut[71];
        v72 = daeoOut[72];
        v73 = daeoOut[73];
        v74 = daeoOut[74];
        v75 = daeoOut[75];
        v76 = daeoOut[76];
        v77 = daeoOut[77];
        v78 = daeoOut[78];
        v79 = daeoOut[79];
        v80 = daeoOut[80];
        v81 = daeoOut[81];
        v82 = daeoOut[82];
        v83 = daeoOut[83];
        v84 = daeoOut[84];
        v85 = daeoOut[85];
        v86 = daeoOut[86];
        v87 = daeoOut[87];
        v88 = daeoOut[88];
        v89 = daeoOut[89];
        v90 = daeoOut[90];
        v91 = daeoOut[91];
        v92 = daeoOut[92];
        v93 = daeoOut[93];
        v94 = daeoOut[94];
        v95 = daeoOut[95];
        v96 = daeoOut[96];
        v97 = daeoOut[97];
        v98 = daeoOut[98];
        v99 = daeoOut[99];
        v100 = daeoOut[100];
        v101 = daeoOut[101];
        v102 = daeoOut[102];
        v103 = daeoOut[103];
        v104 = daeoOut[104];
        v105 = daeoOut[105];
        v106 = daeoOut[106];
        v107 = daeoOut[107];
        v108 = daeoOut[108];
        v109 = daeoOut[109];
        v110 = daeoOut[110];
        v111 = daeoOut[111];
        v112 = daeoOut[112];
        v113 = daeoOut[113];
        v114 = daeoOut[114];
        v115 = daeoOut[115];
        v116 = daeoOut[116];
        v117 = daeoOut[117];
        v118 = daeoOut[118];
        v119 = daeoOut[119];
        v120 = daeoOut[120];
        v121 = daeoOut[121];
        v122 = daeoOut[122];
        v123 = daeoOut[123];
        v124 = daeoOut[124];
        v125 = daeoOut[125];
        v126 = daeoOut[126];
        v127 = daeoOut[127];
        v128 = daeoOut[128];
        v129 = daeoOut[129];
        v130 = daeoOut[130];
        v131 = daeoOut[131];
        v132 = daeoOut[132];
        v133 = daeoOut[133];
        v134 = daeoOut[134];
        v135 = daeoOut[135];
        v136 = daeoOut[136];
        v137 = daeoOut[137];
        v138 = daeoOut[138];
        v139 = daeoOut[139];
        v140 = daeoOut[140];
        v141 = daeoOut[141];
        v142 = daeoOut[142];
        v143 = daeoOut[143];
        v144 = daeoOut[144];
        v145 = daeoOut[145];
        v146 = daeoOut[146];
        v147 = daeoOut[147];
        v148 = daeoOut[148];
        v149 = daeoOut[149];
        v150 = daeoOut[150];
        v151 = daeoOut[151];
        v152 = daeoOut[152];
        v153 = daeoOut[153];
        v154 = daeoOut[154];
        v155 = daeoOut[155];
        v156 = daeoOut[156];
        v157 = daeoOut[157];
        v158 = daeoOut[158];
        v159 = daeoOut[159];
        v160 = daeoOut[160];
        v161 = daeoOut[161];
        v162 = daeoOut[162];
        v163 = daeoOut[163];
        v164 = daeoOut[164];
        v165 = daeoOut[165];
        v166 = daeoOut[166];
        v167 = daeoOut[167];
        v168 = daeoOut[168];
        v169 = daeoOut[169];
        v170 = daeoOut[170];
        v171 = daeoOut[171];
        v172 = daeoOut[172];
        v173 = daeoOut[173];
        v174 = daeoOut[174];
        v175 = daeoOut[175];
        v176 = daeoOut[176];
        v177 = daeoOut[177];
        v178 = daeoOut[178];
        v179 = daeoOut[179];
        v180 = daeoOut[180];
        v181 = daeoOut[181];
        v182 = daeoOut[182];
        v183 = daeoOut[183];
        v184 = daeoOut[184];
        v185 = daeoOut[185];
        v186 = daeoOut[186];
        v187 = daeoOut[187];
        v188 = daeoOut[188];
        v189 = daeoOut[189];
        v190 = daeoOut[190];
        v191 = daeoOut[191];
        v192 = daeoOut[192];
        v193 = daeoOut[193];
        v194 = daeoOut[194];
        v195 = daeoOut[195];
        v196 = daeoOut[196];
        v197 = daeoOut[197];
        v198 = daeoOut[198];
        v199 = daeoOut[199];
        v200 = daeoOut[200];
        v201 = daeoOut[201];
        v202 = daeoOut[202];
        v203 = daeoOut[203];
        v204 = daeoOut[204];
        v205 = daeoOut[205];
        v206 = daeoOut[206];
        v207 = daeoOut[207];
        v208 = daeoOut[208];
        v209 = daeoOut[209];
        v210 = daeoOut[210];
        v211 = daeoOut[211];
        v212 = daeoOut[212];
        v213 = daeoOut[213];
        v214 = daeoOut[214];
        v215 = daeoOut[215];
        v216 = daeoOut[216];
        v217 = daeoOut[217];
        v218 = daeoOut[218];
        v219 = daeoOut[219];
        v220 = daeoOut[220];
        v221 = daeoOut[221];
        v222 = daeoOut[222];
        v223 = daeoOut[223];
        v224 = daeoOut[224];
        v225 = daeoOut[225];
        v226 = daeoOut[226];
        v227 = daeoOut[227];
        v228 = daeoOut[228];
        v229 = daeoOut[229];
        v230 = daeoOut[230];
        v231 = daeoOut[231];
        v232 = daeoOut[232];
        v233 = daeoOut[233];
        v234 = daeoOut[234];
        v235 = daeoOut[235];
        v236 = daeoOut[236];
        v237 = daeoOut[237];
        v238 = daeoOut[238];
        v239 = daeoOut[239];
        v240 = daeoOut[240];
        v241 = daeoOut[241];
        v242 = daeoOut[242];
        v243 = daeoOut[243];
        v244 = daeoOut[244];
        v245 = daeoOut[245];
        v246 = daeoOut[246];
        v247 = daeoOut[247];
        v248 = daeoOut[248];
        v249 = daeoOut[249];
        v250 = daeoOut[250];
        v251 = daeoOut[251];
        v252 = daeoOut[252];
        v253 = daeoOut[253];
        v254 = daeoOut[254];
        v255 = daeoOut[255];
        v256 = daeoOut[256];
        v257 = daeoOut[257];
        v258 = daeoOut[258];
        v259 = daeoOut[259];
        v260 = daeoOut[260];
        v261 = daeoOut[261];
        v262 = daeoOut[262];
        v263 = daeoOut[263];
        v264 = daeoOut[264];
        v265 = daeoOut[265];
        v266 = daeoOut[266];
        v267 = daeoOut[267];
        v268 = daeoOut[268];
        v269 = daeoOut[269];
        v270 = daeoOut[270];
        v271 = daeoOut[271];
        v272 = daeoOut[272];
        v273 = daeoOut[273];
        v274 = daeoOut[274];
        v275 = daeoOut[275];
        v276 = daeoOut[276];
        v277 = daeoOut[277];
        v278 = daeoOut[278];
        v279 = daeoOut[279];
        v280 = daeoOut[280];
        v281 = daeoOut[281];
        v282 = daeoOut[282];
        v283 = daeoOut[283];
        v284 = daeoOut[284];
        v285 = daeoOut[285];
        v286 = daeoOut[286];
        v287 = daeoOut[287];
        v288 = daeoOut[288];
        v289 = daeoOut[289];
        v290 = daeoOut[290];
        v291 = daeoOut[291];
        v292 = daeoOut[292];
        v293 = daeoOut[293];
        v294 = daeoOut[294];
        v295 = daeoOut[295];
        v296 = daeoOut[296];
        v297 = daeoOut[297];
        v298 = daeoOut[298];
        v299 = daeoOut[299];
        v300 = daeoOut[300];
        v301 = daeoOut[301];
        v302 = daeoOut[302];
        v303 = daeoOut[303];
        v304 = daeoOut[304];
        v305 = daeoOut[305];
        v306 = daeoOut[306];
        v307 = daeoOut[307];
        v308 = daeoOut[308];
        v309 = daeoOut[309];
        v310 = daeoOut[310];
        v311 = daeoOut[311];
        v312 = daeoOut[312];
        v313 = daeoOut[313];
        v314 = daeoOut[314];
        v315 = daeoOut[315];
        v316 = daeoOut[316];
        v317 = daeoOut[317];
        v318 = daeoOut[318];
        v319 = daeoOut[319];
        v320 = daeoOut[320];
        v321 = daeoOut[321];
        v322 = daeoOut[322];
        v323 = daeoOut[323];
        v324 = daeoOut[324];
        v325 = daeoOut[325];
        v326 = daeoOut[326];
        v327 = daeoOut[327];
        v328 = daeoOut[328];
        v329 = daeoOut[329];
        v330 = daeoOut[330];
        v331 = daeoOut[331];
        v332 = daeoOut[332];
        v333 = daeoOut[333];
        v334 = daeoOut[334];
        v335 = daeoOut[335];
        v336 = daeoOut[336];
        v337 = daeoOut[337];
        v338 = daeoOut[338];
        v339 = daeoOut[339];
        v340 = daeoOut[340];
        v341 = daeoOut[341];
        v342 = daeoOut[342];
        v343 = daeoOut[343];
        v344 = daeoOut[344];
        v345 = daeoOut[345];
        v346 = daeoOut[346];
        v347 = daeoOut[347];
        v348 = daeoOut[348];
        v349 = daeoOut[349];
        v350 = daeoOut[350];
        v351 = daeoOut[351];
        v352 = daeoOut[352];
        v353 = daeoOut[353];
        v354 = daeoOut[354];
        v355 = daeoOut[355];
        v356 = daeoOut[356];
        v357 = daeoOut[357];
        v358 = daeoOut[358];
        v359 = daeoOut[359];
        v360 = daeoOut[360];
        v361 = daeoOut[361];
        v362 = daeoOut[362];
        v363 = daeoOut[363];
        v364 = daeoOut[364];
        v365 = daeoOut[365];
        v366 = daeoOut[366];
        v367 = daeoOut[367];
        v368 = daeoOut[368];
        v369 = daeoOut[369];
        v370 = daeoOut[370];
        v371 = daeoOut[371];
        v372 = daeoOut[372];
        v373 = daeoOut[373];
        v374 = daeoOut[374];
        v375 = daeoOut[375];
        v376 = daeoOut[376];
        v377 = daeoOut[377];
        v378 = daeoOut[378];
        v379 = daeoOut[379];
        v380 = daeoOut[380];
        v381 = daeoOut[381];
        v382 = daeoOut[382];
        v383 = daeoOut[383];
        v384 = daeoOut[384];
        v385 = daeoOut[385];
        v386 = daeoOut[386];
        v387 = daeoOut[387];
        v388 = daeoOut[388];
        v389 = daeoOut[389];
        v390 = daeoOut[390];
        v391 = daeoOut[391];
        v392 = daeoOut[392];
        v393 = daeoOut[393];
        v394 = daeoOut[394];
        v395 = daeoOut[395];
        v396 = daeoOut[396];
        v397 = daeoOut[397];
        v398 = daeoOut[398];
        v399 = daeoOut[399];
        v400 = daeoOut[400];
        v401 = daeoOut[401];
        v402 = daeoOut[402];
        v403 = daeoOut[403];
        v404 = daeoOut[404];
        v405 = daeoOut[405];
        v406 = daeoOut[406];
        v407 = daeoOut[407];
        v408 = daeoOut[408];
        v409 = daeoOut[409];
        v410 = daeoOut[410];
        v411 = daeoOut[411];
        v412 = daeoOut[412];
        v413 = daeoOut[413];
        v414 = daeoOut[414];
        v415 = daeoOut[415];
        v416 = daeoOut[416];
        v417 = daeoOut[417];
        v418 = daeoOut[418];
        v419 = daeoOut[419];
        v420 = daeoOut[420];
        v421 = daeoOut[421];
        v422 = daeoOut[422];
        v423 = daeoOut[423];
        v424 = daeoOut[424];
        v425 = daeoOut[425];
        v426 = daeoOut[426];
        v427 = daeoOut[427];
        v428 = daeoOut[428];
        v429 = daeoOut[429];
        v430 = daeoOut[430];
        v431 = daeoOut[431];
        v432 = daeoOut[432];
        v433 = daeoOut[433];
        v434 = daeoOut[434];
        v435 = daeoOut[435];
        v436 = daeoOut[436];
        v437 = daeoOut[437];
        v438 = daeoOut[438];
        v439 = daeoOut[439];
        v440 = daeoOut[440];
        v441 = daeoOut[441];
        v442 = daeoOut[442];
        v443 = daeoOut[443];
        v444 = daeoOut[444];
        v445 = daeoOut[445];
        v446 = daeoOut[446];
        v447 = daeoOut[447];
        v448 = daeoOut[448];
        v449 = daeoOut[449];
        v450 = daeoOut[450];
        v451 = daeoOut[451];
        v452 = daeoOut[452];
        v453 = daeoOut[453];
        v454 = daeoOut[454];
        v455 = daeoOut[455];
        v456 = daeoOut[456];
        v457 = daeoOut[457];
        v458 = daeoOut[458];
        v459 = daeoOut[459];
        v460 = daeoOut[460];
        v461 = daeoOut[461];
        v462 = daeoOut[462];
        v463 = daeoOut[463];
        v464 = daeoOut[464];
        v465 = daeoOut[465];
        v466 = daeoOut[466];
        v467 = daeoOut[467];
        v468 = daeoOut[468];
        v469 = daeoOut[469];
        v470 = daeoOut[470];
        v471 = daeoOut[471];
        v472 = daeoOut[472];
        v473 = daeoOut[473];
        v474 = daeoOut[474];
        v475 = daeoOut[475];
        v476 = daeoOut[476];
        v477 = daeoOut[477];
        v478 = daeoOut[478];
        v479 = daeoOut[479];
        v480 = daeoOut[480];
        v481 = daeoOut[481];
        v482 = daeoOut[482];
        v483 = daeoOut[483];
        v484 = daeoOut[484];
        v485 = daeoOut[485];
        v486 = daeoOut[486];
        v487 = daeoOut[487];
        v488 = daeoOut[488];
        v489 = daeoOut[489];
        v490 = daeoOut[490];
        v491 = daeoOut[491];
        v492 = daeoOut[492];
        v493 = daeoOut[493];
        v494 = daeoOut[494];
        v495 = daeoOut[495];
        v496 = daeoOut[496];
        v497 = daeoOut[497];
        v498 = daeoOut[498];
        v499 = daeoOut[499];
        v500 = daeoOut[500];
        v501 = daeoOut[501];
        v502 = daeoOut[502];
        v503 = daeoOut[503];
        v504 = daeoOut[504];
        v505 = daeoOut[505];
        v506 = daeoOut[506];
        v507 = daeoOut[507];
        v508 = daeoOut[508];
        v509 = daeoOut[509];
        v510 = daeoOut[510];
        v511 = daeoOut[511];
        v512 = daeoOut[512];
        v513 = daeoOut[513];
        v514 = daeoOut[514];
        v515 = daeoOut[515];
        v516 = daeoOut[516];
        v517 = daeoOut[517];
        v518 = daeoOut[518];
        v519 = daeoOut[519];
        v520 = daeoOut[520];
        v521 = daeoOut[521];
        v522 = daeoOut[522];
        v523 = daeoOut[523];
        v524 = daeoOut[524];
        v525 = daeoOut[525];
        v526 = daeoOut[526];
        v527 = daeoOut[527];
        v528 = daeoOut[528];
        v529 = daeoOut[529];
        v530 = daeoOut[530];
        v531 = daeoOut[531];
        v532 = daeoOut[532];
        v533 = daeoOut[533];
        v534 = daeoOut[534];
        v535 = daeoOut[535];
        v536 = daeoOut[536];
        v537 = daeoOut[537];
        v538 = daeoOut[538];
        v539 = daeoOut[539];
        v540 = daeoOut[540];
        v541 = daeoOut[541];
        v542 = daeoOut[542];
        v543 = daeoOut[543];
        v544 = daeoOut[544];
        v545 = daeoOut[545];
        v546 = daeoOut[546];
        v547 = daeoOut[547];
        v548 = daeoOut[548];
        v549 = daeoOut[549];
        v550 = daeoOut[550];
        v551 = daeoOut[551];
        v552 = daeoOut[552];
        v553 = daeoOut[553];
        v554 = daeoOut[554];
        v555 = daeoOut[555];
        v556 = daeoOut[556];
        v557 = daeoOut[557];
        v558 = daeoOut[558];
        v559 = daeoOut[559];
        v560 = daeoOut[560];
        v561 = daeoOut[561];
        v562 = daeoOut[562];
        v563 = daeoOut[563];
        v564 = daeoOut[564];
        v565 = daeoOut[565];
        v566 = daeoOut[566];
        v567 = daeoOut[567];
        v568 = daeoOut[568];
        v569 = daeoOut[569];
        v570 = daeoOut[570];
        v571 = daeoOut[571];
        v572 = daeoOut[572];
        v573 = daeoOut[573];
        v574 = daeoOut[574];
        v575 = daeoOut[575];
        v576 = daeoOut[576];
        v577 = daeoOut[577];
        v578 = daeoOut[578];
        v579 = daeoOut[579];
        v580 = daeoOut[580];
        v581 = daeoOut[581];
        v582 = daeoOut[582];
        v583 = daeoOut[583];
        v584 = daeoOut[584];
        v585 = daeoOut[585];
        v586 = daeoOut[586];
        v587 = daeoOut[587];
        v588 = daeoOut[588];
        v589 = daeoOut[589];
        v590 = daeoOut[590];
        v591 = daeoOut[591];
        v592 = daeoOut[592];
        v593 = daeoOut[593];
        v594 = daeoOut[594];
        v595 = daeoOut[595];
        v596 = daeoOut[596];
        v597 = daeoOut[597];
        v598 = daeoOut[598];
        v599 = daeoOut[599];
        v600 = daeoOut[600];
        v601 = daeoOut[601];
        v602 = daeoOut[602];
        v603 = daeoOut[603];
        v604 = daeoOut[604];
        v605 = daeoOut[605];
        v606 = daeoOut[606];
        v607 = daeoOut[607];
        v608 = daeoOut[608];
        v609 = daeoOut[609];
        v610 = daeoOut[610];
        v611 = daeoOut[611];
        v612 = daeoOut[612];
        v613 = daeoOut[613];
        v614 = daeoOut[614];
        v615 = daeoOut[615];
        v616 = daeoOut[616];
        v617 = daeoOut[617];
        v618 = daeoOut[618];
        v619 = daeoOut[619];
        v620 = daeoOut[620];
        v621 = daeoOut[621];
        v622 = daeoOut[622];
        v623 = daeoOut[623];
        v624 = daeoOut[624];
        v625 = daeoOut[625];
        v626 = daeoOut[626];
        v627 = daeoOut[627];
        v628 = daeoOut[628];
        v629 = daeoOut[629];
        v630 = daeoOut[630];
        v631 = daeoOut[631];
        v632 = daeoOut[632];
        v633 = daeoOut[633];
        v634 = daeoOut[634];
        v635 = daeoOut[635];
        v636 = daeoOut[636];
        v637 = daeoOut[637];
        v638 = daeoOut[638];
        v639 = daeoOut[639];
        v640 = daeoOut[640];
        v641 = daeoOut[641];
        v642 = daeoOut[642];
        v643 = daeoOut[643];
        v644 = daeoOut[644];
        v645 = daeoOut[645];
        v646 = daeoOut[646];
        v647 = daeoOut[647];
        v648 = daeoOut[648];
        v649 = daeoOut[649];
        v650 = daeoOut[650];
        v651 = daeoOut[651];
        v652 = daeoOut[652];
        v653 = daeoOut[653];
        v654 = daeoOut[654];
        v655 = daeoOut[655];
        v656 = daeoOut[656];
        v657 = daeoOut[657];
        v658 = daeoOut[658];
        v659 = daeoOut[659];
        v660 = daeoOut[660];
        v661 = daeoOut[661];
        v662 = daeoOut[662];
        v663 = daeoOut[663];
        v664 = daeoOut[664];
        v665 = daeoOut[665];
        v666 = daeoOut[666];
        v667 = daeoOut[667];
        v668 = daeoOut[668];
        v669 = daeoOut[669];
        v670 = daeoOut[670];
        v671 = daeoOut[671];
        v672 = daeoOut[672];
        v673 = daeoOut[673];
        v674 = daeoOut[674];
        v675 = daeoOut[675];
        v676 = daeoOut[676];
        v677 = daeoOut[677];
        v678 = daeoOut[678];
        v679 = daeoOut[679];
        v680 = daeoOut[680];
        v681 = daeoOut[681];
        v682 = daeoOut[682];
        v683 = daeoOut[683];
        v684 = daeoOut[684];
        v685 = daeoOut[685];
        v686 = daeoOut[686];
        v687 = daeoOut[687];
        v688 = daeoOut[688];
        v689 = daeoOut[689];
        v690 = daeoOut[690];
        v691 = daeoOut[691];
        v692 = daeoOut[692];
        v693 = daeoOut[693];
        v694 = daeoOut[694];
        v695 = daeoOut[695];
        v696 = daeoOut[696];
        v697 = daeoOut[697];
        v698 = daeoOut[698];
        v699 = daeoOut[699];
        v700 = daeoOut[700];
        v701 = daeoOut[701];
        v702 = daeoOut[702];
        v703 = daeoOut[703];
        v704 = daeoOut[704];
        v705 = daeoOut[705];
        v706 = daeoOut[706];
        v707 = daeoOut[707];
        v708 = daeoOut[708];
        v709 = daeoOut[709];
        v710 = daeoOut[710];
        v711 = daeoOut[711];
        v712 = daeoOut[712];
        v713 = daeoOut[713];
        v714 = daeoOut[714];
        v715 = daeoOut[715];
        v716 = daeoOut[716];
        v717 = daeoOut[717];
        v718 = daeoOut[718];
        v719 = daeoOut[719];
        v720 = daeoOut[720];
        v721 = daeoOut[721];
        v722 = daeoOut[722];
        v723 = daeoOut[723];
        v724 = daeoOut[724];
        v725 = daeoOut[725];
        v726 = daeoOut[726];
        v727 = daeoOut[727];
        v728 = daeoOut[728];
        v729 = daeoOut[729];
        v730 = daeoOut[730];
        v731 = daeoOut[731];
        v732 = daeoOut[732];
        v733 = daeoOut[733];
        v734 = daeoOut[734];
        v735 = daeoOut[735];
        v736 = daeoOut[736];
        v737 = daeoOut[737];
        v738 = daeoOut[738];
        v739 = daeoOut[739];
        v740 = daeoOut[740];
        v741 = daeoOut[741];
        v742 = daeoOut[742];
        v743 = daeoOut[743];
        v744 = daeoOut[744];
        v745 = daeoOut[745];
        v746 = daeoOut[746];
        v747 = daeoOut[747];
        v748 = daeoOut[748];
        v749 = daeoOut[749];
        v750 = daeoOut[750];
        v751 = daeoOut[751];
        v752 = daeoOut[752];
        v753 = daeoOut[753];
        v754 = daeoOut[754];
        v755 = daeoOut[755];
        v756 = daeoOut[756];
        v757 = daeoOut[757];
        v758 = daeoOut[758];
        v759 = daeoOut[759];
        v760 = daeoOut[760];
        v761 = daeoOut[761];
        v762 = daeoOut[762];
        v763 = daeoOut[763];
        v764 = daeoOut[764];
        v765 = daeoOut[765];
        v766 = daeoOut[766];
        v767 = daeoOut[767];
        v768 = daeoOut[768];
        v769 = daeoOut[769];
        v770 = daeoOut[770];
        v771 = daeoOut[771];
        v772 = daeoOut[772];
        v773 = daeoOut[773];
        v774 = daeoOut[774];
        v775 = daeoOut[775];
        v776 = daeoOut[776];
        v777 = daeoOut[777];
        v778 = daeoOut[778];
        v779 = daeoOut[779];
        v780 = daeoOut[780];
        v781 = daeoOut[781];
        v782 = daeoOut[782];
        v783 = daeoOut[783];
        v784 = daeoOut[784];
        v785 = daeoOut[785];
        v786 = daeoOut[786];
        v787 = daeoOut[787];
        v788 = daeoOut[788];
        v789 = daeoOut[789];
        v790 = daeoOut[790];
        v791 = daeoOut[791];
        v792 = daeoOut[792];
        v793 = daeoOut[793];
        v794 = daeoOut[794];
        v795 = daeoOut[795];
        v796 = daeoOut[796];
        v797 = daeoOut[797];
        v798 = daeoOut[798];
        v799 = daeoOut[799];
        v800 = daeoOut[800];
        v801 = daeoOut[801];
        v802 = daeoOut[802];
        v803 = daeoOut[803];
        v804 = daeoOut[804];
        v805 = daeoOut[805];
        v806 = daeoOut[806];
        v807 = daeoOut[807];
        v808 = daeoOut[808];
        v809 = daeoOut[809];
        v810 = daeoOut[810];
        v811 = daeoOut[811];
        v812 = daeoOut[812];
        v813 = daeoOut[813];
        v814 = daeoOut[814];
        v815 = daeoOut[815];
        v816 = daeoOut[816];
        v817 = daeoOut[817];
        v818 = daeoOut[818];
        v819 = daeoOut[819];
        v820 = daeoOut[820];
        v821 = daeoOut[821];
        v822 = daeoOut[822];
        v823 = daeoOut[823];
        v824 = daeoOut[824];
        v825 = daeoOut[825];
        v826 = daeoOut[826];
        v827 = daeoOut[827];
        v828 = daeoOut[828];
        v829 = daeoOut[829];
        v830 = daeoOut[830];
        v831 = daeoOut[831];
        v832 = daeoOut[832];
        v833 = daeoOut[833];
        v834 = daeoOut[834];
        v835 = daeoOut[835];
        v836 = daeoOut[836];
        v837 = daeoOut[837];
        v838 = daeoOut[838];
        v839 = daeoOut[839];
        v840 = daeoOut[840];
        v841 = daeoOut[841];
        v842 = daeoOut[842];
        v843 = daeoOut[843];
        v844 = daeoOut[844];
        v845 = daeoOut[845];
        v846 = daeoOut[846];
        v847 = daeoOut[847];
        v848 = daeoOut[848];
        v849 = daeoOut[849];
        v850 = daeoOut[850];
        v851 = daeoOut[851];
        v852 = daeoOut[852];
        v853 = daeoOut[853];
        v854 = daeoOut[854];
        v855 = daeoOut[855];
        v856 = daeoOut[856];
        v857 = daeoOut[857];
        v858 = daeoOut[858];
        v859 = daeoOut[859];
        v860 = daeoOut[860];
        v861 = daeoOut[861];
        v862 = daeoOut[862];
        v863 = daeoOut[863];
        v864 = daeoOut[864];
        v865 = daeoOut[865];
        v866 = daeoOut[866];
        v867 = daeoOut[867];
        v868 = daeoOut[868];
        v869 = daeoOut[869];
        v870 = daeoOut[870];
        v871 = daeoOut[871];
        v872 = daeoOut[872];
        v873 = daeoOut[873];
        v874 = daeoOut[874];
        v875 = daeoOut[875];
        v876 = daeoOut[876];
        v877 = daeoOut[877];
        v878 = daeoOut[878];
        v879 = daeoOut[879];
        v880 = daeoOut[880];
        v881 = daeoOut[881];
        v882 = daeoOut[882];
        v883 = daeoOut[883];
        v884 = daeoOut[884];
        v885 = daeoOut[885];
        v886 = daeoOut[886];
        v887 = daeoOut[887];
        v888 = daeoOut[888];
        v889 = daeoOut[889];
        v890 = daeoOut[890];
        v891 = daeoOut[891];
        v892 = daeoOut[892];
        v893 = daeoOut[893];
        v894 = daeoOut[894];
        v895 = daeoOut[895];
        v896 = daeoOut[896];
        v897 = daeoOut[897];
        v898 = daeoOut[898];
        v899 = daeoOut[899];
        v900 = daeoOut[900];
        v901 = daeoOut[901];
        v902 = daeoOut[902];
        v903 = daeoOut[903];
        v904 = daeoOut[904];
        v905 = daeoOut[905];
        v906 = daeoOut[906];
        v907 = daeoOut[907];
        v908 = daeoOut[908];
        v909 = daeoOut[909];
        v910 = daeoOut[910];
        v911 = daeoOut[911];
        v912 = daeoOut[912];
        v913 = daeoOut[913];
        v914 = daeoOut[914];
        v915 = daeoOut[915];
        v916 = daeoOut[916];
        v917 = daeoOut[917];
        v918 = daeoOut[918];
        v919 = daeoOut[919];
        v920 = daeoOut[920];
        v921 = daeoOut[921];
        v922 = daeoOut[922];
        v923 = daeoOut[923];
        v924 = daeoOut[924];
        v925 = daeoOut[925];
        v926 = daeoOut[926];
        v927 = daeoOut[927];
        v928 = daeoOut[928];
        v929 = daeoOut[929];
        v930 = daeoOut[930];
        v931 = daeoOut[931];
        v932 = daeoOut[932];
        v933 = daeoOut[933];
        v934 = daeoOut[934];
        v935 = daeoOut[935];
        v936 = daeoOut[936];
        v937 = daeoOut[937];
        v938 = daeoOut[938];
        v939 = daeoOut[939];
        v940 = daeoOut[940];
        v941 = daeoOut[941];
        v942 = daeoOut[942];
        v943 = daeoOut[943];
        v944 = daeoOut[944];
        v945 = daeoOut[945];
        v946 = daeoOut[946];
        v947 = daeoOut[947];
        v948 = daeoOut[948];
        v949 = daeoOut[949];
        v950 = daeoOut[950];
        v951 = daeoOut[951];
        v952 = daeoOut[952];
        v953 = daeoOut[953];
        v954 = daeoOut[954];
        v955 = daeoOut[955];
        v956 = daeoOut[956];
        v957 = daeoOut[957];
        v958 = daeoOut[958];
        v959 = daeoOut[959];
        v960 = daeoOut[960];
        v961 = daeoOut[961];
        v962 = daeoOut[962];
        v963 = daeoOut[963];
        v964 = daeoOut[964];
        v965 = daeoOut[965];
        v966 = daeoOut[966];
        v967 = daeoOut[967];
        v968 = daeoOut[968];
        v969 = daeoOut[969];
        v970 = daeoOut[970];
        v971 = daeoOut[971];
        v972 = daeoOut[972];
        v973 = daeoOut[973];
        v974 = daeoOut[974];
        v975 = daeoOut[975];
        v976 = daeoOut[976];
        v977 = daeoOut[977];
        v978 = daeoOut[978];
        v979 = daeoOut[979];
        v980 = daeoOut[980];
        v981 = daeoOut[981];
        v982 = daeoOut[982];
        v983 = daeoOut[983];
        v984 = daeoOut[984];
        v985 = daeoOut[985];
        v986 = daeoOut[986];
        v987 = daeoOut[987];
        v988 = daeoOut[988];
        v989 = daeoOut[989];
        v990 = daeoOut[990];
        v991 = daeoOut[991];
        v992 = daeoOut[992];
        v993 = daeoOut[993];
        v994 = daeoOut[994];
        v995 = daeoOut[995];
        v996 = daeoOut[996];
        v997 = daeoOut[997];
        v998 = daeoOut[998];
        v999 = daeoOut[999];
        v1000 = daeoOut[1000];
        v1001 = daeoOut[1001];
        v1002 = daeoOut[1002];
        v1003 = daeoOut[1003];
        v1004 = daeoOut[1004];
        v1005 = daeoOut[1005];
        v1006 = daeoOut[1006];
        v1007 = daeoOut[1007];
        v1008 = daeoOut[1008];
        v1009 = daeoOut[1009];
        v1010 = daeoOut[1010];
        v1011 = daeoOut[1011];
        v1012 = daeoOut[1012];
        v1013 = daeoOut[1013];
        v1014 = daeoOut[1014];
        v1015 = daeoOut[1015];
        v1016 = daeoOut[1016];
        v1017 = daeoOut[1017];
        v1018 = daeoOut[1018];
        v1019 = daeoOut[1019];
        v1020 = daeoOut[1020];
        v1021 = daeoOut[1021];
        v1022 = daeoOut[1022];
        v1023 = daeoOut[1023];
        v1024 = daeoOut[1024];
        v1025 = daeoOut[1025];
        v1026 = daeoOut[1026];
        v1027 = daeoOut[1027];
        v1028 = daeoOut[1028];
        v1029 = daeoOut[1029];
        v1030 = daeoOut[1030];
        v1031 = daeoOut[1031];
        v1032 = daeoOut[1032];
        v1033 = daeoOut[1033];
        v1034 = daeoOut[1034];
        v1035 = daeoOut[1035];
        v1036 = daeoOut[1036];
        v1037 = daeoOut[1037];
        v1038 = daeoOut[1038];
        v1039 = daeoOut[1039];
        v1040 = daeoOut[1040];
        v1041 = daeoOut[1041];
        v1042 = daeoOut[1042];
        v1043 = daeoOut[1043];
        v1044 = daeoOut[1044];
        v1045 = daeoOut[1045];
        v1046 = daeoOut[1046];
        v1047 = daeoOut[1047];
        v1048 = daeoOut[1048];
        v1049 = daeoOut[1049];
        v1050 = daeoOut[1050];
        v1051 = daeoOut[1051];
        v1052 = daeoOut[1052];
        v1053 = daeoOut[1053];
        v1054 = daeoOut[1054];
        v1055 = daeoOut[1055];
        v1056 = daeoOut[1056];
        v1057 = daeoOut[1057];
        v1058 = daeoOut[1058];
        v1059 = daeoOut[1059];
        v1060 = daeoOut[1060];
        v1061 = daeoOut[1061];
        v1062 = daeoOut[1062];
        v1063 = daeoOut[1063];
        v1064 = daeoOut[1064];
        v1065 = daeoOut[1065];
        v1066 = daeoOut[1066];
        v1067 = daeoOut[1067];
        v1068 = daeoOut[1068];
        v1069 = daeoOut[1069];
        v1070 = daeoOut[1070];
        v1071 = daeoOut[1071];
        v1072 = daeoOut[1072];
        v1073 = daeoOut[1073];
        v1074 = daeoOut[1074];
        v1075 = daeoOut[1075];

      end partial_Ecoli_metabolicModel_Linear;

      partial model partial_Ecoli_metabolicModel_Quadratic
        // Here, DAEO related variable declarations
        constant Integer n=1152 "number of DAEO variables";
        constant Integer p=3 "number of DAEO inputs";
        constant Integer m=839 "number of DAEO equalities";
        constant Integer q=823 "number of DAEO inequalities";
        constant String params[p]={"b1","b2","b3"} "DAEO input variables";
        Real daeoIn[p] "DAEO input values (need to be provided by user)";
        constant String variables[n]={"v1","v2","v3","v4","v5","v6","v7","v8","v9","v10","v11","v12",
            "v13","v14","v15","v16","v17","v18","v19","v20","v21","v22","v23","v24","v25","v26",
            "v27","v28","v29","v30","v31","v32","v33","v34","v35","v36","v37","v38","v39","v40",
            "v41","v42","v43","v44","v45","v46","v47","v48","v49","v50","v51","v52","v53","v54",
            "v55","v56","v57","v58","v59","v60","v61","v62","v63","v64","v65","v66","v67","v68",
            "v69","v70","v71","v72","v73","v74","v75","v76","v77","v78","v79","v80","v81","v82",
            "v83","v84","v85","v86","v87","v88","v89","v90","v91","v92","v93","v94","v95","v96",
            "v97","v98","v99","v100","v101","v102","v103","v104","v105","v106","v107","v108","v109",
            "v110","v111","v112","v113","v114","v115","v116","v117","v118","v119","v120","v121",
            "v122","v123","v124","v125","v126","v127","v128","v129","v130","v131","v132","v133",
            "v134","v135","v136","v137","v138","v139","v140","v141","v142","v143","v144","v145",
            "v146","v147","v148","v149","v150","v151","v152","v153","v154","v155","v156","v157",
            "v158","v159","v160","v161","v162","v163","v164","v165","v166","v167","v168","v169",
            "v170","v171","v172","v173","v174","v175","v176","v177","v178","v179","v180","v181",
            "v182","v183","v184","v185","v186","v187","v188","v189","v190","v191","v192","v193",
            "v194","v195","v196","v197","v198","v199","v200","v201","v202","v203","v204","v205",
            "v206","v207","v208","v209","v210","v211","v212","v213","v214","v215","v216","v217",
            "v218","v219","v220","v221","v222","v223","v224","v225","v226","v227","v228","v229",
            "v230","v231","v232","v233","v234","v235","v236","v237","v238","v239","v240","v241",
            "v242","v243","v244","v245","v246","v247","v248","v249","v250","v251","v252","v253",
            "v254","v255","v256","v257","v258","v259","v260","v261","v262","v263","v264","v265",
            "v266","v267","v268","v269","v270","v271","v272","v273","v274","v275","v276","v277",
            "v278","v279","v280","v281","v282","v283","v284","v285","v286","v287","v288","v289",
            "v290","v291","v292","v293","v294","v295","v296","v297","v298","v299","v300","v301",
            "v302","v303","v304","v305","v306","v307","v308","v309","v310","v311","v312","v313",
            "v314","v315","v316","v317","v318","v319","v320","v321","v322","v323","v324","v325",
            "v326","v327","v328","v329","v330","v331","v332","v333","v334","v335","v336","v337",
            "v338","v339","v340","v341","v342","v343","v344","v345","v346","v347","v348","v349",
            "v350","v351","v352","v353","v354","v355","v356","v357","v358","v359","v360","v361",
            "v362","v363","v364","v365","v366","v367","v368","v369","v370","v371","v372","v373",
            "v374","v375","v376","v377","v378","v379","v380","v381","v382","v383","v384","v385",
            "v386","v387","v388","v389","v390","v391","v392","v393","v394","v395","v396","v397",
            "v398","v399","v400","v401","v402","v403","v404","v405","v406","v407","v408","v409",
            "v410","v411","v412","v413","v414","v415","v416","v417","v418","v419","v420","v421",
            "v422","v423","v424","v425","v426","v427","v428","v429","v430","v431","v432","v433",
            "v434","v435","v436","v437","v438","v439","v440","v441","v442","v443","v444","v445",
            "v446","v447","v448","v449","v450","v451","v452","v453","v454","v455","v456","v457",
            "v458","v459","v460","v461","v462","v463","v464","v465","v466","v467","v468","v469",
            "v470","v471","v472","v473","v474","v475","v476","v477","v478","v479","v480","v481",
            "v482","v483","v484","v485","v486","v487","v488","v489","v490","v491","v492","v493",
            "v494","v495","v496","v497","v498","v499","v500","v501","v502","v503","v504","v505",
            "v506","v507","v508","v509","v510","v511","v512","v513","v514","v515","v516","v517",
            "v518","v519","v520","v521","v522","v523","v524","v525","v526","v527","v528","v529",
            "v530","v531","v532","v533","v534","v535","v536","v537","v538","v539","v540","v541",
            "v542","v543","v544","v545","v546","v547","v548","v549","v550","v551","v552","v553",
            "v554","v555","v556","v557","v558","v559","v560","v561","v562","v563","v564","v565",
            "v566","v567","v568","v569","v570","v571","v572","v573","v574","v575","v576","v577",
            "v578","v579","v580","v581","v582","v583","v584","v585","v586","v587","v588","v589",
            "v590","v591","v592","v593","v594","v595","v596","v597","v598","v599","v600","v601",
            "v602","v603","v604","v605","v606","v607","v608","v609","v610","v611","v612","v613",
            "v614","v615","v616","v617","v618","v619","v620","v621","v622","v623","v624","v625",
            "v626","v627","v628","v629","v630","v631","v632","v633","v634","v635","v636","v637",
            "v638","v639","v640","v641","v642","v643","v644","v645","v646","v647","v648","v649",
            "v650","v651","v652","v653","v654","v655","v656","v657","v658","v659","v660","v661",
            "v662","v663","v664","v665","v666","v667","v668","v669","v670","v671","v672","v673",
            "v674","v675","v676","v677","v678","v679","v680","v681","v682","v683","v684","v685",
            "v686","v687","v688","v689","v690","v691","v692","v693","v694","v695","v696","v697",
            "v698","v699","v700","v701","v702","v703","v704","v705","v706","v707","v708","v709",
            "v710","v711","v712","v713","v714","v715","v716","v717","v718","v719","v720","v721",
            "v722","v723","v724","v725","v726","v727","v728","v729","v730","v731","v732","v733",
            "v734","v735","v736","v737","v738","v739","v740","v741","v742","v743","v744","v745",
            "v746","v747","v748","v749","v750","v751","v752","v753","v754","v755","v756","v757",
            "v758","v759","v760","v761","v762","v763","v764","v765","v766","v767","v768","v769",
            "v770","v771","v772","v773","v774","v775","v776","v777","v778","v779","v780","v781",
            "v782","v783","v784","v785","v786","v787","v788","v789","v790","v791","v792","v793",
            "v794","v795","v796","v797","v798","v799","v800","v801","v802","v803","v804","v805",
            "v806","v807","v808","v809","v810","v811","v812","v813","v814","v815","v816","v817",
            "v818","v819","v820","v821","v822","v823","v824","v825","v826","v827","v828","v829",
            "v830","v831","v832","v833","v834","v835","v836","v837","v838","v839","v840","v841",
            "v842","v843","v844","v845","v846","v847","v848","v849","v850","v851","v852","v853",
            "v854","v855","v856","v857","v858","v859","v860","v861","v862","v863","v864","v865",
            "v866","v867","v868","v869","v870","v871","v872","v873","v874","v875","v876","v877",
            "v878","v879","v880","v881","v882","v883","v884","v885","v886","v887","v888","v889",
            "v890","v891","v892","v893","v894","v895","v896","v897","v898","v899","v900","v901",
            "v902","v903","v904","v905","v906","v907","v908","v909","v910","v911","v912","v913",
            "v914","v915","v916","v917","v918","v919","v920","v921","v922","v923","v924","v925",
            "v926","v927","v928","v929","v930","v931","v932","v933","v934","v935","v936","v937",
            "v938","v939","v940","v941","v942","v943","v944","v945","v946","v947","v948","v949",
            "v950","v951","v952","v953","v954","v955","v956","v957","v958","v959","v960","v961",
            "v962","v963","v964","v965","v966","v967","v968","v969","v970","v971","v972","v973",
            "v974","v975","v976","v977","v978","v979","v980","v981","v982","v983","v984","v985",
            "v986","v987","v988","v989","v990","v991","v992","v993","v994","v995","v996","v997",
            "v998","v999","v1000","v1001","v1002","v1003","v1004","v1005","v1006","v1007","v1008",
            "v1009","v1010","v1011","v1012","v1013","v1014","v1015","v1016","v1017","v1018","v1019",
            "v1020","v1021","v1022","v1023","v1024","v1025","v1026","v1027","v1028","v1029","v1030",
            "v1031","v1032","v1033","v1034","v1035","v1036","v1037","v1038","v1039","v1040","v1041",
            "v1042","v1043","v1044","v1045","v1046","v1047","v1048","v1049","v1050","v1051","v1052",
            "v1053","v1054","v1055","v1056","v1057","v1058","v1059","v1060","v1061","v1062","v1063",
            "v1064","v1065","v1066","v1067","v1068","v1069","v1070","v1071","v1072","v1073","v1074",
            "v1075","tr_126_1","tr_151_1","tr_151_2","tr_151_3","tr_151_4","tr_151_5","tr_180_1",
            "tr_200_1","tr_200_2","tr_200_3","tr_200_4","tr_200_5","tr_200_6","tr_200_7","tr_200_8",
            "tr_229_1","tr_229_2","tr_231_1","tr_231_2","tr_388_1","tr_388_2","tr_424_1","tr_424_2",
            "tr_424_3","tr_424_4","tr_424_5","tr_424_6","tr_424_7","tr_424_8","tr_424_9","tr_424_10",
            "tr_424_11","tr_424_12","tr_427_1","tr_427_2","tr_427_3","tr_427_4","tr_427_5","tr_427_6",
            "tr_427_7","tr_427_8","tr_427_9","tr_427_10","tr_427_11","tr_427_12","tr_427_13","tr_427_14",
            "tr_427_15","tr_427_16","tr_427_17","tr_428_1","tr_428_2","tr_428_3","tr_428_4","tr_428_5",
            "tr_536_1","tr_536_2","tr_536_3","tr_538_1","tr_538_2","tr_538_3","tr_539_1","tr_539_2",
            "tr_540_1","tr_540_2","tr_542_1","tr_542_2","tr_589_1","tr_589_2","tr_589_3","tr_589_4",
            "tr_589_5","tr_599_1","tr_599_2","tr_599_3","tr_623_1","tr_623_2"}
          "DAEO variables";
        constant String objective="-v150" "DAEO objective function";
        constant String equalities[m]={"v719 - 1.0*v453 - 1.0*v486 - 1.0*v75","0.02*v811 - 0.02*v205 + 0.02*v820",
            "v1 + v643","- 1.0*v1 - 1.0*v287","v485 + v828","v659 - 1.0*v157","v157 - 1.0*v288",
            "v467 - 1.0*v807 - 1.0*v808 + v873","v606 - 1.0*v227","v227 - 1.0*v228","v228 - 3.0*v284",
            "v232 - 1.0*v231","v629 - 1.0*v223","v630 - 1.0*v224","-1.0*v261","v869 - 1.0*v67","- 1.0*v247 - 1.0*v248 - 1.0*v249",
            "v564 - 1.0*v241","v934 - 1.0*v208","v208 - 1.0*v207 + v209 - 1.0*v1029","- 1.0*v209 - 1.0*v289",
            "v33 - 1.0*v630","v991 - 1.0*v523","v867 - 1.0*v613","v219 - 1.0*v245","v218 - 1.0*v277 + v278",
            "v94 + v217 - 1.0*v218 + v714","- 1.0*v217 - 1.0*v290","v216 - 1.0*v220","v473 - 1.0*v216",
            "- 1.0*v2 - 1.0*v3","v247 - 1.0*v5 - 1.0*v4","v717 - 1.0*v263","v235 - 1.0*v102 + v257 + v258 + v449 - 1.0*v466 - 1.0*v536 - 1.0*v601 - 1.0*v732 - 1.0*v733 + v1002 + v1003",
            "v466 - 1.0*v258 - 1.0*v449 - 1.0*v257 + v536 + v601 + v732 + v733 - 1.0*v1002 - 1.0*v1003",
            "v268 - 1.0*v856 + v891 + v893 + v895 + v1005","v856 - 1.0*v264","v496 + v521 + v591 - 1.0*v1023",
            "v626 - 1.0*v627","v250 + v251 - 1.0*v1049","v844 - 1.0*v1009","v681 - 1.0*v701","v683 - 1.0*v681",
            "v271 - 1.0*v694","v692 - 1.0*v691","v992 - 1.0*v788 - 1.0*v33","v801 - 1.0*v793","v798 - 1.0*v794",
            "v796 - 1.0*v254","v794 - 1.0*v796","v793 - 1.0*v798","v800 - 1.0*v801","v165 - 1.0*v692",
            "- 1.0*v283 - 1.0*v831","-1.0*v830","v945 - 1.0*v960","v858 + v1026","- 1.0*v625 - 1.0*v626",
            "v627 + v628","v625 - 1.0*v795","v261","-1.0*v637","v245 - 1.0*v244","v244 - 1.0*v948",
            "v573 - 1.0*v6","- 1.0*v291 - 1.0*v573","v633 - 1.0*v1028 - 1.0*v1032","v590 - 1.0*v7",
            "- 1.0*v292 - 1.0*v590","v613 - 1.0*v1019 - 1.0*v1021","v223 - 1.0*v628 - 1.0*v717 + v1062 - 1.0*v1065",
            "v224 + v614","v572 - 1.0*v800","v529 - 1.0*v826 - 1.0*v828 + v831","v826 - 1.0*v880",
            "v878 - 1.0*v169","v19 - 1.0*v18 + v20 - 1.0*v508 + v510","v508 - 1.0*v293 - 1.0*v20",
            "v887 - 1.0*v19","v49 - 1.0*v242","v50 - 1.0*v49","-1.0*v586","v103 + v586 - 1.0*v844",
            "v694 - 1.0*v165","v799 - 1.0*v587","v997","v170 - 1.0*v572","v8","-1.0*v577","v795 - 1.0*v647",
            "v577 + v997 - 1.0*v1009","v119 + v127","v275 - 1.0*v821","v845 - 1.0*v857","v857 - 1.0*v852",
            "v843 - 1.0*v907 + v908","- 1.0*v77 - 1.0*v869","v462 - 2.0*v850","v110 - 1.0*v843",
            "v241 - 1.0*v110","v76 + v77","v248 - 1.0*v9 + v249 - 1.0*v608 - 1.0*v609","v472 - 1.0*v496 + v497",
            "v723 - 1.0*v722","v722 - 1.0*v688","v955 - 1.0*v718","v721 - 1.0*v697 - 0.05*v150",
            "v718 - 1.0*v723","v758 - 1.0*v928","v236 - 1.0*v588","v588 - 1.0*v242","v548 - 1.0*v547 - 1.0*v278 + v829",
            "v470 - 1.0*v829","v107 - 1.0*v101","v45 - 1.0*v12 - 1.0*v13 - 1.0*v14 - 1.0*v15 - 1.0*v40 - 1.0*v11 + 4.0*v151 + 5.0*v152 + 5.0*v153 + 6.0*v154 + 6.0*v155 + 7.0*v156 + v279 + v280 + v281 + v282 + v631 + v633 - 1.0*v684 + 2.0*v814 + v1028 + v1032",
            "v952","v976","v21 + v22 + v434","v40 - 1.0*v631 + v666","v21 - 1.0*v34 + v41 - 1.0*v46 + v47 + v69 + v90 + v148 + v172 + v190 + v724 + v847 + v1047",
            "- 1.0*v47 - 1.0*v294","v23 - 1.0*v21","- 1.0*v23 - 1.0*v295","v25 - 1.0*v24 - 1.0*v90 + v264 + v285 + v587 + v990",
            "- 1.0*v25 - 1.0*v296","v31 + v72","v44 - 1.0*v72 - 1.0*v724","v459 - 1.0*v1034","v30 - 1.0*v69 + v99",
            "- 1.0*v30 - 1.0*v297","v32 - 1.0*v31","v39 - 1.0*v100","- 1.0*v37 - 1.0*v298","v37 - 1.0*v99 + v100",
            "v38 - 1.0*v39","- 1.0*v38 - 1.0*v299","-1.0*v42","v42","- 1.0*v41 - 1.0*v44","v938 - 1.0*v190",
            "v631 - 1.0*v152 - 1.0*v153 - 1.0*v154 - 1.0*v155 - 1.0*v156 - 1.0*v151 + v632","v34 + v884",
            "v52 - 1.0*v51 - 1.0*v64 + v74 + v104 + v718 + v890 + v891","- 1.0*v52 - 1.0*v300","v60 - 1.0*v59 - 1.0*v48 + v61 + v73 + v773 - 1.0*v890",
            "- 1.0*v60 - 1.0*v61 - 1.0*v301","v159 - 1.0*v62","v62 - 1.0*v26","v63 + v161","v503 - 1.0*v499",
            "v542 - 1.0*v70","v70 - 3.0*v658","v26 - 1.0*v63","v114 - 1.0*v71","0.02*v841 - 0.02*v656 - 0.02*v654",
            "0.02*v840 - 0.02*v655 - 0.02*v653","0.02*v839 - 0.02*v657 - 0.02*v652","v42 - 1.0*v73 - 1.0*v74 + v102 + v254 + v793 + v794 + 2.0*v1050",
            "v563 - 1.0*v260","v67 - 1.0*v75 + v611","v866 - 1.0*v103 - 1.0*v76","v79 - 1.0*v44 - 1.0*v78 - 1.0*v18 - 1.0*v84 - 1.0*v133 + v511 - 1.0*v515 + v597 + v604 - 1.0*v614 + v647 + v792 - 1.0*v803 - 1.0*v806 - 1.0*v835 + v880 - 1.0*v887 - 1.0*v935 - 1.0*v951 - 1.0*v976 + v979 - 1.0*v1026 + v1049 - 1.0*v1062",
            "- 1.0*v79 - 1.0*v302","v125 - 1.0*v810","v81 - 2.0*v80 + v82 - 1.0*v83 - 1.0*v201 + v206",
            "- 1.0*v206 - 1.0*v303","v86 - 1.0*v82 - 1.0*v84 - 1.0*v85 - 1.0*v81 + v87 - 1.0*v107 + v147 - 0.488*v150 + v997 - 1.0*v1037 - 1.0*v1065",
            "- 1.0*v86 - 1.0*v87 - 1.0*v304","v80 - 1.0*v1046","v35 - 1.0*v629","v93 - 1.0*v92",
            "- 1.0*v93 - 1.0*v305","v92 - 1.0*v91","- 1.0*v94 - 1.0*v972","v695 - 1.0*v57 - 1.0*v101 - 1.0*v102 - 1.0*v254 - 1.0*v42 - 1.0*v793 - 1.0*v794 - 2.0*v1050",
            "v57 - 1.0*v955","v101","v727 - 1.0*v306","v106 - 1.0*v105","-1.0*v108","-1.0*v109",
            "0.02*v655 + 0.02*v656 + 0.02*v657","-1.0*v45","v931 - 1.0*v65","v10 - 1.0*v636","v112 - 1.0*v111 + v113",
            "- 1.0*v112 - 1.0*v113 - 1.0*v307","-1.0*v17","v115 - 1.0*v114 + v116 + v118 - 1.0*v138 - 0.281*v150",
            "- 1.0*v115 - 1.0*v118 - 1.0*v308","v117 - 1.0*v116","v121 - 1.0*v120 + v122 + v123 + v124 - 0.229*v150",
            "- 1.0*v123 - 1.0*v124 - 1.0*v309","v120 - 1.0*v117 - 1.0*v68 - 1.0*v121 - 1.0*v122 - 1.0*v125 - 1.0*v126 - 1.0*v127 - 1.0*v128 - 1.0*v129 - 1.0*v130 - 1.0*v131 - 1.0*v132 - 1.0*v133 + v134 + v135 + v136 + v137 - 0.229*v150 - 1.0*v869",
            "- 1.0*v134 - 1.0*v135 - 1.0*v136 - 1.0*v137 - 1.0*v310","v594 - 1.0*v232 - 1.0*v119",
            "-1.0*v178","- 1.0*v142 - 1.0*v143","v148 - 1.0*v434","v145 + v146 + v147","- 1.0*v145 - 1.0*v146",
            "v149 - 1.0*v148","- 1.0*v149 - 1.0*v311","v58","v126 + v239","-1.0*v159","v160 - 1.0*v161",
            "- 1.0*v160 - 1.0*v312","v162 - 1.0*v126 + v163 - 1.0*v789","v199 + 2.0*v658 - 1.0*v744 - 1.0*v920",
            "0.02*v210 - 0.02*v164 - 0.02*v834 - 0.02*v882","v820 - 2.0*v658","v861 - 1.0*v240",
            "v171 - 1.0*v229","v166 + v167 + v551","- 1.0*v166 - 1.0*v167 - 1.0*v313","v169 - 1.0*v106 - 1.0*v168 - 1.0*v50 - 1.0*v170 - 1.0*v605",
            "-1.0*v171","v173 - 1.0*v172 - 1.0*v43 + v182","- 1.0*v173 - 1.0*v314","v789 - 1.0*v117",
            "v634 - 3.0*v658 - 1.0*v715 - 1.0*v716","0.02*v174 - 0.00012899999999999999140964934696285*v150",
            "v164 - 1.0*v175 + v196 - 1.0*v199 + 3.0*v658 + v692 + v715 + v716 - 1.0*v770 + v779 - 1.0*v820 + v834 + v857 + v882",
            "- 1.0*v176 - 1.0*v315","v1052 - 1.0*v177","v181 - 1.0*v179 - 1.0*v178","- 1.0*v181 - 1.0*v316",
            "v178 + v179 - 1.0*v180","v175 - 1.0*v183 + v184","- 1.0*v184 - 1.0*v317","v179","v180 - 1.0*v179",
            "v185 - 0.126*v150 - 1.0*v210 - 1.0*v634 - 1.0*v694 + v744 - 1.0*v779 - 1.0*v857 - 1.0*v924",
            "-1.0*v186","v188 - 1.0*v187","- 1.0*v188 - 1.0*v318","v190 - 0.087*v150 - 1.0*v189 - 1.0*v147 + v192 - 1.0*v509 - 1.0*v857 - 1.0*v950 - 1.0*v997",
            "- 1.0*v192 - 1.0*v319","v950 - 1.0*v191","v197 - 1.0*v196 - 1.0*v195 + v198 + v770",
            "- 1.0*v197 - 1.0*v198 - 1.0*v320","v204 - 1.0*v202 + v772 - 1.0*v891","- 1.0*v204 - 1.0*v321",
            "v203 - 1.0*v749 + v918","v780 - 1.0*v772 - 1.0*v203","v101 - 1.0*v212","v749 - 0.0247*v150 - 1.0*v780 + v922",
            "v211 - 1.0*v907","v68 - 1.0*v66","v200 - 1.0*v748 + v920","v778 - 1.0*v769 - 1.0*v200",
            "v748 - 1.0*v213 - 0.0254*v150 - 1.0*v778 + v924","v215 - 1.0*v214 + v769","- 1.0*v215 - 1.0*v322",
            "v151 - 1.0*v279 - 1.0*v633","v221 - 1.0*v746 + v919","v776 - 1.0*v774 - 1.0*v221","v222 + v774 + v784 - 1.0*v893",
            "- 1.0*v222 - 1.0*v323","v746 - 0.0254*v150 - 1.0*v776 - 1.0*v784 + v923","v226 - 1.0*v225 + v430 + v528",
            "- 1.0*v226 - 1.0*v324","v225 + v435 + v439 + v463 + v464 + v465 + v466 - 1.0*v699 - 1.0*v905 + v917 + v984 - 1.0*v1010",
            "v6 + v229 - 1.0*v230","v234 - 1.0*v233 + v1006","v764 - 1.0*v235","v259 - 1.0*v236",
            "- 1.0*v237 - 1.0*v238 - 1.0*v239","v260 - 1.0*v259","v7 + v240 - 1.0*v589","v242 - 1.0*v234",
            "v912 - 1.0*v243","v202 + v246 - 1.0*v895","- 1.0*v246 - 1.0*v325","v688 - 1.0*v251 - 1.0*v250",
            "-1.0*v758","v907 - 2.0*v908","v253 - 1.0*v252 + v623","v255 + v257","v256 + v258 - 1.0*v326",
            "- 1.0*v255 - 1.0*v257","- 1.0*v256 - 1.0*v258 - 1.0*v327","v757 - 1.0*v736","v885 - 1.0*v262",
            "v1050 - 1.0*v946","v212 - 1.0*v147","v978 - 1.0*v16","v979 - 1.0*v978","v982 - 1.0*v980 - 1.0*v979",
            "v980 - 1.0*v981","v16 + v266 - 1.0*v745","v461 - 1.0*v982","v981","v782 - 1.0*v771 - 1.0*v266 + v1004 + v1006",
            "v745 - 1.0*v461 - 0.0247*v150 - 1.0*v782","v921 - 1.0*v747 + v1057","v267 + v270 - 1.0*v765 - 1.0*v1006 - 1.0*v1057",
            "v214 - 1.0*v267 - 1.0*v268 + v269 + v765","- 1.0*v269 - 1.0*v328","v213 - 1.0*v270 + v747 + v925",
            "v272 - 1.0*v271 + v273 - 1.0*v817 - 1.0*v997","-1.0*v273","v973 - 1.0*v275 - 1.0*v219 - 1.0*v999",
            "v276","v611 - 1.0*v612","v284","v552 - 1.0*v285","v53 + v286","- 1.0*v286 - 1.0*v329",
            "v451 - 1.0*v450","v436 - 1.0*v430 + v452 + v469 - 1.0*v490 + v665 + v677 - 1.0*v822 + v827 + v932 + v973 + v999",
            "v446 - 1.0*v201 - 6.0*v431 - 7.0*v432 - 8.0*v433 - 1.0*v434 - 0.00001*v150 - 1.0*v873 - 1.0*v965 + v966",
            "v201 + 6.0*v431 + 7.0*v432 + 8.0*v433 + v434 + v873 + v965 - 1.0*v966","v438 - 1.0*v439",
            "v437 - 1.0*v438","v450 - 1.0*v436 - 1.0*v435 + v822","v443 - 1.0*v440 - 1.0*v947","- 1.0*v330 - 1.0*v443",
            "v486 + v487 - 1.0*v871","v906 - 1.0*v446","2.0*v103 + v211 + v250 + v251 - 1.0*v441 - 1.0*v442 - 1.0*v445 + v447 + v453 - 1.0*v487 + v563 + v564 + v788 + v824",
            "- 1.0*v331 - 1.0*v447","v871 - 1.0*v866","v75 + v617","v556 - 1.0*v576 - 1.0*v791 - 1.0*v1040",
            "v444 - 1.0*v1072","- 1.0*v332 - 1.0*v451 - 1.0*v452","v569 + v679","v454","- 1.0*v333 - 1.0*v454",
            "v455 - 1.0*v437","- 1.0*v334 - 1.0*v455","v66 + v67 + v116 - 1.0*v130 + v132 - 1.0*v448 - 1.0*v449 - 1.0*v456 + v457 + v458 + v584 + v965 + v967",
            "- 1.0*v335 - 1.0*v457 - 1.0*v458 - 1.0*v967","v495 - 1.0*v461 - 1.0*v479 - 1.0*v481 - 1.0*v460 - 1.0*v503 + v707 + v708 + v709 - 1.0*v832 + v1044",
            "v220 + v264 - 1.0*v272 + v277 + v430 + v435 - 1.0*v485 - 1.0*v973 + v984 + v998 + v999 + v1010 + v1019 + v1021",
            "v654 - 1.0*v551 + v656","v653 - 1.0*v552 + v655","v652 - 1.0*v554 + v657","-1.0*v555",
            "-1.0*v553","v17 + v444 - 1.0*v470 + v471 + v501 + v578 - 1.0*v827 + v832 + v1012 - 1.0*v1014",
            "- 1.0*v336 - 1.0*v471","v476 - 1.0*v1044","v477 - 1.0*v476 + v482 + v483 + v640","- 1.0*v337 - 1.0*v482 - 1.0*v483",
            "v475 - 1.0*v472","- 1.0*v338 - 1.0*v475","v474 - 1.0*v473","- 1.0*v339 - 1.0*v474",
            "v478 - 1.0*v506","- 1.0*v340 - 1.0*v478","v480 - 1.0*v570","- 1.0*v341 - 1.0*v480",
            "- 1.0*v459 - 1.0*v825","v69 - 1.0*v469 + v484 + v490 + v825","- 1.0*v342 - 1.0*v484",
            "v865 - 1.0*v487 - 1.0*v486","v178 - 1.0*v181","v181 - 1.0*v343","v236 - 1.0*v488","v55 + v68 + v196 + v492 + 2.0*v550 - 1.0*v676 - 1.0*v742 - 1.0*v919 + v931 + v1058",
            "v541 - 1.0*v489","v549","v676 - 1.0*v541","v489 - 1.0*v549","v95 + v96 + v97 + v98 + v460 + v477 + v502 - 1.0*v578 + v640 + v702 + v703 + v704 + v705 + v706 + v1012 + 2.0*v1015 + v1072",
            "2.0*v1016 - 1.0*v493 - 1.0*v501 - 1.0*v502 - 1.0*v344","v2 + v3 + v9 + v494 - 1.0*v548",
            "v493 - 1.0*v345 - 1.0*v494","v498 - 1.0*v497","- 1.0*v346 - 1.0*v498","v500 - 1.0*v569",
            "- 1.0*v347 - 1.0*v500","v504 - 1.0*v106 - 1.0*v121 - 0.25*v150 - 1.0*v163 - 1.0*v185 - 1.0*v490 - 1.0*v50 + v505 - 1.0*v512 - 1.0*v513 - 1.0*v515 - 1.0*v546 - 1.0*v611 - 1.0*v871",
            "- 1.0*v348 - 1.0*v505","v516 - 1.0*v462","v507 - 1.0*v468","v468 - 1.0*v467 + v724 + v803",
            "- 1.0*v514 - 1.0*v1036","- 1.0*v349 - 1.0*v508 - 1.0*v518 - 1.0*v519 - 1.0*v520","v509 - 1.0*v562",
            "v517 - 1.0*v516","v534 - 1.0*v531 - 1.0*v532 - 2.0*v521 + v535 + v536 + v607 - 1.0*v667 + v1045",
            "v491 - 0.582*v150 + v523 - 1.0*v530 + v540 - 1.0*v562 - 1.0*v865 + v990","- 1.0*v350 - 1.0*v540",
            "v522 - 1.0*v88","- 1.0*v351 - 1.0*v522","v142 + v143 + v524 + v525","- 1.0*v352 - 1.0*v524 - 1.0*v525",
            "v526 - 1.0*v464 - 1.0*v465 - 1.0*v466 - 1.0*v463 + v527 + v538 + v551 + v552 + v553 + v554 + v555 - 1.0*v814 - 1.0*v834",
            "- 1.0*v353 - 1.0*v526 - 1.0*v527","v592 - 1.0*v529 + v593 + v1023","v88 + v174 - 1.0*v528 - 1.0*v537 - 1.0*v538 + v554",
            "v537 - 1.0*v354","v488 + v531 + v532 + v533 - 1.0*v534 - 1.0*v535 - 1.0*v536 + v830",
            "- 1.0*v355 - 1.0*v533","v499 - 1.0*v495 - 0.154*v150","v543 - 1.0*v544","v544 - 1.0*v542",
            "v929 - 1.0*v543","v63 - 1.0*v492 - 1.0*v545 + v546 + v557 + v566 - 1.0*v775 + v777",
            "-1.0*v550","v252 - 1.0*v556","v558 - 1.0*v557 + v775 + v785 - 1.0*v892","- 1.0*v356 - 1.0*v558",
            "-1.0*v561","v539 + v559 - 1.0*v560 + 2.0*v561 + v562 - 1.0*v650","v742 - 1.0*v55 - 1.0*v68 - 0.203*v150 - 1.0*v196 - 1.0*v563 - 1.0*v564 - 1.0*v26 - 1.0*v777 - 1.0*v785 - 1.0*v923 - 1.0*v931 - 1.0*v1058",
            "v560 - 1.0*v559","v567 - 1.0*v566 - 1.0*v565 + v568 + v892 + v893","- 1.0*v357 - 1.0*v567 - 1.0*v568",
            "v445 - 1.0*v599 - 1.0*v600 - 1.0*v601","v691 - 1.0*v624 - 1.0*v253","v131 - 2.0*v158 + v816 + v819 + v897 + v956",
            "v256 + v258 - 1.0*v358 - 1.0*v493 - 1.0*v571 - 1.0*v727 + v1001 + v1003 - 1.0*v1016",
            "v189 - 1.0*v190 - 1.0*v970","v574 - 1.0*v76 - 1.0*v163 - 1.0*v187 - 1.0*v27","v73 + v191 - 1.0*v697 + v912",
            "v575 - 1.0*v432 - 1.0*v13 + 0.36*v652 + 0.36*v653 + 0.36*v654 + 0.36*v839 + 0.36*v840 + 0.36*v841",
            "- 1.0*v360 - 1.0*v575","0.07*v652 - 1.0*v14 + 0.07*v653 + 0.07*v654 + 0.07*v839 + 0.07*v840 + 0.07*v841",
            "v14 + v155 - 1.0*v281 - 0.14*v814","v576","v579 - 0.09*v150 + v581 + v582","- 1.0*v361 - 1.0*v581 - 1.0*v582",
            "v597 - 1.0*v580","v580 - 1.0*v579","v589 - 1.0*v583","v230 - 1.0*v584","v585 - 1.0*v1051",
            "v243","- 1.0*v594 - 1.0*v595 - 1.0*v596","- 1.0*v591 - 1.0*v592 - 1.0*v593","v17","v51 - 1.0*v598 + v603 + v894 + v895",
            "- 1.0*v362 - 1.0*v603","v128 + v129 + v130 + v131 - 1.0*v905","v605 - 1.0*v606 - 1.0*v945",
            "v43 - 1.0*v604 - 1.0*v607","v4 + v5 + v608 + v609 + v610","- 1.0*v363 - 1.0*v610","v56",
            "v615 - 1.0*v614 - 0.276*v150 + v616","- 1.0*v364 - 1.0*v615 - 1.0*v616","v612 - 1.0*v597",
            "v545 - 1.0*v68 + v598 - 1.0*v617 - 1.0*v618 + v620 - 1.0*v767","v619 + v1018 - 1.0*v1020 + v1021",
            "- 1.0*v365 - 1.0*v619","v555 + v700","v48 - 1.0*v620 + v621 + v622 + v767 - 1.0*v894",
            "- 1.0*v366 - 1.0*v621 - 1.0*v622","v624 - 1.0*v556 - 1.0*v623 - 1.0*v252 - 5.0*v791 - 8.0*v1040",
            "-1.0*v56","v638 + v639","- 1.0*v367 - 1.0*v638 - 1.0*v639","v279 - 1.0*v280","v716 - 1.0*v281 - 1.0*v279",
            "v281 - 1.0*v282","v636 - 1.0*v635","v635 - 1.0*v634","v715 - 1.0*v716","v274 + v539 - 1.0*v645 - 1.0*v646",
            "- 1.0*v274 - 1.0*v368","v641 + v642 - 1.0*v662 - 1.0*v663 + v664","- 1.0*v369 - 1.0*v664",
            "v439 - 1.0*v641 - 1.0*v642 - 1.0*v643 + v917","v644 - 1.0*v640","- 1.0*v370 - 1.0*v644",
            "v647 - 0.428*v150 + v648 + v649","- 1.0*v371 - 1.0*v648 - 1.0*v649","v650 - 1.0*v539",
            "v280 - 1.0*v658","v282","v983 - 1.0*v715","v651 - 1.0*v983","v1061 - 1.0*v651","v658 - 0.0084*v150",
            "v157 - 0.326*v150 + v207 - 1.0*v659 + v660 + v661","- 1.0*v157 - 1.0*v372 - 1.0*v660 - 1.0*v661",
            "v684 - 5.0*v152 - 5.0*v153 - 6.0*v154 - 6.0*v155 - 7.0*v156 - 1.0*v631 - 1.0*v632 - 1.0*v633 - 1.0*v666 - 4.0*v151",
            "v456 + v667 + v674 + v675 - 1.0*v685 - 1.0*v686 - 1.0*v687 - 1.0*v689 - 1.0*v690","- 1.0*v373 - 1.0*v674 - 1.0*v675",
            "v27 - 1.0*v684","v673","v672 - 1.0*v96 - 1.0*v97 - 1.0*v98 - 1.0*v95 + v702","- 1.0*v374 - 1.0*v672 - 1.0*v673",
            "v98 - 1.0*v706 - 1.0*v709","v97 - 1.0*v98 + v668 - 1.0*v705 + v706 - 1.0*v708 + v709",
            "- 1.0*v375 - 1.0*v668","v96 - 1.0*v97 + v669 - 1.0*v704 + v705 - 1.0*v707 + v708","- 1.0*v376 - 1.0*v669",
            "v670 - 1.0*v95 - 1.0*v702 + v703","- 1.0*v377 - 1.0*v670","v95 - 1.0*v96 + v671 - 1.0*v703 + v704 + v707",
            "- 1.0*v378 - 1.0*v671","- 1.0*v676 - 1.0*v842","v678 - 1.0*v677 + v680 + v842","- 1.0*v379 - 1.0*v678",
            "- 1.0*v380 - 1.0*v680","- 1.0*v679 - 1.0*v714","v693 - 1.0*v477","- 1.0*v381 - 1.0*v693",
            "v696","- 1.0*v382 - 1.0*v696","v697 - 1.0*v695 - 0.146*v150 + v698 + v1049","- 1.0*v383 - 1.0*v698",
            "v720 - 1.0*v719","-1.0*v700","v701 - 1.0*v682","v491 + v530 - 1.0*v717 - 1.0*v720 - 1.0*v721 - 1.0*v1006",
            "v712 - 1.0*v711","v711 - 1.0*v710","v713 - 1.0*v665","- 1.0*v384 - 1.0*v713","v129 + v238 - 1.0*v255 - 1.0*v256 + v442 - 1.0*v448 + v465 + v535 + v600 + v663 + v687 + v728 + v731 - 1.0*v762 - 1.0*v1000 - 1.0*v1001",
            "v102 - 1.0*v129 - 1.0*v238 + v255 + v256 - 1.0*v442 + v448 - 1.0*v465 - 1.0*v535 - 1.0*v600 - 1.0*v663 - 1.0*v687 - 1.0*v728 - 1.0*v731 + v762 + v1000 + v1001",
            "v699 - 1.0*v650","v11 + v152 - 1.0*v280 - 1.0*v282 - 0.04*v814","v953","v520 - 1.0*v739 - 2.0*v740 - 1.0*v741 + v846 + v876 + v942 + v996",
            "v739 - 1.0*v520 - 1.0*v385 + 2.0*v740 + v741 - 1.0*v846 - 1.0*v876 - 1.0*v942 - 1.0*v996",
            "v725 - 1.0*v738 + v756 + v758","- 1.0*v386 - 1.0*v725","- 1.0*v387 - 1.0*v727","v753 + v755 - 1.0*v756",
            "- 1.0*v388 - 1.0*v750","v738 + v752 - 1.0*v757 - 1.0*v758 + v759","v726 - 1.0*v751 - 1.0*v752 - 1.0*v753 + v754",
            "v727 - 1.0*v389 - 1.0*v754 - 1.0*v755","v760 + v761 + v762 - 1.0*v763 - 1.0*v786","v763 - 1.0*v760 - 1.0*v390",
            "v763 - 1.0*v762 - 1.0*v761","- 1.0*v391 - 1.0*v763","v158 - 1.0*v7 - 1.0*v131 - 1.0*v6 - 1.0*v171 - 1.0*v177 - 0.5*v193 - 0.5*v194 - 1.0*v230 - 1.0*v250 - 1.0*v589 + v787 - 0.5*v796 - 0.5*v798 - 0.5*v801 - 1.0*v816 - 1.0*v819 - 1.5*v860 - 1.0*v861 - 1.0*v897 + v956 - 1.0*v976",
            "- 1.0*v392 - 1.0*v787","-2.0*v956","v133 + v172 - 1.0*v182 - 1.0*v683 + v685 + v686 + v687 + v851 - 1.0*v853 + v974",
            "v790 - 1.0*v433","- 1.0*v393 - 1.0*v790","0.5*v652 - 1.0*v15 + 0.5*v653 + 0.5*v654 + 0.5*v839 + 0.5*v840 + 0.5*v841",
            "v791 - 1.0*v572 - 1.0*v235","v15 + v156 - 1.0*v814","v821 - 1.0*v792","v583 + v584 - 1.0*v799",
            "v41 - 1.0*v115 - 1.0*v789 - 1.0*v802 - 1.0*v803 + v804 - 1.0*v886","v115 - 1.0*v394 - 1.0*v804 + v886",
            "- 1.0*v797 - 1.0*v805","v237 + v238 + v805","0.02*v164 + 0.02*v205 - 0.02*v210 - 0.02*v811 + 0.02*v814",
            "v89 - 1.0*v809","v819 - 1.0*v89","v13 + v154 - 0.72*v814","v852 - 1.0*v885","v263 - 1.0*v810",
            "v45 - 1.0*v144 + v813","v65 - 1.0*v813","-0.02*v841","v817 - 1.0*v816 - 1.0*v818 + v900",
            "0.02*v879 - 0.02*v820 - 0.02*v840 - 0.0019350000000000000879851747015437*v150","-1.0*v819",
            "v283 - 1.0*v37 - 1.0*v219 - 1.0*v225 - 1.0*v30 - 1.0*v451 - 1.0*v452 - 1.0*v478 - 1.0*v484 - 1.0*v501 - 1.0*v636 - 1.0*v673 - 1.0*v680 - 1.0*v713 - 1.0*v851 + v853 + v863 - 1.0*v878 - 1.0*v902 - 1.0*v933 - 1.0*v969 - 1.0*v1017 - 1.0*v1033",
            "v864 - 0.0276*v150","0.02*v833 - 0.04*v174 - 0.02*v655 - 0.02*v656 - 0.02*v657 - 0.000464*v150 - 0.02*v839",
            "0.02*v834 - 0.02*v833","v809","v836 - 1.0*v835 - 0.176*v150","- 1.0*v395 - 1.0*v836",
            "v440 - 1.0*v576","v595 - 1.0*v993","v835 + v859","v792 - 1.0*v8 - 1.0*v817","2.0*v454 - 1.0*v396 + 2.0*v471 + v527 + 2.0*v678 - 1.0*v837 - 1.0*v838",
            "-1.0*v107","v810 - 1.0*v845 + v846","- 1.0*v397 - 1.0*v846","v849 - 1.0*v28 + v854",
            "v883 - 1.0*v849","v850 - 4.0*v585","v28 - 1.0*v683 + v710 + v788 - 1.0*v854 - 1.0*v883",
            "v168 - 1.0*v858 - 1.0*v859","v860 - 1.0*v440","v177 - 1.0*v860","v784 + v785","v862 - 1.0*v861",
            "- 1.0*v398 - 1.0*v862","v513 - 1.0*v865","v105 - 1.0*v867","v870 - 1.0*v868","v140 - 1.0*v870",
            "v868 - 1.0*v872","v872 - 1.0*v611","v808 - 0.21*v150 - 1.0*v873 + v874 + v875 + v876",
            "- 1.0*v399 - 1.0*v874 - 1.0*v875 - 1.0*v876","v805 - 1.0*v105 - 1.0*v140 - 1.0*v513 - 1.0*v566 - 1.0*v598 - 1.0*v738 - 1.0*v759 - 1.0*v64 + v877 - 1.0*v1053 - 1.0*v1069",
            "0.02*v882 - 0.02*v879 - 0.000051999999999999996799868867691785*v150","v880 - 1.0*v881",
            "v71 - 0.035*v150 + v802 + v886 - 1.0*v887 + v888 + v889 - 1.0*v955","- 1.0*v400 - 1.0*v886 - 1.0*v888 - 1.0*v889",
            "v83 + v85 - 1.0*v602 - 1.0*v897 + v898","v602 - 1.0*v898","v816 - 1.0*v85 - 1.0*v83 + v897 + v899 - 1.0*v901",
            "v901 - 1.0*v899","v818 - 1.0*v900","- 1.0*v401 - 1.0*v904","v193 - 1.0*v128 + v194 - 1.0*v237 - 1.0*v441 - 1.0*v464 - 1.0*v493 - 1.0*v534 - 1.0*v599 - 1.0*v646 - 1.0*v662 - 1.0*v686 - 1.0*v729 - 1.0*v730 + v761 - 1.0*v847 - 1.0*v966",
            "v128 - 1.0*v193 - 1.0*v194 + v237 + v254 + v441 + v464 + v493 + v534 + v599 + v646 + v662 + v686 + v729 + v730 - 1.0*v761 + v847 + v966",
            "v905 - 1.0*v759","v890 - 1.0*v855 + v892 + v894 + v896 + v903","v104 + v175 + v753 + v755 + v855 - 1.0*v877 + v909 - 1.0*v927 - 1.0*v998",
            "v111 - 1.0*v910","v928 - 1.0*v63","v74 - 1.0*v912","v913 - 1.0*v909","- 1.0*v402 - 1.0*v913",
            "v908 - 1.0*v906","v915 - 1.0*v917","v914 - 1.0*v915","v916 - 1.0*v914","- 1.0*v403 - 1.0*v916",
            "v547 - 1.0*v211 - 1.0*v10 - 1.0*v926 + v927","v910 - 1.0*v911 + v1066","v998 - 1.0*v973 - 1.0*v929",
            "v933 - 1.0*v932","- 1.0*v404 - 1.0*v933","v959 - 1.0*v764","v946 - 1.0*v947","-1.0*v936",
            "v936","v265 - 1.0*v939","- 1.0*v265 - 1.0*v405","v553 - 1.0*v491 - 0.205*v150 + v881 - 1.0*v882 - 1.0*v937 - 1.0*v938 - 1.0*v940 + v941 + v942 - 1.0*v1019 - 1.0*v1020",
            "- 1.0*v406 - 1.0*v941 - 1.0*v942","v937 - 3.0*v284","v947","v949 - 1.0*v878","v948 - 1.0*v949",
            "- 1.0*v934 - 1.0*v935","v935 + v986","v186 + v813 + v970 + v976","v971 - 1.0*v931",
            "- 1.0*v407 - 1.0*v971","v559 - 0.007*v150 - 1.0*v560 - 1.0*v952 - 1.0*v953 + v954 + v955",
            "- 1.0*v408 - 1.0*v954","v806 - 1.0*v945","v969 - 1.0*v444","v138 - 1.0*v930","v960 - 1.0*v959",
            "v130 - 1.0*v173 + v448 + v449 + v583 + v607 + v682 - 1.0*v854 + v934 + v943 + v950 + v957 + v958 + v961 + v962 + v963 - 1.0*v964 - 1.0*v965 - 1.0*v967 - 1.0*v968 - 1.0*v975 + v976",
            "v173 - 1.0*v409 - 1.0*v961 - 1.0*v962 - 1.0*v963 + v964 + v967 + v975","v78 - 1.0*v138 - 0.0000030000000000000000760025722912339*v150 - 1.0*v596 - 1.0*v712 + v854 + v968 - 1.0*v986",
            "v944 - 1.0*v943","v951 - 1.0*v944","v596 - 1.0*v950","v930 - 1.0*v951","- 1.0*v410 - 1.0*v969",
            "v18 - 1.0*v957 - 1.0*v958","v506 - 1.0*v823","v823 - 1.0*v984","v570 + v972","v975 - 1.0*v974",
            "- 1.0*v411 - 1.0*v975","v977 - 1.0*v976","- 1.0*v412 - 1.0*v977","v186","v12 + v153 - 0.1*v814",
            "v231 - 1.0*v986","v75 + v233 + v453 + v486 - 1.0*v491 - 1.0*v530 + v697 + v717","v989 - 1.0*v1007",
            "- 1.0*v413 - 1.0*v989","v1007 - 1.0*v1008 + v1009","v945 - 1.0*v806 + v1008","v993 - 1.0*v990 - 1.0*v991 - 1.0*v992 - 0.241*v150 + v994 + v995 + v996",
            "- 1.0*v414 - 1.0*v994 - 1.0*v995 - 1.0*v996","v1005","v771 + v987 + v988 - 1.0*v1004 - 1.0*v1005",
            "- 1.0*v415 - 1.0*v987 - 1.0*v988","v1000 + v1002","v1001 - 1.0*v416 + v1003","- 1.0*v1000 - 1.0*v1002",
            "- 1.0*v417 - 1.0*v1001 - 1.0*v1003","v813 + v918 + v919 + v920 + v921 + v922 + v923 + v924 + v925 - 1.0*v1011",
            "v1011 - 1.0*v918 - 1.0*v919 - 1.0*v920 - 1.0*v921 - 1.0*v922 - 1.0*v923 - 1.0*v924 - 1.0*v925 - 1.0*v813",
            "v1014 - 1.0*v1013 - 1.0*v1012 + v1017","v1013 - 1.0*v1015","- 1.0*v418 - 1.0*v1016 - 1.0*v1017",
            "v516 - 1.0*v517","v1019 - 1.0*v1018 - 0.054*v150 + v1020 + v1022","- 1.0*v419 - 1.0*v1022",
            "v1024 - 1.0*v186","- 1.0*v420 - 1.0*v1024","0.02*v652 - 1.0*v431 - 1.0*v11 + 0.02*v653 + 0.02*v654 + 0.02*v839 + 0.02*v840 + 0.02*v841 + v1025",
            "- 1.0*v421 - 1.0*v1025","0.05*v652 - 1.0*v12 + 0.05*v653 + 0.05*v654 + 0.05*v839 + 0.05*v840 + 0.05*v841",
            "v1027 - 1.0*v997 - 1.0*v1026 - 0.131*v150","- 1.0*v422 - 1.0*v1027","v1028 - 1.0*v651 - 1.0*v1061",
            "v1032 - 1.0*v1047","v1047 - 1.0*v1028","v1035 - 1.0*v864","v1033 - 1.0*v1038","v1034 - 1.0*v1031 - 1.0*v1032 - 1.0*v1033 - 1.0*v29 - 1.0*v1035",
            "v1031 - 1.0*v1030","v1030 - 1.0*v36","v812 - 1.0*v1035","v1037 - 1.0*v1036","v1036 - 1.0*v1029",
            "v1038 - 1.0*v1037","v276 + v864 - 1.0*v1039 + v1040","v1039 - 1.0*v812 - 1.0*v29","v36 + v651 + 2.0*v658 - 1.0*v743 - 1.0*v921 + v1014 + v1035 + v1048",
            "v479 - 0.003*v150 + v481 - 2.0*v658 - 1.0*v1014 - 1.0*v1041 - 1.0*v1043 - 1.0*v1044",
            "v1041 - 1.0*v1042 + v1044","v1042","v1043","v1029 - 1.0*v1046","v1046 - 1.0*v812","v29 - 1.0*v768 + v783 + v797 + v812 - 1.0*v1048 + v1053 + v1058 + v1061",
            "v29 - 1.0*v36","v36 - 1.0*v16","v16 - 1.0*v276","v1051 - 1.0*v1050 - 1.0*v1052","v183 + v268 + v903 - 1.0*v1053 + v1054 + v1055",
            "- 1.0*v423 - 1.0*v1054 - 1.0*v1055","v91 - 1.0*v1045","v71 + v91 + v1056","- 1.0*v424 - 1.0*v1056",
            "v195 + v768 - 1.0*v903 - 1.0*v1058 + v1059 + v1060","- 1.0*v425 - 1.0*v1059 - 1.0*v1060",
            "v743 - 1.0*v185 - 1.0*v479 - 1.0*v481 - 0.136*v150 - 1.0*v783 - 1.0*v925 - 1.0*v1034",
            "v1063 - 1.0*v1062 - 0.402*v150 + v1064 + v1065","- 1.0*v426 - 1.0*v1063 - 1.0*v1064",
            "v565 + v896 + v1067 + v1068 - 1.0*v1069","- 1.0*v427 - 1.0*v1067 - 1.0*v1068","v618 - 1.0*v546 - 1.0*v766 + v1069",
            "v766 - 1.0*v896 + v1070","- 1.0*v428 - 1.0*v1070","v911 + v926 - 1.0*v998 - 1.0*v999 + v1073",
            "v637 - 1.0*v1066","v1074 - 1.0*v1071 + v1075","- 1.0*v429 - 1.0*v1074 - 1.0*v1075",
            "v1071 - 1.0*v1073","v139 - 38/5","v24 - 1.0*v21 - 2.0*v22 - tr_126_1 - 1.0*v27 - 1.0*v32 - 1.0*v40 + v46 - 1.0*v53 - 1.0*v148 - 0.00005*v150 - 1.0*v182 + 7.0*v431 + 8.0*v432 + 9.0*v433 - 1.0*v459 + v523 - 1.0*v628 - 1.0*v632 - 1.0*v667 + v815 + v824 - 1.0*v884 - 1.0*v938 - 1.0*v952 - 1.0*v953 - 1.0*v978",
            "tr_126_1","v27 - tr_151_1 + v28 + v31 + v34 + 2.0*v54 + v55 + v56 + v59 + v62 + v65 + v76 + v80 + v86 + v100 + 2.0*v108 + v109 + v112 + v118 + v123 + v127 + v134 + v139 - 1.0*v141 + 45.5608*v150 + v160 + v162 + 2.0*v163 + v165 + v166 + v185 + v192 + v199 + v200 + v203 + v205",
            "v209 - tr_151_2 + v212 + v216 + v218 + v221 + v234 + v262 + v266 + v267 + v273 + v438 + v443 + v450 + v476 + v482 + v487 + v492 + v499 + v504 + v505 + v507 + v509 + v518 + v524 + v526 + v529 + v538 + v543 + v548 + v557 + v560 + v562 + v577 + v578 + v581",
            "v586 - tr_151_3 + v595 + v615 + v620 + v638 + v648 + 3.0*v658 + v660 + v668 + v669 + v670 + v671 + v672 + v696 + v698 + v723 + v734 + v738 + v742 + v743 + v744 + v745 + v746 + v747 + v748 + v749 + v804 + v822 + v823 + v828 + v837 + v844 + v845 - 1.0*v849 + v853",
            "v865 - tr_151_4 + v866 + v869 + v871 + v874 + v888 + v898 + v899 + v900 - 1.0*v902 + v906 + v909 + v910 + v913 + v915 - 1.0*v918 + v949 + v954 + v961 + v968 + v971 + v977 + v983 + v989 + v994 + v1004 + v1007 + v1008 + v1024 + v1029 + v1036 + v1037 + v1046",
            "v1048 - tr_151_5 + v1057 + v1063 + v1073 + v1074","tr_151_1 + tr_151_2 + tr_151_3 + tr_151_4 + tr_151_5",
            "v11 - tr_180_1 + v12 + v13 + v14 + v15 + v46 - 1.0*v54 - 1.0*v55 - 1.0*v56 + v59 + v64 + v66 - 1.0*v104 + v117 + v121 + v122 + v144 - 0.001*v150 + 6.0*v284 + v431 + v432 + v433 + v517 + v546 + v588 + v726 + v736 - 1.0*v773 + v781 + v809 + v810 + v863 + v877 + v936 + v959 + v997",
            "tr_180_1","v109 - 1.0*v11 - 1.0*v12 - 1.0*v13 - 1.0*v14 - 1.0*v15 - 1.0*v27 - 1.0*v28 - 1.0*v31 - 1.0*v34 - 1.0*v46 - 1.0*v54 - 1.0*v58 - 1.0*v59 - 1.0*v62 - 1.0*v65 - 1.0*v76 - 1.0*v80 - 1.0*v86 - 1.0*v100 - tr_200_1 - 1.0*v112 - 1.0*v117 - 1.0*v118 - 1.0*v121 - 1.0*v122",
            "v141 - 1.0*v123 - 1.0*v127 - 1.0*v134 - 1.0*v139 - 1.0*v140 - tr_200_2 - 45.73179999999999978399500832893*v150 - 1.0*v159 - 1.0*v160 - 1.0*v161 - 1.0*v162 - 2.0*v163 - 1.0*v165 - 1.0*v166 - 1.0*v185 - 1.0*v192 - 1.0*v199 - 1.0*v200 - 1.0*v203 - 1.0*v205 - 1.0*v209",
            "- tr_200_3 - 1.0*v212 - 1.0*v216 - 1.0*v218 - 1.0*v221 - 1.0*v228 - 1.0*v234 - 1.0*v262 - 1.0*v266 - 1.0*v267 - 1.0*v273 - 1.0*v431 - 1.0*v432 - 1.0*v433 - 1.0*v438 - 1.0*v443 - 1.0*v446 - 1.0*v450 - 1.0*v476 - 1.0*v482 - 1.0*v487 - 1.0*v492 - 1.0*v503",
            "- tr_200_4 - 1.0*v504 - 1.0*v505 - 1.0*v507 - 1.0*v509 - 1.0*v517 - 1.0*v518 - 1.0*v524 - 1.0*v526 - 1.0*v529 - 1.0*v538 - 1.0*v542 - 1.0*v543 - 1.0*v546 - 1.0*v548 - 1.0*v557 - 1.0*v560 - 1.0*v562 - 1.0*v577 - 1.0*v578 - 1.0*v581 - 1.0*v586 - 1.0*v588 - 1.0*v595",
            "- tr_200_5 - 1.0*v615 - 1.0*v620 - 1.0*v638 - 1.0*v648 - 1.0*v660 - 1.0*v668 - 1.0*v669 - 1.0*v670 - 1.0*v671 - 1.0*v672 - 1.0*v695 - 1.0*v696 - 1.0*v698 - 1.0*v723 - 1.0*v734 - 1.0*v736 - 1.0*v738 - 1.0*v742 - 1.0*v743 - 1.0*v744 - 1.0*v745 - 1.0*v746 - 1.0*v747",
            "v849 - 1.0*v748 - 1.0*v749 - 1.0*v751 - 1.0*v757 - 1.0*v781 - 1.0*v804 - 1.0*v809 - 1.0*v810 - 1.0*v822 - 1.0*v823 - 1.0*v828 - 1.0*v837 - 1.0*v844 - 1.0*v845 - tr_200_6 - 1.0*v853 - 1.0*v863 - 1.0*v865 - 1.0*v866 - 1.0*v869 - 1.0*v871 - 1.0*v874 - 1.0*v877",
            "v902 - 1.0*v885 - 1.0*v888 - 1.0*v898 - 1.0*v899 - 1.0*v900 - tr_200_7 - 1.0*v906 - 1.0*v909 - 1.0*v910 - 1.0*v913 - 1.0*v915 - 1.0*v922 - 1.0*v931 - 1.0*v936 - 1.0*v937 - 1.0*v949 - 1.0*v954 - 1.0*v959 - 1.0*v961 - 1.0*v968 - 1.0*v971 - 1.0*v977 - 1.0*v983",
            "- tr_200_8 - 1.0*v989 - 1.0*v994 - 1.0*v997 - 1.0*v1004 - 1.0*v1007 - 1.0*v1008 - 1.0*v1024 - 1.0*v1029 - 1.0*v1036 - 1.0*v1037 - 1.0*v1046 - 1.0*v1048 - 1.0*v1057 - 1.0*v1063 - 1.0*v1073 - 1.0*v1074",
            "tr_200_1 + tr_200_2 + tr_200_3 + tr_200_4 + tr_200_5 + tr_200_6 + tr_200_7 + tr_200_8",
            "v33 - tr_229_1 + v35 + v57 + v78 + v107 + v114 + v125 + 4.0*v151 + 5.0*v152 + 5.0*v153 + 6.0*v154 + 6.0*v155 + 7.0*v156 - 1.0*v162 + v176 + 2.0*v177 + 2.0*v187 + v207 - 1.0*v212 + v235 + v272 + v441 + v442 + v445 + v510 + v521 + v530 + v547 - 1.0*v574 + v604 + v613",
            "v631 - tr_229_2 + v632 + v633 + v637 + v659 + v666 + v689 + v690 + v710 + v759 + v795 + v797 + v800 + v802 + v806 + v815 + v817 + v847 - 1.0*v851 + v852 + v853 + v858 + v859 + v879 + v930 + v976 + v997 + v1045 + 4.0*v1052",
            "tr_229_1 + tr_229_2","v22 - tr_231_1 - 1.0*v24 - 1.0*v28 + v32 + v40 - 1.0*v45 - 1.0*v46 + v53 - 1.0*v78 + v107 + v138 - 0.0000060000000000000001520051445824677*v150 + v182 + v262 - 7.0*v431 - 8.0*v432 - 9.0*v433 + v459 - 1.0*v523 + v596 + v628 + v632 + v667 + v683 + v684 + v764 - 1.0*v788",
            "v883 - 1.0*v809 - 1.0*v815 - 1.0*v824 - tr_231_2 + v884 + v938 + v952 + v953 - 1.0*v959 - 1.0*v968 + v978 + v986",
            "tr_231_1 + tr_231_2","v18 - tr_388_1 - 1.0*v32 + v44 + v50 + v84 + v106 + v121 + v133 - 0.25*v150 + v163 + v185 - 1.0*v234 + v490 - 1.0*v504 - 1.0*v507 + v508 - 1.0*v509 - 1.0*v510 - 1.0*v511 + v512 + v513 + v514 + 2.0*v515 - 1.0*v517 + v518 + v519 + v520 + v546 - 1.0*v597 + v611 + v614",
            "v803 - 1.0*v647 - 1.0*v792 - tr_388_2 + v807 + v835 + v871 - 1.0*v880 + v887 + v935 + v943 + v951 - 1.0*v979 + v1026 - 1.0*v1049 + v1062",
            "tr_388_1 + tr_388_2","v6 - tr_424_1 + v7 - 1.0*v8 - 1.0*v17 - 1.0*v19 - 1.0*v41 - 1.0*v48 - 1.0*v51 - 1.0*v69 - 1.0*v71 - 1.0*v73 - 1.0*v74 - 1.0*v86 - 1.0*v89 - 1.0*v90 - 1.0*v91 - 1.0*v92 + v94 - 1.0*v103 - 1.0*v104 - 1.0*v108 - 1.0*v109 - 1.0*v112 - 1.0*v118 - 1.0*v120 - 1.0*v121",
            "v141 - 1.0*v123 - 1.0*v134 - 1.0*v139 - tr_424_2 - 1.0*v142 - 1.0*v143 - 1.0*v144 + v145 + v146 - 45.5608*v150 + 5.0*v151 + 6.0*v152 + 6.0*v153 + 7.0*v154 + 7.0*v155 + 8.0*v156 + 2.0*v158 - 1.0*v159 - 1.0*v160 - 1.0*v161 - 1.0*v163 - 1.0*v164 - 1.0*v166 - 1.0*v175",
            "2.0*v177 - tr_424_3 + v180 - 1.0*v182 - 1.0*v183 - 1.0*v185 - 1.0*v189 - 1.0*v191 - 1.0*v192 + v193 + v194 - 1.0*v195 - 1.0*v201 - 1.0*v202 - 1.0*v209 - 1.0*v213 - 1.0*v214 - 1.0*v219 + v223 + v224 + 2.0*v232 - 1.0*v239 - 1.0*v241 + v243 + v244 - 1.0*v250",
            "v253 - 3.0*v251 - tr_424_4 + v255 + v257 - 1.0*v259 - 1.0*v260 - 1.0*v270 - 1.0*v275 + v278 + v283 - 6.0*v431 - 7.0*v432 - 8.0*v433 - 1.0*v434 - 1.0*v436 - 1.0*v443 - 1.0*v444 - 1.0*v453 - 1.0*v456 - 1.0*v460 + v467 - 1.0*v469 + v472 + v473 - 1.0*v477 - 1.0*v482",
            "v491 - 1.0*v488 - tr_424_5 + v497 - 1.0*v505 - 1.0*v511 - 1.0*v512 - 1.0*v513 - 1.0*v518 - 1.0*v524 - 1.0*v526 - 1.0*v539 + v541 - 1.0*v544 - 1.0*v546 - 1.0*v550 - 1.0*v551 - 1.0*v552 - 1.0*v553 - 1.0*v554 - 1.0*v555 - 1.0*v559 - 1.0*v563 - 3.0*v564",
            "v571 - 1.0*v565 - tr_424_6 - 1.0*v574 - 1.0*v576 - 1.0*v579 - 1.0*v580 - 1.0*v581 - 1.0*v583 - 1.0*v584 - 1.0*v585 - 1.0*v602 - 1.0*v606 + v612 + v613 - 1.0*v615 - 1.0*v617 - 1.0*v618 + v624 + v626 - 1.0*v627 - 1.0*v628 - 1.0*v635 - 1.0*v636 - 1.0*v638 - 1.0*v640",
            "v681 - 1.0*v641 - 1.0*v642 - 1.0*v648 - 1.0*v652 - 1.0*v653 - 1.0*v654 - 1.0*v660 - 1.0*v667 - 1.0*v668 - 1.0*v669 - 1.0*v670 - 1.0*v671 - 1.0*v672 - tr_424_7 - 1.0*v683 + v688 + v691 - 1.0*v695 - 1.0*v696 - 1.0*v698 - 1.0*v700 - 1.0*v701 - 1.0*v702 - 1.0*v703",
            "v714 - 1.0*v704 - 1.0*v705 - 1.0*v706 - tr_424_8 - 1.0*v717 - 1.0*v718 - 1.0*v719 - 1.0*v724 - 1.0*v726 - 1.0*v735 - 1.0*v738 - 1.0*v752 - 1.0*v753 - 1.0*v755 - 1.0*v756 + v761 + v762 - 1.0*v765 - 1.0*v766 - 1.0*v767 - 1.0*v768 - 1.0*v769 - 1.0*v770 - 1.0*v771",
            "2.0*v786 - 1.0*v772 - 1.0*v773 - 1.0*v774 - 1.0*v775 - 1.0*v776 - 1.0*v777 - 1.0*v778 - 1.0*v779 - 1.0*v780 - 1.0*v781 - 1.0*v782 - 1.0*v783 - 1.0*v784 - 1.0*v785 - tr_424_9 - 1.0*v799 - 1.0*v804 - 2.0*v807 - 1.0*v811 + 2.0*v817 - 1.0*v818 - 1.0*v819",
            "2.0*v850 - 1.0*v829 - 1.0*v830 - 1.0*v833 - 1.0*v837 - 1.0*v839 - 1.0*v840 - 1.0*v841 - 1.0*v843 - 1.0*v847 - 1.0*v848 - tr_424_10 - 1.0*v851 + v859 + 3.0*v860 - 1.0*v863 - 1.0*v868 - 1.0*v870 - 1.0*v871 - 1.0*v874 - 1.0*v881 - 1.0*v888 - 1.0*v897 - 1.0*v901",
            "2.0*v905 - tr_424_11 + 2.0*v907 - 1.0*v913 + v918 + v919 + v920 + v921 + v922 + v923 + v924 + v925 - 1.0*v928 - 2.0*v930 - 1.0*v931 - 1.0*v934 - 1.0*v936 - 1.0*v943 - 1.0*v944 - 1.0*v954 - 1.0*v957 - 1.0*v958 + v960 - 1.0*v961 - 3.0*v970 - 1.0*v971 + v974 - 1.0*v977",
            "v982 - tr_424_12 - 1.0*v986 - 1.0*v989 - 1.0*v993 - 1.0*v994 + v997 + v1000 + v1002 - 1.0*v1012 - 1.0*v1013 - 1.0*v1015 - 1.0*v1018 + v1019 + v1020 - 1.0*v1024 - 1.0*v1030 - 1.0*v1039 - 1.0*v1043 - 1.0*v1045 - 1.0*v1047 + v1051 - 1.0*v1061 - 1.0*v1063 - 1.0*v1074",
            "tr_424_1 + tr_424_2 + tr_424_3 + tr_424_4 + tr_424_5 + tr_424_6 + tr_424_7 + tr_424_8 + tr_424_9 + tr_424_10 + tr_424_11 + tr_424_12",
            "v16 - 1.0*v2 - 1.0*v3 - 1.0*v4 - 1.0*v5 - 1.0*v6 - 1.0*v7 - 1.0*v9 - tr_427_1 + 2.0*v19 + v20 + v23 + v24 - 1.0*v26 + v27 + v32 - 1.0*v33 - 1.0*v35 + v36 + v38 + v45 + v47 - 1.0*v48 + v49 - 1.0*v51 + v52 - 2.0*v53 - 1.0*v57 + v59 + v60 + v61 + v62 + v63 + v65",
            "2.0*v68 - tr_427_2 + v72 + v76 + v79 + v80 + v86 + v87 - 1.0*v88 + 2.0*v89 + 2.0*v90 + v92 + v93 + v100 + v102 + 4.0*v103 + v106 - 1.0*v107 + 2.0*v108 + 2.0*v109 - 1.0*v110 + v112 + v113 - 1.0*v114 + v117 + v118 + v119 + v121 + v122 + v123 + v124 - 1.0*v125 + v126",
            "v134 - tr_427_3 + v135 + 2.0*v136 + 3.0*v137 + v138 + v139 + 3.0*v141 + 2.0*v142 + 2.0*v143 - 1.0*v145 - 1.0*v146 + 2.0*v147 + v149 + 45.560349999999999681676854379475*v150 - 14.0*v151 - 17.0*v152 - 16.0*v153 - 20.0*v154 - 19.0*v155 - 22.0*v156 + v157 + v160",
            "2.0*v162 - tr_427_4 + 2.0*v163 + 2.0*v164 + v165 + v166 + v167 - 1.0*v171 - 2.0*v177 + v182 - 1.0*v183 + v184 + 2.0*v185 + v186 - 3.0*v187 + v188 + v190 + v192 - 2.0*v193 - 2.5*v194 - 1.0*v195 + v196 + v197 + v198 - 1.0*v202 + v204 + v205 + v206 - 1.0*v207",
            "v209 - tr_427_5 - 1.0*v210 + v211 + 3.0*v212 - 1.0*v213 - 1.0*v214 + v215 + v216 + v217 + v218 + v222 + v227 + v229 - 1.0*v231 + v232 - 1.0*v233 + v235 + v239 + v240 - 1.0*v241 + v242 + v246 - 1.0*v247 - 1.0*v248 - 1.0*v249 + 2.0*v250 + 6.0*v251 - 1.0*v253",
            "v254 - tr_427_6 + v260 - 1.0*v261 + v262 - 1.0*v263 + v265 + v267 + v269 + v270 - 1.0*v271 - 1.0*v272 + v273 + v274 + 2.0*v275 + v276 + 6.0*v284 + v286 + 6.0*v431 + 7.0*v432 + 8.0*v433 + v434 + v438 + 2.0*v440 - 3.0*v441 - 3.0*v442 + v443 - 1.0*v445 - 1.0*v446",
            "v450 - tr_427_7 + v453 + v455 + 2.0*v457 + 3.0*v458 + v459 - 1.0*v461 + v463 + v467 - 1.0*v468 + v470 + v474 + v475 + v476 - 1.0*v479 + v480 - 1.0*v481 + v482 + v483 + v485 + v486 + v487 + 2.0*v488 + v494 + v498 + v499 + v500 + v502 - 1.0*v503 + v504 + v505",
            "v506 - tr_427_8 + v509 - 1.0*v510 + v511 - 1.0*v515 - 1.0*v516 + v518 + v519 - 1.0*v521 + v524 + v525 + v526 + v528 + v529 - 1.0*v531 - 1.0*v532 + v533 + v538 + v539 + v540 - 1.0*v542 + v543 - 2.0*v545 + 2.0*v546 + v548 - 1.0*v549 + 2.0*v550 + v551 + v552",
            "v553 - tr_427_9 + v554 + v555 + v557 + v558 + v560 - 1.0*v561 + v562 + 2.0*v564 - 1.0*v565 + v568 + v573 + v574 + v575 + v577 + v578 + 3.0*v579 + v581 + v582 + 2.0*v583 + 2.0*v584 + v586 + v588 + v590 - 1.0*v592 - 1.0*v593 + v594 + v595 - 2.0*v599 - 2.0*v600",
            "v610 - 2.0*v601 - 1.0*v608 - 1.0*v609 - tr_427_10 + v611 - 1.0*v613 + v615 + v616 + v618 + v619 + v620 + v621 + v622 - 1.0*v624 + v625 + v628 - 1.0*v629 - 1.0*v630 - 1.0*v631 - 1.0*v632 - 2.0*v633 - 1.0*v637 + v638 + v639 + 2.0*v641 + 2.0*v642 - 1.0*v643 + v644",
            "v645 - tr_427_11 + v648 + v649 + v651 + v652 + v653 + v654 + 10.0*v658 - 1.0*v659 + v660 + v661 + v664 + v665 - 1.0*v666 + v667 + v668 + v669 + v670 + v671 + v672 + 2.0*v674 + 3.0*v675 - 1.0*v676 + v679 + v683 + v685 - 1.0*v691 + v693 - 1.0*v694 + v696 + v698",
            "v715 - 1.0*v710 - tr_427_12 + v716 + v720 - 1.0*v721 + v723 + 2.0*v726 - 1.0*v728 - 1.0*v729 - 4.5*v730 - 3.0*v731 - 3.8*v732 - 1.0*v733 + v734 + v736 + v739 + 3.0*v740 + 2.0*v741 - 1.0*v751 + v753 + v755 - 1.0*v757 + v758 - 2.0*v759 + v760 - 2.0*v761 - 2.0*v762",
            "v776 - tr_427_13 + v777 + v778 + v779 + v780 + v781 + v782 + v783 - 5.0*v786 + v789 + v790 + v793 + v794 - 1.0*v795 - 1.0*v797 - 1.0*v800 - 1.0*v802 + v804 - 1.0*v806 + v807 - 2.0*v808 + v810 + 2.0*v813 + v817 - 1.0*v820 + v821 + v822 + v823 + v826 + v829",
            "v834 - tr_427_14 + v836 + v837 + v838 + v839 + v840 + v841 + v845 + v848 + v850 + v851 - 1.0*v852 + v857 - 1.0*v859 - 1.0*v861 + v862 + 2.0*v863 + v864 + v865 + 2.0*v866 + v869 + v870 + v871 + v873 + v874 + v875 + v877 - 1.0*v879 + v882 - 1.0*v885 + v888 + v889",
            "v898 - tr_427_15 + v899 + v900 - 1.0*v902 + v904 + v905 + v906 + v909 + v910 + v913 + v915 + v916 - 2.0*v930 + v932 - 1.0*v937 + v941 + 2.0*v944 + v946 + 3.0*v947 - 1.0*v948 + v949 + v950 + v952 + v953 + v954 + v955 - 2.0*v956 + 2.0*v957 + 2.0*v958 + v961 + 2.0*v962",
            "3.0*v963 - tr_427_16 - 1.0*v964 + 5.0*v970 + v971 + v972 + v976 + v977 + v978 - 1.0*v981 + v983 + 2.0*v985 + v987 + v988 + v989 + v991 + v994 + v995 + v997 - 1.0*v1000 - 1.0*v1002 + v1004 + v1007 - 1.0*v1009 - 1.0*v1011 + v1014 + v1022 - 1.0*v1023 + v1024",
            "v1025 - tr_427_17 + v1027 + v1028 + v1029 + 3.0*v1030 - 1.0*v1034 + v1035 + v1036 + v1037 - 1.0*v1038 + v1039 + 3.0*v1043 - 2.0*v1045 + v1046 + v1050 - 4.0*v1052 + v1054 + v1055 + v1058 + v1059 + v1060 + 2.0*v1061 + v1063 + v1064 + v1068 + v1070 + v1073 + v1074 + v1075",
            "tr_427_1 + tr_427_2 + tr_427_3 + tr_427_4 + tr_427_5 + tr_427_6 + tr_427_7 + tr_427_8 + tr_427_9 + tr_427_10 + tr_427_11 + tr_427_12 + tr_427_13 + tr_427_14 + tr_427_15 + tr_427_16 + tr_427_17",
            "2.0*v193 - 1.0*v20 - 1.0*v23 - 1.0*v38 - 1.0*v47 - 1.0*v52 - 1.0*v60 - 1.0*v61 - 1.0*v79 - 1.0*v87 - 1.0*v93 - 1.0*v113 - 1.0*v124 - 1.0*v135 - 2.0*v136 - 3.0*v137 - 4.0*v141 - 1.0*v149 - 1.0*v157 - 1.0*v167 - 1.0*v184 - 1.0*v188 - tr_428_1 + 2.5*v194 - 1.0*v197",
            "2.0*v441 - 1.0*v198 - 1.0*v204 - 1.0*v206 - 1.0*v215 - 1.0*v217 - 1.0*v222 - 1.0*v246 - 1.0*v265 - 1.0*v269 - 1.0*v274 - 1.0*v286 - 1.0*v359 - tr_428_2 + 2.0*v442 - 1.0*v455 - 2.0*v457 - 3.0*v458 - 1.0*v474 - 1.0*v475 - 1.0*v480 - 1.0*v483 + v493 - 1.0*v494",
            "2.0*v599 - 1.0*v498 - 1.0*v500 - 1.0*v502 - 1.0*v519 - 1.0*v525 - 1.0*v533 - 1.0*v540 - 1.0*v558 - 1.0*v568 - 1.0*v573 - 1.0*v575 - 1.0*v582 - 1.0*v590 - tr_428_3 + 2.0*v600 + 2.0*v601 - 1.0*v610 - 1.0*v616 - 1.0*v619 - 1.0*v621 - 1.0*v622 - 1.0*v639 - 1.0*v644",
            "2.0*v727 - 1.0*v649 - 1.0*v661 - 1.0*v664 - 2.0*v674 - 3.0*v675 - 1.0*v693 - tr_428_4 + 3.5*v730 + 2.0*v731 + 2.8*v732 - 1.0*v739 - 3.0*v740 - 2.0*v741 - 1.0*v760 + 2.0*v761 + 2.0*v762 - 1.0*v790 - 1.0*v836 - 1.0*v838 - 1.0*v862 - 1.0*v875 - 1.0*v889 - 1.0*v904",
            "v964 - 1.0*v916 - 1.0*v941 - 2.0*v962 - 3.0*v963 - tr_428_5 - 2.0*v985 - 1.0*v987 - 1.0*v988 - 1.0*v995 - 1.0*v1001 - 1.0*v1003 - 1.0*v1022 - 1.0*v1025 - 1.0*v1027 - 1.0*v1054 - 1.0*v1055 - 1.0*v1059 - 1.0*v1060 - 1.0*v1064 - 1.0*v1068 - 1.0*v1070 - 1.0*v1075",
            "tr_428_1 + tr_428_2 + tr_428_3 + tr_428_4 + tr_428_5","v2 - tr_536_1 + v4 + v6 + v7 - 1.0*v19 - 1.0*v24 + 2.0*v53 - 1.0*v78 + v88 - 1.0*v89 - 1.0*v90 - 1.0*v142 + v145 - 0.00215*v150 + v171 - 1.0*v227 - 1.0*v229 - 1.0*v240 + v248 + v253 + v261 - 1.0*v275 - 6.0*v431 - 7.0*v432 - 8.0*v433 - 1.0*v434 - 1.0*v485 - 1.0*v488",
            "v531 - 1.0*v506 - 1.0*v528 - 1.0*v530 - tr_536_2 - 2.0*v579 + v592 + v608 - 1.0*v618 + v624 - 1.0*v625 - 1.0*v641 - 1.0*v642 + v643 - 1.0*v645 - 1.0*v665 - 1.0*v679 - 1.0*v685 - 1.0*v689 + v721 - 1.0*v726 + v728 + v729 + v730 + v731 + v732 + v733 - 1.0*v734",
            "v735 - tr_536_3 + v736 - 1.0*v737 + v751 + 3.0*v786 - 1.0*v807 - 1.0*v815 - 1.0*v817 - 1.0*v821 - 1.0*v826 - 1.0*v858 + v861 - 1.0*v932 - 1.0*v944 - 1.0*v946 - 1.0*v957 - 1.0*v972 + v985 - 1.0*v991 + v1023 - 2.0*v1030 - 2.0*v1043",
            "tr_536_1 + tr_536_2 + tr_536_3","v19 - 1.0*v2 - 1.0*v4 - 1.0*v6 - 1.0*v7 - tr_538_1 + v24 - 2.0*v53 + v78 - 1.0*v88 + v89 + v90 + v142 - 1.0*v145 - 0.00005*v150 - 1.0*v171 + v227 + v229 + v240 - 1.0*v248 - 1.0*v253 - 1.0*v261 + v275 + 6.0*v431 + 7.0*v432 + 8.0*v433 + v434 + v485 + v488 + v506",
            "v528 - tr_538_2 + v530 - 1.0*v531 + 2.0*v579 - 1.0*v592 - 1.0*v608 + v618 - 1.0*v624 + v625 + v641 + v642 - 1.0*v643 + v645 + v665 + v679 + v685 + v689 - 1.0*v721 - 1.0*v728 - 1.0*v729 - 1.0*v730 - 1.0*v731 - 1.0*v732 - 1.0*v733 + v737 - 3.0*v786 + v807 + v815",
            "v817 - tr_538_3 + v821 + v826 + v858 - 1.0*v861 + v932 + v944 + v946 + v957 + v972 - 1.0*v985 + v991 - 1.0*v1023 + 2.0*v1030 + 2.0*v1043",
            "tr_538_1 + tr_538_2 + tr_538_3","v3 - tr_539_1 + v5 + v9 - 1.0*v72 + v110 - 1.0*v119 - 1.0*v143 + v146 - 0.00013*v150 + 10.0*v151 + 12.0*v152 + 11.0*v153 + 14.0*v154 + 13.0*v155 + 15.0*v156 + v231 + v233 + v247 + v249 + v263 + v271 - 1.0*v463 + v468 - 1.0*v470 - 1.0*v511 + v515 + v516 + v532",
            "v545 - tr_539_2 - 1.0*v547 + v549 + v561 + v593 - 1.0*v594 - 1.0*v604 + v609 + v629 + v630 + v633 - 1.0*v690 - 1.0*v720 + v734 - 1.0*v735 + v737 + v808 + v948 - 1.0*v958 - 3.0*v970 + v981 - 1.0*v985 + v1011 + v1038",
            "tr_539_1 + tr_539_2","v72 - 1.0*v3 - 1.0*v5 - 1.0*v9 - tr_540_1 - 1.0*v110 + v119 + v143 - 1.0*v146 - 0.0004*v150 - 10.0*v151 - 12.0*v152 - 11.0*v153 - 14.0*v154 - 13.0*v155 - 15.0*v156 - 1.0*v231 - 1.0*v233 - 1.0*v247 - 1.0*v249 - 1.0*v263 - 1.0*v271 + v463 - 1.0*v468 + v470 + v511",
            "v547 - 1.0*v515 - 1.0*v516 - 1.0*v532 - 1.0*v545 - tr_540_2 - 1.0*v549 - 1.0*v561 - 1.0*v593 + v594 + v604 - 1.0*v609 - 1.0*v629 - 1.0*v630 - 1.0*v633 + v690 + v720 - 1.0*v737 - 1.0*v808 - 1.0*v948 + v958 + 3.0*v970 - 1.0*v981 + v985 - 1.0*v1011 - 1.0*v1038",
            "tr_540_1 + tr_540_2","v48 - tr_542_1 + v51 + v120 - 1.0*v122 + v132 - 1.0*v162 + v183 + v187 + v189 + v191 + v195 + v201 + v202 + v213 + v214 + v241 + v285 + v469 - 1.0*v504 + v511 + v512 + v530 + v545 + v565 + 4.0*v585 - 1.0*v736 + v750 + v752 + v756 + v786 + v819 + v897 + 2.0*v930",
            "v939 - tr_542_2 + v940 + v992 + v1018 + 2.0*v1045","tr_542_1 + tr_542_2","v8 - tr_589_1 + v27 + v28 + v68 - 1.0*v72 + v76 + v80 + v86 + v112 + v118 - 1.0*v119 + v123 + v126 + v134 + v139 - 1.0*v141 + v144 + 45.562800000000002853539626812562*v150 + v159 + v160 + v161 + v163 + v166 + v169 + v185 + v192 + v209 + v212 + v219 + v234 + v245",
            "v250 - tr_589_2 + v251 + v259 - 1.0*v268 + v436 + v443 - 2.0*v454 + v460 + v468 - 2.0*v471 + v482 - 1.0*v485 + v487 - 1.0*v495 + v504 + v505 + v509 + v518 + v524 + v526 - 1.0*v527 + v544 + v560 + v562 + v580 + v581 + v602 + v615 + v635 + v636 + v638 + v648",
            "v660 - tr_589_3 + v668 + v669 + v670 + v671 + v672 + v676 - 2.0*v678 + v695 + v696 + v698 + v699 + v700 - 1.0*v707 - 1.0*v708 - 1.0*v709 + v735 + v738 + v765 + v766 + v767 + v768 + v769 + v770 + v771 + v772 + v773 + v774 + v775 + v789 + v804 + v811 + v817 + v818",
            "v830 - tr_589_4 + v833 + 2.0*v837 + v838 + v843 + 2.0*v848 + v851 + v863 + v865 + v866 + v869 + v871 + v874 + v878 + v881 - 1.0*v883 - 1.0*v884 + v888 - 1.0*v890 - 1.0*v891 - 1.0*v892 - 1.0*v893 - 1.0*v894 - 1.0*v895 - 1.0*v896 + v901 - 1.0*v903 + v905 + v907",
            "v913 - tr_589_5 + v928 + v931 + v936 + v954 + v961 + v968 + v971 + v977 + v989 + v993 + v994 - 1.0*v1005 + v1013 + v1024 + v1029 + v1033 + v1036 + v1037 + v1039 + v1046 + v1063 + v1074",
            "tr_589_1 + tr_589_2 + tr_589_3 + tr_589_4 + tr_589_5","v11 - tr_599_1 + v12 + v13 + v14 + v15 + v26 + v46 + v58 + v64 + v105 + v117 + v121 + v122 + v140 + 0.7302*v150 + v159 + v161 + v210 + v228 + v235 + v242 + v252 + v260 + v270 + v431 + v432 + v433 + v446 + v461 + v479 + v481 + v503 + v513 + v517 + v542 + v546 + v556",
            "v564 - tr_599_2 + v566 + v572 + v576 + v598 + v634 + v694 + v695 + v736 + v738 + v751 + v757 + v759 + v776 + v777 + v778 + v779 + v780 + v781 + v782 + v783 + 5.0*v791 - 1.0*v805 + v809 + v810 - 1.0*v848 + v857 + v870 + v885 + v931 + v937 + v959 + v997 + v1009",
            "v1034 - tr_599_3 + 8.0*v1040 + v1053 + v1069","tr_599_1 + tr_599_2 + tr_599_3","v30 - tr_623_1 - 1.0*v33 - 2.0*v35 + v37 + v39 + v49 + v83 + v84 + v85 + v106 + v170 + v189 + v191 + v201 + v220 + v225 - 1.0*v232 - 1.0*v272 + v277 + v451 + v452 + v478 + v484 + v496 + v501 + v587 + v606 + v645 + v646 + v662 + v663 + v673 + v680 + v682 + v689",
            "v690 - tr_623_2 + v713 - 1.0*v815 - 1.0*v824 - 1.0*v847 - 1.0*v863 + v902 + v904 + v933 + v939 + v940 + v945 + v969 + v1017 + v1018 + v1065",
            "tr_623_1 + tr_623_2"} "DAEO equality constraints";
        constant String inequalities[q]={"-1.0*v344","-1.0*v392","b1 + v344","b2 + v429","b3 + v392",
            "-1.0*v429","v2","v3","v4","v5","v6","v7","v8","v11","v12","v13","v14","v15","v16","v17",
            "v18","v19","v20","v21","v24","v26","v28","v29","v30","v31","v32","v33","v35","v36",
            "v37","v38","v39","v41","v42","v45","v46","v48","v49","v50","v51","v58","v59","v60",
            "v62","v63","v64","v65","v68","v69","v70","v71","v74","v76","v78","v82","v83","v85",
            "v86","v89","v90","v91","v92","v94","v95","v96","v97","v98","v99","v100","v102","v103",
            "v104","v105","v106","v108","v109","v110","v112","v114","v117","v118","v120","v121",
            "v122","v123","v125","v126","v128","v129","v130","v131","v132","v134","v135","v136",
            "v137","v138","v140","v142","v143","v144","v145","v146","v148","v150","v151","v152",
            "v153","v154","v155","v156","v157","v158","v160","v162","v163","v164","v165","v166",
            "v168","v169","v170","v171","v172","v173","v175","v177","v181","v182","v183","v184",
            "v185","v186","v187","v188","v189","v190","v191","v192","v193","v194","v195","v196",
            "v197","v201","v202","v204","v205","v207","v209","v211","v213","v214","v215","v216",
            "v218","v219","v222","v223","v224","v225","v229","v230","v231","v232","v234","v235",
            "v236","v237","v238","v240","v241","v242","v243","v245","v246","v247","v248","v249",
            "v250","v251","v252","v253","v254","v255","v256","v257","v258","v259","v260","v261",
            "v262","v263","v264","v267","v269","v270","v271","v272","v273","v276","v277","v278",
            "v279","v280","v281","v282","v284","v285","v287","v288","v289","v290","v291","v292",
            "v293","v294","v295","v296","v297","v298","v299","v300","v301","v302","v303","v304",
            "v305","v306","v307","v308","v309","v310","v311","v312","v313","v314","v316","v317",
            "v318","v319","v320","v321","v322","v323","v324","v325","v326","v327","v328","v329",
            "v331","v332","v333","v334","v335","v336","v337","v338","v339","v340","v341","v342",
            "v343","v345","v346","v347","v348","v349","v350","v351","v352","v353","v354","v355",
            "v356","v357","v360","v361","v362","v363","v364","v365","v366","v368","v369","v370",
            "v371","v372","v373","v374","v375","v376","v377","v378","v379","v380","v381","v382",
            "v383","v384","v386","v387","v389","v390","v391","v393","v394","v395","v397","v398",
            "v399","v400","v401","v402","v403","v404","v405","v406","v408","v409","v410","v411",
            "v412","v413","v414","v415","v416","v417","v418","v419","v420","v421","v422","v423",
            "v424","v425","v426","v427","v428","v431","v432","v433","v434","v436","v438","v440",
            "v441","v442","v443","v444","v445","v446","v448","v449","v450","v451","v452","v453",
            "v454","v457","v458","v459","v460","v461","v462","v464","v465","v466","v467","v468",
            "v469","v471","v472","v473","v477","v478","v481","v482","v483","v484","v487","v488",
            "v489","v490","v491","v493","v495","v496","v497","v499","v501","v502","v503","v504",
            "v505","v507","v509","v510","v512","v513","v515","v516","v517","v518","v520","v521",
            "v523","v524","v526","v527","v528","v529","v530","v531","v532","v534","v535","v536",
            "v538","v539","v541","v542","v543","v544","v545","v546","v547","v548","v549","v550",
            "v551","v552","v553","v554","v555","v556","v557","v558","v559","v560","v562","v563",
            "v564","v565","v566","v568","v572","v575","v576","v577","v578","v579","v580","v581",
            "v583","v584","v585","v586","v587","v588","v589","v592","v593","v595","v596","v597",
            "v598","v599","v600","v601","v602","v605","v606","v607","v609","v611","v612","v613",
            "v615","v618","v620","v621","v623","v624","v625","v628","v629","v630","v631","v632",
            "v633","v634","v635","v636","v637","v638","v640","v642","v646","v647","v648","v650",
            "v651","v652","v653","v654","v655","v656","v657","v658","v659","v660","v662","v663",
            "v666","v667","v668","v669","v670","v671","v672","v673","v674","v675","v676","v678",
            "v680","v681","v683","v686","v687","v688","v689","v690","v691","v692","v693","v694",
            "v695","v696","v697","v698","v699","v700","v701","v702","v703","v704","v705","v706",
            "v710","v712","v713","v714","v715","v716","v717","v718","v721","v723","v724","v725",
            "v726","v727","v728","v729","v730","v731","v732","v733","v734","v735","v736","v737",
            "v738","v740","v741","v751","v752","v753","v754","v755","v756","v757","v758","v759",
            "v761","v762","v763","v764","v765","v766","v767","v768","v769","v770","v771","v772",
            "v773","v774","v775","v776","v777","v778","v779","v780","v781","v782","v783","v784",
            "v785","v786","v788","v790","v791","v793","v794","v795","v796","v797","v798","v799",
            "v800","v801","v802","v803","v804","v806","v807","v808","v809","v810","v811","v812",
            "v813","v814","v815","v817","v818","v819","v822","v823","v824","v826","v829","v830",
            "v833","v837","v839","v840","v841","v843","v844","v845","v846","v847","v848","v850",
            "v851","v852","v853","v854","v857","v858","v859","v860","v861","v863","v864","v866",
            "v867","v868","v870","v871","v872","v873","v874","v876","v879","v880","v881","v883",
            "v885","v887","v888","v897","v898","v899","v900","v901","v902","v905","v906","v907",
            "v908","v909","v910","v912","v913","v915","v916","v918","v919","v920","v921","v922",
            "v923","v924","v925","v928","v929","v930","v931","v933","v934","v936","v939","v940",
            "v942","v943","v944","v945","v946","v947","v949","v950","v951","v952","v953","v954",
            "v955","v956","v957","v958","v959","v960","v961","v962","v963","v964","v965","v969",
            "v971","v974","v976","v977","v978","v979","v980","v981","v982","v983","v985","v986",
            "v987","v989","v991","v992","v993","v994","v996","v997","v1000","v1001","v1002","v1003",
            "v1004","v1006","v1009","v1011","v1012","v1013","v1014","v1015","v1016","v1017","v1019",
            "v1020","v1021","v1023","v1024","v1025","v1028","v1029","v1030","v1031","v1033","v1034",
            "v1035","v1036","v1037","v1038","v1039","v1040","v1042","v1043","v1045","v1046","v1047",
            "v1049","v1050","v1051","v1052","v1053","v1054","v1058","v1059","v1061","v1063","v1065",
            "v1066","v1068","v1069","v1072","v1073","v1074","v1075"}
          "DAEO inequality constraints";
        parameter Real Gdiag[n+p]( fixed=false)
          "Diagonal entries of quadratic objective matrix G";
        parameter Solver.MyDaeoObject dfba=Solver.MyDaeoObject(
            params,
            variables,
            objective,
            equalities,
            inequalities,
            Gdiag,
            size(Gdiag, 1)) "Linear external DAEO object" annotation (fixed=true);

        // outputs of DAEO
        Real daeoOut[n] "DAEO solution vector";
        Real sigma[q] "DAEO switching function values";
        // names of fluxes to be mapped to solution vector
        Real v1;
        Real v2;
        Real v3;
        Real v4;
        Real v5;
        Real v6;
        Real v7;
        Real v8;
        Real v9;
        Real v10;
        Real v11;
        Real v12;
        Real v13;
        Real v14;
        Real v15;
        Real v16;
        Real v17;
        Real v18;
        Real v19;
        Real v20;
        Real v21;
        Real v22;
        Real v23;
        Real v24;
        Real v25;
        Real v26;
        Real v27;
        Real v28;
        Real v29;
        Real v30;
        Real v31;
        Real v32;
        Real v33;
        Real v34;
        Real v35;
        Real v36;
        Real v37;
        Real v38;
        Real v39;
        Real v40;
        Real v41;
        Real v42;
        Real v43;
        Real v44;
        Real v45;
        Real v46;
        Real v47;
        Real v48;
        Real v49;
        Real v50;
        Real v51;
        Real v52;
        Real v53;
        Real v54;
        Real v55;
        Real v56;
        Real v57;
        Real v58;
        Real v59;
        Real v60;
        Real v61;
        Real v62;
        Real v63;
        Real v64;
        Real v65;
        Real v66;
        Real v67;
        Real v68;
        Real v69;
        Real v70;
        Real v71;
        Real v72;
        Real v73;
        Real v74;
        Real v75;
        Real v76;
        Real v77;
        Real v78;
        Real v79;
        Real v80;
        Real v81;
        Real v82;
        Real v83;
        Real v84;
        Real v85;
        Real v86;
        Real v87;
        Real v88;
        Real v89;
        Real v90;
        Real v91;
        Real v92;
        Real v93;
        Real v94;
        Real v95;
        Real v96;
        Real v97;
        Real v98;
        Real v99;
        Real v100;
        Real v101;
        Real v102;
        Real v103;
        Real v104;
        Real v105;
        Real v106;
        Real v107;
        Real v108;
        Real v109;
        Real v110;
        Real v111;
        Real v112;
        Real v113;
        Real v114;
        Real v115;
        Real v116;
        Real v117;
        Real v118;
        Real v119;
        Real v120;
        Real v121;
        Real v122;
        Real v123;
        Real v124;
        Real v125;
        Real v126;
        Real v127;
        Real v128;
        Real v129;
        Real v130;
        Real v131;
        Real v132;
        Real v133;
        Real v134;
        Real v135;
        Real v136;
        Real v137;
        Real v138;
        Real v139;
        Real v140;
        Real v141;
        Real v142;
        Real v143;
        Real v144;
        Real v145;
        Real v146;
        Real v147;
        Real v148;
        Real v149;
        Real v150;
        Real v151;
        Real v152;
        Real v153;
        Real v154;
        Real v155;
        Real v156;
        Real v157;
        Real v158;
        Real v159;
        Real v160;
        Real v161;
        Real v162;
        Real v163;
        Real v164;
        Real v165;
        Real v166;
        Real v167;
        Real v168;
        Real v169;
        Real v170;
        Real v171;
        Real v172;
        Real v173;
        Real v174;
        Real v175;
        Real v176;
        Real v177;
        Real v178;
        Real v179;
        Real v180;
        Real v181;
        Real v182;
        Real v183;
        Real v184;
        Real v185;
        Real v186;
        Real v187;
        Real v188;
        Real v189;
        Real v190;
        Real v191;
        Real v192;
        Real v193;
        Real v194;
        Real v195;
        Real v196;
        Real v197;
        Real v198;
        Real v199;
        Real v200;
        Real v201;
        Real v202;
        Real v203;
        Real v204;
        Real v205;
        Real v206;
        Real v207;
        Real v208;
        Real v209;
        Real v210;
        Real v211;
        Real v212;
        Real v213;
        Real v214;
        Real v215;
        Real v216;
        Real v217;
        Real v218;
        Real v219;
        Real v220;
        Real v221;
        Real v222;
        Real v223;
        Real v224;
        Real v225;
        Real v226;
        Real v227;
        Real v228;
        Real v229;
        Real v230;
        Real v231;
        Real v232;
        Real v233;
        Real v234;
        Real v235;
        Real v236;
        Real v237;
        Real v238;
        Real v239;
        Real v240;
        Real v241;
        Real v242;
        Real v243;
        Real v244;
        Real v245;
        Real v246;
        Real v247;
        Real v248;
        Real v249;
        Real v250;
        Real v251;
        Real v252;
        Real v253;
        Real v254;
        Real v255;
        Real v256;
        Real v257;
        Real v258;
        Real v259;
        Real v260;
        Real v261;
        Real v262;
        Real v263;
        Real v264;
        Real v265;
        Real v266;
        Real v267;
        Real v268;
        Real v269;
        Real v270;
        Real v271;
        Real v272;
        Real v273;
        Real v274;
        Real v275;
        Real v276;
        Real v277;
        Real v278;
        Real v279;
        Real v280;
        Real v281;
        Real v282;
        Real v283;
        Real v284;
        Real v285;
        Real v286;
        Real v287;
        Real v288;
        Real v289;
        Real v290;
        Real v291;
        Real v292;
        Real v293;
        Real v294;
        Real v295;
        Real v296;
        Real v297;
        Real v298;
        Real v299;
        Real v300;
        Real v301;
        Real v302;
        Real v303;
        Real v304;
        Real v305;
        Real v306;
        Real v307;
        Real v308;
        Real v309;
        Real v310;
        Real v311;
        Real v312;
        Real v313;
        Real v314;
        Real v315;
        Real v316;
        Real v317;
        Real v318;
        Real v319;
        Real v320;
        Real v321;
        Real v322;
        Real v323;
        Real v324;
        Real v325;
        Real v326;
        Real v327;
        Real v328;
        Real v329;
        Real v330;
        Real v331;
        Real v332;
        Real v333;
        Real v334;
        Real v335;
        Real v336;
        Real v337;
        Real v338;
        Real v339;
        Real v340;
        Real v341;
        Real v342;
        Real v343;
        Real v344;
        Real v345;
        Real v346;
        Real v347;
        Real v348;
        Real v349;
        Real v350;
        Real v351;
        Real v352;
        Real v353;
        Real v354;
        Real v355;
        Real v356;
        Real v357;
        Real v358;
        Real v359;
        Real v360;
        Real v361;
        Real v362;
        Real v363;
        Real v364;
        Real v365;
        Real v366;
        Real v367;
        Real v368;
        Real v369;
        Real v370;
        Real v371;
        Real v372;
        Real v373;
        Real v374;
        Real v375;
        Real v376;
        Real v377;
        Real v378;
        Real v379;
        Real v380;
        Real v381;
        Real v382;
        Real v383;
        Real v384;
        Real v385;
        Real v386;
        Real v387;
        Real v388;
        Real v389;
        Real v390;
        Real v391;
        Real v392;
        Real v393;
        Real v394;
        Real v395;
        Real v396;
        Real v397;
        Real v398;
        Real v399;
        Real v400;
        Real v401;
        Real v402;
        Real v403;
        Real v404;
        Real v405;
        Real v406;
        Real v407;
        Real v408;
        Real v409;
        Real v410;
        Real v411;
        Real v412;
        Real v413;
        Real v414;
        Real v415;
        Real v416;
        Real v417;
        Real v418;
        Real v419;
        Real v420;
        Real v421;
        Real v422;
        Real v423;
        Real v424;
        Real v425;
        Real v426;
        Real v427;
        Real v428;
        Real v429;
        Real v430;
        Real v431;
        Real v432;
        Real v433;
        Real v434;
        Real v435;
        Real v436;
        Real v437;
        Real v438;
        Real v439;
        Real v440;
        Real v441;
        Real v442;
        Real v443;
        Real v444;
        Real v445;
        Real v446;
        Real v447;
        Real v448;
        Real v449;
        Real v450;
        Real v451;
        Real v452;
        Real v453;
        Real v454;
        Real v455;
        Real v456;
        Real v457;
        Real v458;
        Real v459;
        Real v460;
        Real v461;
        Real v462;
        Real v463;
        Real v464;
        Real v465;
        Real v466;
        Real v467;
        Real v468;
        Real v469;
        Real v470;
        Real v471;
        Real v472;
        Real v473;
        Real v474;
        Real v475;
        Real v476;
        Real v477;
        Real v478;
        Real v479;
        Real v480;
        Real v481;
        Real v482;
        Real v483;
        Real v484;
        Real v485;
        Real v486;
        Real v487;
        Real v488;
        Real v489;
        Real v490;
        Real v491;
        Real v492;
        Real v493;
        Real v494;
        Real v495;
        Real v496;
        Real v497;
        Real v498;
        Real v499;
        Real v500;
        Real v501;
        Real v502;
        Real v503;
        Real v504;
        Real v505;
        Real v506;
        Real v507;
        Real v508;
        Real v509;
        Real v510;
        Real v511;
        Real v512;
        Real v513;
        Real v514;
        Real v515;
        Real v516;
        Real v517;
        Real v518;
        Real v519;
        Real v520;
        Real v521;
        Real v522;
        Real v523;
        Real v524;
        Real v525;
        Real v526;
        Real v527;
        Real v528;
        Real v529;
        Real v530;
        Real v531;
        Real v532;
        Real v533;
        Real v534;
        Real v535;
        Real v536;
        Real v537;
        Real v538;
        Real v539;
        Real v540;
        Real v541;
        Real v542;
        Real v543;
        Real v544;
        Real v545;
        Real v546;
        Real v547;
        Real v548;
        Real v549;
        Real v550;
        Real v551;
        Real v552;
        Real v553;
        Real v554;
        Real v555;
        Real v556;
        Real v557;
        Real v558;
        Real v559;
        Real v560;
        Real v561;
        Real v562;
        Real v563;
        Real v564;
        Real v565;
        Real v566;
        Real v567;
        Real v568;
        Real v569;
        Real v570;
        Real v571;
        Real v572;
        Real v573;
        Real v574;
        Real v575;
        Real v576;
        Real v577;
        Real v578;
        Real v579;
        Real v580;
        Real v581;
        Real v582;
        Real v583;
        Real v584;
        Real v585;
        Real v586;
        Real v587;
        Real v588;
        Real v589;
        Real v590;
        Real v591;
        Real v592;
        Real v593;
        Real v594;
        Real v595;
        Real v596;
        Real v597;
        Real v598;
        Real v599;
        Real v600;
        Real v601;
        Real v602;
        Real v603;
        Real v604;
        Real v605;
        Real v606;
        Real v607;
        Real v608;
        Real v609;
        Real v610;
        Real v611;
        Real v612;
        Real v613;
        Real v614;
        Real v615;
        Real v616;
        Real v617;
        Real v618;
        Real v619;
        Real v620;
        Real v621;
        Real v622;
        Real v623;
        Real v624;
        Real v625;
        Real v626;
        Real v627;
        Real v628;
        Real v629;
        Real v630;
        Real v631;
        Real v632;
        Real v633;
        Real v634;
        Real v635;
        Real v636;
        Real v637;
        Real v638;
        Real v639;
        Real v640;
        Real v641;
        Real v642;
        Real v643;
        Real v644;
        Real v645;
        Real v646;
        Real v647;
        Real v648;
        Real v649;
        Real v650;
        Real v651;
        Real v652;
        Real v653;
        Real v654;
        Real v655;
        Real v656;
        Real v657;
        Real v658;
        Real v659;
        Real v660;
        Real v661;
        Real v662;
        Real v663;
        Real v664;
        Real v665;
        Real v666;
        Real v667;
        Real v668;
        Real v669;
        Real v670;
        Real v671;
        Real v672;
        Real v673;
        Real v674;
        Real v675;
        Real v676;
        Real v677;
        Real v678;
        Real v679;
        Real v680;
        Real v681;
        Real v682;
        Real v683;
        Real v684;
        Real v685;
        Real v686;
        Real v687;
        Real v688;
        Real v689;
        Real v690;
        Real v691;
        Real v692;
        Real v693;
        Real v694;
        Real v695;
        Real v696;
        Real v697;
        Real v698;
        Real v699;
        Real v700;
        Real v701;
        Real v702;
        Real v703;
        Real v704;
        Real v705;
        Real v706;
        Real v707;
        Real v708;
        Real v709;
        Real v710;
        Real v711;
        Real v712;
        Real v713;
        Real v714;
        Real v715;
        Real v716;
        Real v717;
        Real v718;
        Real v719;
        Real v720;
        Real v721;
        Real v722;
        Real v723;
        Real v724;
        Real v725;
        Real v726;
        Real v727;
        Real v728;
        Real v729;
        Real v730;
        Real v731;
        Real v732;
        Real v733;
        Real v734;
        Real v735;
        Real v736;
        Real v737;
        Real v738;
        Real v739;
        Real v740;
        Real v741;
        Real v742;
        Real v743;
        Real v744;
        Real v745;
        Real v746;
        Real v747;
        Real v748;
        Real v749;
        Real v750;
        Real v751;
        Real v752;
        Real v753;
        Real v754;
        Real v755;
        Real v756;
        Real v757;
        Real v758;
        Real v759;
        Real v760;
        Real v761;
        Real v762;
        Real v763;
        Real v764;
        Real v765;
        Real v766;
        Real v767;
        Real v768;
        Real v769;
        Real v770;
        Real v771;
        Real v772;
        Real v773;
        Real v774;
        Real v775;
        Real v776;
        Real v777;
        Real v778;
        Real v779;
        Real v780;
        Real v781;
        Real v782;
        Real v783;
        Real v784;
        Real v785;
        Real v786;
        Real v787;
        Real v788;
        Real v789;
        Real v790;
        Real v791;
        Real v792;
        Real v793;
        Real v794;
        Real v795;
        Real v796;
        Real v797;
        Real v798;
        Real v799;
        Real v800;
        Real v801;
        Real v802;
        Real v803;
        Real v804;
        Real v805;
        Real v806;
        Real v807;
        Real v808;
        Real v809;
        Real v810;
        Real v811;
        Real v812;
        Real v813;
        Real v814;
        Real v815;
        Real v816;
        Real v817;
        Real v818;
        Real v819;
        Real v820;
        Real v821;
        Real v822;
        Real v823;
        Real v824;
        Real v825;
        Real v826;
        Real v827;
        Real v828;
        Real v829;
        Real v830;
        Real v831;
        Real v832;
        Real v833;
        Real v834;
        Real v835;
        Real v836;
        Real v837;
        Real v838;
        Real v839;
        Real v840;
        Real v841;
        Real v842;
        Real v843;
        Real v844;
        Real v845;
        Real v846;
        Real v847;
        Real v848;
        Real v849;
        Real v850;
        Real v851;
        Real v852;
        Real v853;
        Real v854;
        Real v855;
        Real v856;
        Real v857;
        Real v858;
        Real v859;
        Real v860;
        Real v861;
        Real v862;
        Real v863;
        Real v864;
        Real v865;
        Real v866;
        Real v867;
        Real v868;
        Real v869;
        Real v870;
        Real v871;
        Real v872;
        Real v873;
        Real v874;
        Real v875;
        Real v876;
        Real v877;
        Real v878;
        Real v879;
        Real v880;
        Real v881;
        Real v882;
        Real v883;
        Real v884;
        Real v885;
        Real v886;
        Real v887;
        Real v888;
        Real v889;
        Real v890;
        Real v891;
        Real v892;
        Real v893;
        Real v894;
        Real v895;
        Real v896;
        Real v897;
        Real v898;
        Real v899;
        Real v900;
        Real v901;
        Real v902;
        Real v903;
        Real v904;
        Real v905;
        Real v906;
        Real v907;
        Real v908;
        Real v909;
        Real v910;
        Real v911;
        Real v912;
        Real v913;
        Real v914;
        Real v915;
        Real v916;
        Real v917;
        Real v918;
        Real v919;
        Real v920;
        Real v921;
        Real v922;
        Real v923;
        Real v924;
        Real v925;
        Real v926;
        Real v927;
        Real v928;
        Real v929;
        Real v930;
        Real v931;
        Real v932;
        Real v933;
        Real v934;
        Real v935;
        Real v936;
        Real v937;
        Real v938;
        Real v939;
        Real v940;
        Real v941;
        Real v942;
        Real v943;
        Real v944;
        Real v945;
        Real v946;
        Real v947;
        Real v948;
        Real v949;
        Real v950;
        Real v951;
        Real v952;
        Real v953;
        Real v954;
        Real v955;
        Real v956;
        Real v957;
        Real v958;
        Real v959;
        Real v960;
        Real v961;
        Real v962;
        Real v963;
        Real v964;
        Real v965;
        Real v966;
        Real v967;
        Real v968;
        Real v969;
        Real v970;
        Real v971;
        Real v972;
        Real v973;
        Real v974;
        Real v975;
        Real v976;
        Real v977;
        Real v978;
        Real v979;
        Real v980;
        Real v981;
        Real v982;
        Real v983;
        Real v984;
        Real v985;
        Real v986;
        Real v987;
        Real v988;
        Real v989;
        Real v990;
        Real v991;
        Real v992;
        Real v993;
        Real v994;
        Real v995;
        Real v996;
        Real v997;
        Real v998;
        Real v999;
        Real v1000;
        Real v1001;
        Real v1002;
        Real v1003;
        Real v1004;
        Real v1005;
        Real v1006;
        Real v1007;
        Real v1008;
        Real v1009;
        Real v1010;
        Real v1011;
        Real v1012;
        Real v1013;
        Real v1014;
        Real v1015;
        Real v1016;
        Real v1017;
        Real v1018;
        Real v1019;
        Real v1020;
        Real v1021;
        Real v1022;
        Real v1023;
        Real v1024;
        Real v1025;
        Real v1026;
        Real v1027;
        Real v1028;
        Real v1029;
        Real v1030;
        Real v1031;
        Real v1032;
        Real v1033;
        Real v1034;
        Real v1035;
        Real v1036;
        Real v1037;
        Real v1038;
        Real v1039;
        Real v1040;
        Real v1041;
        Real v1042;
        Real v1043;
        Real v1044;
        Real v1045;
        Real v1046;
        Real v1047;
        Real v1048;
        Real v1049;
        Real v1050;
        Real v1051;
        Real v1052;
        Real v1053;
        Real v1054;
        Real v1055;
        Real v1056;
        Real v1057;
        Real v1058;
        Real v1059;
        Real v1060;
        Real v1061;
        Real v1062;
        Real v1063;
        Real v1064;
        Real v1065;
        Real v1066;
        Real v1067;
        Real v1068;
        Real v1069;
        Real v1070;
        Real v1071;
        Real v1072;
        Real v1073;
        Real v1074;
        Real v1075;
      initial algorithm
         for i in 1:p loop
           Gdiag[i]:=1e-12;
         end for;
         for i in p+1:p+n loop
           Gdiag[i]:=1e-6;
         end for;
      equation
        // mapping of fluxes to their respective entries in solution vector
        v1 = daeoOut[1];
        v2 = daeoOut[2];
        v3 = daeoOut[3];
        v4 = daeoOut[4];
        v5 = daeoOut[5];
        v6 = daeoOut[6];
        v7 = daeoOut[7];
        v8 = daeoOut[8];
        v9 = daeoOut[9];
        v10 = daeoOut[10];
        v11 = daeoOut[11];
        v12 = daeoOut[12];
        v13 = daeoOut[13];
        v14 = daeoOut[14];
        v15 = daeoOut[15];
        v16 = daeoOut[16];
        v17 = daeoOut[17];
        v18 = daeoOut[18];
        v19 = daeoOut[19];
        v20 = daeoOut[20];
        v21 = daeoOut[21];
        v22 = daeoOut[22];
        v23 = daeoOut[23];
        v24 = daeoOut[24];
        v25 = daeoOut[25];
        v26 = daeoOut[26];
        v27 = daeoOut[27];
        v28 = daeoOut[28];
        v29 = daeoOut[29];
        v30 = daeoOut[30];
        v31 = daeoOut[31];
        v32 = daeoOut[32];
        v33 = daeoOut[33];
        v34 = daeoOut[34];
        v35 = daeoOut[35];
        v36 = daeoOut[36];
        v37 = daeoOut[37];
        v38 = daeoOut[38];
        v39 = daeoOut[39];
        v40 = daeoOut[40];
        v41 = daeoOut[41];
        v42 = daeoOut[42];
        v43 = daeoOut[43];
        v44 = daeoOut[44];
        v45 = daeoOut[45];
        v46 = daeoOut[46];
        v47 = daeoOut[47];
        v48 = daeoOut[48];
        v49 = daeoOut[49];
        v50 = daeoOut[50];
        v51 = daeoOut[51];
        v52 = daeoOut[52];
        v53 = daeoOut[53];
        v54 = daeoOut[54];
        v55 = daeoOut[55];
        v56 = daeoOut[56];
        v57 = daeoOut[57];
        v58 = daeoOut[58];
        v59 = daeoOut[59];
        v60 = daeoOut[60];
        v61 = daeoOut[61];
        v62 = daeoOut[62];
        v63 = daeoOut[63];
        v64 = daeoOut[64];
        v65 = daeoOut[65];
        v66 = daeoOut[66];
        v67 = daeoOut[67];
        v68 = daeoOut[68];
        v69 = daeoOut[69];
        v70 = daeoOut[70];
        v71 = daeoOut[71];
        v72 = daeoOut[72];
        v73 = daeoOut[73];
        v74 = daeoOut[74];
        v75 = daeoOut[75];
        v76 = daeoOut[76];
        v77 = daeoOut[77];
        v78 = daeoOut[78];
        v79 = daeoOut[79];
        v80 = daeoOut[80];
        v81 = daeoOut[81];
        v82 = daeoOut[82];
        v83 = daeoOut[83];
        v84 = daeoOut[84];
        v85 = daeoOut[85];
        v86 = daeoOut[86];
        v87 = daeoOut[87];
        v88 = daeoOut[88];
        v89 = daeoOut[89];
        v90 = daeoOut[90];
        v91 = daeoOut[91];
        v92 = daeoOut[92];
        v93 = daeoOut[93];
        v94 = daeoOut[94];
        v95 = daeoOut[95];
        v96 = daeoOut[96];
        v97 = daeoOut[97];
        v98 = daeoOut[98];
        v99 = daeoOut[99];
        v100 = daeoOut[100];
        v101 = daeoOut[101];
        v102 = daeoOut[102];
        v103 = daeoOut[103];
        v104 = daeoOut[104];
        v105 = daeoOut[105];
        v106 = daeoOut[106];
        v107 = daeoOut[107];
        v108 = daeoOut[108];
        v109 = daeoOut[109];
        v110 = daeoOut[110];
        v111 = daeoOut[111];
        v112 = daeoOut[112];
        v113 = daeoOut[113];
        v114 = daeoOut[114];
        v115 = daeoOut[115];
        v116 = daeoOut[116];
        v117 = daeoOut[117];
        v118 = daeoOut[118];
        v119 = daeoOut[119];
        v120 = daeoOut[120];
        v121 = daeoOut[121];
        v122 = daeoOut[122];
        v123 = daeoOut[123];
        v124 = daeoOut[124];
        v125 = daeoOut[125];
        v126 = daeoOut[126];
        v127 = daeoOut[127];
        v128 = daeoOut[128];
        v129 = daeoOut[129];
        v130 = daeoOut[130];
        v131 = daeoOut[131];
        v132 = daeoOut[132];
        v133 = daeoOut[133];
        v134 = daeoOut[134];
        v135 = daeoOut[135];
        v136 = daeoOut[136];
        v137 = daeoOut[137];
        v138 = daeoOut[138];
        v139 = daeoOut[139];
        v140 = daeoOut[140];
        v141 = daeoOut[141];
        v142 = daeoOut[142];
        v143 = daeoOut[143];
        v144 = daeoOut[144];
        v145 = daeoOut[145];
        v146 = daeoOut[146];
        v147 = daeoOut[147];
        v148 = daeoOut[148];
        v149 = daeoOut[149];
        v150 = daeoOut[150];
        v151 = daeoOut[151];
        v152 = daeoOut[152];
        v153 = daeoOut[153];
        v154 = daeoOut[154];
        v155 = daeoOut[155];
        v156 = daeoOut[156];
        v157 = daeoOut[157];
        v158 = daeoOut[158];
        v159 = daeoOut[159];
        v160 = daeoOut[160];
        v161 = daeoOut[161];
        v162 = daeoOut[162];
        v163 = daeoOut[163];
        v164 = daeoOut[164];
        v165 = daeoOut[165];
        v166 = daeoOut[166];
        v167 = daeoOut[167];
        v168 = daeoOut[168];
        v169 = daeoOut[169];
        v170 = daeoOut[170];
        v171 = daeoOut[171];
        v172 = daeoOut[172];
        v173 = daeoOut[173];
        v174 = daeoOut[174];
        v175 = daeoOut[175];
        v176 = daeoOut[176];
        v177 = daeoOut[177];
        v178 = daeoOut[178];
        v179 = daeoOut[179];
        v180 = daeoOut[180];
        v181 = daeoOut[181];
        v182 = daeoOut[182];
        v183 = daeoOut[183];
        v184 = daeoOut[184];
        v185 = daeoOut[185];
        v186 = daeoOut[186];
        v187 = daeoOut[187];
        v188 = daeoOut[188];
        v189 = daeoOut[189];
        v190 = daeoOut[190];
        v191 = daeoOut[191];
        v192 = daeoOut[192];
        v193 = daeoOut[193];
        v194 = daeoOut[194];
        v195 = daeoOut[195];
        v196 = daeoOut[196];
        v197 = daeoOut[197];
        v198 = daeoOut[198];
        v199 = daeoOut[199];
        v200 = daeoOut[200];
        v201 = daeoOut[201];
        v202 = daeoOut[202];
        v203 = daeoOut[203];
        v204 = daeoOut[204];
        v205 = daeoOut[205];
        v206 = daeoOut[206];
        v207 = daeoOut[207];
        v208 = daeoOut[208];
        v209 = daeoOut[209];
        v210 = daeoOut[210];
        v211 = daeoOut[211];
        v212 = daeoOut[212];
        v213 = daeoOut[213];
        v214 = daeoOut[214];
        v215 = daeoOut[215];
        v216 = daeoOut[216];
        v217 = daeoOut[217];
        v218 = daeoOut[218];
        v219 = daeoOut[219];
        v220 = daeoOut[220];
        v221 = daeoOut[221];
        v222 = daeoOut[222];
        v223 = daeoOut[223];
        v224 = daeoOut[224];
        v225 = daeoOut[225];
        v226 = daeoOut[226];
        v227 = daeoOut[227];
        v228 = daeoOut[228];
        v229 = daeoOut[229];
        v230 = daeoOut[230];
        v231 = daeoOut[231];
        v232 = daeoOut[232];
        v233 = daeoOut[233];
        v234 = daeoOut[234];
        v235 = daeoOut[235];
        v236 = daeoOut[236];
        v237 = daeoOut[237];
        v238 = daeoOut[238];
        v239 = daeoOut[239];
        v240 = daeoOut[240];
        v241 = daeoOut[241];
        v242 = daeoOut[242];
        v243 = daeoOut[243];
        v244 = daeoOut[244];
        v245 = daeoOut[245];
        v246 = daeoOut[246];
        v247 = daeoOut[247];
        v248 = daeoOut[248];
        v249 = daeoOut[249];
        v250 = daeoOut[250];
        v251 = daeoOut[251];
        v252 = daeoOut[252];
        v253 = daeoOut[253];
        v254 = daeoOut[254];
        v255 = daeoOut[255];
        v256 = daeoOut[256];
        v257 = daeoOut[257];
        v258 = daeoOut[258];
        v259 = daeoOut[259];
        v260 = daeoOut[260];
        v261 = daeoOut[261];
        v262 = daeoOut[262];
        v263 = daeoOut[263];
        v264 = daeoOut[264];
        v265 = daeoOut[265];
        v266 = daeoOut[266];
        v267 = daeoOut[267];
        v268 = daeoOut[268];
        v269 = daeoOut[269];
        v270 = daeoOut[270];
        v271 = daeoOut[271];
        v272 = daeoOut[272];
        v273 = daeoOut[273];
        v274 = daeoOut[274];
        v275 = daeoOut[275];
        v276 = daeoOut[276];
        v277 = daeoOut[277];
        v278 = daeoOut[278];
        v279 = daeoOut[279];
        v280 = daeoOut[280];
        v281 = daeoOut[281];
        v282 = daeoOut[282];
        v283 = daeoOut[283];
        v284 = daeoOut[284];
        v285 = daeoOut[285];
        v286 = daeoOut[286];
        v287 = daeoOut[287];
        v288 = daeoOut[288];
        v289 = daeoOut[289];
        v290 = daeoOut[290];
        v291 = daeoOut[291];
        v292 = daeoOut[292];
        v293 = daeoOut[293];
        v294 = daeoOut[294];
        v295 = daeoOut[295];
        v296 = daeoOut[296];
        v297 = daeoOut[297];
        v298 = daeoOut[298];
        v299 = daeoOut[299];
        v300 = daeoOut[300];
        v301 = daeoOut[301];
        v302 = daeoOut[302];
        v303 = daeoOut[303];
        v304 = daeoOut[304];
        v305 = daeoOut[305];
        v306 = daeoOut[306];
        v307 = daeoOut[307];
        v308 = daeoOut[308];
        v309 = daeoOut[309];
        v310 = daeoOut[310];
        v311 = daeoOut[311];
        v312 = daeoOut[312];
        v313 = daeoOut[313];
        v314 = daeoOut[314];
        v315 = daeoOut[315];
        v316 = daeoOut[316];
        v317 = daeoOut[317];
        v318 = daeoOut[318];
        v319 = daeoOut[319];
        v320 = daeoOut[320];
        v321 = daeoOut[321];
        v322 = daeoOut[322];
        v323 = daeoOut[323];
        v324 = daeoOut[324];
        v325 = daeoOut[325];
        v326 = daeoOut[326];
        v327 = daeoOut[327];
        v328 = daeoOut[328];
        v329 = daeoOut[329];
        v330 = daeoOut[330];
        v331 = daeoOut[331];
        v332 = daeoOut[332];
        v333 = daeoOut[333];
        v334 = daeoOut[334];
        v335 = daeoOut[335];
        v336 = daeoOut[336];
        v337 = daeoOut[337];
        v338 = daeoOut[338];
        v339 = daeoOut[339];
        v340 = daeoOut[340];
        v341 = daeoOut[341];
        v342 = daeoOut[342];
        v343 = daeoOut[343];
        v344 = daeoOut[344];
        v345 = daeoOut[345];
        v346 = daeoOut[346];
        v347 = daeoOut[347];
        v348 = daeoOut[348];
        v349 = daeoOut[349];
        v350 = daeoOut[350];
        v351 = daeoOut[351];
        v352 = daeoOut[352];
        v353 = daeoOut[353];
        v354 = daeoOut[354];
        v355 = daeoOut[355];
        v356 = daeoOut[356];
        v357 = daeoOut[357];
        v358 = daeoOut[358];
        v359 = daeoOut[359];
        v360 = daeoOut[360];
        v361 = daeoOut[361];
        v362 = daeoOut[362];
        v363 = daeoOut[363];
        v364 = daeoOut[364];
        v365 = daeoOut[365];
        v366 = daeoOut[366];
        v367 = daeoOut[367];
        v368 = daeoOut[368];
        v369 = daeoOut[369];
        v370 = daeoOut[370];
        v371 = daeoOut[371];
        v372 = daeoOut[372];
        v373 = daeoOut[373];
        v374 = daeoOut[374];
        v375 = daeoOut[375];
        v376 = daeoOut[376];
        v377 = daeoOut[377];
        v378 = daeoOut[378];
        v379 = daeoOut[379];
        v380 = daeoOut[380];
        v381 = daeoOut[381];
        v382 = daeoOut[382];
        v383 = daeoOut[383];
        v384 = daeoOut[384];
        v385 = daeoOut[385];
        v386 = daeoOut[386];
        v387 = daeoOut[387];
        v388 = daeoOut[388];
        v389 = daeoOut[389];
        v390 = daeoOut[390];
        v391 = daeoOut[391];
        v392 = daeoOut[392];
        v393 = daeoOut[393];
        v394 = daeoOut[394];
        v395 = daeoOut[395];
        v396 = daeoOut[396];
        v397 = daeoOut[397];
        v398 = daeoOut[398];
        v399 = daeoOut[399];
        v400 = daeoOut[400];
        v401 = daeoOut[401];
        v402 = daeoOut[402];
        v403 = daeoOut[403];
        v404 = daeoOut[404];
        v405 = daeoOut[405];
        v406 = daeoOut[406];
        v407 = daeoOut[407];
        v408 = daeoOut[408];
        v409 = daeoOut[409];
        v410 = daeoOut[410];
        v411 = daeoOut[411];
        v412 = daeoOut[412];
        v413 = daeoOut[413];
        v414 = daeoOut[414];
        v415 = daeoOut[415];
        v416 = daeoOut[416];
        v417 = daeoOut[417];
        v418 = daeoOut[418];
        v419 = daeoOut[419];
        v420 = daeoOut[420];
        v421 = daeoOut[421];
        v422 = daeoOut[422];
        v423 = daeoOut[423];
        v424 = daeoOut[424];
        v425 = daeoOut[425];
        v426 = daeoOut[426];
        v427 = daeoOut[427];
        v428 = daeoOut[428];
        v429 = daeoOut[429];
        v430 = daeoOut[430];
        v431 = daeoOut[431];
        v432 = daeoOut[432];
        v433 = daeoOut[433];
        v434 = daeoOut[434];
        v435 = daeoOut[435];
        v436 = daeoOut[436];
        v437 = daeoOut[437];
        v438 = daeoOut[438];
        v439 = daeoOut[439];
        v440 = daeoOut[440];
        v441 = daeoOut[441];
        v442 = daeoOut[442];
        v443 = daeoOut[443];
        v444 = daeoOut[444];
        v445 = daeoOut[445];
        v446 = daeoOut[446];
        v447 = daeoOut[447];
        v448 = daeoOut[448];
        v449 = daeoOut[449];
        v450 = daeoOut[450];
        v451 = daeoOut[451];
        v452 = daeoOut[452];
        v453 = daeoOut[453];
        v454 = daeoOut[454];
        v455 = daeoOut[455];
        v456 = daeoOut[456];
        v457 = daeoOut[457];
        v458 = daeoOut[458];
        v459 = daeoOut[459];
        v460 = daeoOut[460];
        v461 = daeoOut[461];
        v462 = daeoOut[462];
        v463 = daeoOut[463];
        v464 = daeoOut[464];
        v465 = daeoOut[465];
        v466 = daeoOut[466];
        v467 = daeoOut[467];
        v468 = daeoOut[468];
        v469 = daeoOut[469];
        v470 = daeoOut[470];
        v471 = daeoOut[471];
        v472 = daeoOut[472];
        v473 = daeoOut[473];
        v474 = daeoOut[474];
        v475 = daeoOut[475];
        v476 = daeoOut[476];
        v477 = daeoOut[477];
        v478 = daeoOut[478];
        v479 = daeoOut[479];
        v480 = daeoOut[480];
        v481 = daeoOut[481];
        v482 = daeoOut[482];
        v483 = daeoOut[483];
        v484 = daeoOut[484];
        v485 = daeoOut[485];
        v486 = daeoOut[486];
        v487 = daeoOut[487];
        v488 = daeoOut[488];
        v489 = daeoOut[489];
        v490 = daeoOut[490];
        v491 = daeoOut[491];
        v492 = daeoOut[492];
        v493 = daeoOut[493];
        v494 = daeoOut[494];
        v495 = daeoOut[495];
        v496 = daeoOut[496];
        v497 = daeoOut[497];
        v498 = daeoOut[498];
        v499 = daeoOut[499];
        v500 = daeoOut[500];
        v501 = daeoOut[501];
        v502 = daeoOut[502];
        v503 = daeoOut[503];
        v504 = daeoOut[504];
        v505 = daeoOut[505];
        v506 = daeoOut[506];
        v507 = daeoOut[507];
        v508 = daeoOut[508];
        v509 = daeoOut[509];
        v510 = daeoOut[510];
        v511 = daeoOut[511];
        v512 = daeoOut[512];
        v513 = daeoOut[513];
        v514 = daeoOut[514];
        v515 = daeoOut[515];
        v516 = daeoOut[516];
        v517 = daeoOut[517];
        v518 = daeoOut[518];
        v519 = daeoOut[519];
        v520 = daeoOut[520];
        v521 = daeoOut[521];
        v522 = daeoOut[522];
        v523 = daeoOut[523];
        v524 = daeoOut[524];
        v525 = daeoOut[525];
        v526 = daeoOut[526];
        v527 = daeoOut[527];
        v528 = daeoOut[528];
        v529 = daeoOut[529];
        v530 = daeoOut[530];
        v531 = daeoOut[531];
        v532 = daeoOut[532];
        v533 = daeoOut[533];
        v534 = daeoOut[534];
        v535 = daeoOut[535];
        v536 = daeoOut[536];
        v537 = daeoOut[537];
        v538 = daeoOut[538];
        v539 = daeoOut[539];
        v540 = daeoOut[540];
        v541 = daeoOut[541];
        v542 = daeoOut[542];
        v543 = daeoOut[543];
        v544 = daeoOut[544];
        v545 = daeoOut[545];
        v546 = daeoOut[546];
        v547 = daeoOut[547];
        v548 = daeoOut[548];
        v549 = daeoOut[549];
        v550 = daeoOut[550];
        v551 = daeoOut[551];
        v552 = daeoOut[552];
        v553 = daeoOut[553];
        v554 = daeoOut[554];
        v555 = daeoOut[555];
        v556 = daeoOut[556];
        v557 = daeoOut[557];
        v558 = daeoOut[558];
        v559 = daeoOut[559];
        v560 = daeoOut[560];
        v561 = daeoOut[561];
        v562 = daeoOut[562];
        v563 = daeoOut[563];
        v564 = daeoOut[564];
        v565 = daeoOut[565];
        v566 = daeoOut[566];
        v567 = daeoOut[567];
        v568 = daeoOut[568];
        v569 = daeoOut[569];
        v570 = daeoOut[570];
        v571 = daeoOut[571];
        v572 = daeoOut[572];
        v573 = daeoOut[573];
        v574 = daeoOut[574];
        v575 = daeoOut[575];
        v576 = daeoOut[576];
        v577 = daeoOut[577];
        v578 = daeoOut[578];
        v579 = daeoOut[579];
        v580 = daeoOut[580];
        v581 = daeoOut[581];
        v582 = daeoOut[582];
        v583 = daeoOut[583];
        v584 = daeoOut[584];
        v585 = daeoOut[585];
        v586 = daeoOut[586];
        v587 = daeoOut[587];
        v588 = daeoOut[588];
        v589 = daeoOut[589];
        v590 = daeoOut[590];
        v591 = daeoOut[591];
        v592 = daeoOut[592];
        v593 = daeoOut[593];
        v594 = daeoOut[594];
        v595 = daeoOut[595];
        v596 = daeoOut[596];
        v597 = daeoOut[597];
        v598 = daeoOut[598];
        v599 = daeoOut[599];
        v600 = daeoOut[600];
        v601 = daeoOut[601];
        v602 = daeoOut[602];
        v603 = daeoOut[603];
        v604 = daeoOut[604];
        v605 = daeoOut[605];
        v606 = daeoOut[606];
        v607 = daeoOut[607];
        v608 = daeoOut[608];
        v609 = daeoOut[609];
        v610 = daeoOut[610];
        v611 = daeoOut[611];
        v612 = daeoOut[612];
        v613 = daeoOut[613];
        v614 = daeoOut[614];
        v615 = daeoOut[615];
        v616 = daeoOut[616];
        v617 = daeoOut[617];
        v618 = daeoOut[618];
        v619 = daeoOut[619];
        v620 = daeoOut[620];
        v621 = daeoOut[621];
        v622 = daeoOut[622];
        v623 = daeoOut[623];
        v624 = daeoOut[624];
        v625 = daeoOut[625];
        v626 = daeoOut[626];
        v627 = daeoOut[627];
        v628 = daeoOut[628];
        v629 = daeoOut[629];
        v630 = daeoOut[630];
        v631 = daeoOut[631];
        v632 = daeoOut[632];
        v633 = daeoOut[633];
        v634 = daeoOut[634];
        v635 = daeoOut[635];
        v636 = daeoOut[636];
        v637 = daeoOut[637];
        v638 = daeoOut[638];
        v639 = daeoOut[639];
        v640 = daeoOut[640];
        v641 = daeoOut[641];
        v642 = daeoOut[642];
        v643 = daeoOut[643];
        v644 = daeoOut[644];
        v645 = daeoOut[645];
        v646 = daeoOut[646];
        v647 = daeoOut[647];
        v648 = daeoOut[648];
        v649 = daeoOut[649];
        v650 = daeoOut[650];
        v651 = daeoOut[651];
        v652 = daeoOut[652];
        v653 = daeoOut[653];
        v654 = daeoOut[654];
        v655 = daeoOut[655];
        v656 = daeoOut[656];
        v657 = daeoOut[657];
        v658 = daeoOut[658];
        v659 = daeoOut[659];
        v660 = daeoOut[660];
        v661 = daeoOut[661];
        v662 = daeoOut[662];
        v663 = daeoOut[663];
        v664 = daeoOut[664];
        v665 = daeoOut[665];
        v666 = daeoOut[666];
        v667 = daeoOut[667];
        v668 = daeoOut[668];
        v669 = daeoOut[669];
        v670 = daeoOut[670];
        v671 = daeoOut[671];
        v672 = daeoOut[672];
        v673 = daeoOut[673];
        v674 = daeoOut[674];
        v675 = daeoOut[675];
        v676 = daeoOut[676];
        v677 = daeoOut[677];
        v678 = daeoOut[678];
        v679 = daeoOut[679];
        v680 = daeoOut[680];
        v681 = daeoOut[681];
        v682 = daeoOut[682];
        v683 = daeoOut[683];
        v684 = daeoOut[684];
        v685 = daeoOut[685];
        v686 = daeoOut[686];
        v687 = daeoOut[687];
        v688 = daeoOut[688];
        v689 = daeoOut[689];
        v690 = daeoOut[690];
        v691 = daeoOut[691];
        v692 = daeoOut[692];
        v693 = daeoOut[693];
        v694 = daeoOut[694];
        v695 = daeoOut[695];
        v696 = daeoOut[696];
        v697 = daeoOut[697];
        v698 = daeoOut[698];
        v699 = daeoOut[699];
        v700 = daeoOut[700];
        v701 = daeoOut[701];
        v702 = daeoOut[702];
        v703 = daeoOut[703];
        v704 = daeoOut[704];
        v705 = daeoOut[705];
        v706 = daeoOut[706];
        v707 = daeoOut[707];
        v708 = daeoOut[708];
        v709 = daeoOut[709];
        v710 = daeoOut[710];
        v711 = daeoOut[711];
        v712 = daeoOut[712];
        v713 = daeoOut[713];
        v714 = daeoOut[714];
        v715 = daeoOut[715];
        v716 = daeoOut[716];
        v717 = daeoOut[717];
        v718 = daeoOut[718];
        v719 = daeoOut[719];
        v720 = daeoOut[720];
        v721 = daeoOut[721];
        v722 = daeoOut[722];
        v723 = daeoOut[723];
        v724 = daeoOut[724];
        v725 = daeoOut[725];
        v726 = daeoOut[726];
        v727 = daeoOut[727];
        v728 = daeoOut[728];
        v729 = daeoOut[729];
        v730 = daeoOut[730];
        v731 = daeoOut[731];
        v732 = daeoOut[732];
        v733 = daeoOut[733];
        v734 = daeoOut[734];
        v735 = daeoOut[735];
        v736 = daeoOut[736];
        v737 = daeoOut[737];
        v738 = daeoOut[738];
        v739 = daeoOut[739];
        v740 = daeoOut[740];
        v741 = daeoOut[741];
        v742 = daeoOut[742];
        v743 = daeoOut[743];
        v744 = daeoOut[744];
        v745 = daeoOut[745];
        v746 = daeoOut[746];
        v747 = daeoOut[747];
        v748 = daeoOut[748];
        v749 = daeoOut[749];
        v750 = daeoOut[750];
        v751 = daeoOut[751];
        v752 = daeoOut[752];
        v753 = daeoOut[753];
        v754 = daeoOut[754];
        v755 = daeoOut[755];
        v756 = daeoOut[756];
        v757 = daeoOut[757];
        v758 = daeoOut[758];
        v759 = daeoOut[759];
        v760 = daeoOut[760];
        v761 = daeoOut[761];
        v762 = daeoOut[762];
        v763 = daeoOut[763];
        v764 = daeoOut[764];
        v765 = daeoOut[765];
        v766 = daeoOut[766];
        v767 = daeoOut[767];
        v768 = daeoOut[768];
        v769 = daeoOut[769];
        v770 = daeoOut[770];
        v771 = daeoOut[771];
        v772 = daeoOut[772];
        v773 = daeoOut[773];
        v774 = daeoOut[774];
        v775 = daeoOut[775];
        v776 = daeoOut[776];
        v777 = daeoOut[777];
        v778 = daeoOut[778];
        v779 = daeoOut[779];
        v780 = daeoOut[780];
        v781 = daeoOut[781];
        v782 = daeoOut[782];
        v783 = daeoOut[783];
        v784 = daeoOut[784];
        v785 = daeoOut[785];
        v786 = daeoOut[786];
        v787 = daeoOut[787];
        v788 = daeoOut[788];
        v789 = daeoOut[789];
        v790 = daeoOut[790];
        v791 = daeoOut[791];
        v792 = daeoOut[792];
        v793 = daeoOut[793];
        v794 = daeoOut[794];
        v795 = daeoOut[795];
        v796 = daeoOut[796];
        v797 = daeoOut[797];
        v798 = daeoOut[798];
        v799 = daeoOut[799];
        v800 = daeoOut[800];
        v801 = daeoOut[801];
        v802 = daeoOut[802];
        v803 = daeoOut[803];
        v804 = daeoOut[804];
        v805 = daeoOut[805];
        v806 = daeoOut[806];
        v807 = daeoOut[807];
        v808 = daeoOut[808];
        v809 = daeoOut[809];
        v810 = daeoOut[810];
        v811 = daeoOut[811];
        v812 = daeoOut[812];
        v813 = daeoOut[813];
        v814 = daeoOut[814];
        v815 = daeoOut[815];
        v816 = daeoOut[816];
        v817 = daeoOut[817];
        v818 = daeoOut[818];
        v819 = daeoOut[819];
        v820 = daeoOut[820];
        v821 = daeoOut[821];
        v822 = daeoOut[822];
        v823 = daeoOut[823];
        v824 = daeoOut[824];
        v825 = daeoOut[825];
        v826 = daeoOut[826];
        v827 = daeoOut[827];
        v828 = daeoOut[828];
        v829 = daeoOut[829];
        v830 = daeoOut[830];
        v831 = daeoOut[831];
        v832 = daeoOut[832];
        v833 = daeoOut[833];
        v834 = daeoOut[834];
        v835 = daeoOut[835];
        v836 = daeoOut[836];
        v837 = daeoOut[837];
        v838 = daeoOut[838];
        v839 = daeoOut[839];
        v840 = daeoOut[840];
        v841 = daeoOut[841];
        v842 = daeoOut[842];
        v843 = daeoOut[843];
        v844 = daeoOut[844];
        v845 = daeoOut[845];
        v846 = daeoOut[846];
        v847 = daeoOut[847];
        v848 = daeoOut[848];
        v849 = daeoOut[849];
        v850 = daeoOut[850];
        v851 = daeoOut[851];
        v852 = daeoOut[852];
        v853 = daeoOut[853];
        v854 = daeoOut[854];
        v855 = daeoOut[855];
        v856 = daeoOut[856];
        v857 = daeoOut[857];
        v858 = daeoOut[858];
        v859 = daeoOut[859];
        v860 = daeoOut[860];
        v861 = daeoOut[861];
        v862 = daeoOut[862];
        v863 = daeoOut[863];
        v864 = daeoOut[864];
        v865 = daeoOut[865];
        v866 = daeoOut[866];
        v867 = daeoOut[867];
        v868 = daeoOut[868];
        v869 = daeoOut[869];
        v870 = daeoOut[870];
        v871 = daeoOut[871];
        v872 = daeoOut[872];
        v873 = daeoOut[873];
        v874 = daeoOut[874];
        v875 = daeoOut[875];
        v876 = daeoOut[876];
        v877 = daeoOut[877];
        v878 = daeoOut[878];
        v879 = daeoOut[879];
        v880 = daeoOut[880];
        v881 = daeoOut[881];
        v882 = daeoOut[882];
        v883 = daeoOut[883];
        v884 = daeoOut[884];
        v885 = daeoOut[885];
        v886 = daeoOut[886];
        v887 = daeoOut[887];
        v888 = daeoOut[888];
        v889 = daeoOut[889];
        v890 = daeoOut[890];
        v891 = daeoOut[891];
        v892 = daeoOut[892];
        v893 = daeoOut[893];
        v894 = daeoOut[894];
        v895 = daeoOut[895];
        v896 = daeoOut[896];
        v897 = daeoOut[897];
        v898 = daeoOut[898];
        v899 = daeoOut[899];
        v900 = daeoOut[900];
        v901 = daeoOut[901];
        v902 = daeoOut[902];
        v903 = daeoOut[903];
        v904 = daeoOut[904];
        v905 = daeoOut[905];
        v906 = daeoOut[906];
        v907 = daeoOut[907];
        v908 = daeoOut[908];
        v909 = daeoOut[909];
        v910 = daeoOut[910];
        v911 = daeoOut[911];
        v912 = daeoOut[912];
        v913 = daeoOut[913];
        v914 = daeoOut[914];
        v915 = daeoOut[915];
        v916 = daeoOut[916];
        v917 = daeoOut[917];
        v918 = daeoOut[918];
        v919 = daeoOut[919];
        v920 = daeoOut[920];
        v921 = daeoOut[921];
        v922 = daeoOut[922];
        v923 = daeoOut[923];
        v924 = daeoOut[924];
        v925 = daeoOut[925];
        v926 = daeoOut[926];
        v927 = daeoOut[927];
        v928 = daeoOut[928];
        v929 = daeoOut[929];
        v930 = daeoOut[930];
        v931 = daeoOut[931];
        v932 = daeoOut[932];
        v933 = daeoOut[933];
        v934 = daeoOut[934];
        v935 = daeoOut[935];
        v936 = daeoOut[936];
        v937 = daeoOut[937];
        v938 = daeoOut[938];
        v939 = daeoOut[939];
        v940 = daeoOut[940];
        v941 = daeoOut[941];
        v942 = daeoOut[942];
        v943 = daeoOut[943];
        v944 = daeoOut[944];
        v945 = daeoOut[945];
        v946 = daeoOut[946];
        v947 = daeoOut[947];
        v948 = daeoOut[948];
        v949 = daeoOut[949];
        v950 = daeoOut[950];
        v951 = daeoOut[951];
        v952 = daeoOut[952];
        v953 = daeoOut[953];
        v954 = daeoOut[954];
        v955 = daeoOut[955];
        v956 = daeoOut[956];
        v957 = daeoOut[957];
        v958 = daeoOut[958];
        v959 = daeoOut[959];
        v960 = daeoOut[960];
        v961 = daeoOut[961];
        v962 = daeoOut[962];
        v963 = daeoOut[963];
        v964 = daeoOut[964];
        v965 = daeoOut[965];
        v966 = daeoOut[966];
        v967 = daeoOut[967];
        v968 = daeoOut[968];
        v969 = daeoOut[969];
        v970 = daeoOut[970];
        v971 = daeoOut[971];
        v972 = daeoOut[972];
        v973 = daeoOut[973];
        v974 = daeoOut[974];
        v975 = daeoOut[975];
        v976 = daeoOut[976];
        v977 = daeoOut[977];
        v978 = daeoOut[978];
        v979 = daeoOut[979];
        v980 = daeoOut[980];
        v981 = daeoOut[981];
        v982 = daeoOut[982];
        v983 = daeoOut[983];
        v984 = daeoOut[984];
        v985 = daeoOut[985];
        v986 = daeoOut[986];
        v987 = daeoOut[987];
        v988 = daeoOut[988];
        v989 = daeoOut[989];
        v990 = daeoOut[990];
        v991 = daeoOut[991];
        v992 = daeoOut[992];
        v993 = daeoOut[993];
        v994 = daeoOut[994];
        v995 = daeoOut[995];
        v996 = daeoOut[996];
        v997 = daeoOut[997];
        v998 = daeoOut[998];
        v999 = daeoOut[999];
        v1000 = daeoOut[1000];
        v1001 = daeoOut[1001];
        v1002 = daeoOut[1002];
        v1003 = daeoOut[1003];
        v1004 = daeoOut[1004];
        v1005 = daeoOut[1005];
        v1006 = daeoOut[1006];
        v1007 = daeoOut[1007];
        v1008 = daeoOut[1008];
        v1009 = daeoOut[1009];
        v1010 = daeoOut[1010];
        v1011 = daeoOut[1011];
        v1012 = daeoOut[1012];
        v1013 = daeoOut[1013];
        v1014 = daeoOut[1014];
        v1015 = daeoOut[1015];
        v1016 = daeoOut[1016];
        v1017 = daeoOut[1017];
        v1018 = daeoOut[1018];
        v1019 = daeoOut[1019];
        v1020 = daeoOut[1020];
        v1021 = daeoOut[1021];
        v1022 = daeoOut[1022];
        v1023 = daeoOut[1023];
        v1024 = daeoOut[1024];
        v1025 = daeoOut[1025];
        v1026 = daeoOut[1026];
        v1027 = daeoOut[1027];
        v1028 = daeoOut[1028];
        v1029 = daeoOut[1029];
        v1030 = daeoOut[1030];
        v1031 = daeoOut[1031];
        v1032 = daeoOut[1032];
        v1033 = daeoOut[1033];
        v1034 = daeoOut[1034];
        v1035 = daeoOut[1035];
        v1036 = daeoOut[1036];
        v1037 = daeoOut[1037];
        v1038 = daeoOut[1038];
        v1039 = daeoOut[1039];
        v1040 = daeoOut[1040];
        v1041 = daeoOut[1041];
        v1042 = daeoOut[1042];
        v1043 = daeoOut[1043];
        v1044 = daeoOut[1044];
        v1045 = daeoOut[1045];
        v1046 = daeoOut[1046];
        v1047 = daeoOut[1047];
        v1048 = daeoOut[1048];
        v1049 = daeoOut[1049];
        v1050 = daeoOut[1050];
        v1051 = daeoOut[1051];
        v1052 = daeoOut[1052];
        v1053 = daeoOut[1053];
        v1054 = daeoOut[1054];
        v1055 = daeoOut[1055];
        v1056 = daeoOut[1056];
        v1057 = daeoOut[1057];
        v1058 = daeoOut[1058];
        v1059 = daeoOut[1059];
        v1060 = daeoOut[1060];
        v1061 = daeoOut[1061];
        v1062 = daeoOut[1062];
        v1063 = daeoOut[1063];
        v1064 = daeoOut[1064];
        v1065 = daeoOut[1065];
        v1066 = daeoOut[1066];
        v1067 = daeoOut[1067];
        v1068 = daeoOut[1068];
        v1069 = daeoOut[1069];
        v1070 = daeoOut[1070];
        v1071 = daeoOut[1071];
        v1072 = daeoOut[1072];
        v1073 = daeoOut[1073];
        v1074 = daeoOut[1074];
        v1075 = daeoOut[1075];

      end partial_Ecoli_metabolicModel_Quadratic;

      model main_Ecoli_Direct
        extends partial_Ecoli_metabolicModel_Linear;

        // kinetic parameters for CG example
        ParameterSet Ecoli_params;

        // definition of differential variables
        Real V(start=1, fixed=true);
        Real X(start=0.03, fixed=true);
        Real C_Glc(start=15.5, fixed=true);
        Real C_Xyl(start=8, fixed=true);
        Real C_O2(start=0.24, fixed=true);
        Real C_Ethanol(start=0.0, fixed=true);

        // extracellular fluxes
        Real v_X;
        Real v_Glc;
        Real v_Xyl;
        Real v_O2;
        Real v_Ethanol;

        // additional quantities of interest
        Real C_total;
      equation
        // condition to terminate simulation when carbon sources are depleted
        when (C_total < Ecoli_params.C_threshold) then
          terminate("Carbon sources are depleted");
        end when;

         // DAEO input definition
        daeoIn[1] = if C_Glc > 0 then Ecoli_params.vmax_g*C_Glc/(C_Glc + Ecoli_params.K_g)*1/(1 + C_Ethanol/Ecoli_params.K_ie)
           else 0.0;
        daeoIn[2] = if C_Xyl > 0 then Ecoli_params.vmax_x*C_Xyl/(C_Xyl + Ecoli_params.K_x)*1/(1 + C_Ethanol/Ecoli_params.K_ie)*1/(1 +
          C_Glc/Ecoli_params.K_ig) else 0.0;
        daeoIn[3] = Ecoli_params.vmax_o2*C_O2/(C_O2 + Ecoli_params.K_o2);

        // daeo calculations
        (daeoOut,sigma) =Solver.solveDirectSigma(
          dfba,
          daeoIn,
          n,
          q);

        // differential equations
        der(V) = 0;
        der(X) = v_X*X;
        der(C_Glc) = v_Glc*Ecoli_params.MW_Glc*X;
        der(C_Xyl) = v_Xyl*Ecoli_params.MW_Xyl*X;
        der(C_O2) = 0;
        der(C_Ethanol) = v_Ethanol*Ecoli_params.MW_Ethanol*X;

        // calculation of additional quantities of interest
        C_total = C_Glc + C_Xyl;

        // mapping of extracellular fluxes
        v_Glc = daeoOut[344];
        v_Xyl = daeoOut[429];
        v_O2 = daeoOut[392];
        v_Ethanol = daeoOut[329];
        v_X = daeoOut[150];
         annotation (experiment(
            StopTime=8.2,
            Interval=0.01,
            Tolerance=1e-012));
      end main_Ecoli_Direct;

      model main_Ecoli_Direct_Quadratic
        extends partial_Ecoli_metabolicModel_Quadratic;

        // kinetic parameters for CG example
        ParameterSet Ecoli_params;

        // definition of differential variables
        Real V(start=1, fixed=true);
        Real X(start=0.03, fixed=true);
        Real C_Glc(start=15.5, fixed=true);
        Real C_Xyl(start=8, fixed=true);
        Real C_O2(start=0.24, fixed=true);
        Real C_Ethanol(start=0.0, fixed=true);

        // extracellular fluxes
        Real v_X;
        Real v_Glc;
        Real v_Xyl;
        Real v_O2;
        Real v_Ethanol;

        // additional quantities of interest
        Real C_total;
        Real minsigma;
      equation
        // condition to terminate simulation when carbon sources are depleted
        when (C_total < Ecoli_params.C_threshold) then
          terminate("Carbon sources are depleted");
        end when;

         // DAEO input definition
        daeoIn[1] = if C_Glc > 0 then Ecoli_params.vmax_g*C_Glc/(C_Glc + Ecoli_params.K_g)*1/(1 + C_Ethanol/Ecoli_params.K_ie)
           else 0.0;
        daeoIn[2] = if C_Xyl > 0 then Ecoli_params.vmax_x*C_Xyl/(C_Xyl + Ecoli_params.K_x)*1/(1 + C_Ethanol/Ecoli_params.K_ie)*1/(1 +
          C_Glc/Ecoli_params.K_ig) else 0.0;
        daeoIn[3] = Ecoli_params.vmax_o2*C_O2/(C_O2 + Ecoli_params.K_o2);

        // daeo calculations
        (daeoOut,sigma) =Solver.solveDirectSigma(
          dfba,
          daeoIn,
          n,
          q);
        minsigma = min(sigma);

        // differential equations
        der(V) = 0;
        der(X) = v_X*X;
        der(C_Glc) = v_Glc*Ecoli_params.MW_Glc*X;
        der(C_Xyl) = v_Xyl*Ecoli_params.MW_Xyl*X;
        der(C_O2) = 0;
        der(C_Ethanol) = v_Ethanol*Ecoli_params.MW_Ethanol*X;

        // calculation of additional quantities of interest
        C_total = C_Glc + C_Xyl;

        // mapping of extracellular fluxes
        v_Glc = daeoOut[344];
        v_Xyl = daeoOut[429];
        v_O2 = daeoOut[392];
        v_Ethanol = daeoOut[329];
        v_X = daeoOut[150];
         annotation (experiment(
            StopTime=8.2,
            Interval=0.01,
            Tolerance=1e-012));
      end main_Ecoli_Direct_Quadratic;

      model main_Ecoli_ActiveSet
        extends partial_Ecoli_metabolicModel_Linear;

        // kinetic parameters for CG example
        ParameterSet Ecoli_params;

        // definition of differential variables
        Real V(start=1, fixed=true);
        Real X(start=0.03, fixed=true);
        Real C_Glc(start=15.5, fixed=true);
        Real C_Xyl(start=8, fixed=true);
        Real C_O2(start=0.24, fixed=true);
        Real C_Ethanol(start=0.0, fixed=true);

        // extracellular fluxes
        Real v_X;
        Real v_Glc;
        Real v_Xyl;
        Real v_O2;
        Real v_Ethanol;

        // additional quantities of interest
        Real C_total;
        // for active set approach
        Real minsigma;
        Integer eventCounter(start=0, fixed=true);
        Integer min_indice(fixed=false);
      initial equation
        min_indice=Solver.min_ind(sigma);
      equation
        // condition to terminate simulation when carbon sources are depleted
        when (C_total < Ecoli_params.C_threshold) then
          terminate("Carbon sources are depleted");
        end when;

         // DAEO input definition
        daeoIn[1] = if C_Glc > 0 then Ecoli_params.vmax_g*C_Glc/(C_Glc + Ecoli_params.K_g)*1/(1 + C_Ethanol/Ecoli_params.K_ie)
           else 0.0;
        daeoIn[2] = if C_Xyl > 0 then Ecoli_params.vmax_x*C_Xyl/(C_Xyl + Ecoli_params.K_x)*1/(1 + C_Ethanol/Ecoli_params.K_ie)*1/(1 +
          C_Glc/Ecoli_params.K_ig) else 0.0;
        daeoIn[3] = Ecoli_params.vmax_o2*C_O2/(C_O2 + Ecoli_params.K_o2);

        // daeo calculations
        (daeoOut,sigma) = Solver.solveActiveSetDer(
                dfba,
                daeoIn,
                n,
                q);
        minsigma = min(sigma);
        when min(sigma) < -Ecoli_params.tolEvent then
          eventCounter=Solver.updateActiveSetNew(pre(eventCounter),dfba,daeoIn);
          min_indice=Solver.min_ind(pre(sigma));
        end when;

        // differential equations
        der(V) = 0;
        der(X) = v_X*X;
        der(C_Glc) = v_Glc*Ecoli_params.MW_Glc*X;
        der(C_Xyl) = v_Xyl*Ecoli_params.MW_Xyl*X;
        der(C_O2) = 0;
        der(C_Ethanol) = v_Ethanol*Ecoli_params.MW_Ethanol*X;

        // calculation of additional quantities of interest
        C_total = C_Glc + C_Xyl;

        // mapping of extracellular fluxes
        v_Glc = daeoOut[344];
        v_Xyl = daeoOut[429];
        v_O2 = daeoOut[392];
        v_Ethanol = daeoOut[329];
        v_X = daeoOut[150];
         annotation (experiment(
            StopTime=8.2,
            Interval=0.1,
            Tolerance=1e-12));
      end main_Ecoli_ActiveSet;

      model main_Ecoli_ActiveSet_Der
        extends partial_Ecoli_metabolicModel_Linear;

        // kinetic parameters for CG example
        ParameterSet Ecoli_params;

        // definition of differential variables
        Real V(start=1, fixed=true);
        Real X(start=0.03, fixed=true);
        Real C_Glc(start=15.5, fixed=true);
        Real C_Xyl(start=8, fixed=true);
        Real C_O2(start=0.24, fixed=true);
        Real C_Ethanol(start=0.0, fixed=true);

        // extracellular fluxes
        Real v_X;
        Real v_Glc;
        Real v_Xyl;
        Real v_O2;
        Real v_Ethanol;

        // additional quantities of interest
        Real C_total;
        // for active set approach
        Real minsigma;
        Integer eventCounter(start=0, fixed=true);
        Integer min_indice(fixed=false);
      initial equation
        min_indice=Solver.min_ind(sigma);
      equation
        // condition to terminate simulation when carbon sources are depleted
        when (C_total < Ecoli_params.C_threshold) then
          terminate("Carbon sources are depleted");
        end when;

         // DAEO input definition
        daeoIn[1] = if C_Glc > 0 then Ecoli_params.vmax_g*C_Glc/(C_Glc + Ecoli_params.K_g)*1/(1 + C_Ethanol/Ecoli_params.K_ie)
           else 0.0;
        daeoIn[2] = if C_Xyl > 0 then Ecoli_params.vmax_x*C_Xyl/(C_Xyl + Ecoli_params.K_x)*1/(1 + C_Ethanol/Ecoli_params.K_ie)*1/(1 +
          C_Glc/Ecoli_params.K_ig) else 0.0;
        daeoIn[3] = Ecoli_params.vmax_o2*C_O2/(C_O2 + Ecoli_params.K_o2);

        // daeo calculations
        (daeoOut,sigma) = Solver.solveActiveSetDer(
                dfba,
                daeoIn,
                n,
                q);
        minsigma = min(sigma);
        when min(sigma) < -Ecoli_params.tolEvent then
          eventCounter=Solver.updateActiveSetNew(pre(eventCounter),dfba,daeoIn);
          min_indice=Solver.min_ind(pre(sigma));
        end when;

        // differential equations
        der(V) = 0;
        der(X) = v_X*X;
        der(C_Glc) = v_Glc*Ecoli_params.MW_Glc*X;
        der(C_Xyl) = v_Xyl*Ecoli_params.MW_Xyl*X;
        der(C_O2) = 0;
        der(C_Ethanol) = v_Ethanol*Ecoli_params.MW_Ethanol*X;

        // calculation of additional quantities of interest
        C_total = C_Glc + C_Xyl;

        // mapping of extracellular fluxes
        v_Glc = daeoOut[344];
        v_Xyl = daeoOut[429];
        v_O2 = daeoOut[392];
        v_Ethanol = daeoOut[329];
        v_X = daeoOut[150];
         annotation (experiment(
            StopTime=8.2,
            Interval=0.1,
            Tolerance=1e-12));
      end main_Ecoli_ActiveSet_Der;

      model main_Ecoli_ActiveSet_Quadratic
        extends partial_Ecoli_metabolicModel_Quadratic;

        // kinetic parameters for CG example
        ParameterSet Ecoli_params;

        // definition of differential variables
        Real V(start=1, fixed=true);
        Real X(start=0.03, fixed=true);
        Real C_Glc(start=15.5, fixed=true);
        Real C_Xyl(start=8, fixed=true);
        Real C_O2(start=0.24, fixed=true);
        Real C_Ethanol(start=0.0, fixed=true);

        // extracellular fluxes
        Real v_X;
        Real v_Glc;
        Real v_Xyl;
        Real v_O2;
        Real v_Ethanol;

        // additional quantities of interest
        Real C_total;
        // for active set approach
        Real minsigma;
        Integer eventCounter(start=0, fixed=true);
        Integer min_indice(fixed=false);
      initial equation
        min_indice=Solver.min_ind(sigma);
      equation
        // condition to terminate simulation when carbon sources are depleted
        when (C_total < Ecoli_params.C_threshold) then
          terminate("Carbon sources are depleted");
        end when;

         // DAEO input definition
        daeoIn[1] = if C_Glc > 0 then Ecoli_params.vmax_g*C_Glc/(C_Glc + Ecoli_params.K_g)*1/(1 + C_Ethanol/Ecoli_params.K_ie)
           else 0.0;
        daeoIn[2] = if C_Xyl > 0 then Ecoli_params.vmax_x*C_Xyl/(C_Xyl + Ecoli_params.K_x)*1/(1 + C_Ethanol/Ecoli_params.K_ie)*1/(1 +
          C_Glc/Ecoli_params.K_ig) else 0.0;
        daeoIn[3] = Ecoli_params.vmax_o2*C_O2/(C_O2 + Ecoli_params.K_o2);

        // daeo calculations
        (daeoOut,sigma) = Solver.solveActiveSet(
                dfba,
                daeoIn,
                n,
                q);
        minsigma = min(sigma);
        when min(sigma) < -Ecoli_params.tolEvent then
          eventCounter=Solver.updateActiveSetNew(pre(eventCounter),dfba,daeoIn);
          min_indice=Solver.min_ind(pre(sigma));
        end when;

        // differential equations
        der(V) = 0;
        der(X) = v_X*X;
        der(C_Glc) = v_Glc*Ecoli_params.MW_Glc*X;
        der(C_Xyl) = v_Xyl*Ecoli_params.MW_Xyl*X;
        der(C_O2) = 0;
        der(C_Ethanol) = v_Ethanol*Ecoli_params.MW_Ethanol*X;

        // calculation of additional quantities of interest
        C_total = C_Glc + C_Xyl;

        // mapping of extracellular fluxes
        v_Glc = daeoOut[344];
        v_Xyl = daeoOut[429];
        v_O2 = daeoOut[392];
        v_Ethanol = daeoOut[329];
        v_X = daeoOut[150];
         annotation (experiment(
            StopTime=8.2,
            Interval=0.01,
            Tolerance=1e-012));
      end main_Ecoli_ActiveSet_Quadratic;

      model main_Ecoli_ActiveSet_Quadratic_Der
        extends partial_Ecoli_metabolicModel_Quadratic;

        // kinetic parameters for CG example
        ParameterSet Ecoli_params;

        // definition of differential variables
        Real V(start=1, fixed=true);
        Real X(start=0.03, fixed=true);
        Real C_Glc(start=15.5, fixed=true);
        Real C_Xyl(start=8, fixed=true);
        Real C_O2(start=0.24, fixed=true);
        Real C_Ethanol(start=0.0, fixed=true);

        // extracellular fluxes
        Real v_X;
        Real v_Glc;
        Real v_Xyl;
        Real v_O2;
        Real v_Ethanol;

        // additional quantities of interest
        Real C_total;
        // for active set approach
        Real minsigma;
        Integer eventCounter(start=0, fixed=true);
        Integer min_indice(fixed=false);
      initial equation
        min_indice=Solver.min_ind(sigma);
      equation
        // condition to terminate simulation when carbon sources are depleted
        when (C_total < Ecoli_params.C_threshold) then
          terminate("Carbon sources are depleted");
        end when;

         // DAEO input definition
        daeoIn[1] = if C_Glc > 0 then Ecoli_params.vmax_g*C_Glc/(C_Glc + Ecoli_params.K_g)*1/(1 + C_Ethanol/Ecoli_params.K_ie)
           else 0.0;
        daeoIn[2] = if C_Xyl > 0 then Ecoli_params.vmax_x*C_Xyl/(C_Xyl + Ecoli_params.K_x)*1/(1 + C_Ethanol/Ecoli_params.K_ie)*1/(1 +
          C_Glc/Ecoli_params.K_ig) else 0.0;
        daeoIn[3] = Ecoli_params.vmax_o2*C_O2/(C_O2 + Ecoli_params.K_o2);

        // daeo calculations
        // daeo calculations
        (daeoOut,sigma) = Solver.solveActiveSetDer(
                dfba,
                daeoIn,
                n,
                q);
        minsigma = min(sigma);
        when min(sigma) < -Ecoli_params.tolEvent then
          //Solver.updateActiveSet(dfba, daeoIn);
          eventCounter=Solver.updateActiveSetNew(pre(eventCounter),dfba,daeoIn);
          min_indice=Solver.min_ind(pre(sigma));
        end when;

        // differential equations
        der(V) = 0;
        der(X) = v_X*X;
        der(C_Glc) = v_Glc*Ecoli_params.MW_Glc*X;
        der(C_Xyl) = v_Xyl*Ecoli_params.MW_Xyl*X;
        der(C_O2) = 0;
        der(C_Ethanol) = v_Ethanol*Ecoli_params.MW_Ethanol*X;

        // calculation of additional quantities of interest
        C_total = C_Glc + C_Xyl;

        // mapping of extracellular fluxes
        v_Glc = daeoOut[344];
        v_Xyl = daeoOut[429];
        v_O2 = daeoOut[392];
        v_Ethanol = daeoOut[329];
        v_X = daeoOut[150];
         annotation (experiment(
            StopTime=8.2,
            Interval=0.01,
            Tolerance=1e-012));
      end main_Ecoli_ActiveSet_Quadratic_Der;

      package CPU_comparison

        model run_Case_Study_LP
          parameter Integer i=2;
          parameter Real Y0_matrix[:,:]=[1.0000, 0.6686, 9.7910, 21.5348, 0.2400, 0;
            1.0000, 0.7872, 13.8159, 1.7102, 0.2400, 0;
            1.0000, 0.5460, 6.9498, 12.3367, 0.2400, 0;
            1.0000, 0.8750, 18.8879, 16.8265, 0.2400, 0;
            1.0000, 0.5006, 16.6731, 27.6927, 0.2400, 0;
            1.0000, 0.9253, 23.9413, 7.8096, 0.2400, 0;
            1.0000, 0.3820, 26.0074, 3.5745, 0.2400, 0;
            1.0000, 0.1642, 28.7271, 18.7348, 0.2400, 0;
            1.0000, 0.2389, 5.1892, 25.8678, 0.2400, 0;
            1.0000, 0.0734, 2.4088, 11.3519, 0.2400, 0];
          parameter Real V_start=Y0_matrix[i,1];
          parameter Real X_start=Y0_matrix[i,2];
          parameter Real C_Glc_start=Y0_matrix[i,3];
          parameter Real C_Xyl_start=Y0_matrix[i,4];
          main_Ecoli_ActiveSet ActiveSet(
            V(start=V_start),
            X(start=X_start),
            C_Glc(start=C_Glc_start),
            C_Xyl(start=C_Xyl_start));
          annotation (experiment(
              StopTime=15,
              Interval=0.01,
              Tolerance=1e-09));
        end run_Case_Study_LP;

        model run_Case_Study_QP
          parameter Integer i=1;
          parameter Real Y0_matrix[:,:]=[1.0000, 0.6686, 9.7910, 21.5348, 0.2400, 0;
            1.0000, 0.7872, 13.8159, 1.7102, 0.2400, 0;
            1.0000, 0.5460, 6.9498, 12.3367, 0.2400, 0;
            1.0000, 0.8750, 18.8879, 16.8265, 0.2400, 0;
            1.0000, 0.5006, 16.6731, 27.6927, 0.2400, 0;
            1.0000, 0.9253, 23.9413, 7.8096, 0.2400, 0;
            1.0000, 0.3820, 26.0074, 3.5745, 0.2400, 0;
            1.0000, 0.1642, 28.7271, 18.7348, 0.2400, 0;
            1.0000, 0.2389, 5.1892, 25.8678, 0.2400, 0;
            1.0000, 0.0734, 2.4088, 11.3519, 0.2400, 0];
          parameter Real V_start=Y0_matrix[i,1];
          parameter Real X_start=Y0_matrix[i,2];
          parameter Real C_Glc_start=Y0_matrix[i,3];
          parameter Real C_Xyl_start=Y0_matrix[i,4];
          main_Ecoli_ActiveSet_Quadratic ActiveSet(
            V(start=V_start),
            X(start=X_start),
            C_Glc(start=C_Glc_start),
            C_Xyl(start=C_Xyl_start));
          annotation (experiment(
              StopTime=15,
              Interval=0.01,
              Tolerance=1e-09));
        end run_Case_Study_QP;
      end CPU_comparison;
    end EscherichiaColi_iJR904;

  end DFBA_Examples;

  annotation (uses(                           ModelicaServices(version="3.2.2"),
        Modelica(version="3.2.3")));
end Daeo;
